package cubeplay.utils
{
	import com.greensock.layout.AlignMode;
	
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontWeight;
	import flash.utils.Dictionary;
	
	import cubeplay.elements.HorizontalScrollContainer;
	import cubeplay.elements.VerticalScrollContainer;
	import cubeplay.elements.listRenderer.InvitePanel;
	import cubeplay.elements.listRenderer.NotificationsPanel;
	import cubeplay.elements.listRenderer.friendlist.DefaultSelectedSkin;
	import cubeplay.elements.listRenderer.friendlist.DefaultSkin;
	import cubeplay.elements.listRenderer.friendlist.FacebookListPanel;
	import cubeplay.elements.listRenderer.friendlist.LatestListPanel;
	import cubeplay.elements.listRenderer.friendlist.PeopleItem;
	import cubeplay.elements.listRenderer.friendlist.PeopleItemNoti;
	import cubeplay.elements.listRenderer.requestlist.NotificationsListPanel;
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomFriendsPanel;
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomList;
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomPanel;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ScrollContainer;
	import feathers.controls.TextInput;
	import feathers.controls.renderers.BaseDefaultItemRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.display.Scale3Image;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale3Textures;
	import feathers.textures.Scale9Textures;
	import feathers.themes.MetalWorksMobileTheme;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	
	public class CubeplayStyle extends MetalWorksMobileTheme 
	{
		[Embed(source="/../elements/listRenderer/assets/default-avatar.png")]
		private static const DEFAULTAVATAR:Class;
		

		// Embed the Atlas XML
		[Embed(source="/../assets/textures/1x/cubeplayTexture.xml", mimeType="application/octet-stream")]
		public static const CUTEPLAYTEXTURE_XML:Class;
		
		// Embed the Atlas Texture:
		[Embed(source="/../assets/textures/1x/cubeplayTexture.png")]
		private static const CUTEPLAYTEXTURE_PNG:Class;
		
		
		private static const BACKGROUND_COLOR:Number = 0xf5f5f5;
		private static const BACKGROUND_COLOR2:Number = 0xebebeb;
		private static const BLUE_COLOR:Number = 0x449fe5;
//		public static var COLOR_BLUE:Number = 0x009cff;
//		public static var COLOR_ORANGE:Number = 0xD35400;
//		public static var COLOR_WHITE:Number = 0xFFFFFF;
//		public static var COLOR_LIGHT_GRAY:Number = 0x95A5A6;
		
		
		public static const HOME_SCREEN:String = "HOME_SCREEN";
		
		public static var ITEM_HEIGHT:int = 120;
		public static var HEADER_HEIGHT:Number = 120;
		public static var FOOTER_HEIGHT:Number = 156;
		
		public static var HEADER:String = "HEADER";
		public static var INVITE_ITEM_RENDERER:String = "INVITE_ITEM_RENDERER";
		public static var INVITE_POPUP_ITEM_RENDERER:String = "INVITE_POPUP_ITEM_RENDERER";
		public static var NOTI_LIST:String = "NOTI_LIST";
		public static var WAITING_LIST:String = "WAITING_LIST";
		public static var LIST_PANEL:String = "LIST_PANEL";
		public static var LIST_POPUP_PANEL:String = "LIST_POPUP_PANEL";
		public static var INVITE_PANEL:String = "INVITE_PANEL";
		public static var LIST:String = "LIST";
		public static var LIST_NOTI:String = "LIST_NOTI";
		
		public static var LIST_POPUP:String = "LIST_POPUP";
		public static var TEXT_INPUT:String = "TEXT_INPUT";
		public static var BACKGROUND:String = "BACKGROUND";
		
		
		public static var BUTTON_OK:String = "BUTTON_OK";
		public static var BUTTON_INVITE:String = "BUTTON_INVITE";
		
		public static var BUTTON_ADD:String = "BUTTON_ADD";
		public static var BUTTON_START:String = "BUTTON_START";
		public static var BUTTON_BACK:String = "BUTTON_BACK";
		public static var BUTTON_ACCEPT:String = "BUTTON_ACCEPT";
		public static var BUTTON_SINGLE_PLAY:String = "BUTTON_SINGLE_PLAY";
		public static var BUTTON_PLAY_WITH_FRIENDS:String = "BUTTON_PLAY_WITH_FRIENDS";
		public static var BUTTON_RANDOM_MATCHES:String = "BUTTON_RANDOM_MATCHES";
		public static var BUTTON_NOTIFICATIONS:String = "BUTTON_NOTIFICATIONS";
		public static var BUTTON_LEADER_BOARD:String = "BUTTON_LEADER_BOARD";
		public static var BUTTON_CONNECT_WITH_FACEBOOK:String = "BUTTON_CONNECT_WITH_FACEBOOK";
		public static var BUTTON_LOGIN_AS_GUEST:String = "BUTTON_LOGIN_AS_GUEST";
		
		private static var cachedIcons:Dictionary = new Dictionary();

		private static var _cubeplayAtlas:TextureAtlas;
		public function CubeplayStyle()
		{
			super();
			ITEM_HEIGHT = Starling.current.stage.stageHeight*(ITEM_HEIGHT/1024);
			HEADER_HEIGHT = Starling.current.stage.stageHeight*(HEADER_HEIGHT/1024);
			FOOTER_HEIGHT = Starling.current.stage.stageHeight*(FOOTER_HEIGHT/1024);
			// create atlas
			var texture:Texture = Texture.fromBitmap(new CUTEPLAYTEXTURE_PNG());
			var xml:XML = XML(new CUTEPLAYTEXTURE_XML());
			_cubeplayAtlas = new TextureAtlas(texture, xml);			
		}
		
		
		public static function get cubeplayAtlas():TextureAtlas
		{
			return _cubeplayAtlas;
		}

		public static function set cubeplayAtlas(value:TextureAtlas):void
		{
			_cubeplayAtlas = value;
		}

		override protected function initialize():void
		{
			super.initialize();
			
			this.setInitializerForClass(Button, customThumbName, "customThumbName");
			this.setInitializerForClass(Button, customOnTrackName, "customOnTrackName");
			this.setInitializerForClass(Button, customOffTrackName, "customOffTrackName");
			this.setInitializerForClass(Header, header, HEADER);
			this.setInitializerForClass(Panel, homeScreen, HOME_SCREEN);
			this.setInitializerForClass(HorizontalScrollContainer, scrollContainerHomeScreen, HOME_SCREEN);
			this.setInitializerForClass(VerticalScrollContainer, scrollContainerHomeScreen, HOME_SCREEN);
			this.setInitializerForClass(FacebookListPanel, listPanel, LIST_PANEL);
			this.setInitializerForClass(LatestListPanel, listPanel, LIST_PANEL);
			this.setInitializerForClass(WaitingRoomList, listPanel, LIST_PANEL);
			this.setInitializerForClass(NotificationsListPanel, listPanel, LIST_PANEL);
			
			this.setInitializerForClass(InvitePanel, invitePanel, INVITE_PANEL);
			this.setInitializerForClass(FacebookListPanel, listPopupPanel, LIST_POPUP_PANEL);
			this.setInitializerForClass(LatestListPanel, listPopupPanel, LIST_POPUP_PANEL);
			
			
			this.setInitializerForClass(WaitingRoomFriendsPanel, invitePanel, INVITE_PANEL);
			this.setInitializerForClass(WaitingRoomPanel, invitePanel, INVITE_PANEL);
			this.setInitializerForClass(NotificationsPanel, invitePanel, INVITE_PANEL);
			this.setInitializerForClass(DefaultListItemRenderer, listCustom, INVITE_ITEM_RENDERER);
			this.setInitializerForClass(DefaultListItemRenderer, listCustomPopup, INVITE_POPUP_ITEM_RENDERER);
			this.setInitializerForClass(DefaultListItemRenderer, listCustomNoti, NOTI_LIST);
			this.setInitializerForClass(DefaultListItemRenderer, listCustomWaiting, WAITING_LIST);
			this.setInitializerForClass(List, list, LIST);
			this.setInitializerForClass(List, listNoti, LIST_NOTI);
			this.setInitializerForClass(List, listPopup, LIST_POPUP);
			
			this.setInitializerForClass(TextInput, txtInput, TEXT_INPUT);
			
			this.setInitializerForClass(Button, btnOk, BUTTON_OK);
			this.setInitializerForClass(Button, btnInvite, BUTTON_INVITE);
			this.setInitializerForClass(Button, btnAdd, BUTTON_ADD);
			this.setInitializerForClass(Button, btnStart, BUTTON_START);
			this.setInitializerForClass(Button, btnBack, BUTTON_BACK);
			this.setInitializerForClass(Button, btnAccept, BUTTON_ACCEPT);
			this.setInitializerForClass(Button, btnSinglePlay, BUTTON_SINGLE_PLAY);
			this.setInitializerForClass(Button, btnPlayWithFriends, BUTTON_PLAY_WITH_FRIENDS);
			this.setInitializerForClass(Button, btnRandomMatches, BUTTON_RANDOM_MATCHES);
			this.setInitializerForClass(Button, btnButtonNotifications, BUTTON_NOTIFICATIONS);
			this.setInitializerForClass(Button, btnLeaderBoard, BUTTON_LEADER_BOARD);
			
			this.setInitializerForClass(Button, btnConnectWithFacebook, BUTTON_CONNECT_WITH_FACEBOOK);
			this.setInitializerForClass(Button, btnLoginAsGuest, BUTTON_LOGIN_AS_GUEST);
			
		}				
		
		private function customThumbName(button:Button):void
		{
			button.defaultSkin = new Image(  cubeplayAtlas.getTexture("btnFacebookFriends"));
			button.width = 266;
			button.height = 60;
		}
		
		private function customOffTrackName(button:Button):void
		{
			trace("customOffTrackName");
			button.defaultSkin = new Image(  cubeplayAtlas.getTexture("btnFacebookFriends")  );
			button.width = 266;
			button.height = 60;
		}
		
		private function customOnTrackName(button:Button):void
		{
			trace("customOnTrackName");
			button.defaultSkin = new Image(  cubeplayAtlas.getTexture("btnLatest")  );
			button.width = 266;
			button.height = 60;
		}
		
		private function header(header:Header):void
		{
			var bg:Quad = new Quad(10, 10, 0xFFFFFF);	
			bg.alpha = 0;
			bg.width = Starling.current.stage.stageWidth;
			bg.height = HEADER_HEIGHT;
			header.backgroundSkin = bg;			
			//			header.titleProperties.elementFormat = new ElementFormat(this.boldFontDescription, 36, 0x000000);	
			header.titleFactory = function():ITextRenderer
			{
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				textRenderer.wordWrap = true;
				var txtFormat:TextFormat = new TextFormat();
				txtFormat.font = UtilFunctions.MAIN_FONT;
				txtFormat.color = 0x000000;
				txtFormat.size = 36;
				textRenderer.textFormat = txtFormat;
				return textRenderer;
			}
		}
		
		private function btnAdd(btn:Button):void
		{
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnInviteSmall"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnInviteSmall"));
			img.alpha = 0.8;
			btn.downSkin = img;
			btn.width = 60;
			btn.height = 60;
		}		
		
		private function btnStart(btn:Button):void
		{
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnStart2"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnStart2"));
			img.alpha = 0.8;
			btn.downSkin = img;
			btn.width = 314;
			btn.height = 100;
		}
		
		private function invitePanel(panel:Panel):void
		{
			panel.backgroundSkin = new Quad(10,10,BACKGROUND_COLOR2);
		}
		
		private function listPanel(panel:Panel):void
		{
			var scale3img_height:int = Starling.current.stage.stageHeight - FOOTER_HEIGHT - HEADER_HEIGHT - 42*2;
			var scale3img:Scale3Image = new Scale3Image(new Scale3Textures(cubeplayAtlas.getTexture("bgInvite"),42,10,"vertical"));
			panel.backgroundSkin = scale3img;
			panel.width = Starling.current.stage.stageWidth*(768 - 42*2)/768;
			panel.height = Starling.current.stage.stageHeight - FOOTER_HEIGHT - HEADER_HEIGHT;				
		}
		
		private function listPopupPanel(panel:Panel):void
		{
//			var scale3img_height:int = Starling.current.stage.stageHeight*(700/1024);
			var scale3img:Scale9Image = new Scale9Image(new Scale9Textures(cubeplayAtlas.getTexture("bgInvitePopup"),new Rectangle( 33, 33, 20, 20)));
			panel.backgroundSkin = scale3img;
			panel.width = Starling.current.stage.stageWidth*(578/768);
			panel.height = Starling.current.stage.stageHeight*(700/1024);				
		}		
		
		private function listPopup(list:List):void
		{
			list.width = 578;
			list.height = 700;	
		}		
		
		private function listNoti(list:List):void
		{
			list.isSelectable = false;
			list.width = Starling.current.stage.stageWidth*708/768;
			list.height = Starling.current.stage.stageHeight - CubeplayStyle.FOOTER_HEIGHT - CubeplayStyle.HEADER_HEIGHT * 2;	
		}		
		
		private function list(list:List):void
		{
			list.width = Starling.current.stage.stageWidth*708/768;
			list.height = Starling.current.stage.stageHeight - CubeplayStyle.FOOTER_HEIGHT - CubeplayStyle.HEADER_HEIGHT * 2;	
		}
		
		private function txtInput(textip:TextInput):void
		{
			textip.textEditorProperties.fontFamily = UtilFunctions.MAIN_FONT;
			textip.textEditorProperties.fontSize = 36;
			textip.textEditorProperties.color = 0xc8c8c8;
			textip.textEditorProperties.fontWeight = FontWeight.NORMAL; 
			textip.textEditorProperties.textAlign = "center";
			textip.textEditorProperties.maxChars = 20; 
			textip.paddingTop = 20;
			textip.backgroundEnabledSkin = new Image(cubeplayAtlas.getTexture("bgTextInput"));
			textip.backgroundSkin = new Image(cubeplayAtlas.getTexture("bgTextInput"));
			textip.backgroundFocusedSkin = new Image(cubeplayAtlas.getTexture("bgTextInput"));
		}			
		
		private function btnInvite(btn:Button):void
		{
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnInvite"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnInvite"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnOk(btn:Button):void
		{
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnOk"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnOk"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}
		
		private function btnBack(btn:Button):void
		{
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnBack"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnBack"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnLoginAsGuest(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnLoginAsGuest"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnLoginAsGuest"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnConnectWithFacebook(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnConnectWithFacebook"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnConnectWithFacebook"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnLeaderBoard(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnLeaderBoard"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnLeaderBoard"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnButtonNotifications(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnNotifications"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnNotifications"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}				
		
		private function btnRandomMatches(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnRandomMatches"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnRandomMatches"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnPlayWithFriends(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnPlayWithFriends"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnPlayWithFriends"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}		
		
		private function btnSinglePlay(btn:Button):void
		{		
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnSinglePlay"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnSinglePlay"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}
		
		
		private function scrollContainerHomeScreen(panel:ScrollContainer):void
		{
			panel.backgroundSkin = new Quad(10,10,BACKGROUND_COLOR);
		}
		
		private function homeScreen(panel:Panel):void
		{
			panel.backgroundSkin = new Quad(10,10,BACKGROUND_COLOR);
		}
		
		private function btnAccept(btn:Button):void
		{
			// TODO Auto Generated method stub
			btn.defaultSkin = new Image(cubeplayAtlas.getTexture("btnAccept"));			
			var img:Image = new Image(cubeplayAtlas.getTexture("btnAccept"));
			img.alpha = 0.8;
			btn.downSkin = img;
		}
		
		private function listCustomPopup(renderer:DefaultListItemRenderer):void 
		{
			
			
			renderer.defaultLabelProperties.elementFormat = new ElementFormat(this.regularFontDescription, 45, LIGHT_TEXT_COLOR);	
			trace("renderer.width = "+renderer.width);
			renderer.defaultSkin = new DefaultSkin(578,ITEM_HEIGHT);
			renderer.defaultSelectedSkin = new DefaultSelectedSkin(578,ITEM_HEIGHT);
			renderer.labelField = "text";
			renderer.gap = 20;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.padding = 10;
			renderer.height = ITEM_HEIGHT;
			renderer.paddingLeft = 100;
			renderer.useStateDelayTimer = true;
			//			renderer.delayTextureCreationOnScroll = true;
			renderer.iconPosition = BaseDefaultItemRenderer.ICON_POSITION_LEFT;
			
			//			renderer.iconFunction = function( item:PeopleItem ):DisplayObject
			//			{
			//				if(item in cachedIcons)
			//				{
			//					return cachedIcons[item];
			//				}
			//				var icon:Image = new Image( cubeplayAtlas.getTexture( item.id ) );
			//				cachedIcons[item] = icon;
			//				return icon;
			//			};
			
			renderer.selectableFunction = function( item:PeopleItem ):Boolean
			{
				trace("selectableFunction "+item.isSelectable);
				return item.isSelectable;
			};
			
			renderer.iconSourceFunction = function( item:PeopleItem ):Object
			{
				trace("iconSourceFunction "+item.textureUrl);
				return item.textureUrl;
			};
			
			renderer.iconLoaderFactory = function():ImageLoader
			{
				var loader:ImageLoader = new ImageLoader();
				loader.snapToPixels = true;
				loader.width = loader.height = 100;
				loader.addEventListener(Event.COMPLETE,loadCompleteHandler);
				loader.loadingTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				loader.errorTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				return loader;
			};
			
			renderer.labelFactory = function():ITextRenderer
			{
				//or BitmapFontTextRenderer, if you prefer
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				textRenderer.wordWrap = true;
				var txtFormat:TextFormat = new TextFormat();
				txtFormat.font = UtilFunctions.MAIN_FONT;
				txtFormat.align = AlignMode.LEFT;
				txtFormat.color = BLUE_COLOR;
				txtFormat.size = 35;
				textRenderer.textFormat = txtFormat;
				textRenderer.width = 300;
				textRenderer.height = 100;
				return textRenderer;
			}
			
			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			renderer.accessoryFunction  = function( item:PeopleItem ):Object
			{
				return item.accessory;
			};
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			renderer.layoutOrder = BaseDefaultItemRenderer.LAYOUT_ORDER_LABEL_ICON_ACCESSORY;
		}		
		
		private function listCustom(renderer:DefaultListItemRenderer):void 
		{
			
			
			renderer.defaultLabelProperties.elementFormat = new ElementFormat(this.regularFontDescription, 45, LIGHT_TEXT_COLOR);	
			trace("renderer.width = "+renderer.width);
			renderer.defaultSkin = new DefaultSkin(708,ITEM_HEIGHT);
			renderer.defaultSelectedSkin = new DefaultSelectedSkin(708,ITEM_HEIGHT);
			renderer.labelField = "text";
			renderer.gap = 50;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.padding = 10;
			renderer.height = ITEM_HEIGHT;
			renderer.paddingLeft = 100;
			renderer.useStateDelayTimer = true;
			//			renderer.delayTextureCreationOnScroll = true;
			renderer.iconPosition = BaseDefaultItemRenderer.ICON_POSITION_LEFT;
			
			//			renderer.iconFunction = function( item:PeopleItem ):DisplayObject
			//			{
			//				if(item in cachedIcons)
			//				{
			//					return cachedIcons[item];
			//				}
			//				var icon:Image = new Image( cubeplayAtlas.getTexture( item.id ) );
			//				cachedIcons[item] = icon;
			//				return icon;
			//			};
			
			renderer.selectableFunction = function( item:PeopleItem ):Boolean
			{
				trace("selectableFunction "+item.isSelectable);
				return item.isSelectable;
			};
			
			renderer.iconSourceFunction = function( item:PeopleItem ):Object
			{
				trace("iconSourceFunction "+item.textureUrl);
				return item.textureUrl;
			};
			
			renderer.iconLoaderFactory = function():ImageLoader
			{
				var loader:ImageLoader = new ImageLoader();
				loader.snapToPixels = true;
				loader.width = loader.height = 100;
				loader.addEventListener(Event.COMPLETE,loadCompleteHandler);
				loader.loadingTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				loader.errorTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				return loader;
			};
			
			renderer.labelFactory = function():ITextRenderer
			{
				//or BitmapFontTextRenderer, if you prefer
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				textRenderer.wordWrap = true;
				var txtFormat:TextFormat = new TextFormat();
				txtFormat.font = UtilFunctions.MAIN_FONT;
				txtFormat.align = AlignMode.LEFT;
				txtFormat.color = BLUE_COLOR;
				txtFormat.size = 35;
				textRenderer.textFormat = txtFormat;
				textRenderer.width = 300;
				textRenderer.height = 100;
				return textRenderer;
			}
			
			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			renderer.accessoryFunction  = function( item:PeopleItem ):Object
			{
				return item.accessory;
			};
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			renderer.layoutOrder = BaseDefaultItemRenderer.LAYOUT_ORDER_LABEL_ICON_ACCESSORY;
		}
		
		private function listCustomNoti(renderer:DefaultListItemRenderer):void 
		{
			
			trace("listCustomNoti");
			renderer.defaultLabelProperties.elementFormat = new ElementFormat(this.regularFontDescription, 45, LIGHT_TEXT_COLOR);			
			renderer.defaultSkin = new Quad(10,10,0xf5f5f5);
			renderer.defaultSelectedSkin = new Quad(10,10,0xf5f5f5);
			renderer.labelField = "text";
			renderer.gap = 20;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.padding = 10;
			renderer.height = ITEM_HEIGHT;
			renderer.paddingLeft = 30;
			renderer.paddingRight = 60;
			renderer.useStateDelayTimer = true;
			//			renderer.delayTextureCreationOnScroll = true;
			renderer.iconPosition = BaseDefaultItemRenderer.ICON_POSITION_LEFT;
			
			//			renderer.iconFunction = function( item:PeopleItem ):DisplayObject
			//			{
			//				if(item in cachedIcons)
			//				{
			//					return cachedIcons[item];
			//				}
			//				var icon:Image = new Image( cubeplayAtlas.getTexture( item.id ) );
			//				cachedIcons[item] = icon;
			//				return icon;
			//			};
			
			renderer.selectableFunction = function( item:PeopleItemNoti ):Boolean
			{
				trace("selectableFunction "+item.isSelectable);
				return item.isSelectable;
			};
			
			renderer.iconSourceFunction = function( item:PeopleItemNoti ):Object
			{
				trace("iconSourceFunction "+item.textureUrl);
				return item.textureUrl;
			};
			
			renderer.iconLoaderFactory = function():ImageLoader
			{
				var loader:ImageLoader = new ImageLoader();
				loader.snapToPixels = true;
				loader.width = loader.height = 100;
				loader.addEventListener(Event.COMPLETE,loadCompleteHandler);
				loader.loadingTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				loader.errorTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				return loader;
			};
			
			renderer.labelFactory = function():ITextRenderer
			{
				//or BitmapFontTextRenderer, if you prefer
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				textRenderer.wordWrap = true;
				var txtFormat:TextFormat = new TextFormat();
				txtFormat.font = UtilFunctions.MAIN_FONT;
				txtFormat.align = AlignMode.LEFT;
				txtFormat.color = BLUE_COLOR;
				txtFormat.size = 35;
				textRenderer.textFormat = txtFormat;
				textRenderer.width = 300;
				textRenderer.height = 100;
				return textRenderer;
			}
			
			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			renderer.accessoryFunction  = function( item:PeopleItemNoti ):Object
			{
				return item.accessory;
			};
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			renderer.layoutOrder = BaseDefaultItemRenderer.LAYOUT_ORDER_LABEL_ICON_ACCESSORY;
		}		
		
		private function listCustomWaiting(renderer:DefaultListItemRenderer):void 
		{
			
			
			renderer.defaultLabelProperties.elementFormat = new ElementFormat(this.regularFontDescription, 45, LIGHT_TEXT_COLOR);			
			renderer.defaultSkin = new Quad(10,10,BACKGROUND_COLOR);
			renderer.defaultSelectedSkin = new Quad(10,10,BACKGROUND_COLOR);
			renderer.labelField = "text";
			renderer.gap = 20;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.padding = 10;
			renderer.height = ITEM_HEIGHT;
			renderer.paddingLeft = 30;
			renderer.useStateDelayTimer = true;
			renderer.iconPosition = BaseDefaultItemRenderer.ICON_POSITION_LEFT;
			
			renderer.selectableFunction = function( item:PeopleItem ):Boolean
			{
				trace("selectableFunction "+item.isSelectable);
				return item.isSelectable;
			};
			
			renderer.iconSourceFunction = function( item:PeopleItem ):Object
			{
				trace("iconSourceFunction "+item.textureUrl);
				return item.textureUrl;
			};
			
			renderer.iconLoaderFactory = function():ImageLoader
			{
				var loader:ImageLoader = new ImageLoader();
				loader.snapToPixels = true;
				loader.width = loader.height = 100;
				loader.addEventListener(Event.COMPLETE,loadCompleteHandler);
				loader.loadingTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				loader.errorTexture = Texture.fromBitmap(new DEFAULTAVATAR());
				return loader;
			};
			
			renderer.labelFactory = function():ITextRenderer
			{
				//or BitmapFontTextRenderer, if you prefer
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				textRenderer.wordWrap = true;
				var txtFormat:TextFormat = new TextFormat();
				txtFormat.font = UtilFunctions.MAIN_FONT;
				txtFormat.align = AlignMode.LEFT;
				txtFormat.color = BLUE_COLOR;
				txtFormat.size = 35;
				textRenderer.textFormat = txtFormat;
				textRenderer.width = 300;
				textRenderer.height = 100;
				return textRenderer;
			}
			
			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			renderer.accessoryFunction  = function( item:PeopleItem ):Object
			{
				return item.accessory;
			};
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			renderer.layoutOrder = BaseDefaultItemRenderer.LAYOUT_ORDER_LABEL_ICON_ACCESSORY;
		}				
		
		private function loadCompleteHandler(e:Event):void
		{
			trace("loadCompleteHandler "+String((e.target as ImageLoader).source));
			//			cubeplayAtlas.enqueueWithName(String((e.target as ImageLoader).source),String((e.target as ImageLoader).source));
			//			cubeplayAtlas.addTexture(String((e.target as ImageLoader).source),(e.target as Texture));
		}
		
	}
}

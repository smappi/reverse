package cubeplay.smappiOnlineGame
{
	import flash.net.SharedObject;
	
	import citrus.utils.AGameData;
	
	import cubeplay.smappiOnlineGame.entities.Profile;
	
	public class MyGameData extends AGameData
	{
		private var so:SharedObject;
		private var _score:int;
		
		private var _profile:Profile;
		private var _jsonProfile:String;
		
		public function MyGameData()
		{
			super();
			so = SharedObject.getLocal("app_name");
		}
		
		public function get jsonProfile():String
		{
			if(so.data.jsonProfile){
				_jsonProfile = so.data.jsonProfile;
			}
			return _jsonProfile;
		}
		
		public function set jsonProfile(value:String):void
		{
			_jsonProfile = value;
			so.data.jsonProfile = _jsonProfile;
			save();
			dataChanged.dispatch("jsonProfile", _jsonProfile);			
		}		
		
		public function save():void{
			so.flush();
		}
		
		
		public function get profile():Profile
		{
			_profile = new Profile(jsonProfile);
			return _profile;
		}
		
		public function get score():int
		{
			if(so.data.score){
				_score = so.data.score;
			}
			return _score;
		}
		
		public function set score(value:int):void
		{
			_score = value;
			so.data.score = _score;
			dataChanged.dispatch("score", _score);
		}
		
	}
}
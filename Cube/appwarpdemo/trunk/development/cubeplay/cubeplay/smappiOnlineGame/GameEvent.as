package cubeplay.smappiOnlineGame
{
	public class GameEvent
	{
		public static const NEW_REMOTE_PLAYER_JOINED_ROOM:String = "NEW_REMOTE_PLAYER_JOINED_ROOM";
		public static const CREATE_ROOM_DONE:String = "CREATE_ROOM_DONE";
		public static const GET_ROOM_DONE:String = "GET_ROOM_DONE";
		public static const CONNECTED_TO_SERVER:String = "connected_to_server";
		public static const RECONNECTED_TO_SERVER:String = "reconnected_to_server";
		public static const JOINED_ROOM_SUCCESSFULLY:String = "join_room_successfully";
		
		public static const INCREASE_SCORE:String = "increasePoint";
		public static const DECREASE_SCORE:String = "decreasePoint";
		public static const KILLED:String = "killed";
		public static const FINISH:String = "finish";
		public static const ADDED_NEW_PLAYER:String = "added_new_player";
		
		public static const AUTHENTICATED_FACEBOOK:String = "AUTHENTICATED_FACEBOOK";
		public static const GET_FRIENDS_COMPLETE_FACEBOOK:String = "GET_FRIENDS_COMPLETE_FACEBOOK";
		
		public static const FIND_DOCUMENT_BY_KEY_VALUE_SUCCESS:String = "findDocumentByKeyValueSuccess";
		public static const DELETE_DOCUMENT_BY_KEY_VALUE_SUCCESS:String = "DELETE_DOCUMENT_BY_KEY_VALUE_SUCCESS";
		public static const ON_USER_CHANGE_ROOM_PROPERTIES:String = "onUserChangeRoomProperties";
		public static const START_GAME:String = "START_GAME";
		public static const STOP_GAME:String = "STOP_GAME";
		
		public static const ON_INVITE:String = "ON_INVITE";
		public static const INVITE_COMPLETE:String = "INVITE_COMPLETE";
		public static const ON_JOIN_ROOM_FROM_NOTIFICATION:String = "ON_JOIN_ROOM_FROM_NOTIFICATION";
		
		public static const BACK:String = "BACK";
		public static const CLOSE:String = "CLOSE";
		
		public function GameEvent()
		{
		}
	}
}
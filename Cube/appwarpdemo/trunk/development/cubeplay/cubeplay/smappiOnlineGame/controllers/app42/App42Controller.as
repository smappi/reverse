package cubeplay.smappiOnlineGame.controllers.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42API;
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.buddy.BuddyService;
	import com.shephertz.app42.paas.sdk.as3.game.ScoreBoardService;
	import com.shephertz.app42.paas.sdk.as3.social.SocialService;
	import com.shephertz.app42.paas.sdk.as3.storage.StorageService;
	import com.shephertz.app42.paas.sdk.as3.user.Profile;
	import com.shephertz.app42.paas.sdk.as3.user.UserService;
	
	import flash.utils.Dictionary;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.buddy.AcceptFriendRequestCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.buddy.GetAllFriendsRequestCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.buddy.SendFriendRequestCallBack;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.leaderboard.SaveScoreCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.social.LinkUserFacebookAccountCallBack;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.storage.DeleteDocumentByKeyValueCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.storage.FindDocumentByKeyValueCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.storage.InsertJSONDocumentCallback;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.user.CreateUserCallBack;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.user.CreateUserWithProfileCallBack;
	import cubeplay.smappiOnlineGame.controllers.facebook.FacebookController;
	import cubeplay.smappiOnlineGame.entities.Profile;
	import cubeplay.utils.GameFactory;
	import cubeplay.utils.UtilFunctions;
	
	public class App42Controller extends FacebookController
	{	
		// AppWarp String Constants                
		public var userService:UserService;
		public var buddyService:BuddyService;
		public var socialService:SocialService;
		public var scoreBoardService:ScoreBoardService;
		public var storageService:StorageService;
		
		public function App42Controller(owner:StarlingState = null)
		{
			super(owner);
			initServiceApi();
		}
		
		private function initServiceApi():void{
			trace("initialize App42Controller");
		
			userService = App42API.buildUserService();   
			buddyService = App42API.buildBuddyService();   
			socialService = App42API.buildSocialService();  
			scoreBoardService = App42API.buildScoreBoardService();
			storageService = App42API.buildStorageService(); 
		}
		
		//STORAGE SERVICE

		
		public function deteleInvite(fromTo:String):void{
			deleteDocumentsByKeyValue("fromto",fromTo,GameFactory.REQUESTS,GameFactory.SMAPPI);
		}
		
		public function deleteDocumentByKeyValueSuccess(object:Object):void{
			trace("deleteDocumentByKeyValueSuccess");
			this.dispatchEventWith(GameEvent.DELETE_DOCUMENT_BY_KEY_VALUE_SUCCESS,false,object);
		}		
		
		public function deleteDocumentsByKeyValue(key:String, value:String,collectionName:String,dbName:String = GameFactory.SMAPPI,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new DeleteDocumentByKeyValueCallback(this);
			}		
			storageService.deleteDocumentsByKeyValue(dbName,collectionName,key,value,callBackFunction);
		}		
		
		public function findDocumentByKeyValueSuccess(object:Object):void{
			this.dispatchEventWith(GameEvent.FIND_DOCUMENT_BY_KEY_VALUE_SUCCESS,false,object);
		}
		
		public function findDocumentByKeyValue(key:String, value:String,collectionName:String,sortDir:String = null,sortKey:String = null,dbName:String = GameFactory.SMAPPI,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new FindDocumentByKeyValueCallback(this);
			}		
			if(sortDir && sortKey){
				var dict:Dictionary = new Dictionary();
				dict[sortDir] = sortKey; // Use orderByDescending for Descending or orderByAscending for Ascending
				storageService.setOtherMetaHeaders(dict);
			}
			storageService.findDocumentByKeyValue(dbName,collectionName,key,value,callBackFunction); 
		}
		
		
		
		public function doInvite(jsonInvite:String):void{
			insertJSONDocument(jsonInvite,GameFactory.REQUESTS,GameFactory.SMAPPI);
		}
		
		public function insertJSONDocument(jsonParam:String,collectionName:String,dbName:String = GameFactory.SMAPPI,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new InsertJSONDocumentCallback(this);
			}						
			storageService.insertJSONDocument(dbName,collectionName,JSON.parse(jsonParam), callBackFunction);    			
		}
		
		//USER
		
		public function createUser(userId:String,pwd:String = "123456",email:String = "", callBackFunction:App42CallBack = null):void{
			if(email == ""){
				email = userId+"@smappi.com";
			}
			if(callBackFunction == null) {
				callBackFunction = new CreateUserCallBack(this);
			}
			userService.createUser(userId,pwd,email, callBackFunction); 
		}
		
		public function createUserWithFacebookProfile(data:Object,callBackFunction:App42CallBack = null):void{
			saveProfile(JSON.stringify(new cubeplay.smappiOnlineGame.entities.Profile(data.jsonData,data.accessToken,true)));
			var profileJson:Object = JSON.parse(data.jsonData);

			
			if(callBackFunction == null) {
				callBackFunction = new CreateUserWithProfileCallBack(this);
			}
			var profile:com.shephertz.app42.paas.sdk.as3.user.Profile = new com.shephertz.app42.paas.sdk.as3.user.Profile();  
			profile.setFirstName(profileJson.first_name);  
			profile.setLastName(profileJson.last_name);  
			profile.setSex(profileJson.gender);  
			
			userService.createUserWithProfile(profileJson.id,"123456",profileJson.id+"@smappi.com",profile,callBackFunction); 
		}				
		
		public function createUserWithGuestProfile(guestName:String,callBackFunction:App42CallBack = null):void{
				
			var guestProfile:cubeplay.smappiOnlineGame.entities.Profile = new cubeplay.smappiOnlineGame.entities.Profile();
			guestProfile.id = guestName + "_" +UtilFunctions.randRange(0,999999);;
			guestProfile.firstName = guestName;
			guestProfile.fullname = guestName;
			guestProfile.isGuest = true;
			guestProfile.url = "";
			trace("JSON.stringify "+JSON.stringify(guestProfile));
			saveProfile(JSON.stringify(guestProfile));
				
			if(callBackFunction == null) {
				callBackFunction = new CreateUserWithProfileCallBack(this);
			}
			var profile:com.shephertz.app42.paas.sdk.as3.user.Profile = new com.shephertz.app42.paas.sdk.as3.user.Profile();  
			profile.setFirstName(guestName);  
			profile.setLastName(guestName);  
			profile.setSex("");  
			

			userService.createUserWithProfile(guestProfile.id,"123456",guestProfile.id+"@smappi.com",profile,callBackFunction); 
		}			
		
		public function createUserWithProfile(id:String,firstName:String,lastName:String = "",gender:String = "",accessToken:String = "",pwd:String = "123456",email:String = "", callBackFunction:App42CallBack = null):void{
			
			if(callBackFunction == null) {
				callBackFunction = new CreateUserWithProfileCallBack(this);
			}
			var profile:com.shephertz.app42.paas.sdk.as3.user.Profile = new com.shephertz.app42.paas.sdk.as3.user.Profile();  
			profile.setFirstName(firstName);  
			profile.setLastName(lastName);  
			profile.setSex(gender);  
			
			if(email == ""){
				email = id+"@smappi.com";
			}
			userService.createUserWithProfile(id,pwd,email,profile,callBackFunction); 
		}		
		
		//SOCIAL
		
		public function linkUserFacebookAccount(userId:String,accessToken:String = "",callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new LinkUserFacebookAccountCallBack(this);
			}
			socialService.linkUserFacebookAccount(userId,accessToken,callBackFunction);			
		}			
		
		//BUDDY
		
		public function sendFriendRequest(userId:String,buddyId:String,message:String = "autoMakeFriend", callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new SendFriendRequestCallBack(this);
			}			
			buddyService.sendFriendRequest(userId,buddyId,message,callBackFunction); 
		}
		
		public function acceptFriendRequest(userId:String,buddyId:String,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new AcceptFriendRequestCallback(this);
			}			
			buddyService.acceptFriendRequest(userId,buddyId,callBackFunction); 
		}	
		
		public function getAllFriends(userId:String,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new GetAllFriendsRequestCallback(this);
			}			
			buddyService.getAllFriends(userId,callBackFunction); 
		}
		
		//SCORE
		public function saveUserScore(gameName:String,userName:String,gameScore:Number,callBackFunction:App42CallBack = null):void{
			if(callBackFunction == null) {
				callBackFunction = new SaveScoreCallback(this);
			}						
			scoreBoardService.saveUserScore(gameName,userName,gameScore,callBackFunction);
		}		
	}
}

package cubeplay.smappiOnlineGame.controllers.appwarp
{
	import com.shephertz.appwarp.WarpClient;
	import com.shephertz.appwarp.listener.ConnectionRequestListener;
	import com.shephertz.appwarp.listener.LobbyRequestListener;
	import com.shephertz.appwarp.listener.NotificationListener;
	import com.shephertz.appwarp.listener.RoomRequestListener;
	import com.shephertz.appwarp.listener.ZoneRequestListener;
	import com.shephertz.appwarp.messages.Chat;
	import com.shephertz.appwarp.messages.LiveLobby;
	import com.shephertz.appwarp.messages.LiveResult;
	import com.shephertz.appwarp.messages.LiveRoom;
	import com.shephertz.appwarp.messages.LiveUser;
	import com.shephertz.appwarp.messages.Lobby;
	import com.shephertz.appwarp.messages.MatchedRooms;
	import com.shephertz.appwarp.messages.Move; 
	import com.shephertz.appwarp.messages.Room;
	import com.shephertz.appwarp.types.ResultCode;
	
	import flash.utils.ByteArray;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	
	public class AppWarpListener implements ConnectionRequestListener, RoomRequestListener, NotificationListener, ZoneRequestListener, LobbyRequestListener
	{
		private var _owner:AppWarpController;
		
		public function AppWarpListener(owner:AppWarpController)
		{
			_owner = owner;
		}
		
		public function onInitUDPDone(res:int):void
		{ 
			if(res == ResultCode.success){
				_owner.udpEnabled = true;
				_owner.updateStatus("udp enabled");
			}
		} 
		 
		public function onConnectDone(res:int):void  
		{
			if(res == ResultCode.success){

				_owner.connectedToServerSuccessfully();
				if(GameFactory.USE_UDP){
					try{
						WarpClient.getInstance().initUDP();                    
					}
					catch(e:Error){
						// happens when running in flash player (browser)
						_owner.udpEnabled = false;
					}
				}
			}
			else if(res == ResultCode.auth_error || res == ResultCode.auth_error){
				_owner.updateStatus("Verify your api key and secret key");
			}
			else if(res == ResultCode.connection_error){
				_owner.updateStatus("Network Error. Check your internet connectivity and retry.");
			}
			else{
				_owner.updateStatus("Unknown Error");
			}
		}
		
		public function onDisConnectDone(res:int):void
		{
		}
		
		public function onSubscribeRoomDone(event:Room):void
		{
		}
		
		public function onUnsubscribeRoomDone(event:Room):void
		{
		}
		
		public function onJoinRoomDone(event:Room):void
		{
			if(event.result == ResultCode.success){
				_owner.joinedRoomSuccessfully(event.roomId);
				_owner.updateStatus("Joined room "+event.roomId);
				WarpClient.getInstance().getLiveRoomInfo(event.roomId);
				
			}
			else{
				WarpClient.getInstance().deleteRoom(event.roomId);
				_owner.updateStatus("Room join failed. Verify your room id." + event.roomId);
			}
		}
		
		public function onLeaveRoomDone(event:Room):void
		{
		}
		
		public function onGetLiveRoomInfoDone(event:LiveRoom):void
		{
			trace("onGetLiveRoomInfoDone "+event.users+" --- " + event.properties.start + " --- "+event.properties.type);
			if(event.properties.start == true){
				_owner.dispatchEventWith(GameEvent.START_GAME);
			}
			for (var i:int = 0; i < event.users.length; i++) 
			{
				if(event.users[i] != _owner.me.id){
					WarpClient.getInstance().getLiveUserInfo(event.users[i]);
				}
			}
		}
		
		public function onSetCustomRoomDataDone(event:LiveRoom):void
			
		{
		}
		
		public function onLockPropertiesDone(result:int):void
		{
			
		}
		 
		public function onUnlockPropertiesDone(result:int):void
		{
			
		}
		
		public function onUpdatePropertiesDone(event:LiveRoom):void
		{
//			trace("onUpdatePropertiesDone "+event);
		}
		
		public function onRoomCreated(event:Room):void
		{
		}
		
		public function onRoomDestroyed(event:Room):void
		{
		}
		
		public function onUserLeftRoom(event:Room, user:String):void
		{
			_owner.removePlayerById(user);
		}
		
		public function onUserJoinedRoom(event:Room, user:String):void
		{
			WarpClient.getInstance().getLiveUserInfo(user);
//			_owner.addPlayerById(user);
		}
		public function onUserResumed(roomid:String, isLobby:Boolean, username:String):void
		{
			
		}
		public function onUserPaused(roomid:String, isLobby:Boolean, username:String):void
		{
			
		}
		public function onUserLeftLobby(event:Lobby, user:String):void
		{
			_owner.removePlayerById(user);
		}
		
		public function onUserJoinedLobby(event:Lobby, user:String):void
		{
			_owner.addPlayerById(user);
		}
		
		public function onPrivateChatReceived(sender:String, chat:String):void
		{
			
		}
		
		public function onChatReceived(event:Chat):void
		{
			
		}
		
		public function onUpdatePeersReceived(update:ByteArray, fromUDP:Boolean):void
		{
			var msg:String = update.readUTF();
			_owner.updateStatus(msg);
			_owner.updateRemotePlayer(msg);
		}

		
		public function onUserChangeRoomProperties(room:Room, user:String, properties:Object, lockTable:Object):void
		{
			
			_owner.updateRoomPropertiessSuccess(room,user,properties);
		}
		
		public function onMoveCompleted(moveEvent:Move):void
		{
			
		}
		
		public function onGameStarted(sender:String, roomid:String, nextTurn:String):void
		{

		}
		
		public function onGameStopped(sender:String, roomid:String):void
		{
			
		}
		
		/**
		 *  Following are ZoneRequestListener interface implementation methods.
		 *  Not used in this sample but this illustrates how to implement them.
		 */
		
		public function onCreateRoomDone(event:Room):void
		{
			_owner.createRoomCallback(event);
		}
		
		public function onDeleteRoomDone(event:Room):void
		{
			
		}
		
		public function onGetLiveUserInfoDone(event:LiveUser):void
		{
			trace("onGetLiveUserInfoDone "+event.customData);
			_owner.addPlayerByJsonData(event.customData);
			if(event.customData != "") _owner.updateRemotePlayer(event.customData);
		}
		
		public function onGetAllRoomsDone(event:LiveResult):void
		{
			
		}
		
		public function onGetOnlineUsersDone(event:LiveResult):void
		{
			
		}
		
		public function onSetCustomUserInfoDone(event:LiveUser):void
		{
			trace("onSetCustomUserInfoDone "+event.customData);
		}
		
		// getRoomInRangeCallback
		
		public function onGetMatchedRoomsDone(event:MatchedRooms):void
		{
			trace("onGetMatchedRoomsDone "+event.rooms);
			_owner.getRoomsWithPropertiesCallback(event);
		}
		
		/**
		 *  Following are LobbyRequestListener interface implementation methods.
		 *  Not used in this sample but this illustrates how to implement them.
		 */
		
		public function onJoinLobbyDone(event:Lobby):void{
			if(event.result == ResultCode.success){
				_owner.updateStatus("Joined room "+event.lobbyid);
				WarpClient.getInstance().getLiveLobbyInfo();
			}
			else{
				_owner.updateStatus("Room join failed. Verify your room id.");
			}	
		}
		
		public function onLeaveLobbyDone(event:Lobby):void{
			
		}
		 
		public function onSubscribeLobbyDone(event:Lobby):void{
			
		}
		
		public function onUnsubscribeLobbyDone(event:Lobby):void{
			
		}
		
		public function onGetLiveLobbyInfoDone(event:LiveLobby):void{
			trace("onGetLiveLobbyInfoDone "+event.users);
			for (var i:int = 0; i < event.users.length; i++) 
			{
				if(event.users[i] != _owner.me.id){
					_owner.addPlayerById(event.users[i]);
					WarpClient.getInstance().getLiveUserInfo(event.users[i]);
				}
			}				
		}
	}
}
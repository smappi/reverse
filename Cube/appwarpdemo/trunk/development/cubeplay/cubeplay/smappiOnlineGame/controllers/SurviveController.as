package cubeplay.smappiOnlineGame.controllers
{
	import citrus.core.starling.StarlingState;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.entities.ResultInformation;
	import cubeplay.smappiOnlineGame.players.PlayerView;
	
	import starling.events.Event;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;
	

	public class SurviveController extends AppWarpController
	{
		public function SurviveController(owner:StarlingState)
		{
			super(owner);
		}
		
		override public function beKilledHandler(e:Event):void
		{
			super.beKilledHandler(e);
			var playerInformation:PlayerView = e.currentTarget as PlayerView;
			var score:int = playerInformation.score;
			this.dispatchEventWith(GameEvent.FINISH,false,new ResultInformation(score));	
		}
		
		
	
	}
}
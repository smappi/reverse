package cubeplay.smappiOnlineGame.controllers
{
	//	import com.adobe.serialization.json.JSON;
	
	import com.milkmangames.nativeextensions.GoViral;
	import com.shephertz.app42.paas.sdk.as3.App42API;
	import com.shephertz.appwarp.WarpClient;
	import com.shephertz.appwarp.types.ConnectionState;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.geom.Point;
	
	import citrus.core.CitrusEngine;
	import citrus.core.starling.StarlingState;
	
	import cubeplay.basicscreens.CPFriendsMatchScreen;
	import cubeplay.basicscreens.CPPlayScreen;
	import cubeplay.basicscreens.CPPracticeScreen;
	import cubeplay.basicscreens.CPRandomMatchScreen;
	import cubeplay.elements.smappipopup.SmappiPopupManager;
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.smappiOnlineGame.entities.Profile;
	import cubeplay.smappiOnlineGame.entities.UserDataObject;
	import cubeplay.smappiOnlineGame.players.LocalPlayer;
	import cubeplay.smappiOnlineGame.players.Player;
	import cubeplay.smappiOnlineGame.players.PlayerView;
	import cubeplay.smappiOnlineGame.players.RemotePlayer;
	import cubeplay.utils.CubeplayStyle;
	import cubeplay.utils.GameFactory;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class SmappiController extends Sprite
	{
		public static const apiKey:String = "fb0bdd78f14baef045f935d890e25528ea57cc2c6f702a6284492a3ce602e423"
		public static const secretKey:String = "895e74a04e1d07596f158bde10bf21d0015612c7b9c1645e847d569d3cdea74e";
		
		// GAME CONFIGURATION
		public var finishScore:int = 10;
		public var timesUpSeconds:int = 10;
		
		
		//
		public var me:Profile;
		public var remotePlayers:Vector.<Player> = new Vector.<Player>();
		public var localPlayer:Player;
		private var _owner:CPPlayScreen;
		private static var _instance:SmappiController;
		 
		public static function initialize(apiKeyParam:String = apiKey,secretKeyParam = secretKey,facebook_app_id:String = "939566379391048"):void{
			trace("SmappiController");
			//SAVE GAME
			CitrusEngine.getInstance().gameData = new MyGameData();
			
			//CUBE PLAY STYLE
			new CubeplayStyle();
			
			//BAAS
			App42API.initialize(apiKeyParam,secretKeyParam); 
			
			//REALTIME SERVICE
			WarpClient.initialize(apiKeyParam, secretKeyParam);
			
			//FACEBOOK SERVICE
			if(GoViral.isSupported())
			{
				GoViral.create();
				GameFactory.FACEBOOK_APP_ID = facebook_app_id;
			}
			
			//LET CUBEPLAY KNOW WHICH CLASS TO USE
			GameFactory.PRACTICE_SCREEN_CLASS = CPPracticeScreen;
			GameFactory.FRIENDS_SCREEN_CLASS = CPFriendsMatchScreen;
			GameFactory.RANDOM_SCREEN_CLASS = CPRandomMatchScreen;
			GameFactory.LOCAL_PLAYER_CLASS = LocalPlayer;
			GameFactory.REMOTE_PLAYER_CLASS = RemotePlayer;
			GameFactory.PLAYVIEW_CLASS = PlayerView;
			
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.EXITING, onAppExit);

		}
		
		protected static function onAppExit(event:flash.events.Event):void
		{
			trace("onAppExit");
			(CitrusEngine.getInstance().gameData as MyGameData).save();
			if(WarpClient.getInstance() != null && WarpClient.getInstance().getConnectionState() == ConnectionState.connected)
			{
				WarpClient.getInstance().disconnect();
			}	
		}
		
		public static function isLoggedIn():Boolean{
			
			if((CitrusEngine.getInstance().gameData as MyGameData).jsonProfile == null){
				return true;		
			}
			return false;
		}
		
		public static function logout():void{
			(CitrusEngine.getInstance().gameData as MyGameData).jsonProfile = null;
		}
		
		public static function saveProfile(jsonString:String):void{
			(CitrusEngine.getInstance().gameData as MyGameData).jsonProfile = jsonString;
		}
		
		public static function getMyProfile():Profile{
			return (CitrusEngine.getInstance().gameData as MyGameData).profile;
		}
		
		public function SmappiController(owner:CPPlayScreen = null)
		{
			_instance = this;
			_owner = owner;
		}
		
		public function get owner():CPPlayScreen
		{
			return _owner;
		}

		public function set owner(value:CPPlayScreen):void
		{
			_owner = value;
		}

		public static function getInstance():SmappiController{
			return _instance;
		}	
		
		public function updateStatus(msg:String):void{
			return;
			SmappiPopupManager.showInfoMessage(msg);
		}
		
		public function addPlayerByJsonData(msg:String):Player{	
			var userData:Object = JSON.parse(msg);
			var sender:String = userData.id;		
			if(sender == me.id){
				return null;
			}			
			var player:Player = new GameFactory.REMOTE_PLAYER_CLASS(sender,{},userData);
			remotePlayers.push(player);		
			_owner.addRemotePlayer(player);
			trace("addPlayerByJsonData "+remotePlayers.length + " " + _owner + " " + player.x +" - " +player.y+ " " + msg);
			this.dispatchEventWith(GameEvent.NEW_REMOTE_PLAYER_JOINED_ROOM,false,userData);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.INCREASE_SCORE,increaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.DECREASE_SCORE,decreaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.KILLED,beKilledHandler);
			return player;
		}				
		
		public function addPlayerById(userId:String):Player{		
			if(userId == me.id){
				return null;
			}
			var player:Player = new GameFactory.REMOTE_PLAYER_CLASS(userId,{id:userId},null);
			remotePlayers.push(player);							
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.INCREASE_SCORE,increaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.DECREASE_SCORE,decreaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.KILLED,beKilledHandler);
			return player;
		}			
		
		public function addPlayer(player:Player):Player{
			trace("player "+player);
			if(!player is LocalPlayer){
				remotePlayers.push(player);				
			}else{
				localPlayer = player;
			}
			_owner.add(localPlayer);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.INCREASE_SCORE,increaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.DECREASE_SCORE,decreaseScoreHandler);
			(player.view as GameFactory.PLAYVIEW_CLASS).addEventListener(GameEvent.KILLED,beKilledHandler);
			return player;
		}			
		
		public function removePlayerById(id:String):void{
			for (var i:int = 0; i < remotePlayers.length; i++) 
			{
				if(remotePlayers[i].id == id){
					remotePlayers[i].kill = true;
					remotePlayers.splice(i,1);
					return;
				}
			}			
		}			
		
		public function getPlayerById(id:String):Player{
			for (var i:int = 0; i < remotePlayers.length; i++) 
			{
				if(remotePlayers[i].id == id){
					return remotePlayers[i];
				}
			}			
			return null;
		}
		
		public function getPlayersById(id:String,remove:Boolean = false):Vector.<Player>{
			var players:Vector.<Player> = new Vector.<Player>();
			for (var i:int = 0; i < remotePlayers.length; i++) 
			{
				if(remotePlayers[i].id == id){
					players.push(remotePlayers[i]);
					if(remove){
						remotePlayers.splice(i,1);
					}
				}
			}			
			return players;
		}
		
		public function updateRemotePlayer(msg:String):void{
			var userData:UserDataObject = JSON.parse(msg) as UserDataObject;
			
			var sender:String = userData.id;
			var x:int = userData.x;
			var y:int = userData.y;
			
			var point:Point = new Point(x,y);
			
			if(sender == me.id){
				return;
			}
		}		
		
		public function beKilledHandler(e:starling.events.Event):void
		{
			
		}
		
		public function decreaseScoreHandler(e:starling.events.Event):void
		{
			
		}		
		
		public function increaseScoreHandler(e:starling.events.Event):void
		{
			
		}
		
	}
}

package cubeplay.smappiOnlineGame.controllers.facebook
{
	import com.milkmangames.nativeextensions.GVFacebookFriend;
	import com.milkmangames.nativeextensions.GoViral;
	import com.milkmangames.nativeextensions.events.GVFacebookEvent;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.controllers.SmappiController;
	
	public class FacebookController extends SmappiController
	{
		public function FacebookController(owner:StarlingState = null)
		{
			super(owner);
			initFacebook();
		}
		
		private function initFacebook():void{
			trace("initFacebook");
			if(GoViral.isSupported())
			{
				
				if(GoViral.goViral.isFacebookSupported())
				{
					// replace 000000 with your facebook app id!
					
					GoViral.goViral.initFacebook(GameFactory.FACEBOOK_APP_ID,"");
					trace("iniialized.");
					// facebook events
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_DIALOG_CANCELED,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_DIALOG_FAILED,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_DIALOG_FINISHED,onFacebookEvent);
					
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGGED_OUT,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGIN_CANCELED,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGIN_FAILED,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_FAILED,onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, onFacebookEvent);
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_AD_ID_RESPONSE, onFacebookEvent);
					
				}
			}
			else {
				trace("GoViral only works on mobile!");
			}
		}
		
		public function loginFacebook():void
		{
			if(GoViral.isSupported()){
				if(!GoViral.goViral.isFacebookAuthenticated())
				{			
					// you must set at least one read permission.  if you don't know what to pick, 'basic_info' is fine.
					// PUBLISH PERMISSIONS are NOT permitted by Facebook here anymore.
					GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGGED_IN,onFacebookEvent);
					GoViral.goViral.authenticateWithFacebook("public_profile,user_friends"); 
				}else{
					this.getMeFacebook();
				}
			}
		}		
		
		/** Get your own facebook profile */
		public function getMeFacebook():void
		{
			if (!checkLoggedInFacebook()) return;
			var accessToken:String = GoViral.goViral.getFbAccessToken();
			//			trace("accessToken= "+accessToken);
			//			trace("Getting profile...");
			GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, onFacebookEvent);
			//			GoViral.goViral.requestMyFacebookProfile();
			GoViral.goViral.facebookGraphRequest("me","GET",{fields:"picture,name,gender,first_name,last_name"});
			//			trace("sent profile request.");
		}		
		
		public function getFriendsFacebook():void
		{
			if (!checkLoggedInFacebook()) return;
			GoViral.goViral.requestFacebookFriends({fields:"picture"});
			//			GoViral.goViral.addEventListener(GVFacebookEvent.FB_REQUEST_RESPONSE, onFacebookEvent);
			//			GoViral.goViral.facebookGraphRequest("/me/invitable_friends","GET",{fields:"installed,id,name"});
		}		
		
		/** Check you're logged in to facebook before doing anything else. */
		public function checkLoggedInFacebook():Boolean
		{
			// make sure you're logged in first
			if (!GoViral.goViral.isFacebookAuthenticated())
			{
				trace("Not logged in!");
				return false;
			}
			return true;
		}		
		
		private function onFacebookEvent(e:GVFacebookEvent):void
		{
			switch(e.type)
			{
				case GVFacebookEvent.FB_DIALOG_CANCELED:
					trace("Facebook dialog '"+e.dialogType+"' canceled.");
					break;
				case GVFacebookEvent.FB_DIALOG_FAILED:
					trace("dialog err:"+e.errorMessage);
					break;
				case GVFacebookEvent.FB_DIALOG_FINISHED:
					trace("fin dialog '"+e.dialogType+"'="+e.jsonData);
					break;
				case GVFacebookEvent.FB_LOGGED_IN:
					trace("Logged in to facebook! " + e);
					this.getMeFacebook();
					break;
				case GVFacebookEvent.FB_LOGGED_OUT:
					trace("Logged out of facebook.");
					break;
				case GVFacebookEvent.FB_LOGIN_CANCELED:
					trace("Canceled facebook login.");
					break;
				case GVFacebookEvent.FB_LOGIN_FAILED:
					trace("Login failed:"+e.errorMessage+"(notify? "+e.shouldNotifyFacebookUser+" "+e.facebookUserErrorMessage+")");
					break;
				case GVFacebookEvent.FB_REQUEST_FAILED:
					trace("Facebook '"+e.graphPath+"' failed:"+e.errorMessage);
					break;
				case GVFacebookEvent.FB_REQUEST_RESPONSE:
					// handle a friend list- there will be only 1 item in it if 
					// this was a 'my profile' request.				
					if (e.friends!=null)
					{					
						// 'me' was a request for own profile.
						if (e.graphPath=="me")
						{
							var myProfile:GVFacebookFriend=e.friends[0];
							//							trace("Profile data = " + e.jsonData + " - " + e.data);
							this.dispatchEventWith(GameEvent.AUTHENTICATED_FACEBOOK,false,{jsonData:e.jsonData,accessToken:GoViral.goViral.getFbAccessToken()});
							return;
						}
						
						// 'me/friends' was a friends request.
						if (e.graphPath=="me/friends")
						{					
							trace(e.graphPath+" res="+e.jsonData);	
							//							var allFriends:String="";
							//							
							//							var friendsArray:Array = new Array();
							//							for each(var friend:GVFacebookFriend in e.friends)
							//							{
							//								friendsArray.push(friend.id);
							//								allFriends+=","+friend.name+","+friend.id;
							//							}
							//							
							//							trace("requestFacebookFriends "+e.graphPath+"= ("+e.friends.length+")="+allFriends+",json="+e.jsonData);
							this.dispatchEventWith(GameEvent.GET_FRIENDS_COMPLETE_FACEBOOK,false,JSON.parse(e.jsonData));
						}
						else
						{
							trace(e.graphPath+" res="+e.jsonData);	
						}
					}
					else
					{
						trace(e.graphPath+" res="+e.jsonData);
					}
					break;
				case GVFacebookEvent.FB_AD_ID_RESPONSE:
					trace("Facebook Ad ID Response:"+e.facebookMobileAdId);
					break;
			}
		}
		
	}
}
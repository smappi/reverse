package cubeplay.smappiOnlineGame.controllers
{
	import flash.geom.Point;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;
	import cubeplay.smappiOnlineGame.entities.ResultInformation;
	import cubeplay.smappiOnlineGame.players.Player;
	import cubeplay.smappiOnlineGame.players.PlayerView;
	
	import starling.events.Event;
	

	public class RaceController extends AppWarpController
	{
		public function RaceController(owner:StarlingState)
		{
			super(owner);
		}
		
		override public function increaseScoreHandler(e:Event):void
		{ 
			// TODO Auto Generated method stub
			super.increaseScoreHandler(e);
			
			var playerInformation:PlayerView = e.currentTarget as PlayerView;
			var score:int = playerInformation.score;
			if(score >= finishScore){
				this.dispatchEventWith(GameEvent.FINISH,false,new ResultInformation(score));
			}
			
			this.updateStatus("increasePoint "+playerInformation.score);
		}
		
		override public function updateRemotePlayer(msg:String):void
		{
			trace("updateRemotePlayer "+msg);
			var userData:Object = JSON.parse(msg);
			var sender:String = userData.id;
			var x:int = userData.x;
			var y:int = userData.y;
			var score:int = userData.score;
			var point:Point = new Point(x,y);
			
			if(sender == me.id){
				return;
			}
			
			
			var player:Player = getPlayerById(sender);
			if(player != null){

				player.moveTo(point);
				player.score = score;
			}
		}
		
		
		
	}
}

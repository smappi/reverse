package cubeplay.smappiOnlineGame.controllers.app42.callback.leaderboard
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	
	import flash.utils.getQualifiedClassName;
	
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.Callback;
	
	public class SaveScoreCallback extends Callback implements App42CallBack
	{
		public function SaveScoreCallback(owner:App42Controller)
		{
			super(owner);
		}
		
		public function onSuccess(object:Object):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onSuccess "+object);
		}
		
		public function onException(exception:App42Exception):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onException "+exception);
		}
	}
}
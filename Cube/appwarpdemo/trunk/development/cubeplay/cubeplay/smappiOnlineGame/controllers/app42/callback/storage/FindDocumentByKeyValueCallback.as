package cubeplay.smappiOnlineGame.controllers.app42.callback.storage
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	
	import flash.utils.getQualifiedClassName;
	
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.Callback;
	
	public class FindDocumentByKeyValueCallback extends Callback implements App42CallBack
	{
		public function FindDocumentByKeyValueCallback(owner:App42Controller)
		{
			super(owner);
		}
		
		public function onSuccess(object:Object):void
		{
			owner.findDocumentByKeyValueSuccess(object);

		}
		
		public function onException(exception:App42Exception):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onException "+exception);
		
		}
	}
}
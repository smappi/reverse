package cubeplay.smappiOnlineGame.controllers.appwarp
{
	import com.shephertz.appwarp.WarpClient;
	import com.shephertz.appwarp.messages.MatchedRooms;
	import com.shephertz.appwarp.messages.Room;
	import com.shephertz.appwarp.types.ConnectionState;
	
	import flash.utils.ByteArray;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	
	import cubeplay.utils.UtilFunctions;
	
	public class AppWarpController extends App42Controller
	{
		//APPWARP
		public var udpEnabled:Boolean;
		
		
		// AppWarp String Constants                
		public var currentRoomID:String = "464676121";
		public var isHost:Boolean = false;
		public var userID:String = "";
		private var listener:AppWarpListener;
		
		public function AppWarpController(owner:StarlingState = null)
		{
			super(owner);
			trace("initialize AppWarpController");
			listener = new AppWarpListener(this);
			
			WarpClient.getInstance().setConnectionRequestListener(listener);
			WarpClient.getInstance().setRoomRequestListener(listener);
			WarpClient.getInstance().setNotificationListener(listener);
			WarpClient.getInstance().setZoneRequestListener(listener);
			WarpClient.getInstance().setLobbyRequestListener(listener);
			
			WarpClient.enableTrace(true);
		}		
		

		
		public function connectedToServerSuccessfully():void{ 
			// TO DO WHEN CONNECTED TO APPWARP SERVER
			this.dispatchEventWith(GameEvent.CONNECTED_TO_SERVER);
		}
		
		public function setUserDataObject(id:String,data:String):void{
			WarpClient.getInstance().setCustomUserData(id,data);
		}
		
		//start Room

		public function stopRoom():void{
			this.updateRoomProperties(currentRoomID,{start:false,stopTime:(new Date()).getTime()});
		}		
		
		public function startRoom(properties:Object = null):void{
			if(properties == null){
				properties = new Object();
			}
			
			properties.start = true;
			properties.startTime = (new Date()).getTime();
			trace("START ROOM: "+currentRoomID);
			this.updateRoomProperties(currentRoomID,properties);
		}
		
		public function startPlay():void{
			owner.add(localPlayer);
			trace("startPlay "+remotePlayers.length);
			for (var i:int = 0; i < remotePlayers.length; i++) 
			{
				owner.add(remotePlayers[i]); 
			}
		}
		
		//updateRoomProperties
		public function updateRoomPropertiessSuccess(room:Room, user:String, properties:Object):void{
			this.dispatchEventWith(GameEvent.ON_USER_CHANGE_ROOM_PROPERTIES,false,{room:room,user:user,properties:properties});
			trace("updateRoomPropertiessSuccess "+properties);
			if(properties.start != null){
				if(properties.start == true){
					this.dispatchEventWith(GameEvent.START_GAME,false,properties);
				}
				if(properties.start == false){
					this.dispatchEventWith(GameEvent.STOP_GAME);
				}
			}
			
		}
		
		public function updateRoomProperties(roomId:String,properties:Object,removeArray:Array = null):void{
			trace("start game updateRoomProperties "+roomId +", properties: "+ properties.level);
			WarpClient.getInstance().updateRoomProperties(roomId,properties,removeArray);
		}
		
		//createRoom
		
		public function createRoomCallback(event:Room):void{
			 trace("createRoomCallback "+event);
			 currentRoomID = event.roomId;
			 this.dispatchEventWith(GameEvent.CREATE_ROOM_DONE,false,event);
		}
		
		public function createFriendsRoom(name:String = ""):void{
			var json:Object = new Object();
			json.type = "friends";				
			isHost = true;
			WarpClient.getInstance().createRoom("friends_"+UtilFunctions.randRange(0,9999999)+name, me.id, GameFactory.MAX_USERS, json);
		}			
		
		public function createRandomRoom(name:String = ""):void{
			var json:Object = new Object();
			json.type = "random";				
			isHost = true;
			WarpClient.getInstance().createRoom("random_"+UtilFunctions.randRange(0,9999999)+name, me.id, GameFactory.MAX_USERS, json);
		}		
		
		public function createRoom(name:String, owner:String, tableProperties:Object = null, maxUsers:int = GameFactory.MAX_USERS):void{
			trace("tableProperties "+tableProperties.type);
			isHost = true;
			WarpClient.getInstance().createRoom(name, owner, maxUsers, tableProperties);
		}		
		
		//getLiveRoomInfo
		
		public function getRoomsInRange(minUsr:int = 1, maxUsr:int = GameFactory.MAX_USERS):void{
			WarpClient.getInstance().getRoomsInRange(minUsr,maxUsr);
		}			
		
		//getLiveRoomInfo
		
		public function getLiveRoomInfo(id:String):void{
			WarpClient.getInstance().getLiveRoomInfo(id);
		}				
		
		//getRoomsWithProperties
		
		public function getRoomsWithPropertiesCallback(event:MatchedRooms):void{
			trace(event);
			this.dispatchEventWith(GameEvent.GET_ROOM_DONE,false,event);
		}			
		
		public function getRoomsWithProperties(obj:Object):void{
			WarpClient.getInstance().getRoomsWithProperties(obj);
		}		
		
		//joinRoomById
		
		public function joinedRoomSuccessfully(roomId:String):void{
			// TO DO WHEN JOINED A ROOM SUCCESSFULLY
			this.dispatchEventWith(GameEvent.JOINED_ROOM_SUCCESSFULLY,false,roomId);
		}
		
		public function joinRoomById(id:String):void{
			currentRoomID = id;
			if(id == GameFactory.LOBBY){				
				WarpClient.getInstance().joinLobby();
				WarpClient.getInstance().subscribeLobby();
			}else{
				WarpClient.getInstance().joinRoom(currentRoomID);
				WarpClient.getInstance().subscribeRoom(currentRoomID);
			}
		}
		
		//leaveLobby
		
		public function leaveLobby():void{
			if(WarpClient.getInstance().getConnectionState() == ConnectionState.connected)
			{
				WarpClient.getInstance().leaveLobby();
				WarpClient.getInstance().unsubscribeLobby();
			}					
		}
		
		public function leaveRoom(roomId:String):void{
			if(WarpClient.getInstance().getConnectionState() == ConnectionState.connected)
			{
				WarpClient.getInstance().leaveRoom(roomId);
				WarpClient.getInstance().unsubscribeRoom(roomId);
			}			
		}
		
		//connect
		
		public function connect(userIdParam:String):void{
			
			if(WarpClient.getInstance().getConnectionState() == ConnectionState.disconnected)
			{
				WarpClient.getInstance().connect(userIdParam);
				userID = userIdParam;
			}else{
				this.dispatchEventWith(GameEvent.RECONNECTED_TO_SERVER);
			}
		}
		
		public function sendMessage(message:String):void
		{
			var msgBytes:ByteArray = new ByteArray();
			msgBytes.writeUTF(message);
			if(udpEnabled){
				WarpClient.getInstance().sendUdpUpdate(msgBytes);
			}
			else{
				WarpClient.getInstance().sendUpdate(msgBytes);
			}
		}
		
	}
}

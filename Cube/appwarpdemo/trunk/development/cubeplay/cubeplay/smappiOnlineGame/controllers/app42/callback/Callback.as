package cubeplay.smappiOnlineGame.controllers.app42.callback
{
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	
	public class Callback
	{
		private var _owner:App42Controller;
		public function Callback(owner:App42Controller)
		{
			_owner = owner;
		}

		public function get owner():App42Controller
		{
			return _owner;
		}

		public function set owner(value:App42Controller):void
		{
			_owner = value;
		}

	}
}
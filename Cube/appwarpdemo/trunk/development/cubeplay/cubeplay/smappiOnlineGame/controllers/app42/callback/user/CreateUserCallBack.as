package cubeplay.smappiOnlineGame.controllers.app42.callback.user
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.user.User;
	
	import flash.utils.getQualifiedClassName;
	
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.Callback;
	
	public class CreateUserCallBack extends Callback implements App42CallBack
	{
		public function CreateUserCallBack(owner:App42Controller)
		{
			super(owner);
		}
		
		public function onSuccess(object:Object):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onSuccess "+object);
			var user:User = User(object);   
			var userId:String = user.getUserName();
		}
		
		public function onException(exception:App42Exception):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onException "+exception);
		}
	}
}
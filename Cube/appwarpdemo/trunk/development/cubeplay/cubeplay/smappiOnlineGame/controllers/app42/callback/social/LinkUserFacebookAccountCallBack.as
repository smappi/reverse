package cubeplay.smappiOnlineGame.controllers.app42.callback.social
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.social.Social;
	
	import flash.utils.getQualifiedClassName;
	
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.Callback;
	
	public class LinkUserFacebookAccountCallBack extends Callback implements App42CallBack
	{
		public function LinkUserFacebookAccountCallBack(owner:App42Controller)
		{
			super(owner);
		}
		
		public function onSuccess(object:Object):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onSuccess "+object);
			var social:Social = Social(object);  
			trace("userName is " + social.getUserName());  
			trace("friendList is " + social.getFriendList());
			trace("fb Access Token is " + social.getFacebookAccessToken());  			
		}
		
		public function onException(exception:App42Exception):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onException "+exception);
		}
	}
}
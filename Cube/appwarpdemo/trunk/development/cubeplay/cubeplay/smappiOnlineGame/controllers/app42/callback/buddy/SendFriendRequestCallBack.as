package cubeplay.smappiOnlineGame.controllers.app42.callback.buddy
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.buddy.Buddy;
	
	import flash.utils.getQualifiedClassName;
	
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	import cubeplay.smappiOnlineGame.controllers.app42.callback.Callback;
	
	public class SendFriendRequestCallBack extends Callback implements App42CallBack
	{
		public function SendFriendRequestCallBack(owner:App42Controller)
		{
			super(owner);
		}
		
		public function onSuccess(object:Object):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onSuccess "+object);
			var buddy:Buddy = Buddy(object);   
			var userId:String = buddy.getUserName();
			var buddyId:String = buddy.getBuddyName();
		}
		
		public function onException(exception:App42Exception):void
		{
			trace(flash.utils.getQualifiedClassName(this)+ " onException "+exception);
		}
	}
}
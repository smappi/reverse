package cubeplay.smappiOnlineGame.players
{
	public class RemotePlayer extends Player
	{
		public function RemotePlayer(name:String, params:Object=null,customParams:Object = null)
		{
			super(name, params, customParams);
		}
		
		override public function setView(params:Object = null):void
		{
			view = new PlayerView(false);
		}
		
	}
}
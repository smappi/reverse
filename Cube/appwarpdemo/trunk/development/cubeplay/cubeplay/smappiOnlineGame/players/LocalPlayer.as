package cubeplay.smappiOnlineGame.players
{
	public class LocalPlayer extends Player
	{	
		public static var USER_ID2:String = "";
		public function LocalPlayer(name:String, params:Object=null,customParams:Object = null)
		{
			super(name, params, customParams);
		}
		
		override public function setView(params:Object = null):void
		{
			view = new PlayerView(false);
		}
		
	}
}
package cubeplay.smappiOnlineGame.players
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.DynamicObject;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	
	public class Player extends DynamicObject //useful for detecting collision using citrus solver
	{
		
		private var _score:int = 0;
		private var _id:String;
		private var _fullname:String;
		private var _url:String;
		
		public function Player(name:String, params:Object,customParam:Object)
		{
			super(name, params);
			if(customParam != null) {
				if(customParam.id != null) id = customParam.id;
				if(customParam.fullname != null) fullname = customParam.fullname;
				if(customParam.url != null) url = customParam.url;
			}
			setView(params);
		}
		
		public function get url():String
		{
			return _url;
		}

		public function set url(value:String):void
		{
			_url = value;
		}

		public function get fullname():String
		{
			return _fullname;
		}

		public function set fullname(value:String):void
		{
			_fullname = value;
		}

		public function setView(params:Object = null):void{
			view = new GameFactory.PLAYVIEW_CLASS(params.width,params.height);
		}
			
		
		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}
		
		public function moveTo(point:flash.geom.Point):void{
			trace("moveTo "+point);
			TweenMax.to(this,0.5,{x:point.x,y:point.y});
		}
		
		public function increaseScore():void{
			score++;
			trace("score "+score);
			if(view is PlayerView){
				(view as PlayerView).dispatchEventWith(GameEvent.INCREASE_SCORE);
			}
		}
		
		public function decreaseScore():void{
			score--;
			if(view is PlayerView){
				(view as PlayerView).dispatchEventWith(GameEvent.DECREASE_SCORE);
			}			
		}
		
		public function beKilled():void{
			if(view is PlayerView){
				(view as PlayerView).dispatchEventWith(GameEvent.KILLED);
			}	
		}	
		
		public function get score():Number
		{
			return _score;
		}

		public function set score(value:Number):void
		{
			if(view is PlayerView){
				(view as PlayerView).score = value;
			}			
			_score = value;
		}

	}
}
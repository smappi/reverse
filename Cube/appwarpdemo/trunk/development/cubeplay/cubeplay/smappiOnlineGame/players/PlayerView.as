package cubeplay.smappiOnlineGame.players
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class PlayerView extends Sprite
	{

		private var _score:int;

		private var txtField:TextField;
		public function PlayerView(isRemotePlayer:Boolean = false)
		{
			super();
			score = 0;
			if(isRemotePlayer){
				txtField = new TextField(100,100,score+"","Verdana",50,0xf1c40f);				
			}else{
				txtField = new TextField(100,100,score+"","Verdana",50,0x2ecc71);
			}
			addChild(txtField);
		}
		
		public function get score():int
		{
			return _score;
		}

		public function set score(value:int):void
		{
			_score = value;
			if(txtField != null) txtField.text = score+"";
		}

		public function increaseScore():void{
			score++;
		}
		
		public function decreaseScore():void{
			score++;
		}		
	}
}
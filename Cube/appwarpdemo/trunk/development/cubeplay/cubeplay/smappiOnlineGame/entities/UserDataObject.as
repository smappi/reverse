package cubeplay.smappiOnlineGame.entities
{
	public class UserDataObject extends Object
	{
		public var id:String;
		public var fullname:String;
		public var score:int;
		public var x:int;
		public var y:int;
		public var url:String;
		public var width:int;
		public var height:int;
		public function UserDataObject()
		{
		}
	}
}
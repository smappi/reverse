package cubeplay.smappiOnlineGame.entities
{
	public class ResultInformation
	{
		private var _score:int;
		public function ResultInformation(scoreParam:int)
		{
			score = scoreParam;
		}

		public function get score():int
		{
			return _score;
		}

		public function set score(value:int):void
		{
			_score = value;
		}

	}
}
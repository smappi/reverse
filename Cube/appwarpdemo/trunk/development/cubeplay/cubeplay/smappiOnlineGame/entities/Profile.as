package cubeplay.smappiOnlineGame.entities
{
	public class Profile
	{
		public var fullname:String;
		public var id:String;
		public var accessToken:String;
		public var firstName:String;
		public var lastName:String;
		public var sex:String;
		public var isGuest:Boolean = false;
		public var url:String;
		public function Profile(facebookJson:String = "",accessTokenParam:String = "",fromFacebook:Boolean = false)
		{
			super();
			if(facebookJson != ""){
				var profileJson:Object = JSON.parse(facebookJson);
				
				firstName = profileJson.first_name;
				lastName = profileJson.last_name;
				sex = profileJson.gender;
				if(fromFacebook)
					fullname = profileJson.name;
				else 
					fullname = profileJson.fullname;
				id = profileJson.id;
				isGuest = profileJson.isGuest;
				
				if(fromFacebook)
					url = profileJson.picture.data.url;
				else
					url = profileJson.url;
				accessToken = accessTokenParam;
			}
		}
		
		
	}
}
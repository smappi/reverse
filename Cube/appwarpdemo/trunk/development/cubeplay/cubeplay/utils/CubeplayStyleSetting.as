package cubeplay.utils
{
	import feathers.layout.VerticalLayout;

	public class CubeplayStyleSetting
	{

		
		public function CubeplayStyleSetting()
		{
			
		}
		
		public static function verticalLayout(paddingTop:int = 0,gap:int = 50,align:String = VerticalLayout.VERTICAL_ALIGN_MIDDLE):VerticalLayout{
			const verticalLayout:VerticalLayout = new VerticalLayout();
			verticalLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			verticalLayout.verticalAlign = align;
//			verticalLayout.padding = Starling.current.stage.stageHeight*0.1;
			verticalLayout.gap = gap;
			return verticalLayout;
		}
	}
}
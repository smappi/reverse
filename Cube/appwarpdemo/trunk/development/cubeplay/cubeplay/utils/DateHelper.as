package cubeplay.utils
{
	public class DateHelper
	{
		static protected const msS:Number = 1000;
		static protected const msM:Number = 60000;
		static protected const msH:Number = 3600000;
		static protected const msD:Number = 86400000;
		public function DateHelper()
		{
		}
		
		static public function addDate(dt:Date, d:Number, h:Number=0, m:Number=0, s:Number=0):Date
		{
			var v:Number = dt.valueOf() + d * msD + h * msH + m * msM + s * msS;
			return new Date(v);
		}
		
		static public function subtractDate(dt:Date, d:Number, h:Number=0, m:Number=0, s:Number=0):Date
		{
			var v:Number = dt.valueOf() - d * msD - h * msH - m * msM - s * msS;
			return new Date(v);
		}
		
		static public function difDate(dA:Date, dB:Date):Object
		{
			var n:Number = Math.abs(dA.valueOf() - dB.valueOf());
			var ret:Object = new Object();
			ret.day = Math.floor(n/msD);
			var n0:Number = n % msD;
			ret.hour = Math.floor(n0/msH);
			var n1:Number = n0 % msH;
			ret.minute = Math.floor(n1/msM);
			var n2:Number = n1 % msM;
			ret.second = n2 / msS;
			return ret;
		}
		
		static public function difEnglish(dA:Date, dB:Date):String
		{
			var o:Object = difDate(dA, dB);
			var str:String = o.day.toString() + "d "
				+ o.hour.toString() + "h "
				+ o.minute.toString() + "m ago";
			if (o.day == 0) 
			{
				
				str =  o.hour.toString() + "h "
					+ o.minute.toString() + "m ago";
				if (o.hour == 0) 
				{
					str = o.minute.toString() + "m ago";
					if (o.minute == 0) 
					{
						str = "just now";
					}
				}
			}
			return str;
		}
		
		static public function difDay(dA:Date, dB:Date):Number
		{
			return (dA.valueOf() - dB.valueOf())/msD;
		}
		
		static public function difHour(dA:Date, dB:Date):Number
		{
			return (dA.valueOf() - dB.valueOf())/msH;
		}
		
		static public function difMinute(dA:Date, dB:Date):Number
		{
			return (dA.valueOf() - dB.valueOf())/msM;
		}
		
		static public function difSecond(dA:Date, dB:Date):Number
		{
			return (dA.valueOf() - dB.valueOf())/msS;
		}
	}
}
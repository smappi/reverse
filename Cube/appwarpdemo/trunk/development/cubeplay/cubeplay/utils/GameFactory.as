package cubeplay.utils
{
	import com.shephertz.appwarp.messages.MatchedRooms;
	import com.shephertz.appwarp.messages.Room;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.basicscreens.CPInviteScreen;
	import cubeplay.basicscreens.CPNotificationsScreen;
	import cubeplay.smappiOnlineGame.controllers.RaceController;
	import cubeplay.smappiOnlineGame.controllers.SurviveController;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;

	public class GameFactory
	{
		public static var MODE:String;
		public static var PLAYER_WITH:String;		
		
		public static const SMAPPI:String = "SMAPPI";
		
		public static const ORDER_BY_DESCENDING:String = "orderByDescending";
		public static const ORDER_BY_ASCENDING:String = "orderByAscending";
		
		public static const MAX_USERS:int = 5;
		public static const LOBBY:String = "lobby";
		
		public static const RACE:String = "race";
		public static const SURVIVAL:String = "survival";		
		
		public static const PRACTICE:String = "practice";
		public static const RANDOM:String = "random";
		public static const FRIENDS:String = "friends";
		public static const VIEW_NOTIFICATIONS:String = "VIEW_NOTIFICATIONS";
		
		public static const LASTEST_PLAYERS:String = "LASTEST_PLAYERS";
		public static const REQUESTS:String = "REQUESTS";		
		
		public static const USE_UDP:Boolean = false;
		
		public static var FACEBOOK_APP_ID:String = "939566379391048";
		
		
		public static var PRACTICE_SCREEN_CLASS:Class;
		public static var FRIENDS_SCREEN_CLASS:Class;
		public static var RANDOM_SCREEN_CLASS:Class;
		
		public static var LOCAL_PLAYER_CLASS:Class;
		public static var REMOTE_PLAYER_CLASS:Class;
		
		public static var PLAYVIEW_CLASS:Class;
		
		public function GameFactory()
		{
			
		}
		
		
		public static function doFriendsMatch(gameController:AppWarpController,roomId:String):void{

			if(roomId){
				gameController.joinRoomById(roomId);
			}else{
//				var json:Object = new Object();
//				json.type = "friends";				
//				gameController.createRoom("friends_"+UtilsAndConfig.randRange(0,9999999),gameController.me.id,json);
				gameController.createFriendsRoom();
			}
		}
		
		public static function doRandomMatch(gameController:AppWarpController,data:MatchedRooms):void{
			var json:Object = new Object();
			json.type = "random";	
			if(data.rooms.length == 0){												
//				gameController.createRoom("random_"+UtilsAndConfig.randRange(0,9999999),gameController.me.id,json);	
				gameController.createRandomRoom();
			}else{
				
				var foundRandomRoom:Boolean = false;
				for (var i:int = 0; i < data.rooms.length; i++) 
				{
					if((data.rooms[i] as Room).name.indexOf("random_") > -1){
						gameController.joinRoomById((data.rooms[i] as Room).roomId);
						foundRandomRoom = true;
						break;
					}
					trace("room "+i+": "+(data.rooms[i] as Room).name);
				}		
				if(!foundRandomRoom){				
//					gameController.createRoom("random_"+UtilsAndConfig.randRange(0,9999999),gameController.me.id,json);	
					gameController.createRandomRoom();
				}
			}
		}
		
		public static function playWithFactory():StarlingState{
			switch(PLAYER_WITH)
			{
				case PRACTICE:
				{
					return new PRACTICE_SCREEN_CLASS;
					break;
				}
				case RANDOM:
				{
					return new RANDOM_SCREEN_CLASS;
					break;
				}			
				case FRIENDS:
				{
					return new CPInviteScreen();
					break;
				}	
				case VIEW_NOTIFICATIONS:
				{
					return new CPNotificationsScreen();
					break;
				}						
				default:
				{
					return new PRACTICE_SCREEN_CLASS;
					break;
				}
			}
		}
		
		public static function controllerFactory(owner:StarlingState):AppWarpController{
			switch(MODE)
			{
				case RACE:
				{
					return new RaceController(owner);
					break;
				}
				case SURVIVAL:
				{
					return new SurviveController(owner);
					break;
				}					
				default:
				{
					return new RaceController(owner);
					break;
				}
			}
		}
	}
}
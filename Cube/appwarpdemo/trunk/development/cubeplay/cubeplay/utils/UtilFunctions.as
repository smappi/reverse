package cubeplay.utils
{
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.display.StageOrientation;
	import flash.system.Capabilities;
	import flash.utils.Dictionary;
	
	import starling.core.Starling;
	import starling.utils.AssetManager;

	public class UtilFunctions
	{
		public static var defaultAssets:AssetManager;
		
		//these use for register pushnotification
		public static const APP_ID:String = "babylearnshape";
		public static const GOOGLE_PROJECT_ID:String = "hale-yew-454";
		public static const MAIN_FONT:String = "Helvetica";
		public static const BASE_URL:String = "http://buzzkid.vn/services";		
		
		public static function isOrientationLandscape():Boolean {
			if (Starling.current.stage.stageWidth > Starling.current.stage.stageHeight) return true;
			return false;
		}
		
		/**
		 * detect current running device is iOS (true) or android (false) 
		 * @return 
		 * 
		 */		
		public static function is_IOS():Boolean
		{
			if(Capabilities.version.toLowerCase().indexOf("ios") > -1)
			{
				return true;
			}
			else if(Capabilities.version.toLowerCase().indexOf("and") > -1)
			{
				return false;
			}
			
			return true;
		}
		
		/**
		 * this function will return random value between min and max parametor 
		 * @param minNum
		 * @param maxNum
		 * @return 
		 * 
		 */		
		public static function randRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		public static function countKeys(myDictionary:Dictionary):int 
		{
			var n:int = 0;
			for (var key:* in myDictionary) {
				n++;
			}
			return n;
		}		
		
		public static function in_array( needle:*, haystack:Array ):Boolean{
			// Search variable needle in array haystack
			var itemIndex:int = haystack.indexOf( needle );
			return ( itemIndex < 0 ) ? false : true;
		}	
		
		public static function in_array_byid( id:String, arr:Array ):Boolean{
			// Search variable needle in array haystack
			for (var i:int = 0; i < arr.length; i++) 
			{
				if(id == arr[i].id) return true;
			}
			
			return false;
		}	
		public static function in_array_bytoId( id:String, arr:Array ):Boolean{
			// Search variable needle in array haystack
			for (var i:int = 0; i < arr.length; i++) 
			{
				if(id == arr[i].toId) return true;
			}
			
			return false;
		}		
	}
}
package cubeplay.basicscreens
{
	import citrus.core.starling.StarlingState;
	
	import feathers.controls.Button;
	import feathers.controls.List;
	import feathers.controls.Panel;
	
	import cubeplay.utils.GameFactory;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	import cubeplay.utils.CubeplayStyleSetting;
	
	public class CPModeSelectionScreen extends StarlingState
	{
		
		private var screen:Panel;
		
		public function CPModeSelectionScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			
			screen = new Panel();			
			screen.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.width = Starling.current.stage.stageWidth;
			screen.height = Starling.current.stage.stageHeight;
			screen.layout = CubeplayStyleSetting.verticalLayout();
			this.addChild(screen);
			
			var button:Button = new Button();
			button.label = "Race";
			button.addEventListener(Event.TRIGGERED,buttonHandler);
			screen.addChild( button );
			
			var button2:Button = new Button();
			button2.label = "Survival";
			button2.addEventListener(Event.TRIGGERED,button2Handler);
			screen.addChild( button2 );
				
		}
		
		private function buttonHandler():void
		{
			GameFactory.MODE = GameFactory.RACE;
			_ce.state = GameFactory.playWithFactory();
		}		
		
		private function button2Handler():void
		{
			GameFactory.MODE = GameFactory.SURVIVAL;
			_ce.state = GameFactory.playWithFactory();
		}			
		
	}
}
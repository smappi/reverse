package cubeplay.basicscreens
{
	import citrus.core.starling.StarlingState;
	
	import feathers.controls.Button;
	
	import starling.events.Event;
	import starling.text.TextField;
	
	public class CPResultScreen extends StarlingState
	{

		private var _score:int;
		public function CPResultScreen(scoreParam:int)
		{
			super();
			score = scoreParam;
		}
		
		public function get score():int
		{
			return _score;
		}

		public function set score(value:int):void
		{
			_score = value;
		}

		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();

			var txtCongra:TextField = new TextField(200,300,"Your score: "+score,"Verdana",50,0xffffff,true);
			addChild(txtCongra);
			
			var buttonRelease:Button = new Button();
			buttonRelease.label = "PLAY AGAIN";
			buttonRelease.addEventListener(Event.TRIGGERED,playAgainHandler);
			addChild(buttonRelease);
			buttonRelease.x = 0;
			buttonRelease.y = stage.stageHeight - 50;
		}
		
		private function playAgainHandler():void
		{
			_ce.state = new CPHomeScreen();
		}
		
	}
}
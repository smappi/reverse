package cubeplay.basicscreens
{
	import flash.utils.Dictionary;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.elements.listRenderer.NotificationsPanel;
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.controllers.SmappiController;
	import cubeplay.utils.GameFactory;
	
	import feathers.controls.List;
	import feathers.controls.Panel;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	public class CPNotificationsScreen extends StarlingState
	{
		
		private var screen:Panel;
		
		public function CPNotificationsScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			if(SmappiController.isLoggedIn()){
				trace("!isLoggedIn");
				_ce.state = new CPLoginScreen();				
			}
			else initSprite();
		}
		
		private function initSprite():void
		{			
			var screen:NotificationsPanel = new NotificationsPanel();
			
			screen.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.width = Starling.current.stage.stageWidth;
			screen.height = Starling.current.stage.stageHeight;
			screen.addEventListener(GameEvent.BACK,backHandler);
			screen.addEventListener(GameEvent.INVITE_COMPLETE,inviteCompleteHandler);
			screen.addEventListener(GameEvent.ON_JOIN_ROOM_FROM_NOTIFICATION,onJoinRoomHandler);
			addChild(screen);
		}
		
		private function onJoinRoomHandler(e:Event,data:String):void
		{
			trace("onJoinRoomHandler "+data);
			_ce.state = new GameFactory.FRIENDS_SCREEN_CLASS(data);
		}
		
		private function backHandler():void
		{
			_ce.state = new CPHomeScreen();
		}
		
		private function inviteCompleteHandler(e:Event,data:Dictionary):void
		{
			trace("inviteCompleteHandler "+data);
			_ce.state = new GameFactory.FRIENDS_SCREEN_CLASS("",data);
		}				
		
		private function findDocSuccessHandler():void
		{
			// TODO Auto Generated method stub
			
		}
	}
}
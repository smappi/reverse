package cubeplay.basicscreens
{
	import com.shephertz.appwarp.messages.MatchedRooms;
	import com.shephertz.appwarp.messages.Room;
	
	import flash.geom.Point;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomPanel;
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.controllers.SmappiController;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;
	import cubeplay.smappiOnlineGame.entities.ResultInformation;
	import cubeplay.smappiOnlineGame.entities.UserDataObject;
	import cubeplay.smappiOnlineGame.players.Player;
	import cubeplay.smappiOnlineGame.players.RemotePlayer;
	import cubeplay.utils.GameFactory;
	
	import feathers.controls.Button;
	
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class CPPlayScreen extends StarlingState 
	{
		
		public var localPlayer:Player;
		
		public var gameController:AppWarpController;
		
		public var waitingRoomPanel:*;
		
		public var userData:UserDataObject;
		
		public var roomId:String;

		private var jsonLatestPlayersStr:String;
		
		public function CPPlayScreen(roomIdParam:String = "")
		{
			roomId = roomIdParam;
		}
		
		private function main():void{
			
			_initEvent();
		}
		
		public function initWaitingRoom():void{
			waitingRoomPanel = new WaitingRoomPanel();
			addChild(waitingRoomPanel);
		}
		
		
		
		public function initSprite(data:Object = null):void
		{
			var button:Button = new Button();
			button.label = "Leave";
			button.y = 500;
			button.addEventListener(Event.TRIGGERED,buttonHandler);
			addChild( button );				
		}			
		
		protected function _initEvent():void{
			
			gameController = GameFactory.controllerFactory(this);
			
			
			gameController.addEventListener(GameEvent.CONNECTED_TO_SERVER,connectedToServerHandler);
			gameController.addEventListener(GameEvent.RECONNECTED_TO_SERVER,reconnectedToServerHandler);
			gameController.addEventListener(GameEvent.JOINED_ROOM_SUCCESSFULLY,joinedRoomSuccessfullyHandler);
			gameController.addEventListener(GameEvent.FINISH,gameFinishHandler);
			gameController.addEventListener(GameEvent.KILLED,gameFinishHandler);
			gameController.addEventListener(GameEvent.GET_ROOM_DONE,getRoomsDoneHandler);
			gameController.addEventListener(GameEvent.CREATE_ROOM_DONE,createRoomDoneHandler);
			gameController.addEventListener(GameEvent.NEW_REMOTE_PLAYER_JOINED_ROOM,newRemotePlayerJoinedRoomHandler);
			gameController.addEventListener(GameEvent.ON_USER_CHANGE_ROOM_PROPERTIES,onUserChangeRoomPropertiesHandler);
			gameController.addEventListener(GameEvent.START_GAME,startGameHandler);
			gameController.addEventListener(GameEvent.STOP_GAME,stopGameHandler);
			
			gameController.me = SmappiController.getMyProfile();
			gameController.connect(gameController.me.id);
			
			initWaitingRoom();
			//			gameController.initCloud();
			//			gameController.saveUserScore("TrueMode","1018006968210684",245);
		}
		
		public function addRemotePlayer(player:Player):void{
			add(player);
		}
		
		public function stopGameHandler():void
		{
			// TODO Auto Generated method stub
			gameController.leaveRoom(gameController.currentRoomID);
			_ce.state = new CPResultScreen(gameController.localPlayer.score);
		}
		
		public function startGameHandler(e:Event,data:Object):void
		{
			trace("startGameHandler");
			waitingRoomPanel.visible = false;
			initSprite();
			//			gameController.startPlay();
			stage.addEventListener(TouchEvent.TOUCH,touchHandler);
		}
		
		public function newRemotePlayerJoinedRoomHandler(event:Event,data:Object):void
		{
			trace("newRemotePlayerJoinedRoomHandler "+data);
			
			var id:String = data.id;
			gameController.addEventListener(GameEvent.DELETE_DOCUMENT_BY_KEY_VALUE_SUCCESS,deleteDocSuccessHandler);
			if(gameController.me.id != id){
				var fullname:String = data.fullname;
				var pictureUrl:String = data.url;
				var fromto:String = gameController.me.id+"_"+id;
				jsonLatestPlayersStr = '{"fromto":"'+fromto+'","picture":{"data":{"url":"'+pictureUrl+'"}},"id":"'+id+'","name":"'+fullname+'","ownerid":"'+gameController.me.id+'","date":"'+(new Date()).getTime()+'"}';
				trace("jsonStr = "+jsonLatestPlayersStr);
//				gameController.deleteDocumentsByKeyValue("fromto",fromto,GameFactory.LASTEST_PLAYERS,GameFactory.SMAPPI);
				gameController.insertJSONDocument(jsonLatestPlayersStr,GameFactory.LASTEST_PLAYERS,GameFactory.SMAPPI);
			}
			
			waitingRoomPanel.addPlayer(data);	
			this.startRoom();
		}	
		
		public function startRoom():void{
			if(gameController.isHost) {
				trace("gameController.startRoom();");
				gameController.startRoom();
			}
		}
		
		private function deleteDocSuccessHandler():void
		{
			gameController.insertJSONDocument(jsonLatestPlayersStr,GameFactory.LASTEST_PLAYERS,GameFactory.SMAPPI);
		}
		
		public function onUserChangeRoomPropertiesHandler(e:Event,data:Object):void
		{
			trace("onUserChangeRoomPropertiesHandler "+data.room+" "+data.user+" "+data.properties.start);
		}
		
		public function createRoomDoneHandler(event:Event,data:Room):void
		{
			trace("createRoomDoneHandler "+data.owner);
			
			gameController.joinRoomById(data.roomId);			
		}
		
		public function getRoomsDoneHandler(event:Event,data:MatchedRooms):void
		{
			trace("getRoomDoneHandler "+data.rooms.length);
			GameFactory.doRandomMatch(gameController,data);			
		}
		
		public function reconnectedToServerHandler():void
		{
			trace("reconnectedToServerHandler");
			userData = new UserDataObject();	
			userData.id = gameController.me.id;
			userData.fullname = gameController.me.fullname;
			userData.url = gameController.me.url;
			var msg:String = JSON.stringify(userData);					 
			trace(msg);
			gameController.setUserDataObject(gameController.me.id,msg);				
		}
		
		public function connectedToServerHandler():void
		{
			trace("connectedToServerHandler");
			userData = new UserDataObject();	
			userData.id = gameController.me.id;
			userData.fullname = gameController.me.fullname;
			userData.url = gameController.me.url;
			var msg:String = JSON.stringify(userData);					 
			trace(msg);
			gameController.setUserDataObject(gameController.me.id,msg);
		}		
		
		public function joinedRoomSuccessfullyHandler(e:Event,roomIdParam:String):void
		{	
			roomId = roomIdParam;
			trace("joinedRoomSuccessfullyHandler "+roomId);
			waitingRoomPanel.addPlayer(userData);
			localPlayer = gameController.addPlayer(new GameFactory.LOCAL_PLAYER_CLASS("localplayer",{id:gameController.me.id},userData));
		}		
		
		public function buttonHandler():void
		{
			gameController.leaveRoom(gameController.currentRoomID);
			//			gameController.leaveLobby();
			_ce.state = new CPHomeScreen();
		}
		
		public function gameFinishHandler(e:Event,data:ResultInformation):void
		{
			trace(data.score);
			gameController.stopRoom();
		}
		
		public function touchHandler(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch){
				if(touch.phase == TouchPhase.ENDED){
					localPlayer.increaseScore();	
					localPlayer.moveTo(new flash.geom.Point(localPlayer.x+20,localPlayer.y));
					
					
					var userData:UserDataObject = new UserDataObject();	
					userData.id = localPlayer.id;
					userData.x = localPlayer.x+20;
					userData.y = localPlayer.y;
					userData.score = localPlayer.score;
					userData.fullname = gameController.me.fullname;
					
					var msg:String = JSON.stringify(userData);					
					trace(msg);
					gameController.sendMessage(msg);
					gameController.setUserDataObject(localPlayer.id,msg);
				}
			}
		}
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
			
		}		
		
		override public function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			if(SmappiController.isLoggedIn()){
				trace("!isLoggedIn");
				_ce.state = new CPLoginScreen();				
			}
			else main();
			
		}				
		
		
	}
}
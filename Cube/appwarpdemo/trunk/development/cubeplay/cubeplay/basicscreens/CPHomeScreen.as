package cubeplay.basicscreens
{
	import citrus.core.starling.StarlingState;
	
	import cubeplay.elements.HorizontalScrollContainer;
	import cubeplay.elements.VerticalScrollContainer;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.utils.CubeplayStyle;
	import cubeplay.utils.GameFactory;
	import cubeplay.utils.UtilFunctions;
	
	import feathers.controls.Button;
	import feathers.controls.ScrollContainer;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class CPHomeScreen extends StarlingState
	{
		
		private var screen:ScrollContainer;
		
		private var buttonContainer:ScrollContainer;
		
		private var twoButtonContainer:ScrollContainer;
		
		public function CPHomeScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			
			
			//LOGO IMAGE
			var logo:Image = new Image(CubeplayStyle.cubeplayAtlas.getTexture("logoCubePlay"));
			
			
			//THREE MIDDLE BUTTON
			var button:Button = new Button();
			button.nameList.add(CubeplayStyle.BUTTON_SINGLE_PLAY);
			button.addEventListener(Event.TRIGGERED,practiceHandler);
			
			
			var button3:Button = new Button();
			button3.nameList.add(CubeplayStyle.BUTTON_PLAY_WITH_FRIENDS);
			
			button3.addEventListener(Event.TRIGGERED,friendsMatchHandler);
			
			var button2:Button = new Button();
			button2.nameList.add(CubeplayStyle.BUTTON_RANDOM_MATCHES);
			button2.addEventListener(Event.TRIGGERED,ramdomMatchHandler);
			
			
			//TWO SMALL BUTTON
			var button4:Button = new Button();
			button4.nameList.add(CubeplayStyle.BUTTON_NOTIFICATIONS);
			button4.addEventListener(Event.TRIGGERED,requestsListHandler);
			
			
			var button5:Button = new Button();
			button5.nameList.add(CubeplayStyle.BUTTON_LEADER_BOARD);
//			button5.addEventListener(Event.TRIGGERED,requestsListHandler);
			
			if(!UtilFunctions.isOrientationLandscape()){
				screen = new VerticalScrollContainer();
				screen.nameList.add(CubeplayStyle.HOME_SCREEN);			
				
				
				buttonContainer = new VerticalScrollContainer();
				buttonContainer.height = 161*3 + 100;
				
				
				twoButtonContainer = new HorizontalScrollContainer();
				twoButtonContainer.height = 161;
				
				screen.addChild(logo);
			}else{
				screen = new HorizontalScrollContainer();
				screen.nameList.add(CubeplayStyle.HOME_SCREEN);			
				
				
				buttonContainer = new VerticalScrollContainer();
				buttonContainer.width = 708+50;
				buttonContainer.addChild(logo);
				
				twoButtonContainer = new VerticalScrollContainer();
				twoButtonContainer.width = 161;
			}
			
			this.addChild(screen);
			
			screen.addChild(buttonContainer);
			screen.addChild(twoButtonContainer);
			
			buttonContainer.addChild( button );
			buttonContainer.addChild( button3 );	
			buttonContainer.addChild( button2 );	
			twoButtonContainer.addChild( button4 );	 
			twoButtonContainer.addChild( button5 );				

			
		}		
		
		private function requestsListHandler():void
		{
			GameFactory.PLAYER_WITH = GameFactory.VIEW_NOTIFICATIONS;
			if((_ce.gameData as MyGameData).jsonProfile == null){
				trace("!isLoggedIn");
				_ce.state = new CPLoginScreen();				
			}else{
				_ce.state = new CPNotificationsScreen();
				
			}
		}
		
		private function friendsMatchHandler():void
		{
			GameFactory.PLAYER_WITH = GameFactory.FRIENDS;
			GameFactory.MODE = GameFactory.RACE;
			_ce.state = GameFactory.playWithFactory();
		}
		
		private function practiceHandler():void
		{
			GameFactory.PLAYER_WITH = GameFactory.PRACTICE;
			GameFactory.MODE = GameFactory.RACE;
			_ce.state = GameFactory.playWithFactory();
			//			_ce.state = new ModeSelectionScreen();
		}		
		
		private function ramdomMatchHandler():void
		{
			GameFactory.PLAYER_WITH = GameFactory.RANDOM;
			GameFactory.MODE = GameFactory.RACE;
			_ce.state = GameFactory.playWithFactory();
			//			_ce.state = new ModeSelectionScreen();
		}			
		
	}
}
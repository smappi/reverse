package cubeplay.basicscreens
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.core.starling.StarlingState;
	
	import feathers.controls.Button;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;
	import cubeplay.smappiOnlineGame.entities.ResultInformation;
	import cubeplay.smappiOnlineGame.players.LocalPlayer;
	import cubeplay.smappiOnlineGame.players.Player;
	
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class CPPracticeScreen extends StarlingState 
	{
		private static var _instance:CPPracticeScreen;

		private var localPlayer:Player;

		private var gameController:AppWarpController;
		
		
		public function CPPracticeScreen()
		{
			super();
			_instance = this;
		}
		
		public static function getInstance():CPPracticeScreen{
			if(_instance == null){
				_instance = new CPPracticeScreen();
			}
			return _instance;
		}
		
		public function initSprite():void
		{		
			var button:Button = new Button();
			button.label = "Leave";
			button.y = 500;
			button.addEventListener(Event.TRIGGERED,buttonHandler);
			addChild( button );			
			
			gameController = GameFactory.controllerFactory(this);			
			gameController.addEventListener(GameEvent.FINISH,gameFinishHandler);
			gameController.addEventListener(GameEvent.KILLED,gameFinishHandler);
			
			localPlayer = gameController.addPlayer(new LocalPlayer("localplayer"));
			
			TweenMax.delayedCall(2.5,function():void{
				localPlayer.beKilled();
			});
			
			
			stage.addEventListener(TouchEvent.TOUCH,touchHandler);
		}		
		
		public function buttonHandler():void
		{
			gameController.leaveRoom(gameController.currentRoomID);
			//			gameController.leaveLobby();
			_ce.state = new CPHomeScreen();
		}		
		
		private function gameFinishHandler(e:Event,data:ResultInformation):void
		{
			_ce.state = new CPResultScreen(data.score);
		}
		
		private function touchHandler(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch){
				if(touch.phase == TouchPhase.ENDED){
					localPlayer.increaseScore();	
					localPlayer.moveTo(new flash.geom.Point(localPlayer.x+20,localPlayer.y));
				}
			}
		}
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
			
		}		
		
		override public function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			initSprite();
			
		}				
		
		
	}
}
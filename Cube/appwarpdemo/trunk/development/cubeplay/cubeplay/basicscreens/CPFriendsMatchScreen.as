package cubeplay.basicscreens
{
	import com.shephertz.appwarp.messages.Room;
	
	import flash.utils.Dictionary;
	
	import citrus.core.CitrusEngine;
	
	import cubeplay.elements.listRenderer.InvitePopup;
	import cubeplay.elements.listRenderer.friendlist.PeopleItem;
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomFriendsPanel;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	import cubeplay.utils.UtilFunctions;
	import cubeplay.elements.smappipopup.SmappiPopupManager;
	
	public class CPFriendsMatchScreen extends CPPlayScreen
	{
		
		private var invitePopup:InvitePopup;
		private var invitedPlayers:Dictionary;
		private var requestedList:Array = new Array();
		
		private var jsonNotificationsObj:Object;

		private var gameController2:App42Controller;
		public function CPFriendsMatchScreen(roomIdParam:String = "",invitedPlayersParam:Dictionary = null)
		{
			super(roomIdParam);
			trace("FriendsMatchScreen "+roomId);
			invitedPlayers = invitedPlayersParam;
			
		}
		
		override public function initWaitingRoom():void
		{
			waitingRoomPanel = new WaitingRoomFriendsPanel();
			addChild(waitingRoomPanel);
			(waitingRoomPanel as WaitingRoomFriendsPanel).addEventListener(GameEvent.ON_INVITE, onInviteHandler);
			
			invitePopup = new InvitePopup(Starling.current.stage.stageWidth*(578/768),Starling.current.stage.stageHeight*(964/1024));

			invitePopup.addEventListener(GameEvent.BACK,backHandler);
			invitePopup.addEventListener(GameEvent.INVITE_COMPLETE,inviteCompleteHandler);
		}		
		
		private function inviteCompleteHandler(e:Event,invitedPlayers:Dictionary):void
		{
			invitePopup.hide();
			this.doInvite(invitedPlayers,roomId);
		}
		
		private function backHandler():void
		{
			invitePopup.hide();
		}
		
		private function onInviteHandler():void
		{
			invitePopup.show();
		}
		
		private function findDocSuccessHandler():void
		{
			// TODO Auto Generated method stub
		}	
		
		private function doInvite(invitedPlayers:Dictionary,roomId:String):void{
			gameController2 = new App42Controller(null);
			if(!UtilFunctions.countKeys(invitedPlayers)){
				SmappiPopupManager.showErrorMessage("Please select atleast one player!");
			}
			else {
				gameController2.me = (CitrusEngine.getInstance().gameData as MyGameData).profile;	
				gameController2.addEventListener(GameEvent.FIND_DOCUMENT_BY_KEY_VALUE_SUCCESS,findDocSuccessHandler);
				gameController2.addEventListener(GameEvent.DELETE_DOCUMENT_BY_KEY_VALUE_SUCCESS,deleteDocSuccessHandler);
				for (var i:Object in invitedPlayers) 
				{
					var pplItem:PeopleItem = invitedPlayers[i];
					 
					jsonNotificationsObj = new Object();
					jsonNotificationsObj.fromId = gameController2.me.id;
					jsonNotificationsObj.fromName = gameController2.me.fullname;
					jsonNotificationsObj.fromUrl = gameController2.me.url;
					jsonNotificationsObj.toId = pplItem.id;
					jsonNotificationsObj.roomId = roomId;
					jsonNotificationsObj.fromto = jsonNotificationsObj.fromId+"_"+jsonNotificationsObj.toId;
					
					if(!UtilFunctions.in_array_bytoId(jsonNotificationsObj.toId,requestedList)){
						requestedList.push(jsonNotificationsObj);
						var date:Date = new Date();
						jsonNotificationsObj.requestTime = date.getTime();
						gameController2.doInvite(JSON.stringify(jsonNotificationsObj));
//						gameController2.deteleInvite(jsonNotificationsObj.fromto);
//						gameController2.deleteDocumentsByKeyValue("fromto",jsonNotificationsObj.fromto,GameFactory.REQUESTS,GameFactory.SMAPPI);						
					}
				}		
			}
		}
		
		private function deleteDocSuccessHandler():void
		{
//			gameController2.doInvite(JSON.stringify(jsonNotificationsObj));
//			gameController2.insertJSONDocument(JSON.stringify(jsonNotificationsObj),GameFactory.REQUESTS,GameFactory.SMAPPI);	
		}
		
		override public function createRoomDoneHandler(event:Event, data:Room):void
		{
			// TODO Auto Generated method stub
			super.createRoomDoneHandler(event, data);
			this.doInvite(invitedPlayers,data.roomId);
		}
		
		override public function connectedToServerHandler():void
		{
			// TODO Auto Generated method stub
			super.connectedToServerHandler();
			
			GameFactory.doFriendsMatch(gameController,roomId);	
		}
		
		override public function reconnectedToServerHandler():void
		{
			// TODO Auto Generated method stub
			super.reconnectedToServerHandler();
			GameFactory.doFriendsMatch(gameController,roomId);	
		}
	}
}
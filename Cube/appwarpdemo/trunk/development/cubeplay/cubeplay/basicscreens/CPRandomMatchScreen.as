package cubeplay.basicscreens
{
	import cubeplay.elements.listRenderer.waitingroom.WaitingRoomPanel;
	
	public class CPRandomMatchScreen extends CPPlayScreen 
	{
		public function CPRandomMatchScreen()
		{
			super();
		}
		
		override public function initWaitingRoom():void
		{
			waitingRoomPanel = new WaitingRoomPanel();
			addChild(waitingRoomPanel);
		}
		
		override public function connectedToServerHandler():void
		{
			// TODO Auto Generated method stub
			super.connectedToServerHandler();
			gameController.getRoomsInRange();
		}
		
		override public function reconnectedToServerHandler():void
		{
			// TODO Auto Generated method stub
			super.reconnectedToServerHandler();
			gameController.getRoomsInRange();
		}
	}
}
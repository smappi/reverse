package cubeplay.basicscreens
{
	import citrus.core.starling.StarlingState;
	
	import cubeplay.utils.CubeplayStyle;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.smappiOnlineGame.controllers.appwarp.AppWarpController;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	import cubeplay.utils.CubeplayStyleSetting;
	import cubeplay.elements.smappipopup.SmappiPopupManager;
	
	public class CPLoginScreen extends StarlingState
	{
		
		private var screen:Panel;
		
		private var facebookController:AppWarpController;
		
		public function CPLoginScreen()
		{
			super();
			trace("LoginScreen");
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			trace("1");
			screen = new Panel();		
			screen.headerFactory = function():Header{
				var header:Header = new Header();
				header.dispose();
				return header;
			}			
			screen.nameList.add(CubeplayStyle.HOME_SCREEN);
			screen.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.width = Starling.current.stage.stageWidth;
			screen.height = Starling.current.stage.stageHeight;
			screen.layout = CubeplayStyleSetting.verticalLayout(100);
			this.addChild(screen);
			trace("2");
			var button:Button = new Button();
			button.nameList.add(CubeplayStyle.BUTTON_CONNECT_WITH_FACEBOOK);
			button.addEventListener(Event.TRIGGERED,buttonHandler);
			screen.addChild( button );			
			trace("3");
			var button2:Button = new Button();
			button2.nameList.add(CubeplayStyle.BUTTON_LOGIN_AS_GUEST);
			button2.addEventListener(Event.TRIGGERED,button2Handler);
			screen.addChild( button2 );	
			trace("4");

			facebookController = new AppWarpController();
			facebookController.addEventListener(GameEvent.AUTHENTICATED_FACEBOOK,authenticatedFacebookHandler);
			trace("5");
			_ce.gameData.dataChanged.add(onDataChanged);
			trace("6");
			(_ce.gameData as MyGameData).score++;
			
		}		
		
		private function button2Handler():void
		{
			
			SmappiPopupManager.showInputPopup("Please input your name", function(resp:String):void{				

				facebookController.createUserWithGuestProfile(resp);
				_ce.state = GameFactory.playWithFactory();
			});	
			
			
		}
		
		private function onDataChanged(data:String, value:Object):void {
			trace("onDataChanged "+data+" ---- " +value);
		}
		
		private function authenticatedFacebookHandler(e:Event,data:Object):void 
		{
			trace("4.5");
			facebookController.createUserWithFacebookProfile(data);	
			_ce.state = GameFactory.playWithFactory();
		}
		
		private function buttonHandler():void 
		{
			
			facebookController.loginFacebook();
		}		
		
	}
}
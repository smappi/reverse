package cubeplay.basicscreens
{
	import flash.utils.Dictionary;
	
	import citrus.core.starling.StarlingState;
	
	import cubeplay.elements.listRenderer.InvitePanel;
	import cubeplay.elements.smappipopup.SmappiPopupManager;
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.utils.GameFactory;
	import cubeplay.utils.UtilFunctions;
	
	import feathers.controls.List;
	import feathers.controls.Panel;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	public class CPInviteScreen extends StarlingState
	{
		
		private var screen:Panel;
		
		public function CPInviteScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			if((_ce.gameData as MyGameData).jsonProfile == null){
				trace("!isLoggedIn");
				_ce.state = new CPLoginScreen();				
			}
			else initSprite();
		}
		
		private function initSprite():void
		{		
			var screen:InvitePanel = new InvitePanel();

			screen.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			screen.width = Starling.current.stage.stageWidth;
			screen.height = Starling.current.stage.stageHeight;
			screen.addEventListener(GameEvent.BACK,backHandler);
			screen.addEventListener(GameEvent.INVITE_COMPLETE,inviteCompleteHandler);
			addChild(screen);
		}
		
		private function backHandler():void
		{
			_ce.state = new CPHomeScreen();
		}
		
		private function inviteCompleteHandler(e:Event,data:Dictionary):void
		{
			trace("inviteCompleteHandler "+data);
			if(!UtilFunctions.countKeys(data)){
//				_ce.state = new GameFactory.FRIENDS_SCREEN_CLASS("",data);
				SmappiPopupManager.showErrorMessage("Please select atleast one player!");
			}
			else {
				_ce.state = new GameFactory.FRIENDS_SCREEN_CLASS("",data);
			}
		}				
		
		private function findDocSuccessHandler():void
		{
			// TODO Auto Generated method stub
			
		}
	}
}
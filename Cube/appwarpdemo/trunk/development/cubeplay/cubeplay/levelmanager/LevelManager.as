package cubeplay.levelmanager {
	
	import flash.filesystem.File;
	
	import citrus.core.CitrusEngine;
	
	import cubeplay.utils.UtilFunctions;
	
	import org.osflash.signals.Signal;
	
	import starling.utils.AssetManager;
	import starling.utils.formatString;
	
	/**
	 * The LevelManager is a complex but powerful class, you can use simple states for levels with SWC/SWF/XML.
	 * 
	 * <p>Before using it, be sure that you have good OOP knowledge. For using it, you must use an Abstract state class 
	 * that you give as constructor parameter : <code>Alevel</code>.</p> 
	 * 
	 * <p>The six ways to set up your level : 
	 * <ul>
	 * <li><code>levelManager.levels = [Level1, Level2];</code></li>
	 * <li><code>levelManager.levels = [[Level1, "level1.swf"], [level2, "level2.swf"]];</code></li>
	 * <li><code>levelManager.levels = [[Level1, "level1.xml"], [level2, "level2.xml"]];</code></li>
	 * <li><code>levelManager.levels = [[Level1, level1XMLVar], [level2, level2XMLVar]];</code></li>
	 * <li><code>levelManager.levels = [[Level1, XML(new level1XMLEmbed())], [level2, XML(new level2XMLEmbed())]];</code></li>
	 * <li><code>levelManager.levels = [[Level1, Level1_SWC], [level2, Level2_SWC]];</code></li>
	 * </ul></p>
	 * 
	 * <p>An instantiation example in your Main class (you may also use the AGameData to store your levels) :
	 * <code>levelManager = new LevelManager(ALevel);
	 * levelManager.onLevelChanged.add(_onLevelChanged);
	 * levelManager.levels = [Level1, Level2];
	 * levelManager.gotoLevel();</code></p>
	 * 
	 * <p>The <code>_onLevelChanged</code> function gives in parameter the <code>Alevel</code> that you associate to your state : <code>state = lvl;</code>
	 * Then you can associate other functions :
	 * <ul>
	 * <li><code>lvl.lvlEnded.add(_nextLevel);</code></li>
	 * <li><code>lvl.restartLevel.add(_restartLevel);</code></li>
	 * </ul>
	 * And their respective actions :
	 * <ul>
	 * <li><code>_levelManager.nextLevel();</code></li>
	 * <li><code>state = _levelManager.currentLevel as IState;</code></li>
	 * </ul></p>
	 * 
	 * <p>The ALevel class must implement <code>public var lvlEnded</code> and <code>restartLevel</code> Signals in its constructor.
	 * If you have associated a SWF or SWC file to your level, you must add a flash MovieClip as a parameter into its constructor, 
	 * or a XML if it is one!</p>
	 */
	public class LevelManager {
		
		static private var _instance:LevelManager;
		
		public var onLevelChanged:Signal;
		public var onLoadedCompleted:Signal;
		public var checkPolicyFile:Boolean = false;
		
		
		public var levels:Array;
		public var currentLevel:*;
		
		/**
		 * If set to true, and the level comes from an SWF, the SWF is only loaded once, then cached.
		 * Enable this if you plan to deliver an IOS app, since IOS does not support SWF reloading
		 * in AOT (build) mode.
		 */
		
		private var _ALevel:Class;
		private var _currentIndex:uint;		
		private var _levelData:Array;
		
		private var assets:AssetManager;
		private var _completeClass:Class;
		public function LevelManager(ALevel:Class,completeClass:Class = null) {
			
			_instance = this;
			
			_ALevel = ALevel;
			_completeClass = completeClass;
			_levelData = new Array();
			
			onLevelChanged = new Signal(_ALevel);
			onLoadedCompleted = new Signal();
			
			onLevelChanged.add(_onLevelChanged);
			onLoadedCompleted.add(_onLoadedCompleted);
			
			_currentIndex = 0;
			assets = new AssetManager();
			
			updateLevel();
		}
		
		private function _onLoadedCompleted():void
		{
			// TODO Auto Generated method stub
			if(_completeClass){
				CitrusEngine.getInstance().state = new _completeClass;
			}
			else
				CitrusEngine.getInstance().state = new LevelsScreen(this);
		}
		
		private function _onLevelChanged(lvl:*):void {
			
			CitrusEngine.getInstance().state = lvl;
			
			lvl.lvlEnded.add(_nextLevel);
			lvl.lvlRestart.add(_restartLevel);
		}
		
		private function _nextLevel():void {
			
			this.nextLevel();
		}
		
		private function _restartLevel():void {
			this.gotoLevel(this.currentIndex);
		}
		
		public function back():void{
			CitrusEngine.getInstance().state = new _completeClass;
		}
		
		public function showMe():void{
			CitrusEngine.getInstance().state = new LevelsScreen(this);
		}
		
		public function getCurrentLevel():Object{
			return levels[_currentIndex];
		}
		
		public function getLevelByIndex(i:int):Object{
			return levels[i];
		}
		
		public function getRandomLevel():Object{
			return levels[UtilFunctions.randRange(0,levels.length-1)];
		}
		
		public function getRandomLevelIndex():Object{
			return UtilFunctions.randRange(0,levels.length-1);
		}
		
		public function set currentIndex(value:uint):void
		{
			_currentIndex = value;
		}
		
		static public function getInstance():LevelManager {
			return _instance;
		}
		
		public function updateLevel():void{
//			assets.enqueueWithName("http://dev.bizzon.com.vn/smappi/reverse/levels.json?i="+UtilFunctions.randRange(1,100000),"levels");
//			assets.removeTexture("levels");
			var appDir:File = File.applicationDirectory;
			assets = new AssetManager(1);
			assets.enqueue( appDir.resolvePath(formatString("assets/textures/1x")));
			assets.loadQueue(function(ratio:Number):void
			{
				
				//assets fully loaded
				//				trace(ratio);
				if (ratio == 1){
					levels = [];
					var levelsJson:Object = assets.getObject("levels");
					for each (var i:Object in levelsJson.layers) 
					{
						//				trace("name = "+i.type as String);
						if((i.type as String).toLowerCase().indexOf("objectgroup") > -1){
							
							levels.push(i);
						}
					}		
					onLoadedCompleted.dispatch();
				}
			});
		}
		
		public function destroy():void {
			
			onLevelChanged.removeAll();
			
			currentLevel = null;
		}
		
		public function nextLevel():void {
			
			if (_currentIndex < levels.length - 1) {
				++_currentIndex;
			}
			
			gotoLevel();
		}
		
		public function prevLevel():void {
			
			if (_currentIndex > 0) {
				--_currentIndex;
			}
			
			gotoLevel();
		}
		
		/**
		 * Call the LevelManager instance's gotoLevel() function to launch your first level, or you may specify it.
		 * @param index the level index from 1 to ... ; different from the levels' array indexes.
		 */
		public function gotoLevel(index:Number = -1):void {
			if (index != -1)
				_currentIndex = index;
			
			currentLevel = new _ALevel(levels[_currentIndex]);
			
			onLevelChanged.dispatch(currentLevel);
		}
		
		
		public function get nameCurrentLevel():String {
			return currentLevel.nameLevel;
		}
		
		public function get currentIndex():uint
		{
			return _currentIndex;
		}
	}
}
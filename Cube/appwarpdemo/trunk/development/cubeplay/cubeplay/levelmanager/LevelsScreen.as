package cubeplay.levelmanager
{
	import citrus.core.starling.StarlingState;
	
	
	import feathers.controls.Button;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	
	public class LevelsScreen extends StarlingState
	{
		
		private var btnUpdateLevels:Button;
		
		private var tileList:LevelList;
		
		private var titleListContainer:Sprite;
		
		private var _lvlManager:LevelManager;
		public function LevelsScreen(lvlManager:LevelManager)
		{
			super();
			_lvlManager = lvlManager;
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			init(); 
		}
		
		private function init():void
		{
			btnUpdateLevels = new Button();
			btnUpdateLevels.label = "UPDATE LEVELS";
			
			btnUpdateLevels.x = 0;
			btnUpdateLevels.y = stage.stageHeight - 150;
			
			var btnBack:Button = new Button();
			btnBack.label = "BACK";
			
			btnBack.x = 200;
			btnBack.y = stage.stageHeight - 150;
			
			titleListContainer = new Sprite();
			addChild(titleListContainer);
			
			addChild(btnUpdateLevels);
			addChild(btnBack);
			
			tileList = new LevelList(_lvlManager);
			titleListContainer.addChild(tileList);
			btnUpdateLevels.addEventListener(Event.TRIGGERED,updateLevelsHandler);
			btnBack.addEventListener(Event.TRIGGERED,backHandler);
		}		
		
		private function backHandler():void
		{
			_lvlManager.back();
		}
		
		private function updateLevels():void{
			btnUpdateLevels.label = "UPDATING LEVELS...";
			btnUpdateLevels.removeEventListener(Event.TRIGGERED,updateLevelsHandler);
			
			_lvlManager.updateLevel();
			_lvlManager.onLoadedCompleted.add(_loadedCompleted);
		}
		
		private function _loadedCompleted():void
		{
			btnUpdateLevels.label = "UPDATE LEVELS";
			btnUpdateLevels.addEventListener(Event.TRIGGERED,updateLevelsHandler);

			tileList = new LevelList(_lvlManager);
			titleListContainer.addChild(tileList);
		}
		
		private function updateLevelsHandler():void
		{
			titleListContainer.removeChild(tileList);
			updateLevels();
			
		}
		
		private function levelHandler():void
		{
			// TODO Auto Generated method stub
			
		}
		
	}
}



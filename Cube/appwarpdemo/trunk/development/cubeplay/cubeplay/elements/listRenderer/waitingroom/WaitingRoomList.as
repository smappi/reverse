package cubeplay.elements.listRenderer.waitingroom
{
	import cubeplay.utils.CubeplayStyle;
	import cubeplay.elements.listRenderer.friendlist.ListPanel;
	import cubeplay.elements.listRenderer.friendlist.PeopleItem;
	
	import feathers.data.ListCollection;
	
	import starling.core.Starling;
	
	public class WaitingRoomList extends ListPanel
	{
		
		public function WaitingRoomList(arrParam:Array,listClass:String,panelClass:String, itemClass:String)
		{
			super(arrParam,listClass,panelClass,itemClass);	
			this.list.touchable = false;
			this.height = Starling.current.stage.stageHeight - CubeplayStyle.FOOTER_HEIGHT - CubeplayStyle.HEADER_HEIGHT;
		}
		
		override public function initList():void
		{
			// TODO Auto Generated method stub
			super.initList();				
		}
		
		public function addPlayer(data:Object):void{

				var id:String = data.id;
				var fullname:String = data.fullname;
				var pictureUrl:String = data.url;
				trace(id + " " + pictureUrl);
				
				itemArr.push(
					new PeopleItem(fullname,"1",pictureUrl,id)
				);
	
			this.list.dataProvider = new ListCollection(itemArr);			
		}
	}
}



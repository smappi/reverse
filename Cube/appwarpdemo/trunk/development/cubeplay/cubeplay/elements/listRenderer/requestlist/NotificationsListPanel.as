package cubeplay.elements.listRenderer.requestlist
{
	import com.shephertz.app42.paas.sdk.as3.storage.JSONDocument;
	import com.shephertz.app42.paas.sdk.as3.storage.Storage;
	
	import citrus.core.CitrusEngine;
	
	import cubeplay.elements.listRenderer.NotificationsPanel;
	import cubeplay.elements.listRenderer.friendlist.ListPanel;
	import cubeplay.elements.listRenderer.friendlist.PeopleItemNoti;
	
	import feathers.data.ListCollection;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.MyGameData;
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	
	import starling.events.Event;
	
	import cubeplay.utils.DateHelper;
	
	public class NotificationsListPanel extends ListPanel
	{		

		private var gameController:App42Controller;
		
		public function NotificationsListPanel(arrParam:Array, listClass:String, panelClass:String, itemClass:String)
		{
			super(arrParam,listClass,panelClass,itemClass);		
		}				
		
		override public function initList():void
		{
			// TODO Auto Generated method stub
			super.initList();
			this.list.allowMultipleSelection = false;
			if(!itemArr.length){
				
				gameController = new App42Controller(null);
				
				gameController.me = (CitrusEngine.getInstance().gameData as MyGameData).profile;
				gameController.addEventListener(GameEvent.FIND_DOCUMENT_BY_KEY_VALUE_SUCCESS,findDocSuccessHandler);
				gameController.findDocumentByKeyValue("toId",gameController.me.id,GameFactory.REQUESTS,GameFactory.ORDER_BY_DESCENDING,"requestTime");	
			}else{
				this.list.dataProvider = new ListCollection(itemArr);
			}			
		}
		
		private function findDocSuccessHandler(e:Event,object:Object):void
		{
			var storage:Storage = Storage(object);  
//			trace("dbName is : " + storage.getDbName());  
//			trace("collectionName is : " + storage.getCollectionName());  
			var jsonDoc:JSONDocument = new JSONDocument();   
			for(var i:int  = 0; i<storage.getJsonDocList().length;i++)  
			{
				jsonDoc = JSONDocument(storage.getJsonDocList()[i]);  
//				trace("objectId is :  " + jsonDoc.getDocId());  
//				trace("jsonDoc is :  " + jsonDoc.getJsonDoc());  
				
				var jsonObj:Object = JSON.parse(jsonDoc.getJsonDoc()); 
				
				var id:String = jsonObj.fromId;
				var name:String = jsonObj.fromName;
				var pictureUrl:String = jsonObj.fromUrl;
				var btn:AccessoryButton = new AccessoryButton(jsonObj.roomId,jsonObj.fromto);
				btn.addEventListener(Event.TRIGGERED,btnHandler);
				itemArr.push(
					new PeopleItemNoti(name,DateHelper.difEnglish(new Date(),new Date(Number(jsonObj.requestTime))),pictureUrl,id,false, btn)
				);				
			}

			this.list.dataProvider = new ListCollection(itemArr);			
		}
		 
		private function btnHandler(e:Event,data:Object):void
		{
			gameController.deleteDocumentsByKeyValue("fromto",data.fromto,GameFactory.REQUESTS);
			(this.parent.parent as NotificationsPanel).dispatchEventWith(GameEvent.ON_JOIN_ROOM_FROM_NOTIFICATION,false,data.roomId);
		}
	}
}



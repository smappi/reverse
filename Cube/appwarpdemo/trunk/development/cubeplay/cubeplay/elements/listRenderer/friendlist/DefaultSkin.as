package cubeplay.elements.listRenderer.friendlist
{
	import cubeplay.utils.CubeplayStyle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class DefaultSkin extends Sprite
	{
		private var w:int;
		private var h:int;
		public function DefaultSkin(widthParam:Number,heightParam:Number)
		{
			super();
			w = widthParam;
			h = heightParam;
			addEventListener(Event.ADDED_TO_STAGE,handleAddedToStage); 
		}
		
		private function handleAddedToStage():void
		{
			removeEventListener(Event.ADDED_TO_STAGE,handleAddedToStage);			
			initSprite();	
		}		
		
		private function initSprite():void
		{
			var bgQuad:Quad = new starling.display.Quad(w,h,0x000000);
			bgQuad.alpha = 0;
			addChild(bgQuad);
			
			var img:Image = new Image(CubeplayStyle.cubeplayAtlas.getTexture("imgUnselect"));
			addChild(img);
			img.width = img.height = 30;
			img.x = img.y = (h - img.height)/2;
		}	
	}
}
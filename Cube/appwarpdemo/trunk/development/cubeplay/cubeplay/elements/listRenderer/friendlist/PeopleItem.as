package cubeplay.elements.listRenderer.friendlist
{
	

	public class PeopleItem extends Object	
	{
		public var text:String;
		public var textureUrl:String;
		public var id:String;
		public var accessory:*;
		public var isSelectable:Boolean = false;
		private var _selected:Boolean = false;
		
		public function PeopleItem(name:String,score:String,url:String,idParam:String,isSelectableParam:Boolean = true,accessoryParam:* = null)
		{
			super();
			text = name + "\n"+score;
			textureUrl = url;
			id = idParam;
			isSelectable = isSelectableParam;
			if(accessoryParam != null){
				accessory = accessoryParam;
			}
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function set selected(value:Boolean):void
		{
			if(value){
				if(accessory != null) accessory.show();
			}else{
				if(accessory != null) accessory.hide();
			}
			_selected = value;
		}

	}
}
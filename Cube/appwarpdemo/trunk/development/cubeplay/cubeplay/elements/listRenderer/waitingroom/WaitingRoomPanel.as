package cubeplay.elements.listRenderer.waitingroom
{
	import citrus.core.CitrusEngine;
	
	import cubeplay.elements.BottomNotice;
	import cubeplay.elements.HorizontalScrollContainer;
	import cubeplay.utils.CubeplayStyle;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.core.IFeathersControl;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	
	import cubeplay.basicscreens.CPHomeScreen;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	public class WaitingRoomPanel extends Panel
	{		
		
		
		private var _backButton:Button;

		private var waitingRoomPanel:WaitingRoomList;

		
		
		
		public function WaitingRoomPanel()
		{
			this.nameList.add(CubeplayStyle.INVITE_PANEL);
			horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			verticalScrollPolicy = List.SCROLL_POLICY_OFF;			
			this.addEventListener(FeathersEventType.INITIALIZE, initializeHandler);
		}				
		
		public function addPlayer(data:Object):void{
			waitingRoomPanel.addPlayer(data);
		}
		
		protected function initializeHandler(event:Event):void
		{
			
			waitingRoomPanel = new WaitingRoomList(new Array(),CubeplayStyle.LIST,CubeplayStyle.LIST_PANEL,CubeplayStyle.WAITING_LIST);
			addChild(waitingRoomPanel);
			
			waitingRoomPanel.x = (Starling.current.stage.stageWidth - waitingRoomPanel.width)/2;
			trace(waitingRoomPanel.x);
			
			this.footerFactory = function():IFeathersControl
			{ 
				var hori:HorizontalScrollContainer = new HorizontalScrollContainer(0,0,VerticalLayout.VERTICAL_ALIGN_MIDDLE);
				hori.height = CubeplayStyle.FOOTER_HEIGHT;
				var notice:BottomNotice = new BottomNotice();
				notice.message = "Please wait! The room will be started automatically...";
				hori.addChild(notice);
				hori.padding = (CubeplayStyle.FOOTER_HEIGHT - notice.height)/2;
				return hori;				
			};		

			
			this.headerFactory = function():IFeathersControl
			{
				var header:Header = new Header();
				header.title = "WAITING ROOM";
				header.nameList.add(CubeplayStyle.HEADER);
				header.maxHeight = CubeplayStyle.HEADER_HEIGHT;
				header.height = CubeplayStyle.HEADER_HEIGHT;
				
				
				
				var backbtn:Button = new Button();
				backbtn.addEventListener(Event.TRIGGERED,backbtnHandler);
				backbtn.nameList.add(CubeplayStyle.BUTTON_BACK);
				
				
				header.leftItems = new <DisplayObject>
					[
						backbtn
					];						
				
				return header;
			};						
		}
		
		private function inviteHandler():void
		{
			
		}
		
		private function btnStartHandler():void
		{
		
		}		
		
		private function backbtnHandler():void
		{
			CitrusEngine.getInstance().state = new CPHomeScreen();
		}
		
		
	}
}

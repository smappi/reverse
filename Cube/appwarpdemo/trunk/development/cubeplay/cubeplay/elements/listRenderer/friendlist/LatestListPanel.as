package cubeplay.elements.listRenderer.friendlist
{
	import com.shephertz.app42.paas.sdk.as3.storage.JSONDocument;
	import com.shephertz.app42.paas.sdk.as3.storage.Storage;
	
	import feathers.data.ListCollection;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.GameFactory;
	import cubeplay.smappiOnlineGame.controllers.SmappiController;
	import cubeplay.smappiOnlineGame.controllers.app42.App42Controller;
	
	import starling.events.Event;
	
	public class LatestListPanel extends ListPanel
	{		
		
		public function LatestListPanel(arrParam:Array, listClass:String, panelClass:String, itemClass:String)
		{
			super(arrParam,listClass,panelClass,itemClass);				
		}				
		
		override public function initList():void
		{
			// TODO Auto Generated method stub
			super.initList();
			
			if(!itemArr.length){
				
				
				var gameController:App42Controller = new App42Controller(null);
				
				gameController.me = SmappiController.getMyProfile();	
				gameController.addEventListener(GameEvent.FIND_DOCUMENT_BY_KEY_VALUE_SUCCESS,findDocSuccessHandler);
				gameController.findDocumentByKeyValue("ownerid",gameController.me.id,GameFactory.LASTEST_PLAYERS,GameFactory.ORDER_BY_DESCENDING,"date");
			}else{
				this.list.dataProvider = new ListCollection(itemArr);
			}			
		}
		
		private function findDocSuccessHandler(e:Event,object:Object):void
		{
			var storage:Storage = Storage(object);  
//			trace("dbName is : " + storage.getDbName());  
//			trace("collectionName is : " + storage.getCollectionName());  
			var jsonDoc:JSONDocument = new JSONDocument();   
			for(var i:int  = 0; i<storage.getJsonDocList().length;i++)  
			{
				jsonDoc = JSONDocument(storage.getJsonDocList()[i]);  
//				trace("objectId is :  " + jsonDoc.getDocId());  
//				trace("jsonDoc is :  " + jsonDoc.getJsonDoc());  
				
				var jsonObj:Object = JSON.parse(jsonDoc.getJsonDoc()); 
				
				var id:String = jsonObj.id;
				var name:String = jsonObj.name;
				var pictureUrl:String = jsonObj.picture.data.url;
				itemArr.push(
					new PeopleItem(name,"1",pictureUrl,id,false)
				);							
			}

			this.list.dataProvider = new ListCollection(itemArr);			
		}
	}
}



package cubeplay.elements.listRenderer.requestlist
{
	import com.greensock.TweenMax;
	
	import cubeplay.utils.CubeplayStyle;
	
	import feathers.controls.Button;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class AccessoryButton extends Sprite
	{

		private var btn:Button;
		private var roomId:String;
		private var fromto:String;
		public function AccessoryButton(roomIdParam:String = "",fromToParam:String = "")
		{
			super();
			roomId = roomIdParam;
			fromto = fromToParam;
			btn = new Button;
			btn.nameList.add(CubeplayStyle.BUTTON_ACCEPT);
			btn.addEventListener(Event.TRIGGERED,btnHandler);
			btn.x = 300;
			addChild(btn);			
		}
		
		private function btnHandler():void
		{
			this.dispatchEventWith(Event.TRIGGERED,false,{roomId:roomId,fromto:fromto});
		}
		
		public function show():void{
			btn.visible = true;
			TweenMax.to(btn,0.5,{x:0});
		}
		
		public function hide():void{
			TweenMax.to(btn,0.5,{x:300});
		}
	}
}
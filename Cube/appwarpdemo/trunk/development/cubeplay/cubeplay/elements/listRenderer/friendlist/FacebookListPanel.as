package cubeplay.elements.listRenderer.friendlist
{
	import com.milkmangames.nativeextensions.GoViral;
	
	import feathers.data.ListCollection;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.smappiOnlineGame.controllers.facebook.FacebookController;
	
	import starling.events.Event;
	
	public class FacebookListPanel extends ListPanel
	{		
		
		public function FacebookListPanel(arrParam:Array, listClass:String, panelClass:String, itemClass:String)
		{
			super(arrParam,listClass,panelClass,itemClass);				
		}				
		
		override public function initList():void
		{
			// TODO Auto Generated method stub
			super.initList();
			if(GoViral.isSupported()){
				if(GoViral.goViral.isFacebookAuthenticated()){
					trace("isFacebookAuthenticated ");
					var facebookController:FacebookController = new FacebookController();									
					facebookController.getFriendsFacebook();
					facebookController.addEventListener(GameEvent.GET_FRIENDS_COMPLETE_FACEBOOK,getFriendsCompleteHandler);
				}
			}	
		}
		
		override public function getFriendsCompleteHandler(e:Event, jsonData:Object):void
		{
			trace("getFriendsCompleteHandler "+jsonData);
			for (var i:int = 0; i < jsonData.data.length; i++)
			{
				var id:String = jsonData.data[i].id;
				var name:String = jsonData.data[i].name;
				var pictureUrl:String = jsonData.data[i].picture.data.url;
				trace(id + " " + pictureUrl);
				
				itemArr.push(
					new PeopleItem(name,"1",pictureUrl,id)
				);
			}
			this.list.dataProvider = new ListCollection(itemArr);
		}
		
		
		
		
	}
}

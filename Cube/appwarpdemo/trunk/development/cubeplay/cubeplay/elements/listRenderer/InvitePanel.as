package cubeplay.elements.listRenderer
{
	import flash.utils.Dictionary;
	
	import cubeplay.elements.HorizontalScrollContainer;
	import cubeplay.elements.listRenderer.friendlist.FacebookListPanel;
	import cubeplay.elements.listRenderer.friendlist.LatestListPanel;
	import cubeplay.elements.listRenderer.friendlist.ListPanel;
	import cubeplay.smappiOnlineGame.GameEvent;
	import cubeplay.utils.CubeplayStyle;
	import cubeplay.utils.UtilFunctions;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ToggleSwitch;
	import feathers.core.IFeathersControl;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	public class InvitePanel extends Panel
	{		
		
		
		private var _backButton:Button;
		private var _settingsButton:Button;
		
		private var togglebtn:ToggleSwitch;
		
		override protected function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			
		}
		
		
		private var latestPanel:LatestListPanel;
		
		private var facebookPanel:FacebookListPanel;
		
		public function InvitePanel(widthParam:Number = 0,heightParam:Number = 0)
		{
			this.nameList.add(CubeplayStyle.INVITE_PANEL);
			horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			verticalScrollPolicy = List.SCROLL_POLICY_OFF;	
			
			if(widthParam && heightParam){
				width = widthParam;
				height = heightParam;
			}else{
				width = Starling.current.stage.stageWidth;
				height = Starling.current.stage.stageHeight;
			}
			this.addEventListener(FeathersEventType.INITIALIZE, initializeHandler);
		}				
		
		protected function initializeHandler(event:Event):void
		{
			facebookPanel = new FacebookListPanel(new Array(),CubeplayStyle.LIST,CubeplayStyle.LIST_PANEL,CubeplayStyle.INVITE_ITEM_RENDERER);
			addChild(facebookPanel);
			facebookPanel.x = (Starling.current.stage.stageWidth - facebookPanel.width)/2;

			latestPanel = new LatestListPanel(new Array(),CubeplayStyle.LIST,CubeplayStyle.LIST_PANEL,CubeplayStyle.INVITE_ITEM_RENDERER);
			latestPanel.x = (Starling.current.stage.stageWidth - facebookPanel.width)/2;

			addChild(latestPanel);
			latestPanel.visible = false;

			this.footerFactory = function():IFeathersControl
			{ 
				var hori:HorizontalScrollContainer = new HorizontalScrollContainer(0,0,VerticalLayout.VERTICAL_ALIGN_MIDDLE);
				var btnStart:Button = new Button();
				btnStart.nameList.add(CubeplayStyle.BUTTON_START);
				btnStart.addEventListener(Event.TRIGGERED,btnStartHandler);
				hori.addChild(btnStart);
				hori.paddingBottom = (CubeplayStyle.FOOTER_HEIGHT - 100)/2;
				trace((CubeplayStyle.FOOTER_HEIGHT - 100)/2);
				return hori;
			};	
			
			
			this.headerFactory = function():IFeathersControl
			{
				var header:Header = new Header();
				header.nameList.add(CubeplayStyle.HEADER);
				header.title = "INVITE LIST";				
				var backbtn:Button = new Button();
				backbtn.addEventListener(Event.TRIGGERED,backbtnHandler);
				backbtn.nameList.add(CubeplayStyle.BUTTON_BACK);
				
				togglebtn = new ToggleSwitch();
				togglebtn.onText = "Latest";
				togglebtn.offText = "Facebook";
//				togglebtn.trackLayoutMode = ToggleSwitch.TRACK_LAYOUT_MODE_ON_OFF;
//
//				togglebtn.customOnTrackName = "customOnTrackName";
//				togglebtn.customOffTrackName = "customOffTrackName";	

				togglebtn.width = 150;
				togglebtn.addEventListener(Event.CHANGE,togglebtnChangedHandler);
				
				header.leftItems = new <DisplayObject>
					[
						backbtn
					];						
				
				header.rightItems = new <DisplayObject>
					[
						togglebtn
					];		
				
				
				return header;
			};						
		}
		
		private function getSelectedTab():ListPanel{
			if(latestPanel.visible) return latestPanel;
			else return facebookPanel;
		}
		
		private function getUnSelectedTab():ListPanel{
			if(!latestPanel.visible) return latestPanel;
			else return facebookPanel;
		}
		
		private function btnStartHandler():void
		{
			this.dispatchEventWith(GameEvent.INVITE_COMPLETE,false,getSelectItems());
		}
		
		private function backbtnHandler():void
		{
			this.dispatchEventWith(GameEvent.BACK);
		}
		
		private function getSelectItems():Dictionary{
			var selectedTabItems:Array = getSelectedTab().getSelectedItem();
			var unselectedTabItems:Array = getUnSelectedTab().getSelectedItem();
			
			
			var selectedItemsArr:Array = selectedTabItems;
			for (var j:int = 0; j < unselectedTabItems.length; j++) 
			{
				if(!UtilFunctions.in_array_byid(unselectedTabItems[j].id,getSelectedTab().itemArr)){
					selectedItemsArr.push(unselectedTabItems[j]);
				}
			}
			
			var selectedItemsDict:Dictionary = new Dictionary();
			for (var i:int = 0; i < selectedItemsArr.length; i++) 
			{
				if(!(selectedItemsArr[i].id in selectedItemsDict)){
					selectedItemsDict[selectedItemsArr[i].id] = selectedItemsArr[i];
				}
			}
			
			for (var key:Object in selectedItemsDict) {
				trace("aaaaaaaaa "+selectedItemsDict[key]);
			}
			
			return selectedItemsDict;
		}
		
		private function togglebtnChangedHandler(e:Event):void
		{
			if(togglebtn.isSelected){
				trace("latest");
				latestPanel.setSelectedItems(getSelectItems());
				facebookPanel.visible = false;
				latestPanel.visible = true;		
				
			}else{
				trace("facebook");
				facebookPanel.setSelectedItems(getSelectItems());
				latestPanel.visible = false;
				facebookPanel.visible = true;
				
			}
		}		
		
		
	}
}

package cubeplay.elements.listRenderer.friendlist
{
	import flash.utils.Dictionary;
	
	import cubeplay.utils.CubeplayStyle;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ToggleSwitch;
	import feathers.core.IFeathersControl;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayoutData;
	
	import starling.events.Event;
	
	public class ListPanel extends Panel
	{		
		
		
		public var list:List;
		public var itemArr:Array;		
		
		private var _backButton:Button;
		private var _settingsButton:Button;
		
		private var togglebtn:ToggleSwitch;
		
		public function ListPanel(arrParam:Array,listClass:String, panelClass:String,itemClass:String)
		{
			horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			verticalScrollPolicy = List.SCROLL_POLICY_OFF;				
			itemArr = arrParam;
			this.nameList.add(panelClass);
			this.headerFactory = function():IFeathersControl
			{
				var header:Header = new Header();
				header.dispose();
				return header;
			}


			this.paddingTop = 45;
			this.list = new List();
	
			this.list.nameList.add(listClass);
			
			this.list.dataProvider = new ListCollection(itemArr);
			this.list.itemRendererName = itemClass;		
			this.list.isSelectable = true;
			this.list.allowMultipleSelection = true;
			this.list.addEventListener(Event.CHANGE, list_changeHandler);
			
			
			this.list.layoutData = new AnchorLayoutData(0, 0, 0, 0);
			this.list.clipContent = false;
			this.list.autoHideBackground = true;
			
			
			
			this.addChild(this.list);
			
			this.initList();			
		}						
		
		public function initList():void{
			
		}
		
		public function getFriendsCompleteHandler(e:Event,jsonData:Object):void
		{
//			trace("getFriendsCompleteHandler "+jsonData);
//			for (var i:int = 0; i < jsonData.data.length; i++)
//			{
//				var id:String = jsonData.data[i].id;
//				var name:String = jsonData.data[i].name;
//				var pictureUrl:String = jsonData.data[i].picture.data.url;
//				trace(id + " " + pictureUrl);
//				
//				itemArr.push(
//					new PeopleItem(name,"1",pictureUrl,id)
//				);
//			}
//			this.list.dataProvider = new ListCollection(itemArr);
		}				
		
		public function setSelectedItems(dict:Dictionary):void{
			var tempArr:Vector.<int> = new Vector.<int>();
			for (var i:String in dict) 
			{
				for (var j:int = 0; j < itemArr.length; j++) 
				{
					trace("itemArr[j].id = "+itemArr[j].id);
					if(i == itemArr[j].id){
						tempArr.push(j);
						break;
					}
				}			
			}
			
			//			trace("tempArr[0] "+tempArr[0]);
			this.list.selectedIndices = tempArr;
			//			this.list.selectedItems = tempArr;
			
		}		
		
		public function list_changeHandler(event:Event):void
		{
			trace("list_changeHandler ");
			const selectedIndices:Vector.<int> = this.list.selectedIndices;			
			for (var i:int = 0; i < itemArr.length; i++) 
			{
				if(itemArr[i].selected){
					itemArr[i].selected = false;
				}
			}			
			
			if(this.list.selectedIndex > -1){
				var item:* = itemArr[this.list.selectedIndex];	
				item.selected = true;
			}
		}
		
		public function getSelectedItem():Array{
			var arr:Array = new Array();
			for (var i:int = 0; i < this.list.selectedItems.length; i++) 
			{
				arr.push(this.list.selectedItems[i]);	
				
			}
			
			return arr;
		}
	}
}

package cubeplay.elements.listRenderer
{
	import cubeplay.elements.listRenderer.requestlist.NotificationsListPanel;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ToggleSwitch;
	import feathers.core.IFeathersControl;
	import feathers.events.FeathersEventType;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import cubeplay.utils.CubeplayStyle;
	
	public class NotificationsPanel extends Panel
	{		
		
		
		private var _backButton:Button;
		private var _settingsButton:Button;
		
		private var togglebtn:ToggleSwitch;	
		
		private var panel:NotificationsListPanel;
		
		public function NotificationsPanel(widthParam:Number = 0,heightParam:Number = 0)
		{
			this.nameList.add(CubeplayStyle.INVITE_PANEL);
			horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			if(widthParam && heightParam){
				width = widthParam;
				height = heightParam;
			}else{
				width = Starling.current.stage.stageWidth;
				height = Starling.current.stage.stageHeight;
			}
			this.addEventListener(FeathersEventType.INITIALIZE, initializeHandler);
		}				
		
		protected function initializeHandler(event:Event):void
		{
			panel = new NotificationsListPanel(new Array(),CubeplayStyle.LIST,CubeplayStyle.LIST_PANEL,CubeplayStyle.NOTI_LIST);
			addChild(panel);
			panel.x = (Starling.current.stage.stageWidth - panel.width)/2;
			
			this.headerFactory = function():IFeathersControl
			{
				var header:Header = new Header();
				header.title = "NOTIFICATIONS";
				header.nameList.add(CubeplayStyle.HEADER);
				var backbtn:Button = new Button();
				backbtn.addEventListener(Event.TRIGGERED,backbtnHandler);
				backbtn.nameList.add(CubeplayStyle.BUTTON_BACK);
				
				
				header.leftItems = new <DisplayObject>
					[
						backbtn
					];						
				

				
				return header;
			};						
		}

		
		private function backbtnHandler():void
		{
			this.dispatchEventWith(GameEvent.BACK);
		}
		

		
		
	}
}

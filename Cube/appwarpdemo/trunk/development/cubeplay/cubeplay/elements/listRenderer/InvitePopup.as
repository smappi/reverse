package cubeplay.elements.listRenderer
{
	import flash.utils.Dictionary;
	
	import cubeplay.elements.HorizontalScrollContainer;
	import cubeplay.elements.listRenderer.friendlist.FacebookListPanel;
	import cubeplay.elements.listRenderer.friendlist.LatestListPanel;
	import cubeplay.elements.listRenderer.friendlist.ListPanel;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ToggleSwitch;
	import feathers.core.IFeathersControl;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	
	import cubeplay.smappiOnlineGame.GameEvent;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	import cubeplay.utils.UtilFunctions;
	import cubeplay.elements.smappipopup.SmappiPopupManager;
	import cubeplay.utils.CubeplayStyle;
	
	public class InvitePopup extends Panel
	{		
		
		
		private var _backButton:Button;
		private var _settingsButton:Button;
		
		private var togglebtn:ToggleSwitch;
		
		private var latestPanel:LatestListPanel;
		
		private var facebookPanel:FacebookListPanel;
		
		public function InvitePopup(widthParam:Number = 0,heightParam:Number = 0)
		{
			horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			if(widthParam && heightParam){
				width = widthParam;
				height = heightParam;
			}else{
				width = Starling.current.stage.stageWidth;
				height = Starling.current.stage.stageHeight;
			}
			this.addEventListener(FeathersEventType.INITIALIZE, initializeHandler);
		}				
		
		public function show():void{
			SmappiPopupManager.addPopUpWithAnimation(this);
		}
		
		public function hide():void{
			SmappiPopupManager.removePopUpWithAnimation(this);
		}
		
		protected function initializeHandler(event:Event):void
		{
			facebookPanel = new FacebookListPanel(new Array(),CubeplayStyle.LIST_POPUP,CubeplayStyle.LIST_POPUP_PANEL,CubeplayStyle.INVITE_POPUP_ITEM_RENDERER);
			addChild(facebookPanel);
			
			latestPanel = new LatestListPanel(new Array(),CubeplayStyle.LIST_POPUP,CubeplayStyle.LIST_POPUP_PANEL,CubeplayStyle.INVITE_POPUP_ITEM_RENDERER);
			addChild(latestPanel);
			latestPanel.visible = false;
			
			this.footerFactory = function():IFeathersControl
			{ 
				var hori:HorizontalScrollContainer = new HorizontalScrollContainer(0,0,VerticalLayout.VERTICAL_ALIGN_MIDDLE);
				var btnInvite:Button = new Button();
				btnInvite.nameList.add(CubeplayStyle.BUTTON_INVITE);
				btnInvite.addEventListener(Event.TRIGGERED,btnStartHandler);
				hori.addChild(btnInvite);
//				hori.paddingBottom = (Mystyle.FOOTER_HEIGHT - 100)/2;
				return hori;
			};	
			
			
			this.headerFactory = function():IFeathersControl
			{
				var header:Header = new Header();
				header.nameList.add(CubeplayStyle.HEADER);
				
				var backbtn:Button = new Button();
				backbtn.addEventListener(Event.TRIGGERED,backbtnHandler);
				backbtn.nameList.add(CubeplayStyle.BUTTON_BACK);
				
				togglebtn = new ToggleSwitch();
				togglebtn.onText = "Lastest Players";
				togglebtn.offText = "Facebook Friends";
				togglebtn.width = 150;
				togglebtn.addEventListener(Event.CHANGE,togglebtnChangedHandler);
				
				header.leftItems = new <DisplayObject>
					[
						backbtn
					];						
				
				header.rightItems = new <DisplayObject>
					[
						togglebtn
					];		
				
				
				return header;
			};						
		}
		
		private function getSelectedTab():ListPanel{
			if(latestPanel.visible) return latestPanel;
			else return facebookPanel;
		}
		
		private function getUnSelectedTab():ListPanel{
			if(!latestPanel.visible) return latestPanel;
			else return facebookPanel;
		}
		
		private function btnStartHandler():void
		{
			
			this.dispatchEventWith(GameEvent.INVITE_COMPLETE,false,getSelectItems());
		}		
		
		private function backbtnHandler():void
		{
			this.dispatchEventWith(GameEvent.BACK);
		}
		
		private function getSelectItems():Dictionary{
			var selectedTabItems:Array = getSelectedTab().getSelectedItem();
			var unselectedTabItems:Array = getUnSelectedTab().getSelectedItem();
			
			
			var selectedItemsArr:Array = selectedTabItems;
			for (var j:int = 0; j < unselectedTabItems.length; j++) 
			{
				if(!UtilFunctions.in_array_byid(unselectedTabItems[j].id,getSelectedTab().itemArr)){
					selectedItemsArr.push(unselectedTabItems[j]);
				}
			}
			
			var selectedItemsDict:Dictionary = new Dictionary();
			for (var i:int = 0; i < selectedItemsArr.length; i++) 
			{
				if(!(selectedItemsArr[i].id in selectedItemsDict)){
					selectedItemsDict[selectedItemsArr[i].id] = selectedItemsArr[i];
				}
			}
			
			for (var key:Object in selectedItemsDict) {
				trace("aaaaaaaaa "+selectedItemsDict[key]);
			}
			
			return selectedItemsDict;
		}
		
		private function togglebtnChangedHandler(e:Event):void
		{
			if(togglebtn.isSelected){
				trace("latest");
				latestPanel.setSelectedItems(getSelectItems());
				facebookPanel.visible = false;
				latestPanel.visible = true;		
				
			}else{
				trace("facebook");
				facebookPanel.setSelectedItems(getSelectItems());
				latestPanel.visible = false;
				facebookPanel.visible = true;
				
			}
		}		
		
		
	}
}

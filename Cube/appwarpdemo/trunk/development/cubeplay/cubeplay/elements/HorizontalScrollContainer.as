package cubeplay.elements
{
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import starling.core.Starling;
	
	public class HorizontalScrollContainer extends ScrollContainer
	{
		public function HorizontalScrollContainer(w:int = 0,h:int = 0,vAlign:String = VerticalLayout.VERTICAL_ALIGN_TOP)
		{
			super();
			this.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
			this.verticalScrollPolicy = List.SCROLL_POLICY_OFF;
			if(w){
				this.width = w;
				
			}else{
				this.width = Starling.current.stage.stageWidth;				
				
			}
			if(h){
				this.height = h;
			}else{
				this.height = Starling.current.stage.stageHeight;
			}
			
			const horizontal:HorizontalLayout = new HorizontalLayout();
			horizontal.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			if(vAlign) horizontal.verticalAlign = vAlign;
			else {
				horizontal.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
				horizontal.padding = Starling.current.stage.stageHeight*0.1;
				horizontal.paddingTop = 25;
				
			}
			
			
			horizontal.gap = 50;			
			
			this.layout = horizontal;			
		}
	}
}
package cubeplay.elements.smappipopup
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import cubeplay.elements.VerticalScrollContainer;
	import cubeplay.utils.CubeplayStyle;
	
	import feathers.controls.Button;
	import feathers.controls.TextInput;
	import feathers.core.PopUpManager;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class SmappiInputPopup extends Sprite
	{
		
		public var message:String= "";;
		public var textfield:TextField;
		
		public var delayClose:TweenMax;
		
		public var inputField:TextInput;
		
		private var btnOk:Button;		
		private var btnCancel:Button;
		private var callback:Function;

		private var w:int;

		private var h:int;
		private var _instance:SmappiInputPopup;
		public function SmappiInputPopup()
		{
			super();
			_instance = this;
			w = 578;
			h = 364;
			x = (Starling.current.stage.stageWidth - w)/2;
//			textfield = new TextField(Starling.current.stage.stageWidth-100,10,message,"Verdana",20);
//			textfield.x = 50;
//			textfield.y = 10;
//			textfield.color = 0xFFFFFF;
//			textfield.autoSize = TextFieldAutoSize.VERTICAL;
//			textfield.hAlign = HAlign.LEFT;
//			textfield.filter = BlurFilter.createDropShadow(1,0.7,0,0.8);
			
			inputField = new TextInput();
			inputField.nameList.add(CubeplayStyle.TEXT_INPUT);
			inputField.height = 90;
			inputField.text = "Simulator";
			
			btnOk = new Button();
			btnOk.nameList.add(CubeplayStyle.BUTTON_OK);
			btnOk.addEventListener(Event.TRIGGERED,btnOkHandler);			
			
			var bgImg:Image = new Image(CubeplayStyle.cubeplayAtlas.getTexture("bgInput"));
			addChild(bgImg);
			
			var ver:VerticalScrollContainer = new VerticalScrollContainer(w,h);			
//			ver.addChild(textfield);
			ver.addChild(inputField);
			ver.addChild(btnOk);
			addChild(ver);
		}
		
		private function btnCancelHandler():void
		{
			closeMe();
		}
		
		private function btnOkHandler():void
		{
			if(inputField.text == ""){
				SmappiPopupManager.showErrorMessage("Please input your name");
			}else{
				this.callback(inputField.text);			
				closeMe();
			}
		}
		
		public function showMe(message:String,callbackParam:Function = null):void{
		
			this.callback = callbackParam;
			alpha = 0;
			y = -Starling.current.stage.stageHeight;
			TweenMax.to(this,0.5,{alpha:1,y:(Starling.current.stage.stageHeight - h)/2,ease:Back.easeOut});
//			textfield.text = message;
		}
		
		public function closeMe():void{
			TweenMax.to(this,0.25,{alpha:0,y:-Starling.current.stage.stageHeight,ease:Back.easeIn,
				onComplete:function():void{
					PopUpManager.removePopUp(_instance);
					inputField.text = "";			
				}
			});
		}
	}
}
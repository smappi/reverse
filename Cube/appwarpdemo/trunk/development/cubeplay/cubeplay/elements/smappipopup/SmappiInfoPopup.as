package cubeplay.elements.smappipopup
{
	import com.greensock.TweenMax;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.HAlign;
	
	public class SmappiInfoPopup extends Sprite
	{
		
		public var bgQuad:Quad;
		public var message:String= "";;
		public var textfield:TextField;
		
		public var delayClose:TweenMax;
		
		public function SmappiInfoPopup()
		{
			super();
		
			textfield = new TextField(Starling.current.stage.stageWidth-100,10,message,"Verdana",20);
			textfield.x = 50;
			textfield.y = 10;
			textfield.color = 0xFFFFFF;
			textfield.autoSize = TextFieldAutoSize.VERTICAL;
			textfield.hAlign = HAlign.LEFT;
			
			bgQuad = new Quad(Starling.current.stage.stageWidth,50,0xFFFFFF);
			bgQuad.alpha = 0.9;
			
			addChild(bgQuad);
			addChild(textfield);
		}
		
		public function setColor(color:uint):void{
			bgQuad.color = color;
		}
		
		public function showMe(message:String,closeafter:uint):void{
			if(delayClose) delayClose.kill();
			this.y = 0;
			TweenMax.from(this,0.5,{y:-this.height});
			textfield.text = message;
			bgQuad.height = textfield.height+20;
			
			delayClose = TweenMax.delayedCall(closeafter,closeMe);
		}
		
		public function closeMe():void{
			TweenMax.to(this,0.5,{y:-this.height});
		}
	}
}
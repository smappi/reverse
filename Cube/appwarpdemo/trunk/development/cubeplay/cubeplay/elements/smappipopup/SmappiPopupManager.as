package cubeplay.elements.smappipopup
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import feathers.core.PopUpManager;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	
	public class SmappiPopupManager
	{
		
		public static var INFO_COLOR:uint = 0x59a84b;
		public static var WARNING_COLOR:uint = 0xf2bb46;
		public static var ERROR_COLOR:uint = 0xca5952;
		
		public static var infoPopup:SmappiInfoPopup;
		public static var inputPopup:SmappiInputPopup;
		
		/**
		 * This is just a replay PopupManager.addPopup method of Feathers with addon animation 
		 * @param popUp
		 * @param isModal
		 * @param isCentered
		 * @param customOverlayFactory
		 * 
		 */		
		public static function addPopUpWithAnimation(popUp:DisplayObject, isModal:Boolean = true):void{
			PopUpManager.addPopUp(popUp,true);
			TweenMax.from(popUp,0.5,{y:-popUp.height,ease:Back.easeOut});
		}
		
		/**
		 *  This is just a replay PopupManager.removePopUp method of Feathers with addon animation 
		 * @param popUp
		 * @param dispose
		 * 
		 */		
		public static function removePopUpWithAnimation(popUp:DisplayObject, dispose:Boolean = false):void{
			TweenMax.to(popUp,0.5,{y:Starling.current.stage.stageHeight,ease:Back.easeIn,onComplete:function():void{
				PopUpManager.removePopUp(popUp,dispose);			
			}});
		}
		
		
		
		/**
		 * show info message, auto close after 3s 
		 * @param message
		 * 
		 */		
		public static function showInfoMessage(message:String,closeafter:uint=3):void{
			getSmappiPopup().setColor(INFO_COLOR);
			getSmappiPopup().showMe(message,closeafter);
			PopUpManager.addPopUp(infoPopup,false,false);
		}
		
		/**
		 * show error message, auto close after 3s 
		 * @param message
		 * 
		 */		
		public static function showErrorMessage(message:String,closeafter:uint=3):void{
			getSmappiPopup().setColor(ERROR_COLOR);
			getSmappiPopup().showMe(message,closeafter);
			PopUpManager.addPopUp(infoPopup,false,false);
		}
		
		/**
		 * show warning message, auto close after 3s 
		 * @param message
		 * 
		 */	
		public static function showWarningMessage(message:String,closeafter:uint=3):void{
			getSmappiPopup().setColor(WARNING_COLOR);
			getSmappiPopup().showMe(message,closeafter);
			PopUpManager.addPopUp(infoPopup,false,false);
		}
		
		/**
		 * show InputPopup, auto close after 3s 
		 * @param message
		 * 
		 */	
		public static function showInputPopup(message:String,callback:Function = null):void{
			getSmappiInputPopup().showMe(message,callback);
			PopUpManager.addPopUp(inputPopup,true,false);
		}		
		
		private static function getSmappiInputPopup():SmappiInputPopup{
			if(inputPopup==null){
				inputPopup = new SmappiInputPopup();
			}
			
			return inputPopup;
		}		
		
		private static function getSmappiPopup():SmappiInfoPopup{
			if(infoPopup==null){
				infoPopup = new SmappiInfoPopup();
			}
			
			return infoPopup;
		}
		
	}
}
package components
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.osflash.signals.Signal;
	
	import screens.PlayScreen;
	
	import starling.utils.AssetManager;

	public class LevelGame
	{
		private static var levels:Array = [];
		private static var currentLevel:int;
		public var onLoadedCompleted:Signal;
		private static var maps:Array;
		private static var decors:Array;
		private static var chars:Array;
		private static var levelMaps:Array;
		private static var sumLevel:int = 0;

		public static var obj:Object;
		public function LevelGame()
		{
			onLoadedCompleted = new Signal();
		}
		
		//---------------------------------
		// DOC FILE VA RANDOM LIST
		//---------------------------------
		private static function readFileWord(fileName:String):String{
			// Define the URLLoader.
			var myFile:File = File.applicationDirectory; // Create out file object and tell our File Object where to look for the file
			myFile = myFile.resolvePath("assets/textures/1x/"+fileName); // Point it to an actual file
			
			var fileStream:FileStream = new FileStream(); // Create our file stream
			fileStream.open(myFile, FileMode.READ);
			
			var fileContents:String = fileStream.readUTFBytes(fileStream.bytesAvailable); // Read the contens of the 
			
			fileStream.close(); // Clean up and close the file stream
			
			return fileContents;
		}
		
		public function readJsonOnline(url:String):void{
			
			var assets:AssetManager = new AssetManager(1);
			assets.enqueueWithName(url,"levels");
			
			assets.loadQueue(function(ratio:Number):void
			{
				
				//assets fully loaded
				if (ratio == 1){
					levels = [];
					var levelsJson:Object = assets.getObject("levels");
					levels = levelsJson.layers;
					onLoadedCompleted.dispatch();
				}
			});
		}
		
		public static function saveLevel():void{
			var s:String = readFileWord("levels.json");
			var object:Object = JSON.parse(s);
			var lists:Array = object.layers;
			for (var i:int = 0; i < lists.length; i++) 
			{
				var obj:Object = lists[i];
				levels.push(obj);
			}
			
		}
		
		public static function level(s:String):void{
			var p:int = findInArray(s);
			obj = levels[p];
			currentLevel = int(obj.properties.l);
			Reverse.instance.state = new PlayScreen();
		}
		
		private static function findInArray(str:String):int {
			for(var i:int = 0; i < levels.length; i++){
				if(levels[i].name == str){
					return i;
					break;
				}
			}
			return -1; //If not found
		}
		
		public static function getLevel():Object{
			return obj;
		}
		
		public static function nextLevel():void{
			currentLevel++;
			if (currentLevel>=levels.length) 
			{
				currentLevel = levels.length;
			}
			Reverse.instance.state = new PlayScreen();
		}
		
		public static function getCurrentLevel():int{
			return currentLevel;
		}
		
		public static function getListLevel():Array{
			return levels;
		}
		
		public static function getMaps():Array{
			return maps;
		}
		
		public static function getChars():Array{
			return chars;
		}
		
		public static function getlevelMaps():Array{
			return levelMaps;
		}
		
		public static function getDecors():Array{
			return decors;
		}
		
		public static function getSumLevel():int{
			return sumLevel;
		}
		
		public static function saveMapLevel():void{
			var s:String = readFileWord("map.json");
			var object:Object = JSON.parse(s);
			maps = object.layers;
			
			for (var i:int = 0; i < maps.length; i++) 
			{
				var obj:Object = maps[i];
				if (obj.name == "decor") 
				{
					decors = obj.objects;
				}else if (obj.name == "char"){
					chars = obj.objects;
				}else if (obj.name == "levels"){
					levelMaps = obj.objects;
					//load level
					sumLevel = String(levelMaps[0].properties.number).split(";").length;
				}
			}
			
		}

		public static function getJsonStory1():String{
			var s:String = readFileWord("story_1.json");
			return s;
		}
		
		public static function getJsonStory2():String{
			var s:String = readFileWord("story_2.json");
			return s;
		}
	}
}
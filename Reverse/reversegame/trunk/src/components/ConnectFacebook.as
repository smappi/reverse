package components
{
	import com.milkmangames.nativeextensions.GVFacebookFriend;
	import com.milkmangames.nativeextensions.GoViral;
	import com.milkmangames.nativeextensions.events.GVFacebookEvent;
	
	import components.app42.CallBack;
	
	import org.osflash.signals.Signal;

	public class ConnectFacebook
	{
		private var _isConnect:Boolean = false;
		public var onSuccess:Signal;
		public var onError:Signal;
		public function ConnectFacebook()
		{
			onSuccess = new Signal();
			onError = new Signal();
			initFacebook();
		}

		public static function get isConnect():Boolean
		{
			return GoViral.goViral.isFacebookAuthenticated();
		}

		public function set isConnect(value:Boolean):void
		{
			_isConnect = value;
		}
		
		//connect facebook
		public function connectFacebook():void
		{
			if (!GoViral.goViral.isFacebookAuthenticated()) 
			{
				//login facebook get user_permission 
				GoViral.goViral.authenticateWithFacebook("user_friends, public_profile");
			}
		}
		
		public function logOutFacebook():void{
			GoViral.goViral.logoutFacebook();
		}
		
		private function initFacebook():void{
			// Listen for events.
			GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGGED_IN,onFacebookEvent);
			GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGGED_OUT,onFacebookEvent);
			GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGIN_CANCELED,onFacebookEvent);
			GoViral.goViral.addEventListener(GVFacebookEvent.FB_LOGIN_FAILED,onFacebookEvent);
			trace("init");
		}
		
		private function onFacebookEvent(e:GVFacebookEvent):void
		{
			trace(e.type);
			switch(e.type)
			{
				case GVFacebookEvent.FB_LOGGED_IN:
					//get info 
					
					GoViral.goViral.requestMyFacebookProfile().addRequestListener(function(e:GVFacebookEvent):void{
						if (e.type==GVFacebookEvent.FB_REQUEST_RESPONSE)
						{
							var accessToken:String=GoViral.goViral.getFbAccessToken();
							var myProfile:GVFacebookFriend=e.friends[0];
							trace("accessToken",accessToken);
							(Reverse.instance.gameData as ReverseGameData).facebookToken = accessToken;
							(Reverse.instance.gameData as ReverseGameData).facebookID = myProfile.id;
							(Reverse.instance.gameData as ReverseGameData).facebookName = myProfile.name;
							
							var objFacebook:FacebookData = new FacebookData();
							objFacebook.id = myProfile.id;
							objFacebook.name = myProfile.name;
							objFacebook.token = accessToken;
							linkFacebook(objFacebook);
//							onSuccess = new Signal();
							trace("connect FB Success");
							onSuccess.dispatch();
						}
						else {
							trace("này là cái quái gì");
						}
					});
					break;
				case GVFacebookEvent.FB_LOGGED_OUT:
					break;
				case GVFacebookEvent.FB_LOGIN_CANCELED:
					break;
				case GVFacebookEvent.FB_LOGIN_FAILED:
					onError.dispatch();
					break;
				case GVFacebookEvent.FB_PUBLISH_PERMISSIONS_FAILED:
				case GVFacebookEvent.FB_READ_PERMISSIONS_FAILED:
					break;
				case GVFacebookEvent.FB_READ_PERMISSIONS_UPDATED:
				case GVFacebookEvent.FB_PUBLISH_PERMISSIONS_UPDATED:
			}
			
		}	
		
		/**
		 * luu tru user len facebook social
		 */
		public function linkFacebook(_obj:FacebookData):void{
			var callback:CallBack = new CallBack();
			
			if((Reverse.instance.gameData as ReverseGameData).linkFacebook == 0)
			{
				Reverse.instance.socialService.linkUserFacebookAccount(_obj.id,_obj.token,callback);
			}
			callback.success.add(function():void{
				(Reverse.instance.gameData as ReverseGameData).linkFacebook = 1;
			});
			callback.error.add(function():void{
				linkFacebook(_obj);
			});
		}
	}
}
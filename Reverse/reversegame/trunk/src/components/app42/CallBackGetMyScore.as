package components.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.game.Game;
	import com.shephertz.app42.paas.sdk.as3.game.Score;
	
	import org.osflash.signals.Signal;
	
	public class CallBackGetMyScore implements App42CallBack
	{
		public var success:Signal;
		public var error:Signal;
		public function CallBackGetMyScore()
		{
			success = new Signal();
			error = new Signal();
		}
		
		public function onSuccess(object:Object):void
		{
			var game:Game   = Game(object);  
			trace("gameName is : " + game.getName());  
			for(var i:int=0;i<game.getScoreList().length;i++)  
			{ 
				success.dispatch(int(Score(game.getScoreList()[i]).getValue()));
				break;
			}
		}
		
		public function onException(exception:App42Exception):void
		{
			error.dispatch();
		}
	}
}
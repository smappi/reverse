package components.app42
{
	import components.ReverseGameData;
	
	import org.osflash.signals.Signal;

	public class GetSetScore
	{
		public static const LEVEL_1:String = "Level 1";
		
		public var success:Signal;
		public var error:Signal;
		
		public function GetSetScore()
		{
			success = new Signal();
			error = new Signal();
		}
		
		/**
		 *	Insert điểm mới của mình lên cloud 
		 * @param level Level hiện tại đang chơi
		 * @param score điểm cao nhất của level đang chơi
		 * 
		 */		
		public function insertScoreOnCloud(level:String,score:Number):void{
			level = "Level 1";
			trace("insertScoreOnCloud",level);
			var callBack:CallBackSaveScore = new CallBackSaveScore(level);
			Reverse.instance.scoreBoardService.saveUserScore(	level,
																"370514556479357",
																1000,
																callBack);
			callBack.success.add(function():void{
				success.dispatch();
			});
			
			// pust lên bị lỗi thì gỡi lại lần nữa
			callBack.error.add(function():void{
				insertScoreOnCloud(level,score);
			});
		}
		
		/**
		 *	Update điểm mới lên cloud 
		 * @param level Level chơi lại
		 * @param score Điểm đạt được
		 * 
		 */		
		public function updateScoreOnCloud(level:String,score):void{
			level = "Level 1";
			var callBack:CallBack = new CallBack();
			Reverse.instance.scoreBoardService.editScoreValueById((Reverse.instance.gameData as ReverseGameData).getSaveGameData("ID_"+level)+"",
				score, 
				callBack);
			callBack.success.add(function():void{
				success.dispatch();
			});
		}
		
		/**
		 *	Lấy điểm của mình theo từng level 
		 * @param level Level muốn lấy điểm
		 * 
		 */		
		public function getMyScore(level:String):void{
			level = "Level 1";
			var callBack:CallBackGetMyScore = new CallBackGetMyScore();
			Reverse.instance.scoreBoardService.getScoresByUser(	level,
																(Reverse.instance.gameData as ReverseGameData).facebookID,
																callBack);
			callBack.success.add(function(value:int):void{
				success.dispatch(value);
			});
		}
	}
}
package components.app42
{
	import components.ReverseGameData;
	
	import org.osflash.signals.Signal;

	public class GetListFriend
	{
		public var success:Signal;
		public var error:Signal;
		public function GetListFriend(level:String)
		{
			success = new Signal();
			error = new Signal();
			
			var callBack:CallBackGetListFriend = new CallBackGetListFriend();
			Reverse.instance.scoreBoardService.getTopNRankersFromFacebook(	level,
				(Reverse.instance.gameData as ReverseGameData).facebookToken,
				1000,
				callBack);
			callBack.success.add(function():void{
				success.dispatch();
			});
			
			callBack.error.add(function():void{
				trace("getListFriend Error");
				error.dispatch();
			});
		}
	}
}
package components.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.game.Game;
	import com.shephertz.app42.paas.sdk.as3.game.Score;
	
	public class callback1 implements App42CallBack    
	{    
		public function onException(excption:App42Exception):void    
		{    
			trace("Exception : " + excption);    
		}    
		public function onSuccess(res:Object):void    
		{    
			var game:Game   = Game(res);  
			trace("gameName is : " + game.getName());  
			for(var i:int=0;i<game.getScoreList().length;i++)  
			{
				trace("username is :  " + Score(game.getScoreList()[i]).getUserName());  
				trace("value is : " + Score(game.getScoreList()[i]).getValue());  
				trace("scoreId is :  " + Score(game.getScoreList()[i]).getScoreId());  
			}  
		}    
	}    
}
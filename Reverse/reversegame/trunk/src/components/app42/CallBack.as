package components.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	
	import org.osflash.signals.Signal;
	
	public class CallBack implements App42CallBack
	{
		public var success:Signal
		public var error:Signal;
		public function CallBack()
		{
			success = new Signal();
			error = new Signal();
		}
		
		public function onSuccess(object:Object):void
		{
			success.dispatch();
		}
		
		public function onException(exception:App42Exception):void
		{
			error.dispatch();
		}
	}
}
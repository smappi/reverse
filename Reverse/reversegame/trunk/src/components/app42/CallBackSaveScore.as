package components.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.game.Game;
	import com.shephertz.app42.paas.sdk.as3.game.Score;
	
	import components.ReverseGameData;
	
	import org.osflash.signals.Signal;
	
	public class CallBackSaveScore implements App42CallBack
	{
		public var success:Signal;
		public var error:Signal;
		private var _level:String;
		public function CallBackSaveScore(level:String)
		{
			success = new Signal();
			error = new Signal();
			_level = level;
		}
		
		public function onSuccess(object:Object):void
		{
			trace(object,"object");
			var game:Game   = Game(object);
			(Reverse.instance.gameData as ReverseGameData).setSaveGameData("ID_"+_level,Score(game.getScoreList()[0]).getScoreId());
			success.dispatch();
		}
		
		public function onException(exception:App42Exception):void
		{
			error.dispatch();
		}
	}
}
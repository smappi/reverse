package components.app42
{
	import com.shephertz.app42.paas.sdk.as3.App42CallBack;
	import com.shephertz.app42.paas.sdk.as3.App42Exception;
	import com.shephertz.app42.paas.sdk.as3.game.FacebookProfile;
	import com.shephertz.app42.paas.sdk.as3.game.Game;
	import com.shephertz.app42.paas.sdk.as3.game.Score;
	
	import components.FacebookData;
	import components.ReverseGameData;
	
	import org.osflash.signals.Signal;
	
	public class CallBackGetListFriend implements App42CallBack
	{
		public var success:Signal;
		public var error:Signal;
		public function CallBackGetListFriend()
		{
			success = new Signal();
			error = new Signal();
		}
		
		public function onSuccess(object:Object):void
		{
			var facebookProfile:FacebookProfile;  
			if(object is Game)  
			{  
				var score:Score = new Score();  
				var gameResponse:Game   = Game(object);  
				trace("GameName is : " + gameResponse.getName());
				(Reverse.instance.gameData as ReverseGameData).listFriend = [];
				for(var l:int =0;l<gameResponse.getScoreList().length;l++)  
				{  
					var fb:FacebookData = new FacebookData();
					score = Score(gameResponse.getScoreList()[l]);  
					trace("username  " +score.getUserName());  
					trace("value   " +score.getValue());                  
					trace("scoreId  " +score.getScoreId());  
					trace("created on"+score.getCreatedOn());     
					facebookProfile = FacebookProfile(score.getFacebookList()[l]);  
					trace("FB Id is : " + facebookProfile.getId());                       
					trace("FB Name is : " + facebookProfile.getName());               
					trace("FB Picture is : " + facebookProfile.getPicture()); 
					fb.id = facebookProfile.getId();
					fb.name = facebookProfile.getName();
					fb.score = score.getValue() + "";
					(Reverse.instance.gameData as ReverseGameData).listFriend.push(fb);
//					(Reverse.instance.gameData as ReverseGameData).listFriend = (Reverse.instance.gameData as ReverseGameData).listFriend.concat(
				}
				
				success.dispatch();
			}  
		}
		
		public function onException(exception:App42Exception):void
		{
			error.dispatch();
		}
	}
}
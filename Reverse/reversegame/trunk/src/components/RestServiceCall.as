package components
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import org.osflash.signals.Signal;
	

	public class RestServiceCall
	{
		private var _variables:URLVariables;
		private var _url:String = "";
		public var onSuccess:Signal;
		public var onFail:Signal;
		public var onDeactive:Signal;
		
		public function RestServiceCall()
		{
			onSuccess = new Signal();
			onFail = new Signal();onDeactive = new Signal();
			
		}

		public function requestService(variables:URLVariables, url:String):void{
			//Create the HTTP request object 
			var request:URLRequest = new URLRequest(url); 
			request.method = URLRequestMethod.POST; 
			request.data = variables; 
			//Initiate the transaction 
			var requestor:URLLoader = new URLLoader(); 
			requestor.load( request ); 
			requestor.addEventListener( flash.events.Event.COMPLETE, httpRequestComplete ); 
			requestor.addEventListener( IOErrorEvent.IO_ERROR, httpRequestError ); 
			requestor.addEventListener( SecurityErrorEvent.SECURITY_ERROR, httpRequestError );
		}
		
		protected function deactive(event:Event):void
		{
			// TODO Auto-generated method stub
			onDeactive.dispatch("ssssss");
		}		
		
		protected function httpRequestError(event:IOErrorEvent):void
		{
			onFail.dispatch();
		}
		
		protected function httpRequestComplete(event:Event):void
		{
			onSuccess.dispatch(event.target.data);
		}		
		
	}
}
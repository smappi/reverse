package components
{
	import flash.net.SharedObject;
	
	import citrus.utils.AGameData;

	public class ReverseGameData extends AGameData
	{
		
		private var shareData:SharedObject;
		
		private var _facebookID:String;
		private var _facebookToken:String;
		private var _facebookName:String;
		private var _myScore:Number;
		private var _linkFacebook:int;
		private var _listFriend:Array;
		private var _isReplay:int;
		private var _playSound:int;
		private var _playFX:int;
		
		public function ReverseGameData()
		{
			loadGameData();
		}
		
		public function get playFX():int
		{
			return _playFX;
		}

		public function set playFX(value:int):void
		{
			_playFX = value;
			if(_playFX == 0){
				_playFX = -1;
			}
			shareData.data["playFX"] = _playFX;
			shareData.flush();
		}

		public function get playSound():int
		{
			return _playSound;
		}

		public function set playSound(value:int):void
		{
			_playSound = value;
			if(_playSound == 0){
				_playSound = -1;
			}
			
			shareData.data["playSound"] = _playSound;
			shareData.flush();
		}

		public function get isReplay():int
		{
			return _isReplay;
		}

		public function set isReplay(value:int):void
		{
			_isReplay = value;
			if(_isReplay == 0){
				_isReplay = -1;
			}
			
			shareData.data["isReplay"] = _isReplay;
			shareData.flush();
		}

		public function get listFriend():Array
		{
			return _listFriend;
		}

		public function set listFriend(value:Array):void
		{
			_listFriend = value;
			shareData.data["listFriend"] = _listFriend;
			shareData.flush();
		}

		public function get linkFacebook():int
		{
			return _linkFacebook;
		}

		public function set linkFacebook(value:int):void
		{
			_linkFacebook = value;
			shareData.data["linkFacebook"] = _linkFacebook;
			shareData.flush();
		}

		public function get myScore():Number
		{
			return _myScore;
		}

		public function set myScore(value:Number):void
		{
			_myScore = value;
			shareData.data["myScore"] = _myScore;
			shareData.flush();
		}

		public function get facebookName():String
		{
			return _facebookName;
		}

		public function set facebookName(value:String):void
		{
			_facebookName = value;
			shareData.data["facebookName"] = _facebookName;
			shareData.flush();
		}

		public function get facebookToken():String
		{
			return _facebookToken;
		}

		public function set facebookToken(value:String):void
		{
			_facebookToken = value;
			shareData.data["facebookToken"] = _facebookToken;
			shareData.flush();
		}

		public function get facebookID():String
		{
			return _facebookID;
		}

		public function set facebookID(value:String):void
		{
			_facebookID = value;
			shareData.data["facebookID"] = _facebookID;
			shareData.flush();
		}
		
		public function getSaveGameData(key:String):Object{
			return shareData.data[key];
		}
		
		public function setSaveGameData(key:String,value:Object):void{
			shareData.data[key] = value;
			shareData.flush();
		}

		private function loadGameData():void{
			shareData = SharedObject.getLocal("gameData");
			trace("khởi tạo gameData");
			if(shareData.data['facebookID']==undefined){
				facebookID = "";
			}else{
				facebookID = shareData.data['facebookID'] as String;
			}
			
			if(shareData.data['facebookToken']==undefined){
				facebookToken = "";
			}else{
				facebookID = shareData.data['facebookToken'] as String;
			}
			
			if(shareData.data['facebookName']==undefined){
				facebookName = "";
			}else{
				facebookName = shareData.data['facebookName'] as String;
			}
			
			if(shareData.data['linkFacebook']==undefined){
				linkFacebook = 0;
			}else{
				linkFacebook = shareData.data['linkFacebook'] as int;
			}
			
			if(shareData.data['listFriend']==undefined){
				listFriend = [];
			}else{
				listFriend = shareData.data['listFriend'] as Array;
			}
			
			if(shareData.data['listFriend']==undefined){
				isReplay = 1;
			}else{
				isReplay = shareData.data['isReplay'] as int;
			}
			
			if(shareData.data['playFX']==undefined){
				playFX = 1;
			}else{
				_playFX = shareData.data['playFX'] as int;
			}
			
			if(shareData.data['playSound']==undefined){
				playSound = 1;
			}else{
				playSound = shareData.data['playSound'] as int;
			}
		}
	}
}
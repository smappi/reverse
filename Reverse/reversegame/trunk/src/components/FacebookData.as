package components
{
	public class FacebookData
	{
		private var _id:String;
		private var _token:String;
		private var _name:String;
		private var _score:String;
		public function FacebookData()
		{
		}

		public function get score():String
		{
			return _score;
		}

		public function set score(value:String):void
		{
			_score = value;
		}

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get token():String
		{
			return _token;
		}

		public function set token(value:String):void
		{
			_token = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

	}
}
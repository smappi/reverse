package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import elements.Supernova;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyTeleport extends Sensor
	{

		private var _arr:Array;

		private var supernova:Supernova;

		private var sprite:Sprite;
		public function MyTeleport(name:String, params:Object=null, position:String = "")
		{
			super(name, params);
			arr = String(position).split(",");
			sprite = new Sprite;
			var imgExit:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("teleport"));
			imgExit.alignPivot();
			imgExit.smoothing = TextureSmoothing.NONE;
			imgExit.scaleX = imgExit.scaleY = 2;
			sprite.addChild(imgExit);
			
			supernova = new Supernova();
			supernova.scaleX = supernova.scaleX = 1.5;
//			supernova.alignPivot();
//			imgExit.alpha = 0.4;
//			sprite.flatten();
			sprite.addChild(supernova);
			supernova.tweenMe();
			view = sprite;
			x = params.x;
			y = params.y;
		}
		
		public function setAttr(params:Object=null, position:String = ""):void{
			arr = String(position).split(",");
			x = params.x;
			y = params.y;
			supernova.tweenMe();
//			view = sprite;
		}

		public function get arr():Array
		{
			return _arr;
		}

		public function set arr(value:Array):void
		{
			_arr = value;
		}

	}
}
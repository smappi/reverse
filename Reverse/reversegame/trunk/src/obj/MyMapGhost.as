package obj
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	
	import views.AnimationMapGhost;
	
	public class MyMapGhost extends Sensor
	{
		private var firstX:int;
		private var firstY:int;
		
		private var lastX:int;
		private var lastY:int;

		private var ghost:AnimationMapGhost;
		private var instance:MyMapGhost;
		public function MyMapGhost(name:String, params:Object=null,listMove:String = "",heightMiss:Number = 0)
		{
			super(name, params);
			var list:Array = listMove.split(";");
			var listFirst:Array = String(list[0]).split(",");
			x = firstX = listFirst[0];
			y = firstY = Number(listFirst[1]) + heightMiss;
			
			var listLast:Array = String(list[1]).split(",");
			lastX = listLast[0];
			lastY = Number(listLast[1]) + heightMiss;
			
			var sprite:Sprite = new Sprite();
			ghost = new AnimationMapGhost();
			ghost.y = ghost.height/2;
			ghost.x = ghost.width/2;
			sprite.addChild(ghost);
			ghost.alpha = 0;
			
			view = sprite;
			instance = this;
			moveMe();
		}
		
		private function moveMe():void{
			if(firstX>lastX){ 
				ghost.moveLeft();
			}
			else{
				ghost.moveRight();
			}
			TweenMax.delayedCall(2,function():void{
				ghost.alpha = 1;
				TweenMax.to(ghost,5,{alpha:0});
				TweenMax.to(instance,5,{x:lastX,y:lastY,ease:Linear.easeNone,onComplete:function():void{
					if(firstX>lastX){ 
						ghost.moveRight();
					}
					else{
						ghost.moveLeft();
					}
					TweenMax.delayedCall(2,function():void{
						ghost.alpha = 1;
						TweenMax.to(ghost,5,{alpha:0});
						TweenMax.to(instance,5,{x:firstX,y:firstY,ease:Linear.easeNone,onComplete:moveMe});
					});
					
				}});
			});
			
		}
	}
}
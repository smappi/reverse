package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationSkull;
	
	public class MySkull extends Sensor
	{
		private var _isImpact:Boolean = false;

		private var skull:AnimationSkull;
		
		private var xSkull:Number;
		private var ySkull:Number;
		
		private var _params:Object;
		
		private var instance:MySkull;
		
		private var _x:Number;
		private var _y:Number;
		
		private var start:Boolean;
		
		private var _isStart:Boolean = false;

		private var quad1:Quad;
		private var _time:uint;
		private var _speed:uint;
		private var widthParams:int;
		
		public var hero:MyHero;
		
		public function MySkull(name:String, params:Object=null,range:Number = 128,speed:uint = 100,time:Number = 4)
		{
			
			super(name, params);
			instance = this;
			_params = params;
			var sprite:Sprite = new Sprite()
			skull = new AnimationSkull();
			sprite.addChild(skull);
			
			
			quad1 = new Quad(10,10,0xff000);
			quad1.alignPivot();
			sprite.addChild(quad1);
			view = sprite;
			
			setAttr(params,range,speed,time);
		}
		
		public function setAttr(params:Object,range:Number,speed:uint,time:Number):void{
			
			_time = time;
			_speed = speed;
			if(_speed==0){
				_speed = 100;
			}
			if(_time==0){
				_time = 4;
			}
			
			params.width = range*2;
			params.height = range*2;
			widthParams = params.width;
			width = params.width;
			height = params.height;
			instance = this;
			_params = params;
			skull.alpha = 0.3;
			skull.pause();
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			
			skull.x = width/2;
			skull.y = height/2;
			x = _x = params.x ;
			y = _y = params.y ;
			
			quad1.alpha = 0;
			_isStart = false;
		}
		
		public function get isStart():Boolean
		{
			return _isStart;
		}

		public function set isStart(value:Boolean):void
		{
			_isStart = value;
			if(_isStart){
				if(_params.width ==0){
					_isStart = false;
					return;
				}
				if(hero.x>instance.x){
					skull.lookRight();
				}
				else{
					skull.lookLeft();
				}
				instance.width = skull.width;
				instance.height = skull.height;
				skull.play();
				TweenMax.to(instance,0.9,{y:"-5"});
				TweenMax.to(skull,1,{alpha:1,onComplete:function():void{
					moveToHero();
					UtilsAndConfig.playSound("monster_3_skull_chatter");
				}});
			}
			else{
				instance.x = _params.x;
				instance.y = _params.y;
				// cho về trạng thái ban đầu
				skull.quiet();
				skull.pause();
				TweenMax.to(skull,1,{alpha:0.3,onComplete:function():void{
					UtilsAndConfig.stopSound("monster_3_skull_chatter");
					skull.alpha = 0.3;
					_params.width = widthParams;
					instance.width = _params.width;
					instance.height = _params.height;
					instance.x = _params.x;
					instance.y = _params.y;
				}});
			}
		}
		
		public function move():void{
			skull.move();
		}
		
		public function quiet():void{
			skull.quiet();
		}
		
		public function get isImpact():Boolean
		{
			return _isImpact;
		}

		public function set isImpact(value:Boolean):void
		{
			_isImpact = value;
			if(_isImpact){
				moveToHero();
			}
		}
		
		public function moveToHero():void{
			skull.move();
			TweenMax.to(quad1,_time,{x:100,
				onUpdate:function():void{
					if(!isStart)
						return;
					// chạy theo thằng hero
					var canhKe:Number = Math.abs(hero.x - instance.x);
					var canhDoi:Number = Math.abs(hero.y - instance.y);
					
					var gocA:Number = Math.atan(canhDoi/canhKe);
					
					var canhHuyen:Number = canhDoi/Math.sin(gocA);
					
					// vận tốc là 100px trên 1s;
					var time:Number = canhHuyen/_speed;
					if(hero.x>instance.x){
						skull.lookRight();
					}
					else{
						skull.lookLeft();
					}
					TweenMax.to(instance,time,{x:hero.x,y:hero.y,ease:Linear.easeNone});
				},
				onComplete:reset
			});
			TweenMax.delayedCall(_time - 1,alphaSkull);
		}
		
		private function alphaSkull():void{
			TweenMax.to(skull,1,{alpha:0});
		}
		
		public function die():void{
			_params.width = 0;
			TweenMax.killTweensOf(quad1);
			TweenMax.killTweensOf(skull);
			TweenMax.killTweensOf(instance);
			TweenMax.killDelayedCallsTo(alphaSkull);
//			isStart = false;
			skull.alpha = 0;
		}
		
		public function reset():void{
//			skull.alpha =1;
			TweenMax.killTweensOf(quad1);
			TweenMax.killTweensOf(skull);
			TweenMax.killTweensOf(instance);
			TweenMax.killDelayedCallsTo(alphaSkull);
			x = _params.x;
			y = _params.y;
			isStart = false;
		}
	}
}
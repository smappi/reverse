package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyBalloon extends CitrusSprite
	{
		public function MyBalloon(name:String, params:Object=null)
		{
			super(name, params);
			var myBalloon:MovieClip = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("balloon_"),3);
			myBalloon.smoothing = TextureSmoothing.NONE;
			myBalloon.scaleX = myBalloon.scaleY = 2;
			myBalloon.play();
			Starling.juggler.add(myBalloon);
			y -= myBalloon.height - 10;
			x = x - myBalloon.width/2 - 7;
			view = myBalloon;
		}
	}
}
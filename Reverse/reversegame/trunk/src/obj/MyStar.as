package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationArrow;
	
	public class MyStar extends Sensor
	{

		private var imgExit:Image;

		private var imgHalo:Image;
		public function MyStar(name:String = "", params:Object=null)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			
			
			imgExit = new Image(UtilsAndConfig.defaultAssets.getTexture("gate"));
			imgExit.alignPivot();
			imgExit.smoothing = TextureSmoothing.NONE;
			imgExit.scaleX = imgExit.scaleY = 2;
			
			var arrow:AnimationArrow = new AnimationArrow();
			arrow.alignPivot();
			arrow.x = imgExit.x;
			arrow.y = imgExit.y - imgExit.height/2 - 3;
			sprite.addChild(arrow);
			
			imgHalo = new Image(UtilsAndConfig.defaultAssets.getTexture("halo_gate"));
			imgHalo.scaleX = imgHalo.scaleY = 0.4;
			imgHalo.alignPivot();
			imgHalo.x = imgExit.x;
			imgHalo.y = imgExit.y + imgExit.height/2 - imgHalo.height/2 ;
			sprite.addChild(imgHalo);
			sprite.addChild(imgExit);
			
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			view = sprite;
		}
		
		private function tweenHalo():void{
			TweenMax.to(imgHalo,500,{rotation:"-500",ease:Linear.easeNone,onComplete:tweenHalo});
		}
		
		public function setAttr(params:Object=null):void{
//			imgExit.texture = UtilsAndConfig.defaultAssets.getTexture("gate");
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			tweenHalo();
		}
	}
}
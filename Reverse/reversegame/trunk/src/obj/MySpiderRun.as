package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Power1;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import views.AnimationSpiderRun;
	
	public class MySpiderRun extends Sensor
	{
		public static var NOT_RUN:int = 0;
		public static var RUN:int = 1;
		public var isActive:int = NOT_RUN;
		private var listID:Array = [];
		private var params_:Object;
		private var aSprite:AnimationSpiderRun;
		private var plists:Array;
		private var _speed:Number;
		private var count:uint = 1;
		private var status:int = 0;
		private var isReverse:Boolean = false;
		
		public function MySpiderRun(name:String, params:Object=null,listMove:String = "",speed:int = 0,start:int = 0)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			aSprite = new AnimationSpiderRun();
			sprite.addChild(aSprite);
			view = sprite;
			setView(name,params,listMove,speed,start);
		}
		
		public function setAttr(name:String,params:Object=null,listMove:String = "",speed:int = 0,start:int = 0):void{
			setView(name,params,listMove,speed,start);
		}
		
		private function setView(name,params:Object=null,listMove:String = "",speed:int = 0,start:int = 0):void{
			params_ = params;
			status = start;
			isActive = start;
			name = name;
			listID = listMove.split(";");
			trace("listMove ",listMove);
			x = params.x ;
			y = params.y ;
			
			var length:Number = 0;
			var lsLengths:Array = [];
			plists = [];
			lsLengths.push(x);
			for (var i:int = 0; i < listID.length; i++) 
			{
				var ls:Array = String(listID[i]).split(",");
				var objD:Object = new Object();
				objD.x = Number(ls[0])+ Math.abs(offsetX);
				objD.y = Number(ls[1])+ Math.abs(offsetY);
				plists.push(objD);
				
			}
			
			_speed = speed;
			
			if (status == RUN && listID.length > 0) 
			{
				run();
			}
			
		}
		
		public function reset():void{
			TweenMax.killTweensOf(this);
			TweenMax.delayedCall(0.25,function():void{
				count = 1;
				isReverse = false;
				
				isActive = status;
				x = params_.x ;
				y = params_.y ;
				aSprite.sleep();
				if (status == RUN && listID.length > 0) 
				{
					run();
				}
			});
			
			
			
		}
		
		private function sleep():void{
			isActive = NOT_RUN;
			aSprite.sleep();
			TweenMax.killTweensOf(this);
		}
		
		public function run():void
		{
			isActive = RUN;
			aSprite.active();
			aSprite.addEventListener("COMPLETE_OPEN_EYES",spiderGo);
		}
		
		private function spiderGo(e:Event):void
		{
			var speed:Number = Point.distance(new Point(x,y),new Point(plists[count].x,plists[count].y))/_speed;
			TweenMax.to(this ,speed,{x:plists[count].x,y:plists[count].y,ease:Linear.easeNone,onComplete:function():void{
				
				if (!isReverse) 
				{
					count++;
				}else{
					count--;
				}
				if (count == plists.length) 
				{
					isReverse = true;
					count = plists.length - 2;
				}
				if(count == 0){
					isReverse = false;
				}
				
				run();
				
				
			}});
			
			
		}
	}
}
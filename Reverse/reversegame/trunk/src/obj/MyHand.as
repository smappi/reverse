package obj
{
	import com.greensock.TweenMax;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationHand;
	
	public class MyHand extends Sensor
	{
		
		private var instance:MyHand;
		
		private var _action:Boolean = false;

		private var hand:AnimationHand;
		private var _myParams:int;
		private var _isActive_:int;
		public function MyHand(name:String, params:Object=null,_isActive:int = 1)
		{
			super(name, params);
			instance = this;
			isActive = _isActive;
			
			var sprite:Sprite = new Sprite();
			
			hand = new AnimationHand();
			sprite.addChild(hand);
			hand.alpha = 0.3;
			_myParams = height;
			
			offsetY = -hand.height/2;
			
			view = sprite;
			hand.changeHeight.add(function(value:uint):void{
				height = value;
			});
			hand.stopAction.add(function():void{
				action = false;
			});
		}
		
		public function setAttr(params:Object):void{
			offsetX = -params.width/2;
			
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		
		public function get isActive():int
		{
			return _isActive_;
		}

		public function set isActive(value:int):void
		{
			_isActive_ = value;
		}

		public function get action():Boolean
		{
			return _action;
		}

		private var myFlag:Boolean = false;
		public function set action(value:Boolean):void
		{
			if(isActive == 0 ){
				return;
			}
			if(value){
				height = 4;
				if(myFlag){
					return;
				}
				myFlag = true;
				UtilsAndConfig.playSound("hand");
				TweenMax.to(hand,0.3,{alpha:1,onComplete:function():void{
					_action = value;
					hand.play();
				}});
			}
			else{
				myFlag = false;
				hand.removeEvent();
				height = _myParams;
				hand.pause();
				TweenMax.to(hand,0.3,{alpha:0.3});
				_action = value;
			}
		}
	}
}
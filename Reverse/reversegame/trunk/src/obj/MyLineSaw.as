package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Shape;
	import starling.display.Sprite;
	
	public class MyLineSaw extends Sensor
	{
		public function MyLineSaw(name:String, params:Object=null,list:String = "")
		{
			super(name, params);
			var shape:Shape = new Shape();
			var lsPo:Array = list.split(";");
			shape.graphics.lineStyle(2, 0x000000, 0.3);
			for (var i:int = 1; i < lsPo.length; i++) 
			{
				
				var arr:Array = String(lsPo[i]).split(",");
				shape.graphics.lineTo(Number(arr[0]) - params.x,Number(arr[1]) - params.y);
			}
			
			shape.graphics.endFill();
			var sprite:Sprite = new Sprite();
			sprite.addChild(shape);
			view = sprite;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
		}
	}
}
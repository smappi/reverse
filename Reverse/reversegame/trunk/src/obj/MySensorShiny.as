package obj
{
	import citrus.objects.platformer.simple.Sensor;
	import views.AnimationBlockShiny;
	
	public class MySensorShiny extends Sensor
	{
		public function MySensorShiny(name:String, params:Object=null)
		{
			super(name, params);
			var imgExit:AnimationBlockShiny = new AnimationBlockShiny();
			view = imgExit;
			imgExit.width = 32;
			imgExit.height = 32;
			x = params.x;
			y = params.y;
		}
	}
}
package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import views.AnimationSaw;
	
	public class MySaw extends Sensor
	{
		private var slistPosition:String = "";
		private var isFinish:int = 0;
		private var list:Array = [];
		private var plists:Array = [];
		
		// count luôn luôn khởi tạo bằng 1 vì bỏ qua vị trí đầu tiên và chạy tiếp tới vị trí thứ 2
		private var count:int = 1;
		
		private var animation2:AnimationSaw;
		private var _params:Object;
		private var _speed:Number = 0;
		private var _start:int;
		private var tmpStart:int;
		private var _id:int;

		private var tmpList:Array;
		public function MySaw(name:String, params:Object=null,finish:String="",listPo:String="",speed:Number = 0,_start_:int = 1,_id_:int=0)
		{
			super(name, params);
			
			animation2 = new AnimationSaw();
			view = animation2;
			
			plists = [];
			_params = params;
			id = _id_;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
			slistPosition = String(listPo);
			isFinish = int(finish);
			list = slistPosition.split(";");
			var length:Number = 0;
			var lsLengths:Array = [];
			lsLengths.push(x);
			for (var i:int = 0; i < list.length; i++) 
			{
				var ls:Array = String(list[i]).split(",");
				var objD:Object = new Object();
				objD.x = Number(ls[0])+ Math.abs(offsetX);
				objD.y = Number(ls[1])+ Math.abs(offsetY);
				plists.push(objD);
			}
			
			_speed = speed;
			count = 1;
			
			tmpList = plists;
			
			if(_start_ == 0){
				_start_ = -1
			}
			tmpStart = _start_;
			start = _start_;
			
		}
		
		public function get id():int
		{
			return _id;
		}

		public function set id(value:int):void
		{
			_id = value;
		}

		public function get start():int
		{
			return _start;
		}

		public function set start(value:int):void
		{
			_start = value;
			if(_start == 1){
				run();
			}
			else{
				
			}
		}

		/**
		 *	Bắt đầu cho saw chạy theo chu kỳ 
		 * 
		 */		
		public function run():void
		{
//			t = s/v
			var time:Number=Point.distance(new Point(this.x,this.y),new Point(plists[count].x,plists[count].y))/_speed;
			TweenMax.to(this ,time,{x:plists[count].x,y:plists[count].y,ease:Linear.easeNone,onComplete:function():void{
				count++;
				if (count == plists.length) 
				{
					if (isFinish==0) 
					{
						// đối với chạy theo vòng tuần hoàn. khi chạy hết list chỉ cần reset lại về 0
					}
					else{
						// đối  với chạy ngược lại mà k theo vòng tuần hoàn thì đảo chiều của list
						plists.reverse();
					}
					count = 0;
				}
				
				run();
			}});
		}
		
		public function reset():void{
			TweenMax.killTweensOf(this);
			x = _params.x + Math.abs(offsetX);
			y = _params.y + Math.abs(offsetY);
			count = 1;
			plists = tmpList;
			start = tmpStart;
		}
	}
}
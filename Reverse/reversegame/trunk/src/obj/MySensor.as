package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MySensor extends Sensor
	{
		public var w:int;
		public var h:int;
		public var startX:int;
		public var startY:int; 
		public var stopX:int;
		public var stopY:int;
		public var speed:int;
		public function MySensor(name:String = "", params:Object=null,stopXP:int = 0,stopYP:int = 0,speedP:int = 0,delay:int = 0,style:int = 0)
		{
			super(name, params);
			trace("style "+style);
			var spriteSensor:Sprite = new Sprite();
			var count:int = 0;
			
			var countW:int = params.width/32;
			var countH:int = params.height/32;
			
			var myQuad:QuadBatch = new QuadBatch();
			
			
			for (var k:int = 0; k < countW; k++) 
			{
				for (var i2:int = 0; i2 < countH; i2++) 
				{
					var imgBlock:Image =  new Image(UtilsAndConfig.defaultAssets.getTexture("sensor"+style));
					imgBlock.smoothing = TextureSmoothing.NONE;
					imgBlock.scaleX = imgBlock.scaleY = 2;
					imgBlock.y = 32*i2;
					imgBlock.x = 32*k;
					myQuad.addImage(imgBlock);
				}
				
			}
			
			
			
			view = myQuad;
			
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			startX = x;
			startY = y;
			if(stopXP && stopYP && speedP){
				stopX = stopXP + Math.abs(offsetX);
				stopY = stopYP + Math.abs(offsetY);
				speed = speedP;
				
				var dX:int = Math.abs(stopX - startX);
				var dY:int = Math.abs(stopY - startY);
				var s:Number = Math.sqrt(dX*dX + dY*dY);
				TweenMax.to(this,s/speed,{x:stopX,y:stopY,repeatDelay:delay,repeat:-1,yoyo:true,ease:Linear.easeNone});
				
			}
		}
	}
}
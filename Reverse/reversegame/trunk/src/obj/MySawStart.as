package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	
	public class MySawStart extends Sensor
	{
		private var _listID:Array;
		private var _isActive:Boolean = false;

		private var quad:Quad;
		public function MySawStart(name:String, params:Object=null,_listID_:String = "")
		{
			super(name, params);
			quad = new Quad(params.width,params.height);
			quad.alpha = 0;
			listID = _listID_.split(",");;
			
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
			view = quad;
		}

		public function setAttr(params:Object=null,_listID_:String = ""):void{
			isActive = false;
			listID = _listID_.split(",");
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			quad.width = width = params.width;
			quad.height = height = params.height;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		
		public function reset():void{
			isActive = false;
		}
		
		public function get isActive():Boolean
		{
			return _isActive;
		}
		
		public function set isActive(value:Boolean):void
		{
			_isActive = value;
		}
		
		public function get listID():Array
		{
			return _listID;
		}
		
		public function set listID(value:Array):void
		{
			_listID = value;
		}

	}
}
package obj
{
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Circ;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationSpider;
	
	public class MySpider extends Sensor
	{
		private var _flag:uint = 0;

		private var mySpider:AnimationSpider;
		
		private var _widthSensor:int;
		private var _heightSensor:int;
		
		private var _isStart:Boolean;
		private var _range:uint;
		private var saveX:Number;
		private var saveY:Number;
		private var _speed:int;
		private var _params:Object;
		private var _hero:MyHero;
		private var instance:MySpider;
		public function MySpider(name:String, params:Object=null,range:uint = 96,speed:int = 100)
		{
			super(name, params);
			instance = this;
			_params = params;
			_speed = speed;
			_range = range*2;
			_widthSensor = params.width;
			_heightSensor = params.height;
			
			width = height = _range;
			var sprite:Sprite = new Sprite();
			
			offsetX = -width/2;
			offsetY = -height/2;
			
			mySpider = new AnimationSpider();
			mySpider.alignPivot();
			sprite.addChild(mySpider);
			mySpider.x = width/2;
			mySpider.y = height/2;
			view = sprite;
			
		}
		
		public function get hero():MyHero
		{
			return _hero;
		}

		public function set hero(value:MyHero):void
		{
			_hero = value;
		}

		public function setAttr(_name:String,params:Object=null,range:uint = 96,speed:int = 100):void{
			this.name = _name;
			_range = range*2;
			_params = params;
			_speed = speed;
			x = params.x;
			y = params.y;
			width = height = _range;
			
			offsetX = -width/2;
			offsetY = -height/2;
			
			mySpider.x = width/2;
			mySpider.y = height/2;
		}

		public function get currentFrame():uint
		{
			return mySpider.currentFrame;
		}

		public function get isStart():Boolean
		{
			return _isStart;
		}
		
		private var notGoInsite:Boolean = false;

		private var obj1:Object;

		private var tlMax:TimelineMax;
		public function set isStart(value:Boolean):void
		{
			if(mySpider.alpha == 0){
				mySpider.currentFrame = 2;
				return;
			}
			_isStart = value;
			if(_isStart && width != _widthSensor && !notGoInsite){
				UtilsAndConfig.playSound("monster_1_spider");
				notGoInsite = true;
				saveX = hero.x;
				saveY = hero.y;
				width = _widthSensor;
				height = _heightSensor;
				moveToHero();
			}
			else{
				mySpider.currentFrame = 0;
				notGoInsite = false;
				width = height = _range;
			}
		}
		
		private function moveToHero():void{
			obj1 = {haha:0};
			tlMax = new TimelineMax({onComplete:function():void{
				mySpider.currentFrame = 2;
//				var canhKe:Number = Math.abs(saveX - instance.x);
//				var canhDoi:Number = Math.abs(saveY - instance.y);
//				
//				var gocA:Number = Math.atan(canhDoi/canhKe);
//				
//				var canhHuyen:Number = canhDoi/Math.sin(gocA);
//				
//				// vận tốc là 100px trên 1s;
//				var time:Number = canhHuyen/_speed;
				_speed= 1;
				TweenMax.to(instance,_speed,{x:saveX,ease:Linear.easeNone,onComplete:function():void{
					isStart = false;
					mySpider.currentFrame = 0;
				}});
				var tlMoveY:TimelineMax = new TimelineMax();
				
				if(instance.y > saveY){
					tlMoveY.to(instance,_speed*3/5,{y:saveY-60,ease:Circ.easeOut,onComplete:function():void{
						mySpider.currentFrame = 3;
					}});
					tlMoveY.to(instance,_speed*2/5,{y:saveY,ease:Circ.easeIn});
				}
				else{
					tlMoveY.to(instance,_speed*2/5,{y:"-60",ease:Circ.easeOut,onComplete:function():void{
						mySpider.currentFrame = 3;
					}});
					tlMoveY.to(instance,_speed*3/5,{y:saveY,ease:Circ.easeIn});
				}
			}});
			tlMax.to(obj1,0.1,{haha:2,onUpdate:function():void{
				mySpider.currentFrame = 0;
			}});
			tlMax.to(obj1,0.2,{haha:1,onUpdate:function():void{
				mySpider.currentFrame = 1;
			}});
		}
		
		public function die():void{
			mySpider.alpha = 0;
		}
		
		public function reset():void{
			if(tlMax!=null){
				tlMax.kill();
			}
			TweenMax.killTweensOf(obj1);
			TweenMax.killTweensOf(instance);
			x = _params.x;
			y = _params.y;
			mySpider.currentFrame = 0;
			mySpider.alpha = 1;
			isStart = false;
			x = _params.x;
			y = _params.y;
			TweenMax.delayedCall(1,function():void{
				x = _params.x;
				y = _params.y;
			});
		}
	}
}
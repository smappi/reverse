package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyPopup extends CitrusSprite
	{

		public var imgPopup:Image;
		public function MyPopup(name:String, params:Object=null)
		{
			super(name, params);
			imgPopup = new Image(UtilsAndConfig.defaultAssets.getTexture("popupMirrorRoom"));
			imgPopup.scaleX = imgPopup.scaleY = 2;
			imgPopup.smoothing = TextureSmoothing.NONE;
			imgPopup.alpha = 0;
			x = params.x - 50;
			y = params.y - imgPopup.height;
			
			view = imgPopup;
		}
	}
}
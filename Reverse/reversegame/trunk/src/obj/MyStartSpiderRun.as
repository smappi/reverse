package obj
{
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	
	public class MyStartSpiderRun extends Sensor
	{
		public var lists:Array = [];

		private var quad:Quad;
		public function MyStartSpiderRun(name:String, params:Object=null,listID:String="")
		{
			super(name, params);
			lists = listID.split(",");
			quad = new Quad(params.width, params.height,0xff0000);
			quad.alpha = 0;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			view = quad;
		}
		
		public function setAttr(params:Object,listID:String = ""):void{
			lists = listID.split(",");
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			quad.width = width = params.width;
			quad.height = height = params.height;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
	}
}
package obj
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationThorn;
	
	public class MyThorn extends Sensor
	{
		private var _isUp:uint;
		private var _start:Boolean;
		public var hero:MyHero;

		private var canhHuyen:Number;
		private var _action:int;
		private var _id:String;
		private var tmpAction:int;

		private var countX:uint;

		private var countY:uint;

		private var tmpListThorn:Array;

		private var listPlayLast:Array;
		private var _timeDelay:Number;

		private var listThornPlay:Array;
		public function MyThorn(name:String, params:Object=null,_start_:uint = 1,_time_:Number = 1,_id_:String="",_action_:int = 1)
		{
			super(name, params);
			_start = _start_; 
			id = _id_;
			timeDelay = _time_;
			var sprite:Sprite = new Sprite(); 
			listPlayLast = [];
			
			countX = params.width/(new AnimationThorn).width;
			countY = params.height/(new AnimationThorn).height;
			tmpListThorn = [];
			listThornPlay = [];
			
			for(var k:uint = 0; k<countX ; k++){
				tmpListThorn[k] = [];
				for(var l:uint = 0;l<countY; l++){
					tmpListThorn[k][l] = new AnimationThorn();
					if(_start_ == 0){
//						(tmpListThorn[k][l] as AnimationThorn).play();
						listThornPlay.push((tmpListThorn[k][l] as AnimationThorn));
					}
					else{
						listPlayLast.push(tmpListThorn[k][l] as AnimationThorn);
					}
					(tmpListThorn[k][l] as AnimationThorn).setTimeDelay(timeDelay);
					(tmpListThorn[k][l] as AnimationThorn).x = k*(tmpListThorn[k][l] as AnimationThorn).width;
					(tmpListThorn[k][l] as AnimationThorn).y = l*(tmpListThorn[k][l] as AnimationThorn).height;
					sprite.addChild(tmpListThorn[k][l]);
				}
			}
			(tmpListThorn[0][0] as AnimationThorn).isUp.add(function(value:uint):void{
				isUp=value;
				if(isUp == 1){
//					var canhKe:Number = Math.abs(hero.x - x);
//					var canhDoi:Number = Math.abs(hero.y - y);
//					
//					var gocA:Number = Math.atan(canhDoi/canhKe);
					
					canhHuyen = Point.distance(new Point(x,y),new Point(hero.x,hero.y));
					if(canhHuyen <=400){
						if(Reverse.instance.sound.getSound("thorn").isPlaying == false){
							UtilsAndConfig.playSound("thorn");
						}
						
						setVolumeSound();
					}
				}
				
				
			});
			
			
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			view = sprite;
			if(_action_ == 0){
				_action_ = -1;
				
			}
			tmpAction = _action_;
			action = tmpAction;
		}
		
		public function get timeDelay():Number
		{
			return _timeDelay;
		}

		public function set timeDelay(value:Number):void
		{
			_timeDelay = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function get action():int
		{
			return _action;
		}

		public function set action(value:int):void
		{
			_action = value;
			if(_action == 1){
				for each (var j:AnimationThorn in listThornPlay) 
				{
					j.play();
				}
				TweenMax.delayedCall(timeDelay,function():void{
					for each (var j:AnimationThorn in listPlayLast) 
					{
						j.play();
					}
				});
			}
			else{
				
			}
		}
		
		public function reset():void{
			if(tmpAction == -1){
				for each (var j:AnimationThorn in listThornPlay) 
				{
					j.stop();
				}
				
				for each (var k:AnimationThorn in listPlayLast) 
				{
					k.stop;
				}
				_action = tmpAction;
			}
			
		}

		private function setVolumeSound():void{
			if(canhHuyen>300){
				
				Reverse.instance.sound.getSound("thorn").volume = 0.3;
			}
			else if(canhHuyen>200){
				Reverse.instance.sound.getSound("thorn").volume = 0.6;
			}
			else{
				Reverse.instance.sound.getSound("thorn").volume = 1;
			}
		}

		public function get isUp():uint
		{
			return _isUp;
		}

		public function set isUp(value:uint):void
		{
			_isUp = value;
		}

	}
}
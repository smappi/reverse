package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import elements.TitleGame;
	
	public class MyTitleGame extends Sensor
	{
		public function MyTitleGame(name:String, params:Object=null)
		{
			super(name, params);
			var title:TitleGame = new TitleGame();
			title.alignPivot();
			view = title;
		}
	}
}
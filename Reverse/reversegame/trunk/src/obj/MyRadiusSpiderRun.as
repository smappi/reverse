package obj
{
	import com.greensock.TweenMax;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	
	public class MyRadiusSpiderRun extends Sensor
	{
		private var quad:Quad;
		public var id:int = 0;
		private var x_:Number = 0;
		private var y_:Number = 0;
		public function MyRadiusSpiderRun(name:String, params:Object=null,id:int = 0)
		{
			super(name, params);
			this.id = id;
			quad = new Quad(params.width, params.height,0xff0000);
			quad.alpha = 0;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			x_ = x;
			y_ = y;
			view = quad;
		}
		
		public function setAttr(params:Object,id:int = 0):void{
			this.id = id;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			quad.width = width = params.width;
			quad.height = height = params.height;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			x_ = x;
			y_ = y;
		}
		
		public function reset():void{
			TweenMax.delayedCall(0.25,function():void{
				x = x_;
				y = y_;
			});
		}
			
	}
}
package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import views.AnimationSpiderRun;
	
	public class MySpiderFollow extends Sensor
	{
		public static var NOT_RUN:int = 0;
		public static var RUN:int = 1;
		public var isActive:int = NOT_RUN;
		private var listID:Array = [];
		private var params_:Object;
		private var aSprite:AnimationSpiderRun;
		private var plists:Array;
		private var _speed:Number;
		private var count:uint;
		private var status:int = 0;
		public var isRun:Boolean = false;
		
		public function MySpiderFollow(name:String, params:Object=null,speed:int = 0)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			aSprite = new AnimationSpiderRun(1);
			sprite.addChild(aSprite);
			view = sprite;
			setView(name,params,speed);
		}
		
		public function setAttr(name:String,params:Object=null,speed:int = 0):void{
			setView(name,params,speed);
		}
		
		private function setView(name,params:Object=null,speed:int = 0):void{
			_speed = speed;
			params_ = params;
			name = name;
			x = params.x ;
			y = params.y ;
			isActive = NOT_RUN;
			isRun = false;
		}
		
		public function timeRun(point:Point):Number{
			var length:Number = Point.distance(new Point(x,y),point);
			return Number(length/_speed);
		}
		
		public function reset():void{
			TweenMax.killTweensOf(this);
			TweenMax.delayedCall(0.25,function():void{
				isActive = NOT_RUN;
				x = params_.x ;
				y = params_.y ;
				aSprite.sleep();
				isRun = false;
			});
			
		}
		
		public function run():void
		{
			if (isRun) 
			{
				return;	
			}
			isActive = RUN;
			aSprite.active();
			aSprite.addEventListener("COMPLETE_OPEN_EYES",spiderGo);
		}
		
		
		public function sleep():void{
			isRun = false;
			isActive = NOT_RUN;
			aSprite.sleep();
		}
		private function spiderGo(e:Event):void
		{
			isRun = true;
		}
	}
}
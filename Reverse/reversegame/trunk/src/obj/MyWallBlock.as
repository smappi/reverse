package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyWallBlock extends Sensor
	{
		public var w:int;
		public var h:int;
		public var startX:int;
		public var startY:int; 
		public var stopX:int;
		public var stopY:int;
		public var speed:int;
		
		private var myQuad:QuadBatch;
		private var size:int = 32;
		public function MyWallBlock(name:String = "", params:Object=null,stopXP:int = 0,stopYP:int = 0,speedP:int = 0,delay:int = 0,style:int = 0)
		{
			super(name, params);
			size = 32;
			var countW:int = params.width/size;
			var countH:int = params.height/size;
			
			myQuad = new QuadBatch();
			sprite2 = new Sprite();
			for(var i:uint = 0; i< countW;i++){
				for(var j:uint=0; j< countH;j++){
					addBlock(i*size,size*j);
				}
			}
			
			view = myQuad;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX) - 4;
			y = params.y + Math.abs(offsetY) - 4;
		}
		
		private function addBlock(_x:Number,_y:Number):void{
			var name:String = nameTexture();
			var imgBlock:Image =  new Image(UtilsAndConfig.defaultAssets.getTexture(name));
			imgBlock.smoothing = TextureSmoothing.NONE;
			imgBlock.scaleX = imgBlock.scaleY = 2;
			imgBlock.x = _x;
			imgBlock.y = _y;
			myQuad.addImage(imgBlock);
		}
		
		private var textures:Array = ["wall_block_1_1", "wall_block_1_2", "wall_block_1_4"];
		private var nameTmp:String = "wall_block_1";
		
		private var sprite:Sprite;
		
		private var sprite2:Sprite;
		private function nameTexture():String{
			var randomPos:int = 0;
			var nameTexture:String = "";
			for (var i:int = 0; i < textures.length; i++)
			{
				randomPos = int(Math.random() * textures.length);
				while (textures[randomPos] == nameTmp)      //repeat as long as the slot is not empty
				{
					randomPos = int(Math.random() * textures.length);
				}
				nameTexture = textures[randomPos];
			}
			nameTmp = nameTexture;
			return nameTexture;
		}
	}
}
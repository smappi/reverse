package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyButton extends Sensor
	{

		public var listID:Array;
		private var _flagButton:Boolean = false;

		private var _status:int = -1;

		private var button:Image;
		public function MyButton(name:String, params:Object=null,id:String = "",_status_:int=0)
		{
			super(name, params);
			listID = id.split(",");
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			var sprite:Sprite = new Sprite();
			button = new Image(UtilsAndConfig.defaultAssets.getTexture("button_gun_01"));
			button.alignPivot();
			button.smoothing = TextureSmoothing.NONE;
			button.scaleX = button.scaleY = 2;
			sprite.addChild(button);
			
			if(_status_ == 0){
				_status_ = -1;
			}
			view = sprite;
		}
		
		public function get status():int
		{
			return _status;
		}

		public function set status(value:int):void
		{
			_status = value;
			if(_status == 1){
				button.texture = UtilsAndConfig.defaultAssets.getTexture("button_gun_02")
			}
			else{
				button.texture = UtilsAndConfig.defaultAssets.getTexture("button_gun_01")
			}
		}

		public function setAttr(params:Object=null,id:String = "",_status_:int=0):void{
			listID = id.split(",");
			status = _status;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			flagButton = 0;
			if(_status_ == 0){
				_status_ = -1;
			}
			status = _status_;
		}

		public function get flagButton():Boolean
		{
			return _flagButton;
		}

		public function set flagButton(value:Boolean):void
		{
			_flagButton = value;
		}

	}
}
package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	import views.AnimationBat;
	
	public class MyBat extends Sensor
	{
		public function MyBat(name:String, params:Object=null)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			var animation:AnimationBat = new AnimationBat();
			animation.idle();
			view = animation;
		}
	}
}
package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.animation.Tween;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyWallMove extends Sensor
	{
		private var _time_:Number;
		private var _delay:Number;
		
		private var repeat:Number;
		
		private var endX:int;
		
		private var endY:int;
		private var _params:Object
		
		private var myQuad:QuadBatch;
		public var style:String;
		
		private var _statusSpeed:Object;
		public static const HORIZONTAL:String = "horizontal";
		public static const VERTICAL:String = "vertical";
		private var instance:MyWallMove;
		
		public var goUp:Boolean;
		
		private var count:int;
		private var _start:int;
		
		private var tmpTeen:TweenMax;
		
		public var id:String;
		
		private var tmpTween:TweenMax;
		private var tmpStart:int;
		private var isFirst:Boolean = true;
		private var tmpDelay:Number;
		public function MyWallMove(name:String, params:Object=null,list:String = "",_time:Number = 2,delay:Number = -1,statusSpeed:int = 3,_start_:int=-1,_id_:String = "",_style:String = "")
		{
			super(name, params);
			id = _id_;
			if(_start_ == 0){
				_start_ = -1;
			}
			tmpStart = _start_;
			if(statusSpeed == 0){
				_statusSpeed = Linear.easeNone;
			}
			else if(statusSpeed == 1){
				_statusSpeed = Cubic.easeIn;
			}
			else{
				_statusSpeed = Cubic.easeOut;
			}
			tmpDelay = _delay = delay;
			_time_ = _time;
			_params = params;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
			var countW:int = params.width/32;
			var countH:int = params.height/32;
			myQuad = new QuadBatch();
			var myQuadTMP:Sprite = new Sprite();
			
			for(var i:uint = 0; i< countW;i++){
				for(var j:uint=0; j< countH;j++){
					//					addBlock(0,32*j,Number(style));
					addBlock(i*32,32*j,Number(_style));
				}
			}
			
			if(countH>2){
				
			}
			var color:uint = UtilsAndConfig.randRange(0x000000,0xffffff);
			var quad:Quad = new Quad(params.width-10,params.height-10,0x141414);
			quad.x = 10;
			quad.y = 10;
			
			myQuadTMP.addChild(myQuad);
			//			myQuadTMP.addChild(quad);
			myQuadTMP.flatten();
			view = myQuadTMP;
			
			var listMove:Array = list.split(";");
			
			var listTmp:Array = String(listMove[0]).split(",");
			var startX:int = listTmp[0];
			var startY:int = listTmp[1];
			
			listTmp = String(listMove[1]).split(",");
			endX = int(listTmp[0]) +Math.abs(offsetX);
			endY = int(listTmp[1]) + Math.abs(offsetY);
			
			if(x<endX){
				style = HORIZONTAL;
			}
			else{
				style = VERTICAL;
			}
			trace("style",style);
			repeat = -1;
			instance = this;
			if (delay == 0) 
			{
				repeat = 0;
			}
			else if(repeat>-1){
				count = 0;
			}
			//			if(start==1){
			//				playMe();
			//			}
			start = _start_;
		}
		
		public function get start():int
		{
			return _start;
		}
		
		public function set start(value:int):void
		{
			_start = value;
			if(_start==1){
				playMe();
				//				tmpTween.
			}
			else{
			}
		}
		
		private function addBlock(_x:Number,_y:Number,_style:int):void{
			var name:String = "";
			if (_style == 1) 
			{
				name = "wall_block_1_1";
			}else if(_style == 2){
				name = "trap_wall_door_h";
			}else{
				name = "trap_wall_door_v";
			}
			var imgBlock:Image =  new Image(UtilsAndConfig.defaultAssets.getTexture(name));
			if (_style == 1) 
			{
				imgBlock.smoothing = TextureSmoothing.NONE;
				imgBlock.scaleX = imgBlock.scaleY = 2;
			}
			
			imgBlock.x = _x;
			imgBlock.y = _y;
			myQuad.addImage(imgBlock);
		}
		
		private function playMe():void{
			if(style==VERTICAL){
				goUp = true;
			}
			//			var tmpTween:TweenMax = new TweenMax(instance,1,{});
			//			tmpTween = 
			if(isFirst){
				_delay = 0;
			}
			isFirst = false;
			TweenMax.to(instance,_time_,{delay:_delay,x:endX,y:endY,ease:_statusSpeed,onComplete:function():void{
				_delay = tmpDelay;
				if(style==VERTICAL){
					goUp = false;
				}
				if(repeat == -1){
					goBack();
				}
				else{
					count+=1;
					if(count>repeat){
						return;
					}
					//					TweenMax.delayedCall(_delay,goBack);
					goBack();
				}
			}});
			//			tmpTeen.pause(
		}
		
		private function goBack():void{
			TweenMax.to(instance,_time_,{delay:_delay,x:_params.x + Math.abs(offsetX),y:_params.y + Math.abs(offsetY),delay:_delay,ease:_statusSpeed,onComplete:function():void{
				playMe();
			}});
		}
		
		public function reset():void{
			TweenMax.killDelayedCallsTo(goBack);
			TweenMax.killTweensOf(instance);
			x = _params.x + Math.abs(offsetX);
			y = _params.y + Math.abs(offsetY);
			//			playMe();
			start = tmpStart;
		}
	}
}
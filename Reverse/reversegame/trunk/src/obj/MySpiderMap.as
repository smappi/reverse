package obj
{
	import com.greensock.TweenMax;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	import views.AnimationSpiderMap;
	
	public class MySpiderMap extends Sensor
	{
		public function MySpiderMap(name:String, params:Object=null,style:int = 1)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			var animation:AnimationSpiderMap = new AnimationSpiderMap(style);
			sprite.addChild(animation);
			view = sprite;
			
			
		}
	}
}
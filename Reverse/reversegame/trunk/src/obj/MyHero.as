package obj
{
	import com.greensock.TweenMax;
	
	import citrus.objects.platformer.simple.DynamicObject;
	
	import org.osflash.signals.Signal;
	
	import screens.PlayScreen;
	
	import views.AnimationHero;
	import views.HeroView;
	
	
	public class MyHero extends DynamicObject
	{
		public static var statusHero:int = 0;
		
		public static var STATUS_LEFT_UP:int = 1;
		public static var STATUS_LEFT_DOWN:int = 2;
		public static var STATUS_RIGHT_UP:int = 3;
		public static var STATUS_RIGHT_DOWN:int = 4;
		public static var STATUS_IDLE:int = 5;
		public static var STATUS_EXIT:int = 6;
		
		public var heroView:HeroView;
		private var _params:Object;
		public var onComplete:Signal;
		
		private var _xHero:Number;
		private var _yHero:Number;
		public var finish:Signal;
		public var completeAnimationHide:Signal;
		public var completeLoader:Signal;
		
		public function MyHero(name:String, params:Object)
		{
			trace("MyHeroMyHeroMyHeroMyHeroMyHeroMyHeroMyHeroMyHeroMyHero");
			super(name, params);
			_params = params;
			finish = new Signal();
			completeAnimationHide = new Signal();
			completeLoader = new Signal();
			onComplete = new Signal();
			
			heroView = new HeroView(params.width,params.height,false);
			view = heroView;

//			setAtrr(params.x,params.y,params.width,params.height);
			
			

		}
		
		public function setAtrr(xParam:int,yParam:int,widthParam:int,heightParam:int):void{
			_params.x = xParam;
			_params.y = yParam;
			offsetX = -widthParam/2;
			offsetY = -heightParam/2;
			if(!widthParam){
				x = xParam;
				y = yParam;
			}
			else{
				x = xParam + Math.abs(offsetX);
				y = yParam + Math.abs(offsetY);
			}
			
			group = 100;
			
			heroView.hero.addEventListener(AnimationHero.FINISH,function():void{
				finish.dispatch();
			});
			
			heroView.hero.addEventListener(AnimationHero.DISAPPEAR_COMPLETE,function():void{
				completeAnimationHide.dispatch();
			});
			
			heroView.hero.addEventListener(AnimationHero.LOAD_COMPLETE,function():void{
				completeLoader.dispatch();
			});

			heroView.alpha = 1;
		}
		
		public function get yHero():Number
		{
			return _params.y + Math.abs(offsetY);
		}

		public function get xHero():Number
		{
			return _params.x + Math.abs(offsetX);
		}

		public function changeStatusHeroDie():void{
			heroView.hero.statusDie();
		}
		
		public function changeStatusHeroFinish(arr:Array = null):void{
			heroView.hero.statusFinish();
			width = 0;
			height = 0;
		}
		
		public function resetHero():void{
			width =  _params.width/2;
			height = _params.height/2;
			
		}
		
		public function changeStatusHeroStart():void{
			heroView.hero.statusStart();
		}
		
		public function reset():void{
			heroView.alpha = 0;
			TweenMax.to(this,0.5,{x: _params.x + Math.abs(offsetX),y:_params.y + Math.abs(offsetY),onComplete:function():void{
				heroView.hero.statusStart(30);
				onComplete.dispatch();
				heroView.alpha = 1;
				PlayScreen.gameOver = false;
			}});
		}
	}
}
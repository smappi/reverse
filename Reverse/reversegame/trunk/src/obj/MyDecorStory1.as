package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyDecorStory1 extends CitrusSprite
	{
		public function MyDecorStory1(name:String, params:Object=null,style:int = 0)
		{
			super(name, params);
			
			var sprite:Sprite = new Sprite();
			var img:Image = new Image(UtilsAndConfig.defaultAssets.getTexture(name));
			img.smoothing = TextureSmoothing.NONE;
			img.scaleX = img.scaleY = 2;
			img.alignPivot();
			sprite.addChild(img);
			view = sprite;
			
		}
	}
}
package obj
{
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	
	public class MyRangeBoomSensor extends Sensor
	{

		private var quad:Quad;
		public function MyRangeBoomSensor(name:String, params:Object=null,range:String="")
		{
			super(name, params);
			quad = new Quad(params.width,params.height,0x141414);
			quad.alpha = 0;
			quad.alignPivot();
			view = quad;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
//			TweenMax.to(quad,0.1,{alpha:0.2,repeat:-1,yoyo:true});
		}
		
	}
}
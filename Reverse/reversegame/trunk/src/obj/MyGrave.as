package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyGrave extends Sensor
	{

		private var imgExit:Image;
		public function MyGrave(name:String, params:Object=null,style:String = "")
		{
			super(name, params);
			imgExit = new Image(UtilsAndConfig.defaultAssets.getTexture("bg_graveyard_tomb_"+Number(style)));
			imgExit.smoothing = TextureSmoothing.NONE;
			imgExit.scaleX = imgExit.scaleY = 2;
			imgExit.alpha = 0.4;
			view = imgExit;
			x = params.x;
			y = params.y;
		}
		
		public function setAttr(style:String = ""):void{
			imgExit.texture = UtilsAndConfig.defaultAssets.getTexture("bg_graveyard_tomb_"+Number(style));
		}
	}
}
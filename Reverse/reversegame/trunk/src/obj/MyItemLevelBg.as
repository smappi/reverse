package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyItemLevelBg extends CitrusSprite
	{

		private var imgItem:Image;
		public function MyItemLevelBg(name:String, params:Object=null)
		{
			super(name, params);
			
			imgItem = new Image(UtilsAndConfig.defaultAssets.getTexture(name));
			imgItem.smoothing = TextureSmoothing.NONE;
			imgItem.scaleX = imgItem.scaleY = 2;
			imgItem.x =params.x;
			imgItem.y =params.y;
			view = imgItem;
		}
		public function setAttr(name:String, params:Object=null):void{
			imgItem.texture = UtilsAndConfig.defaultAssets.getTexture(name);
			imgItem.x =params.x;
			imgItem.y =params.y;
		}
	}
}
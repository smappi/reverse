package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyTree extends Sensor
	{

		private var imgExit:Image;
		public function MyTree(name:String, params:Object=null)
		{
			super(name, params);
			imgExit = new Image(UtilsAndConfig.defaultAssets.getTexture("bg_tree"));
			imgExit.smoothing = TextureSmoothing.NONE;
			imgExit.scaleX = imgExit.scaleY = 2;
			imgExit.alpha = 0.4;
			view = imgExit;
			x = params.x;
			y = params.y;
		}
		
		public function setAttr():void{
			imgExit.texture = UtilsAndConfig.defaultAssets.getTexture("imgExit");
		}
	}
}
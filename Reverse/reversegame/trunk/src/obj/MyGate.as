package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyGate extends Sensor
	{
		public function MyGate(name:String, params:Object=null)
		{
			super(name, params);
			var imgExit:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("gate"));
			imgExit.smoothing = TextureSmoothing.NONE;
			imgExit.scaleX = imgExit.scaleY = 2;
			view = imgExit;
			x = params.x;
			y = params.y;
		}
	}
}
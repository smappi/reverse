package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class MyLevelSensor extends Sensor
	{
		public function MyLevelSensor(name:String, params:Object=null,number:String = "")
		{
			super(name, params);
			
			var listP:Array = String(number).split(";");
			var sprite:Sprite = new Sprite();
			for (var i:int = 0; i < listP.length; i++) 
			{
				var sP:String = listP[i];
				var p:Array = sP.split(",");
				var btLevel:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("btlevel"));
				btLevel.alignPivot();
				btLevel.smoothing = TextureSmoothing.NONE;
				btLevel.scaleX = btLevel.scaleY = 2;
				btLevel.x = Number(p[0]);
				btLevel.y = Number(p[1]);
				sprite.addChild(btLevel);
				btLevel.addEventListener(TouchEvent.TOUCH,function(e:TouchEvent):void{
					trace("a");
				});
				
			}
			
			view = sprite;
		}
	}
}
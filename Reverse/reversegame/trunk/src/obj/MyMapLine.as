package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.CitrusSprite;
	
	import starling.display.Shape;
	import starling.display.Sprite;
	
	public class MyMapLine extends CitrusSprite
	{

		private var shape:Shape;

		private var count:int=0;

		private var _list:Array;

		private var shape1:Shape;

		private var saveArr:Array;
		
		private  var heightMiss:Number = 0;
		private var y_:Number = 0;
		public function MyMapLine(name:String, params:Object=null,list:String = null,heightMiss:Number = 0)
		{
			super(name, params);
			heightMiss = heightMiss;
			_list = String(list).split(";");
			
			x = _list[0].split(",")[0];
			y = _list[0].split(",")[1];
			y_ = y;
			y += heightMiss;
			shape = new Shape();
			shape1 = new Shape();
			var sprite:Sprite = new Sprite();
			shape.graphics.lineStyle(10, 0x000000, 1);
			shape1.graphics.lineStyle(14, 0x000000, 0.3);
			
			shape.graphics.endFill();
			sprite.addChild(shape);
			sprite.addChild(shape1);
			view = sprite;
		}
		
		/**
		 *	Truyền số level hoàn thành thì sẽ vẽ tới level tiếp theo 
		 * @param numDraw
		 * 
		 */		
		public function addLine(numDraw:int=0):void{
			if(numDraw ==0){
				return;
			}
			for (var i:int = 1; i < numDraw; i++) 
			{
				shape.graphics.lineTo(_list[i].split(",")[0] - x, _list[i].split(",")[1] - y_);
				shape1.graphics.lineTo(_list[i].split(",")[0] - x, _list[i].split(",")[1] - y_);
				
			}
			saveArr = [_list[i-1].split(",")[0] - x, _list[i-1].split(",")[1]- y_];
		}
		
		public function tweenAllLine():void{
			if(count==_list.length){
				shape.graphics.endFill();
				return;
			}
			var firstX:int;
			var firstY:int;
			var nextX:int;
			var nextY:int;
			if(count == 0){
				firstX = 0;
				firstY = 0;
			}
			else{
				firstX =_list[count-1].split(",")[0] - x;
				firstY =_list[count-1].split(",")[1] - y_;
			}
			nextX = _list[count].split(",")[0] - x;
			nextY = _list[count].split(",")[1] - y_;
			count++;
		}
		
		public function tweenLine(nextArr:Array,completeFunc:Function = null):void{
			var objs:Object = {x:saveArr[0],y:saveArr[1]};
			TweenMax.to(objs,0.5,{x:nextArr[0]-x,y:nextArr[1] - y,ease:Linear.easeNone,onUpdate:function():void{
				shape.graphics.lineTo(Math.floor(objs.x), Math.floor(objs.y));
				shape1.graphics.lineTo(Math.floor(objs.x), Math.floor(objs.y));
			},onComplete:completeFunc});
		}
	}
}
package obj
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationWheel;
	
	public class MyWheel extends Sensor
	{

		private var spin:Image;
		private var _speed:Number;
		private var _listWheels:Array = [];

		private var quadW:Quad;

		private var quadT:Quad;

		private var sprite:Sprite;

		private var quadB:Quad;

		private var heroAnimation:AnimationWheel;
		public function MyWheel(name:String, params:Object=null, speed:Number = 0)
		{
			super(name,params);
			
			sprite = new Sprite();
			sprite.alignPivot();
			quadW = new Quad(16,16,0xff0000);
			quadW.alignPivot();
			quadW.alpha = 0;
			sprite.addChild(quadW);
			listWheels.push(quadW);
			
			quadT = new Quad(32,32,0xff0000);
			quadT.pivotX = quadT.width + 24;
			quadT.pivotY = quadT.height+ 24;
			quadT.alpha = 0;
			sprite.addChild(quadT);
			listWheels.push(quadT);
			
			quadB = new Quad(32,32,0xff0000);
			quadB.pivotX = - 24;
			quadB.pivotY = - 24;
			quadB.alpha = 0;
			sprite.addChild(quadB);
			listWheels.push(quadB);
			
			heroAnimation = new AnimationWheel();
			sprite.addChild(heroAnimation);
			
			offsetX = -heroAnimation.width;
			offsetY = -heroAnimation.height;
			width = heroAnimation.width;
			height = heroAnimation.height;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
			view = sprite;
			
			
//			tweenMe();
		}
		
		public function setAttr(params:Object=null, speed:Number = 0):void{
			listWheels = [];
			heroAnimation.rotation = quadT.rotation = quadT.rotation = quadW.rotation = 0;
			offsetX = -heroAnimation.width;
			offsetY = -heroAnimation.height;
			width = heroAnimation.width;
			height = heroAnimation.height;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		
		public function tweenMe():void{
			UtilsAndConfig.playSound("trap_wheel");
			TweenMax.to([quadW,quadT,quadB,heroAnimation],500,{rotation:"360",ease:Linear.easeNone});
		}
		
		public function get listWheels():Array
		{
			return _listWheels;
		}

		public function set listWheels(value:Array):void
		{
			_listWheels = value;
		}
	}
}
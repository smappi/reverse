package obj
{
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Shape;
	import starling.display.Sprite;
	
	public class MyPolygon extends Sensor
	{
		public function MyPolygon(name:String, params:Object=null,polygon:Array = null)
		{
			super(name, params);
			var shape:Shape = new Shape();
			shape.graphics.lineStyle(2, 0x141414, 0.3);
			shape.graphics.beginFill(0x141414,1);
			for (var i:int = 1; i < polygon.length; i++) 
			{
				var objP:Object = polygon[i];
				shape.graphics.lineTo(Number(objP.x),Number(objP.y));
			}
			
			shape.graphics.endFill();
			var sprite:Sprite = new Sprite();
			sprite.addChild(shape);
			view = sprite;
		}
	}
}
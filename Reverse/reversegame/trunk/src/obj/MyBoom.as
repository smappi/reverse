package obj
{
	
	import com.greensock.TweenMax;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationBoom;
	
	public class MyBoom extends Sensor
	{
		private var heroAnimation:AnimationBoom;
		private var range_:Number;
		public var onBoom:Signal;

		private var sprite:Sprite;
		public var isDie:Boolean = true;
		private var _params:Object;
		private var flagPlay:Boolean = false;

		private var quad:Quad;
		public function MyBoom(name:String, params:Object=null, range:String="")
		{
			super(name, params);
			onBoom = new Signal();
			_params = params;
			quad = new Quad(int(range)*2,int(range)*2,0x141414);
			quad.alignPivot();
			quad.alpha = 0;
			heroAnimation = new AnimationBoom();
			sprite = new Sprite();
			sprite.addChild(quad);
			sprite.addChild(heroAnimation);
			range_ = Number(range);
			view = sprite;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		
		public function boom():void{
			if(width ==0 || flagPlay){
				return;
			}
			flagPlay = true;
			heroAnimation.play();
			TweenMax.to(quad,0.15,{alpha:0.2,repeat:-1});
			TweenMax.delayedCall(3,scaleSensorBom);
			UtilsAndConfig.playSound("readyBoom");
		}
		
		private function scaleSensorBom():void{
			width = range_*2;
			height = range_*2;
			heroAnimation.boom(Number(range_*2/64));
			UtilsAndConfig.playSound("Boom");
			TweenMax.delayedCall(0.8,resetBoom);
		}
		
		private function resetBoom():void{
			quad.alpha = 0;
			onBoom.dispatch();
			width = 0;
			height = 0;
			heroAnimation.alpha = 0;
			TweenMax.killTweensOf(quad);
		}
		
		public function reset():void{
			tmpReset();
			heroAnimation.stop();
			tmpReset();
			TweenMax.killDelayedCallsTo(scaleSensorBom);
			TweenMax.killDelayedCallsTo(resetBoom);
			TweenMax.killTweensOf(quad);
			TweenMax.delayedCall(1.5,tmpReset);
			TweenMax.delayedCall(1,tmpReset);
			TweenMax.delayedCall(2,tmpReset);
			quad.alpha = 0;
		}
		
		private function tmpReset():void{
			flagPlay = false;
			width = _params.width;
			height = _params.height;
			heroAnimation.alpha = 1;
			heroAnimation.initView();
		}
	}
}
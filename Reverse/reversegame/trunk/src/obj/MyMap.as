package obj
{
	import com.greensock.TweenMax;
	
	import citrus.objects.CitrusSprite;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class MyMap extends CitrusSprite
	{
		
		private var _color_:int = 0;
		private var colors:Array = [0x7D7F79,0x7D7F78];
		private var quad:Quad;

		private var quadTmp:Quad;
		public function MyMap(name:String, params:Object=null)
		{
			trace("MyMapMyMapMyMapMyMapMyMapMyMapMyMapMyMapMyMap");
			super(name, params);
			color_ = colors[int(Math.random() * colors.length)];
			var sprite:Sprite = new Sprite();
			quad = new Quad(params.width,params.height,color_);
			quad.alpha = 0.8;
			sprite.addChild(quad);
			
			quadTmp = new Quad(params.width,params.height,color_);
			quadTmp.alpha = 0;
			sprite.addChild(quadTmp);
			view = sprite;
			x = params.x;
			y = params.y;
		}
		
		public function setAttr(params:Object):void{
			quad.width = params.width;
			quad.height = params.height;
			
			quadTmp.width = params.width;
			quadTmp.height = params.height;
			
			x = params.x;
			y = params.y;
		}
		
		public function reset():void{
			var randomPos:int = 0;
			randomPos = int(Math.random() * colors.length);
			var tmpColor:uint = randomPos;
			while(colors[randomPos] == color_){
				randomPos = int(Math.random() * colors.length);
			}
			color_ = colors[randomPos];
			
			if(quad.alpha==0){
				quad.color = colors[randomPos];
				TweenMax.to(quad,2,{alpha:1});
				TweenMax.to(quadTmp,2,{alpha:0});
			}
			else{
				quadTmp.color = colors[randomPos];
				TweenMax.to(quad,2,{alpha:0});
				TweenMax.to(quadTmp,2,{alpha:1});
			}
		}

		public function get color_():int
		{
			return _color_;
		}

		public function set color_(value:int):void
		{
			_color_ = value;
		}
	}
}
package obj
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationBeam;
	import views.AnimationGun;
	
	public class MyGun extends Sensor
	{
		
		private var animationGun:AnimationGun;
		
		private var listBeam:Array;
		private var _style:String;
		public var _lengthRange:int;
		private var _time:uint;
		private var _widthGun:uint;
		private var _heightGun:uint
		private var i:int;
		private var _xX:Number;
		private var _yY:Number;
		private var _params:Object;
		public var hero:MyHero;
		
		private var sprite:Sprite;
		
		private var _lengthBeam:int;
		private var _id:int;
		
		private var _status_:int;
		
		private var saveStatus:int;
		
		public function MyGun(name:String, params:Object=null,style:String = "",time:int = 0,_length_:uint = 96,_id_:int = 0,_status:int = -1)
		{
			super(name, params);
			_id = _id_;
			lengthBeam = _length_;
			_widthGun = params.width;
			_heightGun = params.height;
			trace(_widthGun);
			trace(_heightGun);
			_style = style;
			_lengthRange = _length_;
			
			_time = time;
			_params = params;
			sprite = new Sprite();
			animationGun = new AnimationGun();
			sprite.addChild(animationGun);
			listBeam = [];
			var count:uint = _lengthRange/32;
			switch(style)
			{
				case "left":
				{
					offsetX = -params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 +_lengthRange/2;
					y = params.y + height/2 + 2;
					for ( i= 0; i < count; i++) 
					{
						listBeam[i] = new AnimationBeam();
						(listBeam[i] as AnimationBeam).x = animationGun.x + animationGun.width/2 + i*32 + params.width/2 + (listBeam[i] as AnimationBeam).width/2;
						(listBeam[i] as AnimationBeam).y = - params.height/2 + (listBeam[i] as AnimationBeam).height/2;
						sprite.addChild(listBeam[i]);
						//						width += (listBeam[i] as AnimationBeam).width;
						listBeam[i].pause();
					}
					
					break;
				}
					
				case "right":
				{
					
					offsetX = params.width*(_lengthRange/32)/2;
					animationGun.scaleX = -1;
					x = params.x + width/2 - _lengthRange/2;
					y = params.y + height/2;
					for ( i = 0; i < count; i++) 
					{
						listBeam[i] = new AnimationBeam();
						(listBeam[i] as AnimationBeam).x = animationGun.x - animationGun.width/2 +i*32 - 48 + (listBeam[i] as AnimationBeam).width/2;
						(listBeam[i] as AnimationBeam).y = -height/2 + (listBeam[i] as AnimationBeam).height/2;
						(listBeam[i] as AnimationBeam).scaleX = -1;
						sprite.addChild(listBeam[i]);
					}
					break;
				}
					
				case "top":
				{
					animationGun.rotation = deg2rad(90);
					offsetY = -params.height/2;
					x = params.x + width/2;
					y = params.y + height/2;
					for ( i = 0; i < count; i++) 
					{
						listBeam[i] = new AnimationBeam();
						(listBeam[i] as AnimationBeam).rotation = deg2rad(90);
						(listBeam[i] as AnimationBeam).y = animationGun.y + animationGun.height/2 +i*32 + (listBeam[i] as AnimationBeam).height/2;
						(listBeam[i] as AnimationBeam).x = -width/2 + (listBeam[i] as AnimationBeam).width/2;
						sprite.addChild(listBeam[i]);
					}
					break;
				}
					
				case "bottom":
				{
					offsetY = params.height*(_lengthRange/32)/2;
					animationGun.rotation = -deg2rad(90);
					x = params.x + width/2;
					y = params.y + height/2 -_lengthRange/2 ;
					for ( i = 0; i < count; i++) 
					{
						listBeam[i] = new AnimationBeam();
						(listBeam[i] as AnimationBeam).scaleX = -1;
						(listBeam[i] as AnimationBeam).rotation = deg2rad(90);
						(listBeam[i] as AnimationBeam).y = animationGun.y - animationGun.height/2 -i*32 - 48 + (listBeam[i] as AnimationBeam).height/2;
						(listBeam[i] as AnimationBeam).x = -width/2 + (listBeam[i] as AnimationBeam).width/2;
						sprite.addChild(listBeam[i]);
					}
					break;
				}
			}
			
			_xX = x;
			_yY = y;
			
			view = sprite;
			
			if(_status ==0){
				_status = -1;
			}
			status = -1;
			saveStatus = -1;
			
			animationGun.complete.add(genarateBeam);
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function get status():int
		{
			return _status_;
		}
		
		public function set status(value:int):void
		{
			_status_ = value;
			if(_status_==-1){
				UtilsAndConfig.stopSound("trap_laser");
				stop();
			}
			else{
				var distance:Number = Point.distance(new Point(x,y),new Point(hero.x,hero.y));
				if(distance<400){
					if(Reverse.instance.sound.getSound("trap_laser").isPlaying == false){
						UtilsAndConfig.playSound("trap_laser");
					}
					setVolumeSound(distance);
				}
				else{
					UtilsAndConfig.stopSound("trap_laser");
				}
				
				shoot();
			}
		}
		
		private function setVolumeSound(distance:Number):void{
			if(distance>300){
				
				Reverse.instance.sound.getSound("trap_laser").volume = 0.3;
			}
			else if(distance>200){
				Reverse.instance.sound.getSound("trap_laser").volume = 0.6;
			}
			else{
				Reverse.instance.sound.getSound("trap_laser").volume = 1;
			}
		}
		
		public function get params():Object
		{
			return _params;
		}
		
		public function set params(value:Object):void
		{
			_params = value;
		}
		
		public function get widthGun():uint
		{
			return _widthGun;
		}
		
		public function set widthGun(value:uint):void
		{
			_widthGun = value;
		}
		
		public function get lengthBeam():int
		{
			return _lengthBeam;
		}
		
		public function set lengthBeam(value:int):void
		{
			_lengthBeam = value;
		}
		
		public function get style():String
		{
			return _style;
		}
		
		public function set style(value:String):void
		{
			_style = value;
		}
		
		private function shoot():void{
			TweenMax.delayedCall(_time,function():void{
				animationGun.shoot();
			});
		}
		
		private function genarateBeam():void{
			for each (var i:AnimationBeam in listBeam) 
			{
				i.play();
			}
			resetSize();
			
			switch(_style)
			{
				case "left":
				{
					offsetX = -_params.width*(lengthBeam/32)/2;
					x = _params.x + _widthGun/2 +lengthBeam/2;
					break;
				}
					
				case "right":
				{
					offsetX = _params.width*(lengthBeam/32)/2;
					x = _params.x + _widthGun/2 -lengthBeam/2;
					break;
				}
					
				case "top":
				{
					offsetY = -params.height*(lengthBeam/32)/2;
					y = params.y + _heightGun/2 + lengthBeam/2 ;
					break;
				}
					
				case "bottom":
				{
					offsetY = params.height*(lengthBeam/32)/2;
					y = params.y + _heightGun/2 -lengthBeam/2 ;
					break;
				}
					
				default:
				{
					break;
				}
			}
			
		}
		
		private function stop():void{
			animationGun.stop();
			
			width = _widthGun;
			height = params.height;
			
			switch(_style)
			{
				case "left":
				{
					offsetX = 0;
					x = _params.x + _widthGun/2;
					break;
				}
					
				case "right":
				{
					offsetX = 0;
					x = _params.x + _widthGun/2;
					break;
				}
					
				case "top":
				{
					offsetY = 0;
					y = _params.y + height/2;
					break;
				}
					
				case "bottom":
				{
					offsetY = 0;
					y = _params.y + height/2;
					break;
				}
					
				default:
				{
					break;
				}
			}
			for each (var i:AnimationBeam in listBeam) 
			{
				i.stop();
			}
		}
		
		
		public function reSizeColliteLess(value:int):void{
			
			if(_style == "left" || _style == "right"){
				_lengthRange -= (value);
				width = _widthGun + _lengthRange;
				
				offsetX = -params.width*(_lengthRange/32)/2;
				x = params.x + _widthGun/2 +_lengthRange/2;
			}
			else{
				
			}
			
			var numBeam:int  = listBeam.length - _lengthRange/32;
			//			if(style == "left"){
			for (var k:int = listBeam.length - 1; k > listBeam.length - numBeam; k--) 
			{
				listBeam[k].alpha = 0;
			}
		}
		
		public function lessVertical(value:int):void{
			
			switch(_style)
			{
				case "left":
				{
					_lengthRange = _lengthRange - (_lengthRange - (value - params.x) ) - 32;
					offsetX = -params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "right":
				{
					_lengthRange = _lengthRange - (_lengthRange - (params.x -value) );
					offsetX = _params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				case "top":
				{
					_lengthRange = _lengthRange - (_lengthRange - (value - params.y) ) - 32;
					offsetY = -params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "bottom":
				{
					_lengthRange = _lengthRange - (_lengthRange - (params.y -value) );
					offsetY = _params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			for (var j:int = lengthBeam/32-1; j >= _lengthRange/32; j--) 
			{
				if(j<0){
					break;
				}
				listBeam[j].alpha = 0;
			}
			if(style == "left" || style == "right"){
				width = _widthGun + _lengthRange;
			}
			else{
				height = _widthGun + _lengthRange;
			}
			
		}
		
		public function tmpLess():void{
			_lengthRange -= (32);
			if(style == "left" || style == "right"){
				width = _widthGun + _lengthRange;
			}
			else{
				height = _widthGun + _lengthRange;
			}
			
			switch(style)
			{
				case "left":
				{
					offsetX = -params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "right":
				{
					offsetX = _params.width*(_lengthRange/32)/2;
					x = _params.x + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				case "top":
				{
					offsetY = -params.height*(_lengthRange/32)/2;
					y = params.y + _heightGun/2 +_lengthRange/2;
					break;
				}
					
				case "bottom":
				{
					offsetY = _params.height*(_lengthRange/32)/2;
					y = _params.y + _heightGun/2 -_lengthRange/2;
					break;
				}
					
				default:
				{
					break;
				}
			}
			if(_lengthRange/32 < listBeam.length){
				listBeam[_lengthRange/32].alpha = 0;
			}
			
		}
		
		public function reSizeColliteGreat(value:Number):void{
			_lengthRange += (value);
			if(_style == "left" || _style == "right"){
				width = _widthGun + _lengthRange;
			}
			else{
				height = _widthGun + _lengthRange;
			}
			
			switch(_style)
			{
				case "left":
				{
					offsetX = -params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "right":
				{
					offsetX = _params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				case "top":
				{
					offsetY = -params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "bottom":
				{
					offsetY = _params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				default:
				{
					break;
				}
			}
			if(_lengthRange/32 -2 < listBeam.length){
				listBeam[_lengthRange/32 -2].alpha = 1;
			}
		}
		
		public function resetSize():void{
			_lengthRange = lengthBeam;
			
			
			switch(style)
			{
				case "left":
				{
					width = _widthGun + _lengthRange;
					offsetX = -params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "right":
				{
					width = _widthGun + _lengthRange;
					offsetX = _params.width*(_lengthRange/32)/2;
					x = params.x + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				case "top":
				{
					height = _widthGun + _lengthRange;
					offsetY = -params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 +_lengthRange/2;
					break;
				}
					
				case "bottom":
				{
					height = _widthGun + _lengthRange;
					offsetY = _params.height*(_lengthRange/32)/2;
					y = params.y + _widthGun/2 -_lengthRange/2;
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			for each (var i:AnimationBeam in listBeam) 
			{
				i.alpha = 1;
			}
		}
		
		public function reset():void{
			resetSize();
			status = saveStatus;
		}
	}
}
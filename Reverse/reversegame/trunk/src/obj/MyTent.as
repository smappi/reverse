package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	public class MyTent extends CitrusSprite
	{
		private var heroAnimation:MovieClip;
		public function MyTent(name:String, params:Object=null)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures(name), 10);
			heroAnimation.alignPivot();
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 4;
			sprite.addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			view = sprite;
		}
	}
}
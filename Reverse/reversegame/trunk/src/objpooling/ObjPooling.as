package objpooling
{
	import utils.UtilsAndConfig;
	
	public class ObjPooling
	{
		private static var typeArr:Array = [];
		private static var poolingArr:Array = [];
		private static var counterArr:Array = [];
		
		
		public function ObjPooling(type:Class, len:int)
		{
			
		}
		
		public static function summon(... args):Object
		{
			var obj:*;
			var type:Class = args[0];
			var pool:Array;
			var counter:int = 1; //Khoi tao luon luon co 1 object
			if(typeArr.indexOf(type) < 0){
				typeArr.push(type);
				//Neu chua co type nay, them vao mang type,counter, dong thoi tao new type()
				pool = [];
				obj = returnNewType(type,args);
				if(obj){
					pool.push(obj);
					poolingArr.push(pool);
					counterArr.push(counter);
				}
			}
			
//			trace("summon typeArr",typeArr.indexOf(type),type);
			//Neu co roi, lay tu` type array, counter array
			pool = poolingArr[typeArr.indexOf(type)];
			counter = counterArr[typeArr.indexOf(type)];
			
			
			
			if(counter > 0){
				
				//Neu van~ con object trong array, tra ve object
				counter--;
				
				obj = pool[counter];
//				trace("summon 1 ",counter,counterArr[typeArr.indexOf(type)]);
			}
			else{
//				trace("summon 2");
				//Neu het object trong array, them object moi
//				counter++;
				obj = returnNewType(type,args);
				if(obj){
					pool.push(obj);
				}
			}
			
			counterArr[typeArr.indexOf(type)] = counter;
			
			if(obj == null){
				throw new Error("Object null, cannot return! Lien he MrKhanh de biet them chi tiet "+args.length+"  "+args);
			}
			
//			trace("summon counter",counter,obj);
			return obj;
		}
		
		public static function release(s:Object):Boolean
		{
			stopMusic();
			var type:Class = UtilsAndConfig.getClass(s);
			var pool:Array = poolingArr[typeArr.indexOf(type)];
			
			var counter:int = counterArr[typeArr.indexOf(type)];
			
			if(pool && counter >= 0) {
//				trace("release",type,typeArr.indexOf(type),pool,counter);
				pool[counter] = s;
				counter++;
				counterArr[typeArr.indexOf(type)] = counter;
				return true;
			}
			return false;
		}
		
		private static function stopMusic():void{
			if (!UtilsAndConfig.playSound("trap_wheel")) 
			{
				UtilsAndConfig.stopSound("trap_wheel");
			}
		}
		
		private static function returnNewType(type:Class,args:Array):*{
			
			switch(args.length)
			{
				case 2:
				{
					return new type(args[1]);
					break;
				}
					
				case 3:
				{
					return new type(args[1],args[2]);
					break;
				}
					
				case 4:
				{
					return new type(args[1],args[2],args[3]);
					break;
				}
				case 5:
				{
					return new type(args[1],args[2],args[3],args[4]);
					break;
				}
					
				case 6:
				{
					return new type(args[1],args[2],args[3],args[4],args[5]);
					break;
				}
					
				case 7:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6]);
					break;
				}
					
				case 8:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
					break;
				}
					
				case 9:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8]);
					break;
				}
					
				case 10:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9]);
					break;
				}
					
				case 11:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10]);
					break;
				}
					
				case 12:
				{
					return new type(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11]);
					break;
				}
			}
			return null;
			
		}
		
	}
}
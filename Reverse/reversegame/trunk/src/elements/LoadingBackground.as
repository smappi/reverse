package elements
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class LoadingBackground extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function LoadingBackground()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		protected function init(event:Event):void
		{
			// TODO Auto-generated method stub
			this.removeEventListener(Event.ADDED_TO_STAGE,init);
			
			var quad:Quad = new Quad(stage.stageWidth, stage.stageHeight,0x000000);
			addChild(quad);
			
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("wghost"), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			heroAnimation.x =100;
			heroAnimation.y =100;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
	}
}
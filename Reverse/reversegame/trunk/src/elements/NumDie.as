package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class NumDie extends Sprite
	{

		private var text:TextSprite;
		private var _numDie:int;
		public function NumDie(numDie:int = 0)
		{
			_numDie = numDie;
			var die:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("numDie"));
			die.scaleX = die.scaleY = 2;
			die.smoothing = TextureSmoothing.NONE;
			addChild(die);
			
			text = new TextSprite("0","AlphaRuler",55,0x000000);
			text.alignPivot();
			text.x = die.x + die.width + 10 + text.width/2; 
			text.y = die.height/2;
			addChild(text);
		}
		
		public function tweenMe():void{
			var obj:Object = {value:0};
			var time:Number;
			TweenMax.to(obj,_numDie/10,{value:_numDie,ease:Linear.easeNone,onUpdate:function():void{
				text.text = ""+Math.floor(obj.value);
			}});
		}
	}
}
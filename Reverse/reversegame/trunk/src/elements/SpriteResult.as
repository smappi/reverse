package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import starling.display.Sprite;
	
	public class SpriteResult extends Sprite
	{

		private var textTime:AnimationTime;

		private var die:NumDie;
		public function SpriteResult(time:Number=0,dieNum:int=0)
		{
			textTime = new AnimationTime(Math.floor(time/1000));
			addChild(textTime);
			
			die = new NumDie(dieNum);
			die.alignPivot();
			die.y = textTime.height/2;
			addChild(die);
			
			textTime.x = -textTime.width/2 - die.width/2;
			die.x = textTime.width + 80 + die.width/2 -textTime.width/2;
			
			tweenMe();
		}
		
		public function tweenMe():void{
			TweenMax.delayedCall(0.5,function():void{
				TweenMax.from(textTime,0.15,{alpha:1 ,ease:Back.easeOut,onComplete:function():void{
					textTime.tweenText();
					TweenMax.from(die,0.15,{delay:0.2,alpha:1,ease:Back.easeOut,onComplete:function():void{
						die.tweenMe();
					}});
			}});});
		}
	}
}
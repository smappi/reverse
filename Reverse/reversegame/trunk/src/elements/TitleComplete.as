package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationTitleComplete;
	
	public class TitleComplete extends Sprite
	{

		private var sprite:Sprite;

		private var title:AnimationTitleComplete;
		private var btnShare:ButtonCustom;
		public function TitleComplete(level:String = "0",score:int = 999)
		{
			sprite = new Sprite();
			title = new AnimationTitleComplete();
			sprite.addChild(title);
			
			var text:TextSprite = new TextSprite("STAGE  "+level,"mini_pixel-7",45,0x7d7f79);
			text.x = title.x + title.width/2 - text.width/2;
			text.y = title.y + 130;
			sprite.addChild(text);
			
			
			var text1:TextField = new TextField(300,60,"0","mini_pixel-7",55,0x7d7f79);
			text1.alignPivot();
			text1.x = title.width/2;
			text1.y = title.height/2 + 100;
			sprite.addChild(text1);
			
			var obj:Object = {_value:0};
			TweenMax.to(obj,10,{_value:score,ease:Linear.easeNone,onUpdate:function():void{
				if (int(text1.text) >= score) 
				{
					text1.text = score+"";
					TweenMax.killTweensOf(obj);
				}else{
					text1.text = int(text1.text) + 123 +"";
				}
				
			}});
			
			btnShare = new ButtonCustom("btShare",0x7d7f79);
			btnShare.alignPivot();
			
			btnShare.x = sprite.width/2;
			btnShare.y = text1.y + text1.height/2 + 100;
			sprite.addChild(btnShare);
			
			addChild(sprite);
			tweenMe();
			UtilsAndConfig.playSound("bat_wing_flap");
		}
		
		private function tweenMe():void{
			
			TweenMax.delayedCall(0.08,function():void{
				sprite.y = 0;
				title.animation.currentFrame = 0;
				TweenMax.delayedCall(0.08,function():void{
					sprite.y = 2;
					title.animation.currentFrame = 1;
					TweenMax.delayedCall(0.08,function():void{
						sprite.y = 4;
						title.animation.currentFrame = 2;
						TweenMax.delayedCall(0.08,function():void{
							sprite.y = 0;
							title.animation.currentFrame = 3;
							TweenMax.delayedCall(0.08,function():void{
								sprite.y = -2;
								title.animation.currentFrame = 4;
								tweenMe();
							});
						});
					});
				});
			});
		};
	}
}
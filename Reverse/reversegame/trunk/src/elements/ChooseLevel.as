package elements
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Sprite;
	
	public class ChooseLevel extends CitrusSprite
	{

		private var imgChooseLevel:Supernova;

		public function ChooseLevel(name:String, params:Object=null)
		{
			super(name, params);
			sprite = new Sprite();
			imgChooseLevel = new Supernova();
			sprite.addChild(imgChooseLevel);
			
			view = sprite;
			group = 10;
		}
		
		private var sprite:Sprite;
		
		public function tweenMe():void{
			imgChooseLevel.tweenMe();
		}
	}
}
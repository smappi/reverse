package elements
{
	import starling.display.Image;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	public class TitleGame extends Sprite
	{
		public function TitleGame()
		{
			var imgTitle:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("titleGame"));
//			imgTitle.smoothing = TextureSmoothing.NONE;
//			imgTitle.scaleX = imgTitle.scaleY = 2;
			addChild(imgTitle);
			
			var imgAdventure:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("adventure_story1"));
			imgAdventure.alignPivot();
//			imgAdventure.smoothing = TextureSmoothing.NONE;
//			imgAdventure.scaleX = imgAdventure.scaleY = 2;
			imgAdventure.x = imgTitle.width/2;
			imgAdventure.y = imgTitle.height - 5 + imgAdventure.height/2;
			addChild(imgAdventure);
			
			var fire1:AnimationFireStory1 = new AnimationFireStory1();
			fire1.alignPivot();
			fire1.x = imgAdventure.x - imgAdventure.width/2 - fire1.width/2;
			fire1.y = imgAdventure.y - 10;
			addChild(fire1);
			
			var fire2:AnimationFireStory1 = new AnimationFireStory1();
			fire2.alignPivot();
			fire2.x = imgAdventure.x + imgAdventure.width/2 + fire2.width/2;
			fire2.y = fire1.y;
			addChild(fire2);
		}
	}
}
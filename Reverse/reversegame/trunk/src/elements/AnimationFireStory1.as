package elements
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	public class AnimationFireStory1 extends Sprite
	{
		public function AnimationFireStory1()
		{
			var animation:MovieClip = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("fire_story1_"),15);
//			animation.smoothing = TextureSmoothing.NONE;
//			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.play();
			Starling.juggler.add(animation);
		}
	}
}
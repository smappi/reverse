package elements
{
	import com.greensock.TweenMax;
	
	import starling.display.Sprite;
	
	public class SpritePause extends Sprite
	{

		private var popup:PopupPause;

		private var cord:Cord;
		private var _color:uint;

		private var skipLevel:SkipLevel;
		public function SpritePause(color:uint = 0xff8282,isOnLevel:Boolean = false)
		{
			TweenMax.pauseAll();
			cord = new Cord(color);
			cord.reAddMe();
			addChild(cord);
			
			popup = new PopupPause(color,isOnLevel);
			addChild(popup);
			
			
			skipLevel = new SkipLevel(color);
			popup.addEventListener(PopupPause.SKIP,function():void{
				addChild(skipLevel);
				popup.removeMe();
				skipLevel.tweenMe();
			});
			skipLevel.addEventListener(SkipLevel.BACK,function():void{
				skipLevel.removeMe();
				TweenMax.delayedCall(0.3,function():void{
					popup.reAddMe();
				});
				
			});
			popup.addEventListener(PopupPause.RESUME,removeMe);
		}
		
		public function get color():uint
		{
			return _color;
		}

		public function set color(value:uint):void
		{
			cord.colorQuad = value;
			popup.color = value;
		}

		private function removeMe():void{
			popup.removeMe();
			TweenMax.delayedCall(1,remove);
			cord.removeMe();
		}
		
		private function remove():void{
			TweenMax.resumeAll();
			removeFromParent(true);
		}
		
		public function tweenMe():void{
			popup.tweenMe();
		}
		
	}
}
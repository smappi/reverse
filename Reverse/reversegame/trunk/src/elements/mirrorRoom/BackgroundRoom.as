package elements.mirrorRoom
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class BackgroundRoom extends CitrusSprite
	{
		public function BackgroundRoom(name:String, params:Object=null,num:int = 5)
		{
			super(name, params);
			x = params.x;
			y = params.y;
			
			var sprite:Sprite = new Sprite();
			for (var i:int = 0; i < num; i++) 
			{
				var imgBackground:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("backgroundRoomMirror"));
				imgBackground.smoothing = TextureSmoothing.NONE;
				imgBackground.scaleX = imgBackground.scaleY = 2;
				imgBackground.x = i*imgBackground.width;
				sprite.addChild(imgBackground);
			}
//			var quad:Quad = new Quad(imgBackground.width*num,imgBackground.height + 100,0x6f716a)
//			sprite.addChild(quad);
//			sprite.setChildIndex(quad,0);
//			
//			var background:QuadBackground = new QuadBackground(quad.width,quad.height);
//			sprite.addChild(background);
//			sprite.setChildIndex(background,1);
			
			view = sprite;
		}
	}
}
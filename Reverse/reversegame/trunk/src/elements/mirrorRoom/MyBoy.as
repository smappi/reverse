package elements.mirrorRoom
{
	import citrus.objects.platformer.simple.DynamicObject;
	
	import elements.mirrorRoom.animation.TheBoy;
	
	import starling.display.Sprite;
	
	public class MyBoy extends DynamicObject
	{
		public var theBoy:TheBoy;
		public var status:String = "idle";
		public var flagMove:Boolean = false;
		
		public function MyBoy(name:String, params:Object=null,isPivot:Boolean = true)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			theBoy = new TheBoy();
			sprite.addChild(theBoy);
			
			view = sprite; 
			group = 10;
			width = theBoy.width;
			height = theBoy.height;
			
			offsetX = -width/2;
			offsetY = -height/2;
			if(isPivot){
				theBoy.x = theBoy.width/2;
				theBoy.y = theBoy.height/2;
			}
			
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		
		public function lookLeft():void{
//			if(statusLook == StatusAnimation.LOOK_LEFT){
//				return;
//			}
			trace("left");
			theBoy.statusLook = StatusAnimation.LOOK_LEFT;
			theBoy.theBoy.scaleX = -2;
		}
		
		public function lookRight():void{
//			if(statusLook == StatusAnimation.LOOK_RIGHT){
//				return;
//			}
			trace("right");
			theBoy.statusLook = StatusAnimation.LOOK_RIGHT;
			theBoy.theBoy.scaleX = 2;
		}
		
		public function move():void{
			status = "move";
			theBoy.move();
		}
		
		public function idle():void{
			status = "idle";
			theBoy.idle();
		}
	}
}
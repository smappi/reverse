package elements.mirrorRoom
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Quad;
	
	public class CollisionBreakMirror extends CitrusSprite
	{
		public function CollisionBreakMirror(name:String, params:Object=null)
		{
			super(name, params);
			params.width = 30;
			params.height = 30;
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			
			x = params.x + Math.abs(offsetX) + 50;
			y = params.y + Math.abs(offsetY) - 10;
			var quad:Quad = new Quad(30,30);
			quad.alpha = 0;
			view = quad;
		}
	}
}
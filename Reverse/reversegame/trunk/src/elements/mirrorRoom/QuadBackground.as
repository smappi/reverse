package elements.mirrorRoom
{
	import starling.display.Quad;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	
	public class QuadBackground extends Sprite
	{
		public function QuadBackground(w:Number,h:Number)
		{
			var quadBatch:QuadBatch = new QuadBatch();
			var value:int = 8;
			for (var i:int = value; i > 0; i--) 
			{
				var quad:Quad = new Quad(w,i*h/value,0x000000);
				quad.alpha =(value-i)/value;
//				trace((value-i)/value,"(value-",i,")/value");
				quadBatch.addQuad(quad);
			}
			addChild(quadBatch);
		}
	}
}
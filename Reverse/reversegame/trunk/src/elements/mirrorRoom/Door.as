package elements.mirrorRoom
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class Door extends CitrusSprite
	{

		private var imgDoor:Image;
		public function Door(name:String, params:Object=null)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			imgDoor = new Image(UtilsAndConfig.defaultAssets.getTexture("door_room_mirror"));
			imgDoor.alignPivot();
			imgDoor.smoothing = TextureSmoothing.NONE;
			imgDoor.scaleX = imgDoor.scaleY = 2;
			sprite.addChild(imgDoor);
			
			width = imgDoor.width;
			height = imgDoor.height;
			
			offsetX = -width/2;
			offsetY = -height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			
			imgDoor.x = imgDoor.width/2;
			imgDoor.y = imgDoor.height/2
			view = sprite;
			
			
		}
		
		public function scale():void{
			imgDoor.scaleX = -2;
		}
	}
}
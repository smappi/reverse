package elements.mirrorRoom
{
	import com.greensock.TweenMax;
	
	import citrus.objects.CitrusSprite;
	
	import elements.mirrorRoom.animation.AnimationMirror;
	import elements.mirrorRoom.animation.Shadow;
	
	import org.osflash.signals.Signal;
	
	import screens.MirrorRoom;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.PixelMaskDisplayObject;
	import utils.UtilsAndConfig;
	
	import views.AnimationHero;
	
	public class Mirror extends CitrusSprite
	{

		public var mirror:Object;
		public static var saveX:int;
		private var _style_:int;

		public var hero:AnimationHero;

		private var shadow:Shadow;
		public var _id:int;
		public function Mirror(name:String, params:Object=null,_style:int = 1,id:int=0)
		{
			super(name, params);
			_style_ = _style;
			_id = id;
			if(_style_==2){
				mirror = new Image(UtilsAndConfig.defaultAssets.getTexture("dark_mirror"));
				(mirror as Image).smoothing = TextureSmoothing.NONE;
				(mirror as Image).scaleX = (mirror as Image).scaleY = 2;
			}
			else{
				mirror = new AnimationMirror();
			}
			sprite = new Sprite();
			
			sprite.addChild(mirror as DisplayObject);
			
			if(_style_ == 2){
				hero = new AnimationHero(true);
				hero.x = 100;
				hero.y = 90;
				hero.alpha = 0;
				sprite.addChild(hero);
			}
			else{
				if(_id !=4){
					shadow = new Shadow(id);
					shadow.alpha = 0.6;
					shadow.alignPivot("left","bottom");
					shadow.y = mirror.height - 30;
					shadow.x = -50;
					
					var mask:Quad = new Quad(158  ,400);
					mask.x = 40 ;
					mask.y = 32 ;
					
					maskedDisplayObject = new PixelMaskDisplayObject();
					maskedDisplayObject.addChild(shadow);
					
					maskedDisplayObject.mask = mask;
					
					sprite.addChild(maskedDisplayObject);
				}
			}
			
			view = sprite;
			width = sprite.width;
			height = sprite.height;
			
			offsetX = -width/2;
			offsetY = -height/2;
			
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
		}
		private var remove:Boolean = false;
		public function removeElements():void{
			if(remove || _id ==4 ){
				return;
			}
			remove = true;
			maskedDisplayObject.removeFromParent(true);
			sprite.removeChild(maskedDisplayObject);
			sprite.removeChild(shadow);
			
		}
		
		public function removeHero():void{
			sprite.removeChild(hero);
			var boyOnMirror:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("boy_on_mirror"));
			boyOnMirror.alignPivot();
			boyOnMirror.alpha = 0.6
			boyOnMirror.smoothing = TextureSmoothing.NONE;
			boyOnMirror.scaleX = boyOnMirror.scaleY = 2;
			boyOnMirror.y = mirror.height - boyOnMirror.height/2 - 18;
			boyOnMirror.x = 100;
			sprite.addChild(boyOnMirror);
		}
		
		public function change():void{
			if(shadow!=null){
				if(MirrorRoom.boy.theBoy.status == shadow.status){
					
				}
				else{
					if(MirrorRoom.boy.theBoy.status == StatusAnimation.IDLE){
						shadow.idle();
					}
					else if(MirrorRoom.boy.theBoy.status == StatusAnimation.MOVE){
						if(_style_ != 3 || (_style_ == 3 && flagID3== false)){
							shadow.move();
						}
					}
					else if(MirrorRoom.boy.theBoy.status == StatusAnimation.STOP){
//						if(_id == 3){
//							shadow.lookMirror();
//						}
//						else{
							shadow.pauseLook();
//						}
					}
					else{
						if(_id == 3){
//							flagID3 = true;
//							shadow.lookMirrorMa();
						}
						else{
							shadow.lookMirror();
						}
					}
				}
				
				shadow.animation.currentFrame = MirrorRoom.boy.theBoy.theBoy.currentFrame;
			}
		}
		
		private var flagID3:Boolean = false;
		public function tweenShadow(status:String = "move"):void{
			if(remove){
				return;
			}
			if(shadow!=null){
				if(_id!=3 ){
					shadow.x = MirrorRoom.boy.x - x + 145;
				}
				if(_id==3 && MirrorRoom.boy.x <= x - 80){
					shadow.x = MirrorRoom.boy.x - x + 145;
				}
				if(_id==3 && MirrorRoom.boy.x >= x - 80){
					flagID3 = true;
					shadow.lookMirrorMa();
					TweenMax.delayedCall(4,function():void{
						TweenMax.to(shadow,1,{alpha:0});
					});
				}
//				else{
				if((MirrorRoom.boy as MyBoy).theBoy.statusLook == StatusAnimation.LOOK_LEFT){
					shadow.lookLeft();
				}
				else if((MirrorRoom.boy as MyBoy).theBoy.statusLook == StatusAnimation.LOOK_RIGHT){
					shadow.lookRight();
				}
//				}
				if(MirrorRoom.boy.theBoy.status == shadow.status){
					
				}
				else{
					if(MirrorRoom.boy.theBoy.status == StatusAnimation.IDLE){
						shadow.idle();
					}
					else if(MirrorRoom.boy.theBoy.status == StatusAnimation.MOVE){
						if(_id != 3 || (_id == 3 && flagID3 == false)){
							shadow.move();
						}
						
					}
					else if(MirrorRoom.boy.theBoy.status == StatusAnimation.STOP){
//						if(_id == 3){
//							shadow.lookMirror();
//						}
//						else{
							shadow.pauseLook();
//						}
						
					}
					else{
						if(_id == 3){
//							flagID3 = true;
//							shadow.lookMirrorMa();
						}
						else{
							shadow.lookMirror();
						}
					}
				}
				if(_id != 3 || (_id == 3  && shadow.status != "look")){
					shadow.animation.currentFrame = MirrorRoom.boy.theBoy.theBoy.currentFrame;
				}
			}
		}
		
		public var complete:Signal;

		private var maskedDisplayObject:PixelMaskDisplayObject;

		private var sprite:Sprite;
		public function breakMirror():void{
			(mirror as AnimationMirror).breakMirror();
		}

		public function get style():int
		{
			return _style_;
		}

		public function set style(value:int):void
		{
			_style_ = value;
		}
	}
}
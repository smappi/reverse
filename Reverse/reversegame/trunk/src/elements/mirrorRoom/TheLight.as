package elements.mirrorRoom
{
	import com.greensock.TweenMax;
	
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class TheLight extends CitrusSprite
	{
		public static var saveX:int;
		public function TheLight(name:String, params:Object=null,i:int = 0)
		{
			super(name, params);
			var sprite:Sprite = new Sprite();
			var lamp:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("lamp_mirror_room"));
			lamp.alignPivot();
			lamp.smoothing = TextureSmoothing.NONE;
			lamp.scaleX = lamp.scaleY =2;
			sprite.addChild(lamp);
			lamp.x = lamp.width/2;
			lamp.y = lamp.height/2;
			
			var theLight:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("light_mirror_room"));
			theLight.alignPivot();
			theLight.smoothing = TextureSmoothing.NONE;
			theLight.scaleX = theLight.scaleY =2;
			sprite.addChild(theLight);
			theLight.x = lamp.x;
			theLight.y = lamp.y;
			
			TweenMax.to(theLight,0.1,{delay:UtilsAndConfig.randRangeNumber(0,0.2),scaleX:2.3,scaleY:2.3,repeat:-1,yoyo:true});
			
			view = sprite;
		}
	}
}
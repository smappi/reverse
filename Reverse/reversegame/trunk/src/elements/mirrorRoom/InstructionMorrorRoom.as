package elements.mirrorRoom
{
	import com.greensock.TweenMax;
	
	import elements.TextSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class InstructionMorrorRoom extends Sprite
	{

		private var hand:Image;

		private var txtSwipe:TextSprite;
		public function InstructionMorrorRoom()
		{
			hand = new Image(UtilsAndConfig.defaultAssets.getTexture("s_handSwipe1"));
			hand.smoothing = TextureSmoothing.NONE;
			hand.scaleX = hand.scaleY = 2;
			addChild(hand);
			
			txtSwipe = new TextSprite("SWIPE TO MOVE","AlphaRuler",50,0xffffff);
			txtSwipe.alignPivot();
			txtSwipe.x =hand.x + hand.width/2;
			txtSwipe.y = hand.y + hand.height + 20;
			addChild(txtSwipe);
			this.x = 180;
			this.y = 150;
			
			TweenMax.delayedCall(2,tweenMe);
		}
		
		private function tweenMe():void{
			hand.texture = UtilsAndConfig.defaultAssets.getTexture("s_handSwipe2");
			TweenMax.to(hand,2,{alpha:0,x:"+"+txtSwipe.width,onComplete:function():void{
				hand.texture = UtilsAndConfig.defaultAssets.getTexture("s_handSwipe1");
				hand.x = 0;
				hand.alpha = 1;
				TweenMax.delayedCall(0.2,tweenMe);
			}});
		}
	}
}
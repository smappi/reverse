package elements.mirrorRoom
{
	public class StatusAnimation
	{
		public static const MOVE:String = "MOVE";
		public static const STOP:String = "STOP";
		public static const IDLE:String = "IDLE";
		public static const START:String = "START";
		public static const LOOK_LEFT:String = "LOOK_LEFT";
		public static const LOOK_RIGHT:String = "LOOK_RIGHT";
		public function StatusAnimation()
		{
		}
	}
}
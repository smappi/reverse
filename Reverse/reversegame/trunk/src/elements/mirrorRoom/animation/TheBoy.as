package elements.mirrorRoom.animation
{
	import com.greensock.TweenMax;
	
	import elements.mirrorRoom.StatusAnimation;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class TheBoy extends Sprite
	{

		public var theBoy:MovieClip;
		public var status:String;
		public var statusLook:String = StatusAnimation.LOOK_RIGHT;
		public function TheBoy()
		{
			status = StatusAnimation.MOVE;
			theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("nomal_boy_"),5);
			theBoy.alignPivot()
			theBoy.smoothing = TextureSmoothing.NONE;
			theBoy.scaleX = theBoy.scaleY = 2;
			theBoy.pause();
			addChild(theBoy);
			Starling.juggler.add(theBoy);
		}
		
		public function startBoy():void{
			removeChild(theBoy);
			Starling.juggler.remove(theBoy);
			theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boy_start_"),5);
			theBoy.alignPivot()
			theBoy.smoothing = TextureSmoothing.NONE;
			theBoy.scaleX = theBoy.scaleY = 2;
			addChild(theBoy);
			Starling.juggler.add(theBoy);
		}
		
		public function move():void{
			status = StatusAnimation.MOVE;
			removeChild(theBoy);
			Starling.juggler.remove(theBoy);
			theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("nomal_boy_"),10);
			theBoy.alignPivot()
			theBoy.smoothing = TextureSmoothing.NONE;
			theBoy.scaleX = theBoy.scaleY = 2;
			addChild(theBoy);
			Starling.juggler.add(theBoy);
		}
		
		public function stopBoy():void{
			idle();
			_lookMirror = false;
			theBoy.addEventListener(Event.COMPLETE,change);
		}
		
		private var _lookMirror:Boolean = false;
		public function lookMirror():void{
			status = StatusAnimation.IDLE;
			idle();
			_lookMirror = true;
			theBoy.addEventListener(Event.COMPLETE,change);
		}
		
		private function change():void{
			removeEventListener(Event.COMPLETE,change);
			removeChild(theBoy);
			Starling.juggler.remove(theBoy);
			if(_lookMirror){
				status = "look";
				theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("stop_look_mirror_"),5);
				theBoy.setFrameDuration(3,5);
			}
			else{
				status = StatusAnimation.STOP;
				theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("stop_boy_"),5);
				theBoy.setFrameDuration(3,1);
			}
			theBoy.alignPivot();
			theBoy.smoothing = TextureSmoothing.NONE;
			theBoy.scaleX = theBoy.scaleY = 2;
			theBoy.loop = false;
			
			addChild(theBoy);
			Starling.juggler.add(theBoy);
			theBoy.addEventListener(Event.COMPLETE,function():void{
				idle();
			});
		}
		
		public function idle():void{
			status = StatusAnimation.IDLE;
			removeChild(theBoy);
			Starling.juggler.remove(theBoy);
			theBoy = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("idle_boy"),3);
			theBoy.alignPivot();
			theBoy.play();
			theBoy.smoothing = TextureSmoothing.NONE;
			
			theBoy.scaleX = theBoy.scaleY = 2;
			if(statusLook== StatusAnimation.LOOK_LEFT){
				theBoy.scaleX  = -2;
			}
			for (var i:int = 0; i < theBoy.numFrames; i++) 
			{
				theBoy.setFrameDuration(i,0.4);
			}
			
			addChild(theBoy);
			Starling.juggler.add(theBoy);
		}
	}
}
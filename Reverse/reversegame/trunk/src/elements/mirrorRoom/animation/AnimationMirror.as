package elements.mirrorRoom.animation
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationMirror extends Sprite
	{
		public var animation:MovieClip;
		
		public function AnimationMirror()
		{
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("mirror_"));
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.pause();
			animation.loop = false;
			Starling.juggler.add(animation);
			animation.setFrameDuration(0,0.5);
			animation.setFrameDuration(1,0.5);
			animation.setFrameDuration(2,0.07);
			animation.setFrameDuration(3,0.07);
			animation.setFrameDuration(4,0.07);
			animation.setFrameDuration(5,0.07);
			animation.setFrameDuration(6,0.07);
			animation.setFrameDuration(7,0.07);
			animation.setFrameDuration(8,0.07);
			animation.setFrameDuration(9,0.07);
			animation.setFrameDuration(10,0.05);
			animation.setFrameDuration(11,0.1);
		}
		
		public function breakMirror():void{
			animation.play();
		}
		
	}
}
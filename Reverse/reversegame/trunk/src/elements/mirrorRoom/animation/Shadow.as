package elements.mirrorRoom.animation
{
	
	import elements.mirrorRoom.StatusAnimation;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class Shadow extends Sprite
	{

		public var animation:MovieClip;
		private var _value:int;
		public var status:String;
		public var look:String;
		public function Shadow(value:int)
		{
			_value = value + 1;
			status = StatusAnimation.MOVE;
			look = StatusAnimation.LOOK_RIGHT;
			var value1:String = "boyMirror0"+(value+1)+"_";
			if(value>=3){
				value1 = "boyMirror01_";
				_value = 1
			}
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures(value1));
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.pause();
			Starling.juggler.add(animation); 
		}
		
		public function move():void{
			status = StatusAnimation.MOVE;
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boyMirror0"+_value+"_"));
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.pause();
			Starling.juggler.add(animation); 
		}
		
		public function idle():void{
			status = StatusAnimation.IDLE;
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boy_MirrorW0"+_value+"idle_"),3);
			animation.alignPivot();
			animation.pause();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
		}
		
		public function pauseLook():void{
			status = StatusAnimation.STOP;
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boyMirror_W0"+_value+"_"),3);
			animation.alignPivot();
			animation.pause();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
		}
		
		public function lookMirrorMa():void{
			status = "look";
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boy_Smile_"),2);
			animation.alignPivot();
			animation.loop = false;
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
			animation.play();
			for (var i:int = 0; i < animation.numFrames; i++) 
			{
				animation.setFrameDuration(i,0.1);
			}
			
		}
		
		public function lookMirror():void{
			status = "look";
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("boy_look_mirror"),3);
			animation.alignPivot();
			animation.loop = false;
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
			animation.play();
		}
		
		public function lookLeft():void{
			look = StatusAnimation.LOOK_LEFT;
			animation.scaleX = -2;
		}
		
		public function lookRight():void{
			look = StatusAnimation.LOOK_RIGHT;
			animation.scaleX = 2;
		}
	}
}
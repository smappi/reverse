package elements.friend
{
	import components.FacebookData;
	
	import elements.TextSprite;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class ShowFriend extends Sprite
	{

		private var img:Image;

		private var txtName:TextSprite;

		private var txtScore:TextSprite;
		
		private var _score:String;
		private var _idFb:String;
		private var _nameFb:String;
		
		public function ShowFriend(_obj_:FacebookData,num:int)
		{
			var txtNum:Object;
			if(num>0){
				txtNum = new TextSprite(""+num,"AlphaRuler",40,0x7d7f79);
			}
			else{
				txtNum = new Image(UtilsAndConfig.defaultAssets.getTexture("imgCup"));
				(txtNum as Image).smoothing = TextureSmoothing.NONE;
				(txtNum as Image).scaleX = (txtNum as Image).scaleY = 2;
			}
			
			addChild(txtNum as DisplayObject)
			
			img = new Image(UtilsAndConfig.defaultAssets.getTexture("default"));
			img.alignPivot();
			img.width = img.height = 50;
			img.x = txtNum.width + 35;
			img.y = txtNum.height/2;
			addChild(img);
			
			// cache avatar
			if(UtilsAndConfig.defaultAssets.getTexture(_obj_.id) == null){
				var url:String = "https://graph.facebook.com/"+_obj_.id+"/picture?type=normal";
				UtilsAndConfig.defaultAssets.enqueueWithName(url,_obj_.id);
				UtilsAndConfig.defaultAssets.loadQueue(function(ratio:Number):void
				{
					if (ratio == 1){
						if(UtilsAndConfig.defaultAssets.getTexture(_obj_.id) != null)
						{
							img.texture = UtilsAndConfig.defaultAssets.getTexture(_obj_.id);
						}
					}
				});
			}
			else{
				img.texture = UtilsAndConfig.defaultAssets.getTexture(_obj_.id);
			}
			
			txtName = new TextSprite(_obj_.name,"AlphaRuler",40,0x7d7f79);
			txtName.x = img.x + 65;
			txtName.y = img.y - 18;
			addChild(txtName);
			
			txtScore = new TextSprite(""+_obj_.score,"AlphaRuler",40,0x7d7f79);
			txtScore.x = txtName.x;
			txtScore.y = img.y + 20 - txtScore.height;
			addChild(txtScore);
		}

		public function get nameFb():String
		{
			return _nameFb;
		}

		public function set nameFb(value:String):void
		{
			_nameFb = value;
			txtName.text = _nameFb;
		}

		public function get idFb():String
		{
			return _idFb;
		}

		public function set idFb(value:String):void
		{
			_idFb = value;
			if(UtilsAndConfig.defaultAssets.getTexture(_idFb)==null){
				UtilsAndConfig.defaultAssets.enqueueWithName("https://graph.facebook.com/"+_idFb+"/picture?type=normal",_idFb);
				UtilsAndConfig.defaultAssets.loadQueue(function(ratio:Number):void
				{
					if (ratio == 1){
						if(UtilsAndConfig.defaultAssets.getTexture(_idFb) != null)
						{
							img.texture = UtilsAndConfig.defaultAssets.getTexture(_idFb);
						}
					}
				});
			}
			else{
				img.texture = UtilsAndConfig.defaultAssets.getTexture(_idFb);
			}
		}

		public function get score():String
		{
			return _score;
		}

		public function set score(value:String):void
		{
			_score = value;
			txtScore.text = _score;
		}

	}
}
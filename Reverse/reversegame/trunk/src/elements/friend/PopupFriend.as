package elements.friend
{
	import components.ConnectFacebook;
	import components.FacebookData;
	import components.LevelGame;
	import components.ReverseGameData;
	import components.app42.GetListFriend;
	import components.app42.GetSetScore;
	
	import elements.TextSprite;
	
	import screens.LevelScreen;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.RenderTexture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class PopupFriend extends Sprite
	{

		private var fb:ConnectFacebook;

		private var listFriend:ListFriend;

		private var best:ShowFriend;

		private var list:Array;

		private var imgStone:Image;

		private var connectFb:Button;
		public function PopupFriend()
		{
			var imgChain:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("charm_list_score"));
			imgChain.smoothing = TextureSmoothing.NONE;
			imgChain.scaleX = imgChain.scaleY = 2;
			addChild(imgChain);
			
			imgStone = new Image(UtilsAndConfig.defaultAssets.getTexture("stone_list_score"));
			imgStone.alignPivot();
			imgStone.smoothing = TextureSmoothing.NONE;
			imgStone.scaleX = imgStone.scaleY = 2;
			imgStone.x = imgChain.x + imgChain.width/2 + 6;
			imgStone.y = imgChain.height + imgStone.height/2 - 16;
			addChild(imgStone);
			
			if(ConnectFacebook.isConnect){
				showFriend();
			}
			else{
				var btnConnect:TextSprite = new TextSprite("THIS IS CONNECT FACEBOOK","AlphaRuler",40,0xffffff);
				
				var render:RenderTexture = new RenderTexture(btnConnect.width,btnConnect.height);
				render.draw(btnConnect);
				connectFb = new Button(render);
				connectFb.alignPivot();
				connectFb.x = imgStone.x;
				connectFb.y = imgStone.y;
				addChild(connectFb);
				connectFb.addEventListener(Event.TRIGGERED,function():void{
					fb = new ConnectFacebook();
					fb.connectFacebook();
					fb.onSuccess.add(getListFriend);
//					fb.onError.add(function():void{
//						trace("error fb");
//					});
				});
			}
		}
		
		private function insertScore():void{
			trace("insertScore");
			var getSetScore:GetSetScore = new GetSetScore();
			getSetScore.insertScoreOnCloud("Level "+LevelGame.getCurrentLevel(),2000);
			getSetScore.success.add(getListFriend);
		}
		
		private function getListFriend():void{
			trace("connectFB Success");
			var getFriend:GetListFriend = new GetListFriend("Level "+LevelGame.getCurrentLevel());
			getFriend.success.add(showFriend);
		}
		
		private function showFriend():void{
			trace("getListFriend Success");
			if(connectFb!=null){
				removeChild(connectFb);
			}
			list = (Reverse.instance.gameData as ReverseGameData).listFriend;
			
			best = new ShowFriend(list[0],0);
			
			var value:int = -1;
			if((list[0] as FacebookData).id == (Reverse.instance.gameData as ReverseGameData).facebookID){
				value = 4;
			}
			
			listFriend = new ListFriend(list.pop(),260,value);
			listFriend.alignPivot();
			listFriend.x = imgStone.x;
			listFriend.y = imgStone.y - imgStone.height/2 + 220 + listFriend.height/2;
			addChild(listFriend);
			
			best.alignPivot();
			best.x = imgStone.x;
			best.y = imgStone.y - imgStone.height/2 + 114 + best.height/2 ;
			addChild(best);
		}
		
	}
}
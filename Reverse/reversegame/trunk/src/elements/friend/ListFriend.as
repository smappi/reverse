package elements.friend
{
	import components.FacebookData;
	import components.ReverseGameData;
	
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Sprite;
	
	public class ListFriend extends Sprite
	{
		public function ListFriend(list:Array = null,_height_:Number = 100,value:int = -1)
		{
			var container:ScrollContainer = new ScrollContainer();
			var layout:VerticalLayout = new VerticalLayout();
			layout.gap = 0;
			container.scrollBarDisplayMode = List.SCROLL_BAR_DISPLAY_MODE_NONE;
			
			container.layout = layout;
			container.height = _height_;
			var maxWidth:Number = 0;
			
			if(value == -1){
				value = list.length;
			}
			
			for (var i:int = 0; i < value; i++) 
			{
				var showFriend:ShowFriend = new ShowFriend(list[i],i+1);
				container.addChild(showFriend);
				if(maxWidth<showFriend.width){
					maxWidth = showFriend.width;
				}
				// kết thúc là fb của mình nếu mình k đứng nhứt
				if((list[i] as FacebookData).id == (Reverse.instance.gameData as ReverseGameData).facebookID){
					break;
				}
			}
			container.width = maxWidth;
			addChild(container);
		}
	}
}
package elements
{
	import com.greensock.TweenMax;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class InstructionStory1 extends Sprite
	{
		
		private var hand:Image;
//		private var arrowAnimation:MovieClip;
		
		private var text:Image;
		public function InstructionStory1()
		{
			super();
			
			text = new Image(UtilsAndConfig.defaultAssets.getTexture("swipe"));
			text.scaleX = text.scaleY = 2;
			text.smoothing = TextureSmoothing.NONE;
			addChild(text);
			
			hand = new Image(UtilsAndConfig.defaultAssets.getTexture("s_handSwipe1"));
			hand.scaleX = hand.scaleY = 2;
			hand.smoothing = TextureSmoothing.NONE;
			hand.y = text.y - 160;
			hand.x = text.x + 60;
			addChild(hand);
			
//			arrowAnimation = new MovieClip(Reverse.instance.assets.getTextures("arrow"), 10);
//			arrowAnimation.alignPivot();
//			arrowAnimation.scaleX = arrowAnimation.scaleY = 2;
//			arrowAnimation.smoothing = TextureSmoothing.NONE;
//			arrowAnimation.y = text.y - 200;
//			arrowAnimation.x = text.x + 80;
//			arrowAnimation.pause();
//			addChild(arrowAnimation);
//			Starling.juggler.add(arrowAnimation);
			
			show();
			animation();
			
		}
		
		public function hideView():void{
			TweenMax.to(this,1,{alpha:0,onComplete:function():void{
				TweenMax.killAll(true);
				removeFromParent(true);
			}});
		}
		
		private function show():void{
			TweenMax.to(text,1,{alpha:0.3,repeat:3,yoyo:true});
		}
		
		private function animation():void{
			hand.texture = UtilsAndConfig.defaultAssets.getTexture("s_handSwipe2");
			TweenMax.delayedCall(0.15,function():void{
//				arrowAnimation.play();
				
				TweenMax.staggerTo([hand],1,{x:"+"+(text.x + text.width - 200),onComplete:function():void{
					TweenMax.staggerTo([hand],0.15,{alpha:0,onComplete:function():void{
						hand.texture = UtilsAndConfig.defaultAssets.getTexture("s_handSwipe1");
						hand.x = text.x + 60;
//						arrowAnimation.x = text.x + 80;
						TweenMax.staggerTo([hand],0.15,{delay:1,alpha:1,onComplete:function():void{
							animation();
						}});
					}});
					
				}});
				
			});
		}
	}
}
package elements.loading
{
	import elements.loading.animation.HeroLoading;
	import elements.loading.animation.SkullLoading;
	import elements.loading.animation.SpiderLoading;
	
	import elements.loading.animation.LoadingAnimationWhiteGhost;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import utils.UtilsAndConfig;
	
	public class LoadingSprite extends Sprite
	{
		public function LoadingSprite()
		{
			var quad:Quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0x7369e8);
			addChild(quad);
			
			var list:Array = [HeroLoading,SpiderLoading,SkullLoading,LoadingAnimationWhiteGhost];
			var i:int = UtilsAndConfig.randRange(0,list.length-1);
			
			var animation:Object = new list[3];
			addChild(animation as DisplayObject);
		}
	}
}
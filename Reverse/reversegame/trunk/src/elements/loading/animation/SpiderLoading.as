package elements.loading.animation
{
	import com.greensock.TimelineMax;
	import com.greensock.easing.Circ;
	
	import views.AnimationSpider;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class SpiderLoading extends Sprite
	{

		private var animation:AnimationSpider;
		public function SpiderLoading()
		{
			animation = new AnimationSpider();
			animation.x = 70 + animation.width;
			animation.y = Starling.current.stage.stageHeight - 70 - animation.height;
			addChild(animation);
			tween();
		}
		
		private function tween():void{
			var tlMax:TimelineMax = new TimelineMax({onComplete:tween});
			tlMax.to(animation,0.1,{onUpdate:function():void{
				animation.currentFrame = 0;
			}});
			tlMax.to(animation,0.2,{onUpdate:function():void{
				animation.currentFrame = 1;
			},onComplete:function():void{
				animation.currentFrame = 2;
			}});
			
			tlMax.to(animation,1,{y:"-80",ease:Circ.easeOut,onComplete:function():void{
				animation.currentFrame = 3;
			}});
			tlMax.to(animation,1,{y:Starling.current.stage.stageHeight - 70 - animation.height,ease:Circ.easeIn});
		}
	}
}
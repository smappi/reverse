package elements.loading.animation
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class LoadingAnimationSkull extends Sprite
	{
		public function LoadingAnimationSkull()
		{
			var animation:MovieClip = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("white_skull"),5);
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 3;
			Starling.juggler.add(animation);
			addChild(animation);
		}
	}
}
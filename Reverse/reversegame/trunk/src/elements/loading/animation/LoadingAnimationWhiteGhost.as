package elements.loading.animation
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class LoadingAnimationWhiteGhost extends Sprite
	{
		public function LoadingAnimationWhiteGhost()
		{
			super();
			var animation:MovieClip = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("wghost_"));
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 3;
			Starling.juggler.add(animation);
			addChild(animation);
		}
	}
}
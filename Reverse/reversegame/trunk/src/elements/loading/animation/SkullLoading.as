package elements.loading.animation
{
	import com.greensock.TweenMax;
	
	import views.AnimationSkull;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class SkullLoading extends Sprite
	{

		private var animation:AnimationSkull;
		public function SkullLoading()
		{
			animation = new AnimationSkull();
			animation.x = 70 + animation.width;
			animation.y = Starling.current.stage.stageHeight - 70 - animation.height;
			addChild(animation);
			animation.play();
			
			changeLook(true);
		}
		
		private function changeLook(isMove:Boolean = false):void{
			if(isMove){
				animation.move();
			}
			TweenMax.delayedCall(2,function():void{
				animation.lookRight();
				TweenMax.delayedCall(2,function():void{
					animation.lookLeft();
					changeLook();
				});
			});
		}
	}
}
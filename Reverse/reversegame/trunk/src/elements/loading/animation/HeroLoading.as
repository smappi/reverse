package elements.loading.animation
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	
	import views.AnimationHero;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class HeroLoading extends Sprite
	{
		public function HeroLoading()
		{
			var animation:AnimationHero = new AnimationHero();
			animation.x = 70 + animation.width;
			animation.y = Starling.current.stage.stageHeight - 70 - animation.height;
			addChild(animation);
			
			TweenMax.to(animation,0.75,{y:"+5",ease:Cubic.easeInOut,repeat:-1,yoyo:true});
		}
	}
}
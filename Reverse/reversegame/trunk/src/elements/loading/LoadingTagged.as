package elements.loading
{
	import com.greensock.TweenMax;
	
	import elements.loading.animation.LoadingAnimationSkull;
	import elements.loading.animation.LoadingAnimationWhiteGhost;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class LoadingTagged extends Sprite
	{

		private var skull:LoadingAnimationSkull;

		private var ghost:LoadingAnimationWhiteGhost;

		private var image:Image;

		private var imageB:Image;
		private var quad:Quad;
		public function LoadingTagged()
		{
			quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0xffffff);
			quad.alpha = 0.3;
			addChild(quad);
			
			image = new Image(UtilsAndConfig.defaultAssets.getTexture("loading_screent"));
			image.alignPivot();
			image.scaleX = image.scaleY = 2;
			image.smoothing = TextureSmoothing.NONE;
			image.x = Starling.current.stage.stageWidth/2;
			addChild(image);
			
			imageB = new Image(UtilsAndConfig.defaultAssets.getTexture("loading_screenb"));
			imageB.alignPivot();
			imageB.scaleX = imageB.scaleY = 2;
			imageB.smoothing = TextureSmoothing.NONE;
			imageB.x = Starling.current.stage.stageWidth/2;
			addChild(imageB);
			
//			TweenMax.delayedCall(0.2,function():void{
//				quad.alpha = 0.1;
//				TweenMax.to(quad,0.8,{alpha:0.3});
//			});
			TweenMax.to(image,1,{y:Starling.current.stage.stageHeight/2 - image.height/2 + 220});
			TweenMax.to(imageB,1,{y:Starling.current.stage.stageHeight/2 + imageB.height/2 - 220});
		}
		
		public function close():void{
			UtilsAndConfig.playSound("open");
			image.y = -image.height/2;
			imageB.y = Starling.current.stage.stageHeight + imageB.height/2;
			TweenMax.to(image,1.2,{y:Starling.current.stage.stageHeight/2 - image.height/2 + 220,onComplete:function():void{
				TweenMax.to(quad,0.5,{alpha:0});
				dispatchEventWith("FINISH");
			}});
			TweenMax.delayedCall(0.7,function():void{
				UtilsAndConfig.playSound("close");
			});
			
			TweenMax.to(imageB,1,{y:Starling.current.stage.stageHeight/2 + imageB.height/2 - 220});
		}
		
		public function open():void{
			image.y = Starling.current.stage.stageHeight/2 - image.height/2 + 220;
			imageB.y = Starling.current.stage.stageHeight/2 + imageB.height/2 - 220;
			TweenMax.to(image,1.5,{y:-image.height/2,onComplete:function():void{
				dispatchEventWith("FINISH");
			}});
			UtilsAndConfig.playSound("open");
			TweenMax.to(imageB,1.5,{y:Starling.current.stage.stageHeight + imageB.height/2});
			TweenMax.to(quad,1,{alpha:0});
		}
		
		public function removeMe():void{
			
			removeFromParent(true);
		}
	}
}
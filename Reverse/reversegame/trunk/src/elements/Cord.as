package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Elastic;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class Cord extends Sprite
	{

		private var quad:Quad;

		private var imgCord:Image;

		private var imgCord2:Image;
		private var _colorQuad:uint;
		public function Cord(colorBackground:uint=0xffffff)
		{
			super();
			trace("cord");
			quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,colorBackground);
			quad.alpha = 0;
			
			imgCord = new Image(UtilsAndConfig.defaultAssets.getTexture("imgCord"));
			imgCord.alignPivot("center","top");
			imgCord.scaleX = imgCord.scaleY = 2;
			imgCord.smoothing = TextureSmoothing.NONE;
			addChild(imgCord);
			
			imgCord2 = new Image(imgCord.texture);
			imgCord2.alignPivot("center","top");
			imgCord2.scaleX = imgCord2.scaleY = 2;
			imgCord2.smoothing = TextureSmoothing.NONE;
			addChild(imgCord2);
			
			reAddMe()
		}
		
		public function get colorQuad():uint
		{
			return quad.color;
		}

		public function set colorQuad(value:uint):void
		{
			quad.color = value;
		}

		private function coodinateElements():void{
			imgCord2.x = Starling.current.stage.stageWidth - imgCord2.width/2 - 10;
			imgCord2.y = -10;
			imgCord.x = imgCord.width*1/3;
			imgCord.y = 0;
		}
		
		public function removeMe():void{
			TweenMax.to(imgCord,1,{scaleY:0,ease:Back.easeIn});
			TweenMax.to(imgCord,1,{y:-imgCord2.height});
			TweenMax.to(imgCord2,1.2,{scaleY:0,ease:Back.easeIn});
			TweenMax.to(imgCord2,1.2,{y:-imgCord2.height});
			TweenMax.to(quad,1,{alpha:0});
		}
		
		public function reAddMe():void{
			coodinateElements();
			addChildAt(quad,0);
			TweenMax.to(quad,1,{alpha:0.7});
			TweenMax.to(imgCord,1,{scaleY:2,ease:Elastic.easeOut});
			TweenMax.to(imgCord,1,{y:0,onUpdate:function():void{}});
			TweenMax.to(imgCord2,1.2,{scaleY:2,ease:Elastic.easeOut});
			TweenMax.to(imgCord2,1.2,{y:0});
		}
	}
}
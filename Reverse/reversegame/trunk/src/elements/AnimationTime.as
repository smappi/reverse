package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationTime extends Sprite
	{

		private var text:TextSprite;

		private var second:int;

		private var minute:int;

		private var hour:int;

		private var value:int = 59;

		private var tmpMinute:int = 0;

		private var tmpHour:int;

		private var obj:Object;
		
		private var _time:Number
		
		private var timeTween:Number;
		public function AnimationTime(time:Number)
		{
			_time = time;
			hour = time/3600;
			time = Math.abs(hour*3600-time);
			
			minute = time/60;
			time = Math.abs(minute*60 - time);
			
			second = time;
			
			var iconTime:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("icon_time"));
			iconTime.smoothing = TextureSmoothing.NONE;
			iconTime.scaleX = iconTime.scaleY = 2;
			addChild(iconTime);
			
			text = new TextSprite("0 : 0s","AlphaRuler",55,0x000000);
			text.alignPivot();
			text.x = iconTime.x + iconTime.width + 10 + text.width/2;
			text.y = iconTime.y + iconTime.height/2;
			addChild(text);
			obj = {_value:0};
			if(minute == 0 && hour == 0){
				value = second;
			}
			timeTween = 1;
			if(hour>0){
				timeTween = 0.1/hour;
			}
			else if(minute > 10){
				timeTween = 0.6;
			}
		}
		
		public function time(time:int = 0):Number{
			if (time > 50) 
			{
				time = 2;
			}else if (time > 35 && time <= 50) 
			{
				time = 1.8;	
			}else if (time > 25 && time <= 35){
				time = 1.6;	
			}else if (time > 15 && time <= 25){
				time = 1.5;	
			}else if (time > 5 && time <=15){
				time = 1;
			}else{
				time = 0.5;
			}
			return time;
		}
	
		
//		public function tweenText():void{
//			
//			TweenMax.to(obj,timeTween,{_value:value,ease:Linear.easeNone,onUpdate:function():void{
//				text.text = tmpHour+" : "+tmpMinute+" : "+Math.floor(obj._value)+"s";
//			},onComplete:function():void{
//				text.text = tmpHour+" : "+tmpMinute+" : "+Math.floor(obj._value)+"s";
//				obj._value = 0;
//				if(tmpMinute==59){
//					tmpMinute = 0;
//					tmpHour+=1;
//				}
//				else{
//					tmpMinute +=1;
//					if(tmpMinute == minute && tmpHour == hour){
//						value = second;
//					}
//				}
//				if(text.text == hour+" : "+minute+" : "+second+"s"){
//					TweenMax.killTweensOf(obj);
//					return;
//				}
//				tweenText();
//			}});
//		}
		public function tweenText():void{
			var obj:Object = {_value:0};
			TweenMax.to(obj,time(second),{_value:second,ease:Linear.easeNone,onUpdate:function():void{
				text.text = "0 : "+Math.floor(obj._value)+"s";
			},onComplete:function():void{
				obj._value = 0;
				TweenMax.to(obj,time(minute),{_value:minute,ease:Linear.easeNone,onUpdate:function():void{
					text.text = Math.floor(obj._value)+" : "+second+"s";
				}});
			}});
		}
	}
}
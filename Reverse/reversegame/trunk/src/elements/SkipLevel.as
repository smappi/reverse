package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	public class SkipLevel extends Sprite
	{

		private var btnBack:Button;

		private var btnSkipLevel:Button;

		public static const BACK:String = "BACK";

		private var btnUnlockAll:ButtonUnlockAll;

		public function SkipLevel(color:uint=0xffffff)
		{
			super();
			
			btnBack = new Button(UtilsAndConfig.defaultAssets.getTexture("btnBack2"),"",UtilsAndConfig.defaultAssets.getTexture("btnBack1"));
			btnBack.alignPivot("center","bottom");
			btnBack.scaleX = btnBack.scaleY = 2;
			Image(Sprite(btnBack.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(btnBack);
			btnBack.addEventListener(Event.TRIGGERED,function():void{
				dispatchEvent(new Event(BACK));
			});
			
			btnSkipLevel = new Button(UtilsAndConfig.defaultAssets.getTexture("imgSkipLevel"));
			btnSkipLevel.alignPivot("center","bottom");
			btnSkipLevel.scaleX = btnSkipLevel.scaleY = 2;
			Image(Sprite(btnSkipLevel.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(btnSkipLevel);
			
			btnUnlockAll = new ButtonUnlockAll();
			btnUnlockAll.alignPivot("center","bottom");
			addChild(btnUnlockAll);
			
			settingCoodinate();
			
		}
		
		private function settingCoodinate():void{
			btnBack.x = 30 + btnBack.width/2;
			btnBack.y = Starling.current.stage.stageHeight + 20;
			btnBack.rotation =0;
			btnSkipLevel.x = btnBack.x + btnBack.width/2 + 50 + btnSkipLevel.width/2;
			btnSkipLevel.y = Starling.current.stage.stageHeight + 20;
			btnSkipLevel.rotation = 0;
			btnUnlockAll.x = btnSkipLevel.x + btnSkipLevel.width + 30;
			btnUnlockAll.y = Starling.current.stage.stageHeight + 20;
			btnUnlockAll.rotation = 0;
		}
		
		public function tweenMe():void{
			settingCoodinate();
			TweenMax.from(btnBack,0.3,{delay:0.5,rotation:""+deg2rad(180),ease:Back.easeOut});
			TweenMax.staggerFrom([btnSkipLevel,btnUnlockAll],0.3,{delay:0.5,rotation:""+deg2rad(180),ease:Back.easeOut},0.2);
		}
		
		public function removeMe():void{
			TweenMax.to(btnBack,0.3,{rotation:""+deg2rad(180),ease:Back.easeIn});
			TweenMax.staggerTo([btnSkipLevel,btnUnlockAll],0.3,{delay:0.15,rotation:""+deg2rad(180),ease:Back.easeIn},0.15,remove);
		}
		
		private function remove():void{
			removeChild(this);
		}
	}
}
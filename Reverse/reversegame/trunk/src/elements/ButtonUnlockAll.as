package elements
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class ButtonUnlockAll extends Sprite
	{
		public static const UNLOCK:String = "UNLOCK";
		public static const RESTORE:String = "RESTORE";
		
		public function ButtonUnlockAll()
		{
			var btnUnlockAll:Button = new Button(UtilsAndConfig.defaultAssets.getTexture("imgUnlockAll"));
			btnUnlockAll.alignPivot("center","bottom");
			btnUnlockAll.scaleX = btnUnlockAll.scaleY = 2;
			Image(Sprite(btnUnlockAll.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(btnUnlockAll);
			
			var btnRestore:Button = new Button(UtilsAndConfig.defaultAssets.getTexture("btnRestore"));
			btnRestore.alignPivot();
			btnRestore.scaleX = btnRestore.scaleY = 1.5;
			Image(Sprite(btnRestore.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(btnRestore);
			btnRestore.y = btnUnlockAll.y - 109 + btnRestore.height/2;
			
			btnUnlockAll.addEventListener(Event.TRIGGERED,function():void{
				dispatchEvent(new Event(UNLOCK));
			});
			
			btnRestore.addEventListener(Event.TRIGGERED,function():void{
				dispatchEvent(new Event(RESTORE));
			});
		}
	}
}
package elements
{
	
	import com.greensock.TweenMax;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class ChatGhost extends Sprite
	{
		private var isTouch:Boolean = false;
		private var heroAnimation:MovieClip;
		public function ChatGhost()
		{
			super();
			UtilsAndConfig.setFirst(1);
			var quad:Quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0x7089cb);
			quad.alpha = 0.7;
			addChild(quad);
			quad.addEventListener(TouchEvent.TOUCH,touchHandler);
			
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("monster_head"), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.x = -80;
			heroAnimation.y = 200;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			
			var imgGhostPopup:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("popupLevel"));
			imgGhostPopup.smoothing = TextureSmoothing.NONE;
			imgGhostPopup.scaleX = imgGhostPopup.scaleY = 2;
			imgGhostPopup.x = 230;
			imgGhostPopup.y = 100;
			addChild(imgGhostPopup);
			TweenMax.from(this,0.5,{alpha:0});
			TweenMax.from(imgGhostPopup,2.5,{alpha:0,onComplete:function():void{
//				UtilsAndConfig.playSound("talk");
				TweenMax.delayedCall(1,function():void{
					UtilsAndConfig.playSound("laugh");
				});
			}});
			
			
			
			TweenMax.delayedCall(4,function():void{
				isTouch = false;
				remove();
			});
		}
		
		private function remove():void
		{
			// TODO Auto Generated method stub
			TweenMax.to(this,0.5,{alpha:0,onComplete:function():void{
				removeFromParent(true);
				dispatchEventWith("COMPLETE");
			}});
		}		
		
		private function touchHandler(e:TouchEvent):void
		{
			if (isTouch) 
			{
				TweenMax.to(this,0.5,{alpha:0,onComplete:function():void{
					TweenMax.killAll();
					removeFromParent(true);
				}});
				
			}
			
		}
	}
}
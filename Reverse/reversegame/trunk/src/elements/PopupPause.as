package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import components.LevelGame;
	import components.ReverseGameData;
	
	import screens.LevelScreen;
	import screens.YouWinScreen;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	public class PopupPause extends Sprite
	{

		private var spritePopup:Sprite;

		private var imgHeadGhost:MovieClip;

		private var imgSkip:Button;

		private var btnFacebook:Button;

		private var btnTwitter:Button;

		private var imgPopup:Image;

		private var btnResume:ButtonCustom;

		private var btnMenu:ButtonCustom;
		
		public static const SKIP:String = "SKIP_LEVEL";
		public static const RESUME:String = "RESUME";

		private var btnSetting:ButtonCustom;

		private var vector:Vector.<Texture>;
		
		private var _color:uint;

		private var txtQuit:TextSprite;

		private var text:TextSprite;

		private var btnSound:ButtonCustom;

		private var btnReplay:ButtonCustom;

		private var btnFX:ButtonCustom;

		private var btnMoreApps:ButtonCustom;

		private var btnYesQuit:ButtonCustom;

		private var btnNoQuit:ButtonCustom;

		private var txtTitle:TextSprite;
		
		public function PopupPause(colorBackground:uint=0xffffff,onLevel:Boolean = false)
		{
			super();
			
			UtilsAndConfig.playSound("popup");
			
			// là chỗ chứa các thành phần chính k thay đổi
			spritePopup = new Sprite();
			
			imgPopup = new Image(UtilsAndConfig.defaultAssets.getTexture("popup_pause"));
			imgPopup.alignPivot("center","top");
			imgPopup.scaleX = imgPopup.scaleY = 2;
			imgPopup.y = -imgPopup.height*1/4;
			imgPopup.smoothing = TextureSmoothing.NONE;
			imgPopup.x = Starling.current.stage.stageWidth/2;
			spritePopup.addChild(imgPopup);
			
			var spriteMenu:Sprite = new Sprite();
			var spriteSetting:Sprite = new Sprite();
			var spriteQuit:Sprite = new Sprite();
			 
			txtTitle = new TextSprite("Level "+LevelGame.getCurrentLevel(),"AlphaRuler",50,0xffffff);
			txtTitle.alignPivot();
			txtTitle.x = imgPopup.x;
			txtTitle.y = 180;
			
			spritePopup.addChild(txtTitle);
				
			btnResume = new ButtonCustom("resume",colorBackground);
			btnResume.alignPivot();
			btnResume.x = imgPopup.x;
			btnResume.y = imgPopup.y + imgPopup.height/2 + btnResume.height;
			spriteMenu.addChild(btnResume);
			btnResume.addEventListener(Event.TRIGGERED,function():void{
				dispatchEvent(new Event(RESUME));
			});
			
			btnSetting = new ButtonCustom("setting_",colorBackground);
			btnSetting.alignPivot();
			btnSetting.x = imgPopup.x;
			btnSetting.y = btnResume.y + btnSetting.height + 40;
			spriteMenu.addChild(btnSetting);
			
			addChild(spritePopup);
			
			btnMenu = new ButtonCustom("btnquit_",colorBackground);
			btnMenu.alignPivot();
			btnMenu.x = imgPopup.x;
			btnMenu.y = btnSetting.y + btnMenu.height + 30;
			spriteMenu.addChild(btnMenu);
			
			btnSound = new ButtonCustom("btnSoundOn",colorBackground);
			btnSound.alignPivot();
			btnSound.x = imgPopup.x;
			btnSound.y = btnResume.y;
			spriteSetting.addChild(btnSound);
			
			if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
				btnSound.texture = "btnSound";
			}
			
			btnReplay = new ButtonCustom("btnTurnOnReplay",colorBackground);
			btnReplay.alignPivot();
			btnReplay.x = btnSound.x - btnSound.width/2 - btnReplay.width/2 - 30;
			btnReplay.y = btnSound.y;
			spriteSetting.addChild(btnReplay);
			
			if((Reverse.instance.gameData as ReverseGameData).isReplay == -1){
				btnReplay.texture = "btnTurnOffReplay";
			}
			
			btnFX = new ButtonCustom("btnFxOn",colorBackground);
			btnFX.alignPivot();
			btnFX.x = btnSound.x + btnSound.width/2 + btnFX.width/2 + 30;
			btnFX.y = btnSound.y;
			spriteSetting.addChild(btnFX);
			
			if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
				btnFX.texture = "btnFx";
			}
			
			btnMoreApps = new ButtonCustom("btnMore",colorBackground);
			btnMoreApps.alignPivot();
			btnMoreApps.x = btnSound.x;
			btnMoreApps.y = imgPopup.y + imgPopup.height - 40 - btnMoreApps.height/2;
			spriteSetting.addChild(btnMoreApps);
			
			text = new TextSprite("The game by Smappi studio 2015","AlphaRuler",50,colorBackground);
			text.alignPivot();
			text.x = btnSound.x;
			text.y = (btnSound.y + btnMoreApps.y)/2;
			spriteSetting.addChild(text);
			
			txtQuit = new TextSprite("Are you sure to quit?","AlphaRuler",50,colorBackground);
			txtQuit.alignPivot();
			txtQuit.x = btnSound.x;
			txtQuit.y = btnSound.y + txtQuit.height/2 - 30;
			spriteQuit.addChild(txtQuit);
			
			btnYesQuit = new ButtonCustom("btnYesQuit",colorBackground);
			btnYesQuit.alignPivot();
			btnYesQuit.x = btnSound.x - 20 - btnYesQuit.width/2;
			btnYesQuit.y = text.y - btnYesQuit.height/2;
			spriteQuit.addChild(btnYesQuit);
			
			btnNoQuit = new ButtonCustom("btnNoQuit",colorBackground);
			btnNoQuit.alignPivot();
			btnNoQuit.x = btnSound.x + 20 + btnYesQuit.width/2;
			btnNoQuit.y = btnYesQuit.y;
			spriteQuit.addChild(btnNoQuit);
			
			vector = UtilsAndConfig.defaultAssets.getTextures("imgHeadGhost")
			imgHeadGhost = new MovieClip(vector);
			imgHeadGhost.alignPivot("center","top");
			imgHeadGhost.scaleX = imgHeadGhost.scaleY = 2;
			imgHeadGhost.smoothing = TextureSmoothing.NONE;
			imgHeadGhost.x = imgPopup.x -imgPopup.width/2 + imgHeadGhost.width - 33;
			imgHeadGhost.y = 22;
			spritePopup.addChild(imgHeadGhost);
			imgHeadGhost.pause();
			imgHeadGhost.setFrameDuration(0,3);
			imgHeadGhost.setFrameDuration(1,0.1);
			Starling.juggler.add(imgHeadGhost);
			
			imgSkip = new Button(UtilsAndConfig.defaultAssets.getTexture("skip_2"),"",UtilsAndConfig.defaultAssets.getTexture("skip_1"));
			imgSkip.alignPivot("center","bottom");
			imgSkip.scaleX = imgSkip.scaleY = 2;
			Image(Sprite(imgSkip.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			imgSkip.x = Starling.current.stage.stageWidth - imgSkip.width/2 - 20;
			imgSkip.y = Starling.current.stage.stageHeight + 20;
			addChild(imgSkip);
			imgSkip.addEventListener(Event.TRIGGERED,function():void{
				dispatchEvent(new Event(SKIP));
			});
			
			
			btnFacebook = new Button(UtilsAndConfig.defaultAssets.getTexture("facebook_1"),"",UtilsAndConfig.defaultAssets.getTexture("facebook_2"));
			btnFacebook.alignPivot("right","center");
			btnFacebook.scaleX = btnFacebook.scaleY = 2;
			Image(Sprite(btnFacebook.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnFacebook.x = Starling.current.stage.stageWidth + 20;
			btnFacebook.y = Starling.current.stage.stageHeight/2 - btnFacebook.height/2;
			addChild(btnFacebook);
			
			btnTwitter = new Button(UtilsAndConfig.defaultAssets.getTexture("twitter_1"),"",UtilsAndConfig.defaultAssets.getTexture("twitter_2"));
			btnTwitter.alignPivot("right","center");
			btnTwitter.scaleX = btnTwitter.scaleY = 2;
			Image(Sprite(btnTwitter.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnTwitter.x = Starling.current.stage.stageWidth + 20;
			btnTwitter.y = btnFacebook.y - btnFacebook.height - 20;
			addChild(btnTwitter);
			btnTwitter.addEventListener(Event.TRIGGERED,function():void{
				Reverse.instance.state = new YouWinScreen();
			});
			
			var backSetting:Button = new Button(UtilsAndConfig.defaultAssets.getTexture("btnBack2"),"",UtilsAndConfig.defaultAssets.getTexture("btnBack1"));
			backSetting.alignPivot("center","bottom");
			backSetting.scaleX = backSetting.scaleY = 2;
			Image(Sprite(backSetting.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(backSetting);
			
			backSetting.x = 30 + backSetting.width/2;
			backSetting.y = Starling.current.stage.stageHeight + 20;
			backSetting.rotation = deg2rad(180);		
			
			UtilsAndConfig.checkConnection(checkHTTP);
			
			function checkHTTP(value:uint):void {
				if (value==1) {
					imgSkip.visible = true;
				} else {
					imgSkip.visible = false;
				}
			}
			
			backSetting.addEventListener(Event.TRIGGERED,function():void{
				spritePopup.removeChild(spriteSetting);
				spritePopup.addChild(spriteMenu);
				TweenMax.to(backSetting,0.3,{rotation:deg2rad(180),ease:Back.easeIn});
				UtilsAndConfig.playSound("cancel");
			});
			
			btnSetting.addEventListener(Event.TRIGGERED,function():void{
				UtilsAndConfig.playSound("ok");
				spritePopup.addChild(spriteSetting);
				spritePopup.removeChild(spriteMenu);
				TweenMax.to(backSetting,0.3,{rotation:0,ease:Back.easeOut});
			});
			
			// thay đổi trạng thái nut replay
			btnReplay.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).isReplay == -1){
					btnReplay.texture = "btnTurnOnReplay";
					UtilsAndConfig.playSound("ok");
				}
				else{
					btnReplay.texture = "btnTurnOffReplay";
					UtilsAndConfig.playSound("cancel");
				}
				
				(Reverse.instance.gameData as ReverseGameData).isReplay *= -1;

			});
			
			btnFX.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
					btnFX.texture = "btnFxOn";
				}
				else{
					btnFX.texture = "btnFx";
					UtilsAndConfig.stopSoundFX();
				}
				UtilsAndConfig.playSound("ok");
				(Reverse.instance.gameData as ReverseGameData).playFX *= -1;
				
			});
			
			btnSound.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
					btnSound.texture = "btnSoundOn";
					UtilsAndConfig.playSound("ok");
					UtilsAndConfig.playSoundBG();
				}
				else{
					UtilsAndConfig.playSound("ok");
					btnSound.texture = "btnSound";
					UtilsAndConfig.stopSoundBG();
				}
				UtilsAndConfig.playSound("ok");
				(Reverse.instance.gameData as ReverseGameData).playFX *= -1;
			});
			
			btnMenu.addEventListener(Event.TRIGGERED,function():void{
				spritePopup.addChild(spriteQuit);
				spritePopup.removeChild(spriteMenu);
				UtilsAndConfig.playSound("ok");
			});
			
			btnYesQuit.addEventListener(Event.TRIGGERED,function():void{
				UtilsAndConfig.playSound("ok");
				Reverse.instance.state = new LevelScreen();
			});
			btnNoQuit.addEventListener(Event.TRIGGERED,function():void{
				UtilsAndConfig.playSound("cancel");
				spritePopup.removeChild(spriteQuit);
				spritePopup.addChild(spriteMenu);
			});
			
			if(onLevel){
				spritePopup.addChild(spriteSetting);
			}
			else{
				spritePopup.addChild(spriteMenu);
			}
		}
		
		public function get color():uint
		{
			return _color;
		}

		public function set color(value:uint):void
		{
			btnResume.color = value;
			btnMenu.color = value;
			btnSetting.color = value;
			btnReplay.color = value;
			btnFX.color = value;
			btnMoreApps.color = value;
			btnNoQuit.color = value;
			btnSound.color = value;
			btnYesQuit.color = value;
		}

		public function tweenMe():void{
			TweenMax.staggerFrom([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + btnFacebook.width,ease:Back.easeOut},0.1);
			TweenMax.from(imgSkip,0.5,{delay:0.5,rotation:"-"+deg2rad(180),ease:Back.easeOut});
			
			TweenMax.from(spritePopup,1,{y:-imgPopup.height*2/3,ease:Back.easeOut});
			imgHeadGhost.play();
			imgHeadGhost.addEventListener(Event.COMPLETE,dupEyes)
			function dupEyes():void{
				while(imgHeadGhost.numFrames>2){
					imgHeadGhost.removeFrameAt(imgHeadGhost.numFrames -1 );
				}
				imgHeadGhost.setFrameTexture(0,vector[vector.length-2]);
				imgHeadGhost.setFrameTexture(1,vector[vector.length-1]);
			}
		}
		
		public function reAddMe():void{
			TweenMax.staggerTo([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + 20,ease:Back.easeOut},0.1);
			TweenMax.to(imgSkip,0.5,{delay:0.5,rotation:"-"+deg2rad(180),ease:Back.easeOut});
			TweenMax.to(spritePopup,2,{y:0,ease:Back.easeOut});
			imgHeadGhost.play();
			TweenMax.to(this,1,{delay:0.5,x:0});
		}
		
		public function removeMe():void{
//			TweenMax.to(this,1,{x:-Starling.current.stage.stageWidth*3/2});
			TweenMax.staggerTo([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + btnFacebook.width,ease:Back.easeIn},0.1);
			TweenMax.to(imgSkip,0.2,{delay:0.1,rotation:"-"+deg2rad(180),ease:Back.easeIn});
			TweenMax.to(spritePopup,0.5,{y:-imgPopup.height,ease:Back.easeIn});
			imgHeadGhost.stop();
		}
	}
}
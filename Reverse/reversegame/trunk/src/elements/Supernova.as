package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	public class Supernova extends Sprite
	{
		private var imgChooseLevel:Image;
		private var dontTween:Boolean = false;
		
		public function Supernova()
		{
			super();
			imgChooseLevel = new Image(UtilsAndConfig.defaultAssets.getTexture("imgChooseLevel"));
			imgChooseLevel.alignPivot();
			imgChooseLevel.smoothing = TextureSmoothing.NONE;
			imgChooseLevel.scaleY = 2;
			imgChooseLevel.y = -imgChooseLevel.height/3 +3 ;
			imgChooseLevel.scaleX = 0;
			addChild(imgChooseLevel);
		}
		
		public function tweenMe():void{
			TweenMax.to(imgChooseLevel,2,{scaleX:2,onComplete:function():void{
				for (var i:int = 0; i < UtilsAndConfig.randRange(10,20); i++) 
				{
					var qWidth:int = UtilsAndConfig.randRange(2,7);
					var quad:Quad = new Quad(qWidth,qWidth,0xffffff);
					quad.alignPivot();
					quad.x = UtilsAndConfig.randRange(0,imgChooseLevel.width - quad.width);
					quad.alpha = 0;
					addChild(quad);
					tweenObject(quad);
				}
			}});
		}
		
		private function tweenObject(_obj:Object):void{
			TweenMax.delayedCall(UtilsAndConfig.randRangeNumber(0.2,1),function():void{
				tweenQuad(_obj);
			});                 
		}
		
		private function tweenQuad(_obj:Object):void{
			_obj.alpha=0.7;
			_obj.y = 0;
			_obj.x = UtilsAndConfig.randRange(-imgChooseLevel.width/2+10,imgChooseLevel.width/2 - _obj.width - 10);
			TweenMax.to(_obj,UtilsAndConfig.randRangeNumber(0.6,1),{x:""+UtilsAndConfig.randRangeNumber(-10,10),repeat:-1,ease:Linear.easeNone,yoyo:true});
			TweenMax.to(_obj,UtilsAndConfig.randRangeNumber(1,3),{rotation:""+50000,ease:Linear.easeNone,alpha:0,y:"-"+UtilsAndConfig.randRangeNumber(imgChooseLevel.height/2,imgChooseLevel.height),onComplete:function():void{
				if(!dontTween){
					tweenQuad(_obj);
				}
			}});
		}
	}
}
package elements
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class ButtonCustom extends Sprite
	{

		private var imgButton:Button;
		private var _width:Number;
		public static var TRIGGER:String = "Click";
		private var quad:Quad;
		private var _color:int = 0;
		private var texture_:String = "";
		private var _upState:String;
		private var _downState:String;
		private var _texture:String;
		public function ButtonCustom(texture:String = "",color_:int = 0xff0000)
		{
			super();
			_color = color_;
			texture_ = texture;
			imgButton = new Button(UtilsAndConfig.defaultAssets.getTexture(texture_+"2"),"",UtilsAndConfig.defaultAssets.getTexture(texture_+"1"));
			imgButton.scaleX = imgButton.scaleY = 2;
			Image(Sprite(imgButton.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			quad = new Quad(imgButton.width,imgButton.height,color_);
			quad.alpha = 0.8;
			addChild(quad);
			addChild(imgButton);
			imgButton.addEventListener(Event.TRIGGERED,touchHandle);
		}

		public function get texture():String
		{
			return _texture;
		}

		public function set texture(value:String):void
		{
			_texture = value;
			imgButton.downState = UtilsAndConfig.defaultAssets.getTexture(_texture+1);
			imgButton.upState = UtilsAndConfig.defaultAssets.getTexture(_texture+2);
		}

		public function get downState():String
		{
			return _downState;
		}

		public function set downState(value:String):void
		{
			_downState = value;
			imgButton.downState = UtilsAndConfig.defaultAssets.getTexture(_downState+1);
		}

		public function get upState():String
		{
			return _upState;
		}

		public function set upState(value:String):void
		{
			_upState = value;
			imgButton.downState = UtilsAndConfig.defaultAssets.getTexture(_upState+1);
		}

		public function get color():int
		{
			return quad.color;
		}

		public function set color(value:int):void
		{
			quad.color = value;
		}
		
		public function changeTexture(texture:String):void{
			imgButton.upState = UtilsAndConfig.defaultAssets.getTexture(texture_+"2");
			imgButton.downState = UtilsAndConfig.defaultAssets.getTexture(texture_+"1")
		}
		
		private function touchHandle():void
		{
			this.dispatchEventWith(TRIGGER);
		}
	}
}
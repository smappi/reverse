package elements
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Elastic;
	
	import components.ReverseGameData;
	
	import screens.YouWinScreen;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	public class PopupSetting extends Sprite
	{

		private var spritePopup:Sprite;

		private var imgHeadGhost:MovieClip;


		private var btnFacebook:Button;

		private var btnTwitter:Button;

		private var imgPopup:Image;


		
		public static const SKIP:String = "SKIP_LEVEL";
		public static const RESUME:String = "RESUME";

		private var btnSetting:ButtonCustom;

		private var vector:Vector.<Texture>;
		
		private var _color:uint;


		private var text:TextSprite;

		private var btnSound:ButtonCustom;


		private var btnFX:ButtonCustom;

		private var btnMoreApps:ButtonCustom;
		private var cord:Cord;
		private var imgCord:Image;
		private var imgCord2:Image;
		private var btnReplay:ButtonCustom;

		private var quad:Quad;

		private var backSetting:Button;


		
		public function PopupSetting(colorBackground:uint=0xffffff)
		{
			super();
			UtilsAndConfig.playSound("popup");
			quad = new Quad(Starling.current.stage.stageWidth, Starling.current.stage.stageHeight,colorBackground);
			quad.alpha = 0.7;
			addChild(quad);
			
			imgCord = new Image(UtilsAndConfig.defaultAssets.getTexture("imgCord"));
			imgCord.alignPivot("center","top");
			imgCord.scaleX = imgCord.scaleY = 2;
			imgCord.smoothing = TextureSmoothing.NONE;
			addChild(imgCord);
			
			imgCord2 = new Image(imgCord.texture);
			imgCord2.alignPivot("center","top");
			imgCord2.scaleX = imgCord2.scaleY = 2;
			imgCord2.smoothing = TextureSmoothing.NONE;
			addChild(imgCord2);
			
			imgCord2.x = Starling.current.stage.stageWidth - imgCord2.width/2 - 10;
			imgCord2.y = -10;
			imgCord.x = imgCord.width*1/3;
			imgCord.y = - imgCord.height/2;
			
			spritePopup = new Sprite();
			imgPopup = new Image(UtilsAndConfig.defaultAssets.getTexture("popup_pause"));
			imgPopup.alignPivot("center","top");
			imgPopup.scaleX = imgPopup.scaleY = 2;
			imgPopup.y = -imgPopup.height*1/4;
			imgPopup.smoothing = TextureSmoothing.NONE;
			imgPopup.x = Starling.current.stage.stageWidth/2;
			spritePopup.addChild(imgPopup);
			
			btnSound = new ButtonCustom("btnSoundOn",colorBackground);
			btnSound.alignPivot();
			btnSound.x = imgPopup.x;
			btnSound.y = 300; 
			spritePopup.addChild(btnSound);
			
			if((Reverse.instance.gameData as ReverseGameData).playSound == -1){
				btnFX.texture = "btnSound";
			}
			
			btnReplay = new ButtonCustom("btnTurnOnReplay",colorBackground);
			btnReplay.alignPivot();
			spritePopup.addChild(btnReplay);
			btnReplay.x = btnSound.x - btnSound.width/2 - btnReplay.width/2 - 30;
			btnReplay.y = 300;
			spritePopup.addChild(btnReplay);
			// thay đổi trạng thái nut replay
			btnReplay.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).isReplay == -1){
					btnReplay.texture = "btnTurnOnReplay";
					UtilsAndConfig.playSound("ok");
				}
				else{
					btnReplay.texture = "btnTurnOffReplay";
					UtilsAndConfig.playSound("cancel");
				}
				
				(Reverse.instance.gameData as ReverseGameData).isReplay *= -1;
				
			});
//			btnReplay.addEventListener(ButtonCustom.TRIGGER,removeMe);
			
			if((Reverse.instance.gameData as ReverseGameData).isReplay == -1){
				btnReplay.texture = "btnTurnOffReplay";
			}
			
			btnFX = new ButtonCustom("btnFxOn",colorBackground);
			btnFX.alignPivot();
			btnFX.x = btnSound.x + btnSound.width/2 + btnFX.width/2 + 30;
			btnFX.y = 300;
			spritePopup.addChild(btnFX);
			
			if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
				btnFX.texture = "btnFx";
			}
			
			btnFX.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
					btnFX.texture = "btnFxOn";
				}
				else{
					btnFX.texture = "btnFx";
					UtilsAndConfig.stopSoundFX();
				}
				UtilsAndConfig.playSound("ok");
				(Reverse.instance.gameData as ReverseGameData).playFX *= -1;
				
			});
			
			btnSound.addEventListener(Event.TRIGGERED,function():void{
				if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
					btnSound.texture = "btnSoundOn";
					UtilsAndConfig.playSound("ok");
					UtilsAndConfig.playSoundBG();
				}
				else{
					btnSound.texture = "btnSound";
					UtilsAndConfig.playSound("ok");
					UtilsAndConfig.stopSoundBG();
				}
				UtilsAndConfig.playSound("ok");
				(Reverse.instance.gameData as ReverseGameData).playFX *= -1;
				
			});
			
			if((Reverse.instance.gameData as ReverseGameData).playFX == -1){
				btnSound.texture = "btnSoundOn";
			}
			
			btnMoreApps = new ButtonCustom("btnMore",colorBackground);
			btnMoreApps.alignPivot();
			btnMoreApps.x = imgPopup.x;
			btnMoreApps.y = imgPopup.y + imgPopup.height - 40 - btnMoreApps.height/2;
			spritePopup.addChild(btnMoreApps);
			
			text = new TextSprite("The game by Smappi studio 2015","AlphaRuler",40,colorBackground);
			text.alignPivot();
			text.x = imgPopup.x;
			text.y = (btnSound.y + btnMoreApps.y)/2;
			spritePopup.addChild(text);
			
			
			
			vector = UtilsAndConfig.defaultAssets.getTextures("imgHeadGhost")
			imgHeadGhost = new MovieClip(vector);
			imgHeadGhost.alignPivot("center","top");
			imgHeadGhost.scaleX = imgHeadGhost.scaleY = 2;
			imgHeadGhost.smoothing = TextureSmoothing.NONE;
			imgHeadGhost.x = imgPopup.x -imgPopup.width/2 + imgHeadGhost.width - 33;
			imgHeadGhost.y = 22;
			spritePopup.addChild(imgHeadGhost);
			imgHeadGhost.pause();
			imgHeadGhost.setFrameDuration(0,3);
			imgHeadGhost.setFrameDuration(1,0.1);
			Starling.juggler.add(imgHeadGhost);
			
			
			btnFacebook = new Button(UtilsAndConfig.defaultAssets.getTexture("facebook_1"),"",UtilsAndConfig.defaultAssets.getTexture("facebook_2"));
			btnFacebook.alignPivot("right","center");
			btnFacebook.scaleX = btnFacebook.scaleY = 2;
			Image(Sprite(btnFacebook.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnFacebook.x = Starling.current.stage.stageWidth + 20;
			btnFacebook.y = Starling.current.stage.stageHeight/2 - btnFacebook.height/2;
			addChild(btnFacebook);
			
			btnTwitter = new Button(UtilsAndConfig.defaultAssets.getTexture("twitter_1"),"",UtilsAndConfig.defaultAssets.getTexture("twitter_2"));
			btnTwitter.alignPivot("right","center");
			btnTwitter.scaleX = btnTwitter.scaleY = 2;
			Image(Sprite(btnTwitter.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnTwitter.x = Starling.current.stage.stageWidth + 20;
			btnTwitter.y = btnFacebook.y - btnFacebook.height - 20;
			addChild(btnTwitter);
			btnTwitter.addEventListener(Event.TRIGGERED,function():void{
				Reverse.instance.state = new YouWinScreen();
			});
			addChild(spritePopup);
			
			backSetting = new Button(UtilsAndConfig.defaultAssets.getTexture("btnBack2"),"",UtilsAndConfig.defaultAssets.getTexture("btnBack1"));
			backSetting.alignPivot("center","bottom");
			backSetting.scaleX = backSetting.scaleY = 2;
			Image(Sprite(backSetting.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			addChild(backSetting);
			
			backSetting.x = 30 + backSetting.width/2;
			backSetting.y = Starling.current.stage.stageHeight + 20;
			backSetting.rotation = deg2rad(180);
			backSetting.addEventListener(Event.TRIGGERED,removeMe);
			
			tweenMe();
		}
		
		public function get color():uint
		{
			return _color;
		}

		public function set color(value:uint):void
		{
			btnFX.color = value;
			btnMoreApps.color = value;
			btnSound.color = value;
		}

		public function tweenMe():void{
			TweenMax.staggerFrom([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + btnFacebook.width,ease:Back.easeOut},0.1);
			TweenMax.from(imgCord,2,{scaleY:0,ease:Elastic.easeOut});
			TweenMax.from(imgCord,2,{y:-imgCord2.height});
			TweenMax.from(imgCord2,2.2,{scaleY:0,ease:Elastic.easeOut});
			TweenMax.from(imgCord2,2.2,{y:-imgCord2.height});
			TweenMax.from(spritePopup,1,{y:-imgPopup.height*2/3,ease:Back.easeOut});
			TweenMax.to(backSetting,0.2,{delay:2,rotation:0});
			imgHeadGhost.play();
			imgHeadGhost.addEventListener(Event.COMPLETE,dupEyes);
			function dupEyes():void{
				while(imgHeadGhost.numFrames>2){
					imgHeadGhost.removeFrameAt(imgHeadGhost.numFrames -1 );
				}
				imgHeadGhost.setFrameTexture(0,vector[vector.length-2]);
				imgHeadGhost.setFrameTexture(1,vector[vector.length-1]);
			}
		}
		
		public function reAddMe():void{
			TweenMax.staggerTo([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + 20,ease:Back.easeOut},0.1);
			TweenMax.to(spritePopup,2,{y:0,ease:Back.easeOut});
			imgHeadGhost.play();
			TweenMax.to(this,1,{delay:0.5,x:0});
		}
		
		public function removeMe():void{
			btnReplay.removeEventListener(ButtonCustom.TRIGGER,removeMe);
//			TweenMax.to(this,1,{x:-Starling.current.stage.stageWidth*3/2});
			TweenMax.staggerTo([btnTwitter,btnFacebook],0.5,{x:Starling.current.stage.stageWidth + btnFacebook.width,ease:Back.easeIn},0.1);
			TweenMax.to(spritePopup,0.5,{y:-imgPopup.height,ease:Back.easeIn});
			imgHeadGhost.stop();
			TweenMax.to(backSetting,0.2,{delay:2,rotation:deg2rad(180)});
			removeMeCord();
		}
		
		public function removeMeCord():void{
			TweenMax.to(imgCord,1,{scaleY:0,ease:Back.easeIn});
			TweenMax.to(imgCord,1,{y:-imgCord2.height});
			TweenMax.to(imgCord2,1.2,{scaleY:0,ease:Back.easeIn});
			TweenMax.to(imgCord2,1.2,{y:-imgCord2.height,onComplete:function():void{
				removeFromParent(true);
			}});
			TweenMax.to(quad,1,{alpha:0});
		}
	}
}
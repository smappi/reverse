package elements
{
	import citrus.objects.CitrusSprite;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	
	import utils.UtilsAndConfig;
	
	public class LevelButton extends CitrusSprite
	{
		
		private var btLevelImg:Image;
		public var level_:int = 0;
		public var onSelected:Signal;
		private var sprite:Sprite;
		public var onComplete:Signal;
		public function LevelButton(name:String = "", params:Object = null,level:int = 0, pX:Number = 0, pY:Number = 0)
		{
			super(name,params);
			onComplete = new Signal();
			onSelected = new Signal();
			level_ = level;
			sprite = new Sprite();
			if (level <= int(UtilsAndConfig.getCurrentLevel())) 
			{
				btLevelImg = new Image(UtilsAndConfig.defaultAssets.getTexture("btlevel_006"));
//				sprite.addEventListener(TouchEvent.TOUCH, gotoPlayScreen);
			}else{
				btLevelImg = new Image(UtilsAndConfig.defaultAssets.getTexture("btlevel_001"));
			}
			
			btLevelImg.alignPivot();
			
			sprite.addChild(btLevelImg);
			if (level_ <= int(UtilsAndConfig.getCurrentLevel())) 
			{
				addTextNumber();
			}
			sprite.name = ""+(level-1);
			view = sprite;
		}
		
		private var isTouched:Boolean = false;
		private function gotoPlayScreen(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(e.target as DisplayObject);
			if(touch==null) return;
			
			if(touch.phase == TouchPhase.ENDED){
				if (isTouched) 
				{
					onSelected.dispatch(level_,x,y);
				}
				
			}else if(touch.phase == TouchPhase.BEGAN){
				isTouched = true;
			}else if(touch.phase == TouchPhase.MOVED){
				isTouched = false;
			}
		}
		
		private function addTextNumber():void{
			
			var text:TextField = new TextField(46, 46,level_+"","OUTLI",10,0x000000);
			text.alignPivot();
			sprite.addChild(text);
		}
		
		public function changeButton():void{
			
			if (UtilsAndConfig.getCurrentLevel() == UtilsAndConfig.getLevelNext()) 
			{
				return;
			}
			
			while (sprite.numChildren > 0) {
				sprite.removeChildAt(0);
			}
			
			var heroAnimation:MovieClip = new MovieClip(Reverse.instance.assets.getTextures("btlevel"), 10);
			heroAnimation.alignPivot();
			heroAnimation.loop = false;
			Starling.juggler.add(heroAnimation);
			sprite.addChild(heroAnimation);
//			sprite.addEventListener(TouchEvent.TOUCH, gotoPlayScreen);
			heroAnimation.addEventListener(Event.COMPLETE,function():void{
				addTextNumber();
				onComplete.dispatch();
			});
			
			view = sprite;
		}
	}
}
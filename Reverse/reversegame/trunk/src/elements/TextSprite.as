package elements
{
	import flash.text.TextFormatAlign;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;

	public class TextSprite extends Sprite
	{
		private var textField:TextField;
		private var _text:String;
		private var _font:String;
		private var _color:uint;
		private var _size:uint;
		public function TextSprite(_text:String,font:String,size:Number,color:uint,autosize:String = TextFieldAutoSize.BOTH_DIRECTIONS)
		{
			textField = new TextField(1,1,_text,font,size,color);
			textField.autoSize = autosize;
			addChild(textField);
			
		}

		public function get size():uint
		{
			return textField.fontSize;
		}

		public function set size(value:uint):void
		{
			_size = value;
			textField.fontSize = _size;
		}

		public function get color():uint
		{
			return textField.color;
		}

		public function set color(value:uint):void
		{
			_color = value;
			textField.color = _color;
		}

		public function get font():String
		{
			return textField.fontName;
		}

		public function set font(value:String):void
		{
			_font = value;
			textField.fontName = _font;
		}

		public function get text():String
		{
			return textField.text;
		}

		public function set text(value:String):void
		{
			_text = value;
			textField.text = _text;
		}
		
		public function setTextCenter():void{
			textField.hAlign = TextFormatAlign.CENTER;
		}
		
		public function setWidthText(w:int = 0):void{
			textField.width = w;
		}

	}
}
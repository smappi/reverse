package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	public class AnimationWheel extends Sprite
	{
		public function AnimationWheel()
		{
			super();
			var heroAnimation:MovieClip = new MovieClip(Reverse.instance.assets.getTextures("trap_fireballs"), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			Starling.juggler.add(heroAnimation);
			addChild(heroAnimation);
			
		}
	}
}
package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class AnimationSaw extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function AnimationSaw()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE ,initView);
		}
		
		private function initView():void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initView);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("saw_32"), 10);
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			
		}
		
	}
}
package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationMapGhost extends Sprite
	{

		private var animation:MovieClip;
		public function AnimationMapGhost()
		{
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("ghost_"));
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			Starling.juggler.add(animation);
			addChild(animation);
		}
		
		public function moveLeft():void{
			animation.scaleX = 2;
		}
		
		public function moveRight():void{
			animation.scaleX = -2;
		}
	}
}
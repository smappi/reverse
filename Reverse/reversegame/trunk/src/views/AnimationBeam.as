package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	public class AnimationBeam extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function AnimationBeam()
		{
			super();
			initView();
		}
		
		private function initView():void
		{
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("beam_"), 10);
			heroAnimation.alignPivot();
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			heroAnimation.visible = false;
			
		}
		
		public function play():void{
			heroAnimation.play();
			heroAnimation.visible=true;
		}
		
		public function pause():void{
			heroAnimation.pause();
			heroAnimation.visible=true;
		}
		
		public function stop():void{
			heroAnimation.stop();
			heroAnimation.visible=false;
		}
	}
}
package views
{
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationSpiderRun extends Sprite
	{
		private var heroAnimation:MovieClip;
		private var imgSpider:Image;
		private var scale_:int = 0;
		public function AnimationSpiderRun(scale:int = 2)
		{
			super();
			scale_ = scale;
			imgSpider = new Image(UtilsAndConfig.defaultAssets.getTexture("sleep_spider_01"));
			imgSpider.smoothing = TextureSmoothing.NONE;
			imgSpider.scaleX = imgSpider.scaleY = scale_;
			addChild(imgSpider);
		}
		
		public function active():void{
			if (heroAnimation) 
			{
				removeChild(heroAnimation);
				Starling.juggler.remove(heroAnimation);	
			}
			
			if (imgSpider) 
			{
				imgSpider.removeFromParent(true);
			}
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("sleep_spider"), 10);
			heroAnimation.loop = false;
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = scale_;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,run);
		}
		
		public function run(e:Event):void{
			if (imgSpider) 
			{
				imgSpider.removeFromParent(true);
			}
			dispatchEventWith("COMPLETE_OPEN_EYES");
			if (heroAnimation) 
			{
				removeChild(heroAnimation);
				Starling.juggler.remove(heroAnimation);	
			}
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("huge_spider"), 20);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.loop = true;
			heroAnimation.scaleX = heroAnimation.scaleY = scale_;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
		
		public function sleep():void{
			if (imgSpider) 
			{
				imgSpider.removeFromParent(true);
			}
			var vector:Vector.<Texture> = Reverse.instance.assets.getTextures("sleep_spider");
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);	
			heroAnimation = new MovieClip(vector.reverse(), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.loop = false;
			heroAnimation.scaleX = heroAnimation.scaleY = scale_;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
	}
}
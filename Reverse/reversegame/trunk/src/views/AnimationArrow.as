package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationArrow extends Sprite
	{
		public function AnimationArrow()
		{
			super();
			var animtion:MovieClip = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("arrow_star_"));
			animtion.smoothing = TextureSmoothing.NONE;
			animtion.scaleX = animtion.scaleY = 2;
			addChild(animtion);
			for (var i:int = 0; i < animtion.numFrames; i++) 
			{
				animtion.setFrameDuration(i,0.1);
			}
			animtion.play();
			Starling.juggler.add(animtion);
			
		}
	}
}
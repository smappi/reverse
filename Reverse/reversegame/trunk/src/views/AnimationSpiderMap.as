package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationSpiderMap extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function AnimationSpiderMap(number:int)
		{
			super();
			
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("spider"+(number+1)), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.loop = false;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,play);
		}
		
		public function play():void{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("spider"+UtilsAndConfig.randRange(1,2)), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.loop = false;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,play);
		}
	}
}
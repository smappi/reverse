package views
{
	import com.greensock.TweenMax;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class AnimationGun extends Sprite
	{
		private var animation:MovieClip;
		public var complete:Signal;
		public function AnimationGun()
		{
			super();
			complete = new Signal();
			this.addEventListener(Event.ADDED_TO_STAGE ,initView);
		}
		
		private function initView():void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initView);
			animation = new MovieClip(Reverse.instance.assets.getTextures("gun_"), 10);
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			animation.pause();
			addChild(animation);
			Starling.juggler.add(animation);
			
			//			heroAnimation.addEventListener(Event.COMPLETE,function():void{
			//				complete.dispatch();
			//			});
			
		}
		
		public function shoot():void{
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(Reverse.instance.assets.getTextures("gun_"),5);
			animation.color = 0xA497AE;
			animation.alignPivot();
			animation.loop = false;
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
			animation.addEventListener(Event.COMPLETE,initShoot);
		}
		
		private function initShoot():void{
			animation.removeEventListener(Event.COMPLETE,initShoot);
			dispatchComplete();
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(Reverse.instance.assets.getTextures("shoot_gun_"),5);
			animation.color = 0xA497AE;
			animation.alignPivot();
			animation.loop = true;
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
			//			animation.addEventListener(Event.COMPLETE,dispatchComplete);
		}
		
		private function dispatchComplete():void{
			//			animation.removeEventListener(Event.COMPLETE,dispatchComplete);
			complete.dispatch();
		}
		
		public function stop():void{
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(Reverse.instance.assets.getTextures("gun_").reverse(),5);
			animation.color = 0xA497AE;
			animation.alignPivot();
			animation.loop = false;
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
		}
		
		public function statusPlay():void{
			var objA:Object = {frameNum:0};
			//			TweenPlugin.activate([RoundPropsPlugin]);
			TweenMax.to(objA,0.4,{frameNum:3, onUpdate:function():void{
				animation.currentFrame = Math.floor(objA.frameNum);
			},onComplete:function():void{
				complete.dispatch();
			}});
		}
		
		private function dispatch():void{
			
		}
		
		public function statusStop():void{
			var objA:Object = {frameNum:0};
			//			TweenPlugin.activate([RoundPropsPlugin]);
			TweenMax.from(objA,0.5,{frameNum:3, onUpdate:function():void{
				animation.currentFrame = Math.floor(objA.frameNum);
			}});
		}
	}
}
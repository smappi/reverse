package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationSpider extends Sprite
	{
		private var _flag:uint =0;

		private var animation:MovieClip;
		private var _currentFrame:int;
		public function AnimationSpider()
		{
			var vector:Vector.<Texture> = UtilsAndConfig.defaultAssets.getTextures("spider_");
			animation = new MovieClip(vector,3);
			animation.smoothing = TextureSmoothing.NONE;
			animation.alignPivot();
//			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.setFrameDuration(0,0.1);
			animation.setFrameDuration(1,0.2);
			animation.setFrameDuration(2,0.5);
			animation.setFrameDuration(3,0.1);
			animation.pause();
			Starling.juggler.add(animation);
			
//			animation.addEventListener(Event.ENTER_FRAME,function():void{
//				if(animation.currentFrame ==0){
//					flag = 0;
//				}
//				if(animation.currentFrame ==2){
//					flag = 1;
//				}
//			});
		}
		
		public function get currentFrame():int
		{
			return _currentFrame;
		}

		public function set currentFrame(value:int):void
		{
			_currentFrame = value;
			animation.currentFrame = _currentFrame;
		}

		public function pause():void{
			animation.pause();
		}
		
		public function play():void{
			animation.play();
		}
		
		public function stop():void{
			animation.stop();
		}
		
		public function get flag():uint
		{
			return _flag;
		}

		public function set flag(value:uint):void
		{
			_flag = value;
		}

	}
}
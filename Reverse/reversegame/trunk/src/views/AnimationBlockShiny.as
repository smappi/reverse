package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	
	public class AnimationBlockShiny extends Sprite
	{
		private var blockAnimation:MovieClip;
		public function AnimationBlockShiny()
		{
			super();
			blockAnimation = new MovieClip(Reverse.instance.assets.getTextures("block_shiny_"), 60);
			addChild(blockAnimation);
			Starling.juggler.add(blockAnimation);
		}
	}
}
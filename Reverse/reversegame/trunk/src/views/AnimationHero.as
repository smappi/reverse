package views
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationHero extends Sprite
	{
		private var heroAnimation:MovieClip;

		private var halo:Image;
		public static const DISAPPEAR_COMPLETE:String = "DISAPPEAR_COMPLETE";
		public static const LOAD_COMPLETE:String = "LOAD_COMPLETE";
		public function AnimationHero(isOnMirror:Boolean = false)
		{
			super();
			if(!isOnMirror){
				halo = new Image(UtilsAndConfig.defaultAssets.getTexture("cc_light_hero"));
				halo.alignPivot();
				halo.alpha = 0;
				halo.y = 15;
				halo.blendMode = BlendMode.ADD;
				addChild(halo);
				halo.scaleX = halo.scaleY = 0;
				statusStart();
			}
			else{
				statusNomal();
			}
			
		}
		
		private function haloTween():void{
			if(halo!==null){
				TweenMax.to(halo,1,{scaleX:1,scaleY:1,onComplete:function():void{
					TweenMax.to(halo,1,{scaleX:0.9,scaleY:0.9,onComplete:haloTween});
				}});
			}
		}
		
		public function statusStart(frame:int = 10):void
		{
			if(halo!==null){
				TweenMax.to(halo,0.8,{alpha:0.4});
				TweenMax.to(halo,1,{scaleX:1,scaleY:1,onComplete:haloTween});
			}
			
			if (heroAnimation) 
			{
				removeChild(heroAnimation);
				Starling.juggler.remove(heroAnimation);
			}
			
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("start_hero_"), frame);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			heroAnimation.loop = false;
			addChild(heroAnimation);
			heroAnimation.y =10;
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,statusNomal);
		}
		
		public function statusNomal():void{
			
			flag = false;
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("herof_"), 15);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			heroAnimation.y =0;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
//			heroAnimation.addEventListener(Event.COMPLETE,function():void{
			TweenMax.delayedCall(0.01,function():void{
				dispatchEvent(new Event(LOAD_COMPLETE));
			});
				
			
//			});
			
		}
		
		public function statusDie():void{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("hero_die"), 25);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 1.5;
			heroAnimation.alignPivot();
			heroAnimation.loop = false;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			if(halo!=null){
				TweenMax.to(halo,0.8,{alpha:0});
				TweenMax.to(halo,1,{scaleX:0,scaleY:0});
			}
			
		}
		
		public function statusFinish():void{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("start_hero_").reverse(), 20);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			heroAnimation.loop = false;
			addChild(heroAnimation);
			heroAnimation.y =10;
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,function():void{
				dispatchEvent(new Event(FINISH));
				dispatchEvent(new Event(DISAPPEAR_COMPLETE));
			});
			if(halo!=null){
				TweenMax.to(halo,0.5,{alpha:0,ease:Linear.easeNone});
			}
			
//			TweenMax.to(halo,1.5,{scaleX:0,scaleY:0,ease:Linear.easeNone});
		}
		
		private var flag:Boolean = false;
		public static const FINISH:String = "FINISH"
		public function statusMove(moveRight:Boolean = false):void{
			if(flag){
				return;
			}
			flag = true;
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("move_hero"), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 1.5;
			heroAnimation.alignPivot();
			heroAnimation.y =0;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
		
		public function moveLeft():void{
			heroAnimation.scaleX = 2;
			heroAnimation.x = 20;
		}
		
		public function moveRight():void{
			heroAnimation.scaleX = -2;
			heroAnimation.x = -20;
		}
	}
}
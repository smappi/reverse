package views
{
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationThorn extends Sprite
	{

		private var animation:MovieClip;
		private var _currentFrame:int;
		public var isUp:Signal;
		public function AnimationThorn()
		{
			super();
			isUp = new Signal();
			vectorThorn = UtilsAndConfig.defaultAssets.getTextures("thorn");
			
			animation = new MovieClip(vectorThorn,5);
			addChild(animation);
			pause();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			Starling.juggler.add(animation);
//			animation.addFrame(vectorThorn[0],null,animation.getFrameDuration(0));
			animation.setFrameDuration(0,0.2);
			animation.setFrameDuration(1,0.2);
			animation.setFrameDuration(2,0.2);
			animation.setFrameDuration(3,0.2);
			animation.addEventListener(Event.ENTER_FRAME,function():void{
				if(animation.getFrameTexture(animation.currentFrame) == vectorThorn[0]){
					isUp.dispatch(0);
				}
				else if(animation.getFrameTexture(animation.currentFrame) == vectorThorn[1]) {
					isUp.dispatch(1);
				}
				else{
					isUp.dispatch(2);
				}
			});
		}
		
		private var frameCurrent:uint;

		private var vectorThorn:Vector.<Texture>;
		public function setTimeDelay(value:Number):void{
//			trace(value);
			animation.setFrameDuration(2,value);
			animation.setFrameDuration(0,value);
		}
		
		public function get currentFrame():int
		{
			return animation.currentFrame;
		}

		public function set currentFrame(value:int):void
		{
			frameCurrent = value;
			animation.currentFrame = value;
		}

		public function pause():void{
			animation.pause();
		}
		
		public function stop():void{
			animation.stop();
		}
		
		public function play():void{
			animation.play();
		}
	}
}
package views
{
	import com.greensock.TweenMax;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationBat extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function AnimationBat()
		{
			super();
		}
		
		public function idle():void{
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("bat_idle"), 10);
			heroAnimation.loop = false;
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			heroAnimation.addEventListener(Event.COMPLETE,fly);
			
		}
		
		private function fly():void{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation.removeEventListener(Event.COMPLETE,fly);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("bat_fly"), 10);
			heroAnimation.loop = true;
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			
			gotoPosition();
			
		}
		
		private function gotoPosition():void{
			var px:int = UtilsAndConfig.randRange(20,200);
			var py:int = Math.random() - UtilsAndConfig.randRange(20,200);
			TweenMax.to(heroAnimation,10,{x:px, y:py,onComplete:function():void{
				gotoPosition();
			}});
		}
	}
}
package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationSkull extends Sprite
	{

		private var animation:MovieClip;

		private var vectorQuiet:Vector.<Texture>;

		private var vectorMove:Vector.<Texture>;
		
		public function AnimationSkull()
		{
			vectorQuiet = UtilsAndConfig.defaultAssets.getTextures("skullquiet_");
			vectorMove = UtilsAndConfig.defaultAssets.getTextures("skullMove");
//			vectorMove[i].h
			quiet();
		}
		
		public function pause():void{
			animation.stop();
		}
		
		public function play():void{
			animation.play();
		}
		
		public function lookLeft():void{
			animation.scaleX =2;
		}
		
		public function lookRight():void{
			animation.scaleX = -2;
		}
		
		public function move():void{
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(vectorMove,5);
			animation.color = 0xA497AE;
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			Starling.juggler.add(animation);
		}
		
		public function quiet():void{
			removeChild(animation);
			Starling.juggler.remove(animation);
			animation = new MovieClip(vectorQuiet,5);
			animation.alignPivot();
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			animation.loop = false;
			addChild(animation);
			Starling.juggler.add(animation);
		}
	}
}
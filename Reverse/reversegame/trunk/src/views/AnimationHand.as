package views
{
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationHand extends Sprite
	{

		private var animation:MovieClip;

		private var _heightHand:Number = 0;
		private var vectorTexture:Vector.<Texture>;
		public var changeHeight:Signal;
		public var stopAction:Signal;
		public function AnimationHand()
		{
			super();
			vectorTexture = UtilsAndConfig.defaultAssets.getTextures("hand");
			changeHeight = new Signal();
			stopAction = new Signal();
			animation = new MovieClip(vectorTexture,7);
			animation.smoothing = TextureSmoothing.NONE;
			animation.scaleX = animation.scaleY = 2;
			addChild(animation);
			animation.pause();
			Starling.juggler.add(animation);
		}
		
		public function get heightHand():Number
		{
			return _heightHand;
		}

		public function set heightHand(value:Number):void
		{
			_heightHand = value;
		}

		public function play():void{
			animation.play();
			addEvent();
			animation.addEventListener(Event.COMPLETE,function():void{
				stopAction.dispatch();
				animation.stop();
			});
		}
		
		public function addEvent():void{
			animation.addEventListener(Event.ENTER_FRAME,changeHeightHand);
		}
		public function removeEvent():void{
			animation.removeEventListener(Event.ENTER_FRAME,changeHeightHand);
		}
		private function changeHeightHand():void{
			switch(animation.currentFrame)
			{
				case 0:
				{
					heightHand = 4;
					break;
				}
				case 1:
				{
					heightHand = 20;
					break;
				}
				case 2:
				{
					heightHand = 40;
					break;
				}
				case 3:
				{
					heightHand = 45;
					break;
				}
				case 4:
				{
					heightHand = 46;
					break;
				}
				case 5:
				{
					heightHand = 30;
					break;
				}
				case 6:
				{
					heightHand = 16;
					break;
				}
				case 7:
				{
					heightHand = 8;
					break;
				}
				case 8:
				{
					heightHand = 5;
					break;
				}
				default:
				{
					heightHand = 10;
					break;
				}
			}
			changeHeight.dispatch(heightHand);
		}
		
		public function pause():void{
			animation.stop();
		}
	}
}
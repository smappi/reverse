package views
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	public class AnimationBoom extends Sprite
	{
		private var heroAnimation:MovieClip;
		public function AnimationBoom()
		{
			super();
			
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("mine_32"), 10);
			heroAnimation.currentFrame = 3;
			heroAnimation.pause();
			heroAnimation.alignPivot();
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
		
		public function initView():void
		{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("mine_32"), 10);
			heroAnimation.currentFrame = 3;
			heroAnimation.pause();
			heroAnimation.alignPivot();
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
			
		}
		
		public function play():void{
			heroAnimation.currentFrame = 0;
			heroAnimation.play();
		}
		public function stop():void{
			heroAnimation.stop();
		}
		
		public function boom(scale:Number):void{
			removeChild(heroAnimation);
			Starling.juggler.remove(heroAnimation);
			heroAnimation = new MovieClip(Reverse.instance.assets.getTextures("mine_explosion"), 10);
			heroAnimation.smoothing = TextureSmoothing.NONE;
			heroAnimation.scaleX = heroAnimation.scaleY = 2;
			heroAnimation.alignPivot();
			heroAnimation.loop = false;
			addChild(heroAnimation);
			Starling.juggler.add(heroAnimation);
		}
		
	}
}
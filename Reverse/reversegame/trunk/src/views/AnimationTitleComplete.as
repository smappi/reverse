package views
{
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class AnimationTitleComplete extends Sprite
	{

		public var animation:MovieClip;
		public function AnimationTitleComplete()
		{
			animation = new MovieClip(UtilsAndConfig.defaultAssets.getTextures("title_level_complete_"),60);
			animation.scaleX = animation.scaleY = 2;
			animation.smoothing = TextureSmoothing.NONE;
			animation.play();
			animation.setFrameDuration(0,0.08);
			animation.setFrameDuration(1,0.08);
			animation.setFrameDuration(2,0.08);
			animation.setFrameDuration(3,0.08);
			animation.setFrameDuration(4,0.08);
			Starling.juggler.add(animation);
			addChild(animation);
			
			var img:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("PopupComplete"));
			img.alignPivot("center","top");
			img.scaleX = img.scaleY = 2;
			img.smoothing = TextureSmoothing.NONE;
			img.x = animation.width/2;
			img.y = 95;
			addChild(img);
		}
	}
}
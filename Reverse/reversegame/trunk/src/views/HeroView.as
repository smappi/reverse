package views
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Cubic;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class HeroView extends Sprite
	{
		
		private var _score:int;
		public var hero:AnimationHero;
		private var txtField:TextField;
		public function HeroView(w:int =1,h:int=1,isRemotePlayer:Boolean = false)
		{
			super();
			score = 0;
			hero = new AnimationHero();
			addChild(hero);
			var quad:Quad;
			if(isRemotePlayer){
				txtField = new TextField(100,100,score+"","Verdana",50,0xc0392b);
				quad = new Quad(1,1,0xc0392b);
			}else{
				txtField = new TextField(100,100,score+"","Verdana",50,0x2ecc71);
				quad = new Quad(1,1,0x2ecc71);
			}
			hero.x = 16;
//			hero.y = 7;
			quad.alpha = 0;
			addChild(quad);
			TweenMax.to(this,0.75,{y:"+5",ease:Cubic.easeInOut,repeat:-1,yoyo:true});
		}
		
		public function get score():int
		{
			return _score;
		}
		
		public function set score(value:int):void
		{
			_score = value;
			if(txtField != null) txtField.text = score+"";
		}
		
		public function increaseScore():void{
			score++;
		}
		
		public function decreaseScore():void{
			score++;
		}		
	}
}
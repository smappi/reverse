package screens
{
	
	import citrus.core.starling.StarlingState;
	
	import components.LevelGame;
	
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.events.FeathersEventType;
	import feathers.layout.TiledRowsLayout;
	
	import starling.display.Button;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.RenderTexture;
	
	public class YouWinScreen extends StarlingState
	{
		private var container:ScrollContainer;
		private var isScroll:Boolean = false;

		private var levelGame:LevelGame;

		private var text:TextField;
		public function YouWinScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
		}
		
		override public function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			
			//			var loading:LoadingTagged = new LoadingTagged();
			//			addChild(loading);
			//			TweenMax.delayedCall(5,function():void{loading.removeMe()});
			//			return;
			
			text = new TextField(stage.stageWidth, 200, "Đang tải chờ xí","Verdana",80,0xffffff);
			text.alignPivot();
			text.x = stage.stageWidth/2;
			text.y = stage.stageHeight - text.height;
			addChild(text);
			
			initView();
			levelGame = new LevelGame();
			
		}
		
		private function buttonRefeshHandler(e:Event):void
		{
			// TODO Auto Generated method stub
			levelGame.readJsonOnline("http://118.69.70.144:8080/downloads/smappi/reverse/levels.json?i="+Math.random()+99999);
			text.alpha = 1;
			container.removeFromParent(true);
			levelGame.onLoadedCompleted.add(initView);
		}
		
		private function initView():void{
			text.alpha = 0;
			container = new ScrollContainer();
			var layout:TiledRowsLayout = new TiledRowsLayout();
			layout.manageVisibility = true;
			layout.verticalGap = 25;
			layout.horizontalGap = 30;
			container.y = 100;
			container.x = 60;
			container.width = stage.stageWidth - 120;
			container.height = stage.stageHeight - 100;
			container.snapToPages = true;
			layout.paging = TiledRowsLayout.PAGING_VERTICAL;
			container.layout = layout;
			container.scrollBarDisplayMode = List.SCROLL_BAR_DISPLAY_MODE_NONE;
			
			container.addEventListener(FeathersEventType.SCROLL_START,handleScrollBegin);
			container.addEventListener(FeathersEventType.SCROLL_COMPLETE,handleScrollComplete);
			addChild(container);
			
			var quadR:Quad = new Quad(100,100,0xff0000);
			var renderR:RenderTexture = new RenderTexture(quadR.width, quadR.height,true);
			renderR.draw(quadR);
			var buttonRefesh:Button = new Button(renderR);
			buttonRefesh.text = "Nam";
			buttonRefesh.fontSize = 20;
			buttonRefesh.fontColor = 0xffffff;
			container.addChild(buttonRefesh);
			buttonRefesh.addEventListener(Event.TRIGGERED,buttonRefeshHandler);
			
			var buttonRefeshTien:Button = new Button(renderR);
			buttonRefeshTien.text = "Tiến";
			buttonRefeshTien.fontSize = 20;
			buttonRefeshTien.fontColor = 0xffffff;
			container.addChild(buttonRefeshTien);
			buttonRefeshTien.addEventListener(Event.TRIGGERED,buttonRefeshTienHandler);
			
			var listLevel:Array = LevelGame.getListLevel();
			if (!listLevel) 
			{
				return;
			}
			for (var i:int = 0; i < listLevel.length; i++) 
			{
				var obj:Object = listLevel[i];
				var quad:Quad = new Quad(100,100,0xffffff);
				var render:RenderTexture = new RenderTexture(quad.width, quad.height,true);
				render.draw(quad);
				var buttonLevel:Button = new Button(render);
				buttonLevel.text = String(obj.name);
				buttonLevel.fontSize = 30;
				buttonLevel.fontColor = 0x000000;
				container.addChild(buttonLevel);
				buttonLevel.addEventListener(Event.TRIGGERED,selectLevel);
			}
		}
		
		private function buttonRefeshTienHandler(e:Event):void
		{
			// TODO Auto Generated method stub
			levelGame.readJsonOnline("http://118.69.70.144:8080/downloads/smappi/reverse/levels1.json?i="+Math.random()+99999);
			text.alpha = 1;
			container.removeFromParent(true);
			levelGame.onLoadedCompleted.add(initView);
		}
		
		private function selectLevel(e:Event):void
		{
			// TODO Auto Generated method stub
			if (isScroll) 
			{
				return;
			}
			
			var bt:Button = e.currentTarget as Button;
			LevelGame.level(bt.text);
		}
		
		private function handleScrollComplete():void
		{
			// TODO Auto Generated method stub
			isScroll = false;
		}
		
		private function handleScrollBegin():void
		{
			// TODO Auto Generated method stub
			isScroll = true;
		}		
		
	}
}
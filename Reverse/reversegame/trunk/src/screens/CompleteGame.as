package screens
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import elements.TitleComplete;
	import elements.friend.PopupFriend;
	import elements.loading.LoadingTagged;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureSmoothing;
	import starling.utils.deg2rad;
	
	import utils.UtilsAndConfig;
	
	public class CompleteGame extends Sprite
	{
		private var _color:int = 0;
		private var _level:int = 0;
		private var _time:Number = 0;
		private var _die:int = 0;
		private var imgCord:Image;

		private var btnContinue:Button;
		private var btnWatchVideo:Button;

		private var imgLightRight:Image;
		private var imgLightLeft:Image;

		private var listFriend:PopupFriend;

		private var title:TitleComplete;
		public function CompleteGame(color:int = 0,level:int = 0,time:Number = 0,die:int = 0)
		{
			super();
			_color = color;
			_level = level;
			_time = time;
			_die = die;
			addEventListener(Event.ADDED_TO_STAGE,init);
			addEventListener(Event.REMOVED_FROM_STAGE, remove);
		}
		
		private function remove(e:Event):void
		{
			if (Reverse.instance.sound.getSound("bat_wing_flap").isPlaying) 
			{
				UtilsAndConfig.stopSound("bat_wing_flap");
			}
			UtilsAndConfig.score =  0;
			
		}
		
		private function init(e:Event):void
		{
			addChild( new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,_color));
			UtilsAndConfig.playSound("stage_complete");
			
			imgCord = new Image(UtilsAndConfig.defaultAssets.getTexture("stonetop"));
			imgCord.alignPivot("center","top");
			imgCord.scaleX = imgCord.scaleY = 2;
			imgCord.smoothing = TextureSmoothing.NONE;
			imgCord.x = stage.stageWidth/2;
			addChild(imgCord);
			
			var itemOnGround:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("itemOnGround"));
			itemOnGround.alignPivot();
			itemOnGround.scaleX = itemOnGround.scaleY = 2;
			itemOnGround.smoothing = TextureSmoothing.NONE;
			itemOnGround.x = stage.stageWidth/2;
			itemOnGround.y = stage.stageHeight - 55;
			addChild(itemOnGround);
			
			var quadGray:Quad = new Quad(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight,0x7d7f79)
			quadGray.alpha = 0.5;
			addChild( quadGray);
			
			btnContinue = new Button(UtilsAndConfig.defaultAssets.getTexture("btn_con2"),"",UtilsAndConfig.defaultAssets.getTexture("btn_con1"));
			btnContinue.alignPivot("center","bottom");
			btnContinue.scaleX = btnContinue.scaleY = 2;
			Image(Sprite(btnContinue.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnContinue.x = stage.stageWidth - 150;
			btnContinue.y = stage.stageHeight + 10;
			btnContinue.addEventListener(Event.TRIGGERED,function():void{
				var loading:LoadingTagged = new LoadingTagged();
				loading.close();
				addChild(loading);
				loading.addEventListener("FINISH",function():void{
					loading.removeMe();
					Reverse.instance.state = new LevelScreen();
				});
				
			});
			addChild(btnContinue);
			
			var imgBranch:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("branch"));
			imgBranch.alignPivot("right","center");
			imgBranch.scaleX = imgBranch.scaleY = 2;
			imgBranch.smoothing = TextureSmoothing.NONE;
			imgBranch.x = stage.stageWidth + 60;
			imgBranch.y = Starling.current.stage.stageHeight/2 - imgBranch.height/2 + 90;
			addChild(imgBranch);
			
			btnWatchVideo = new Button(UtilsAndConfig.defaultAssets.getTexture("btn_watchreplay1"),"",UtilsAndConfig.defaultAssets.getTexture("btn_watchreplay2"));
			btnWatchVideo.alignPivot("left","bottom");
			btnWatchVideo.scaleX = btnWatchVideo.scaleY = 2;
			Image(Sprite(btnWatchVideo.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnWatchVideo.x = stage.stageWidth - imgBranch.width + btnWatchVideo.width/2 - 16;
			btnWatchVideo.y = imgBranch.y - imgBranch.height/2 +40;
			addChild(btnWatchVideo);
			
			title = new TitleComplete(""+_level,UtilsAndConfig.score);
			title.alignPivot();
			title.x = stage.stageWidth/2;
			title.y = stage.stageHeight/2;
			addChild(title);
			
			
			listFriend = new PopupFriend();
			listFriend.alignPivot("center","bottom");
			listFriend.x = title.x - title.width/2 - listFriend.width/2 + 40;
			listFriend.y = stage.stageHeight + 420;
			addChild(listFriend);
			
			
			listFriend.addEventListener(TouchEvent.TOUCH,touchListFriend);
			
			imgLightRight = new Image(UtilsAndConfig.defaultAssets.getTexture("lightComplete"));
			imgLightRight.touchable = false;
			imgLightRight.alignPivot("right","top");
			imgLightRight.scaleX = imgLightRight.scaleY = 2;
			imgLightRight.smoothing = TextureSmoothing.NONE;
			
			imgLightRight.x = stage.stageWidth;
			imgLightRight.y =  -55;
			imgLightRight.alpha = 0;
			addChild(imgLightRight);
			
			
			imgLightLeft = new Image(UtilsAndConfig.defaultAssets.getTexture("lightComplete"));
			imgLightLeft.touchable = false;
			imgLightLeft.alignPivot("right","top");
			imgLightLeft.scaleX = imgLightLeft.scaleY = 2;
			imgLightLeft.scaleX = -2;
			imgLightLeft.smoothing = TextureSmoothing.NONE;
			imgLightLeft.x = 0;
			imgLightLeft.y =  -55;
			imgLightLeft.alpha = 0;
			addChild(imgLightLeft);
			animation();
		}
		
		private function touchListFriend(e:TouchEvent):void
		{
			// TODO Auto Generated method stub
			var touch:Touch = e.getTouch(e.currentTarget as starling.display.DisplayObject);
			if(touch==null) return;
			
			if (touch.phase == TouchPhase.BEGAN) 
			{
				TweenMax.to(listFriend,3,{y:title.y+title.height/2,ease:Back.easeOut});
			}
		}
		
		private function animation():void{
			TweenMax.from(btnContinue,0.3,{delay:1,rotation:""+deg2rad(-180),ease:Back.easeOut});
			
			TweenMax.delayedCall(0.25,function():void{
				imgLightRight.alpha = 1;
				TweenMax.to(imgLightRight,3,{delay:0.5,rotation:deg2rad(-30),yoyo:true,repeat:-1,repeatDelay:0.5});
			});
			
			TweenMax.delayedCall(0.25,function():void{
				imgLightLeft.alpha = 1;
				TweenMax.to(imgLightLeft,3,{delay:0.5,rotation:deg2rad(30),yoyo:true,repeat:-1,repeatDelay:0.5});
			});
		}
		
	}
}
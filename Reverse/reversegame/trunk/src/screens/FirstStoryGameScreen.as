package screens
{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import citrus.core.starling.StarlingState;
	import citrus.objects.CitrusSprite;
	import citrus.view.ACitrusCamera;
	
	import components.LevelGame;
	
	import elements.InstructionStory1;
	import elements.mirrorRoom.MyBoy;
	import elements.mirrorRoom.StatusAnimation;
	
	import obj.MyBalloon;
	import obj.MyDecorStory1;
	import obj.MyTent;
	import obj.MyTitleGame;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	public class FirstStoryGameScreen extends StarlingState
	{
		private var sensor:CitrusSprite;
		private var objItems:Array = [];
		
		private var myBoy:MyBoy;
		private var localPos:Point;
		private var mouseX:Number;
		private var mouseY:Number;
		private var heroX:Number;
		private var heroY:Number;
		private var localPos2:Point;
		private var listPositions:Array = [];
		private var listCitrus:Array = [];
		private var citrusGate:CitrusSprite;
		private var tween:TweenMax;
		private var imagePopupAnswer:Image;
		private var _camera:ACitrusCamera;
		public function FirstStoryGameScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
			
			for(var i:int=numChildren-1;i>=0;i--){
				getChildAt(i).removeFromParent(true); //remove tất cả thành phần trong screen
			}
			
			removeEventListeners();
			for(var j:int=listCitrus.length-1;j>=0;j--){
				remove(listCitrus[j]);
			}
			
			_camera.enabled = false;
		}
		
		override public function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			UtilsAndConfig.playSound("story_song_mixdown");
			var quad:Quad = new Quad(4000,2000,0xb9b2b2);
			var bg1:CitrusSprite = new CitrusSprite("bg1",{x:0,y:0});
			bg1.view = quad;
			add(bg1);
			
			var imgBg:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("gamestory_1bg"));
			imgBg.scaleY = 2;
			imgBg.width = 4000;
			var bg2:CitrusSprite = new CitrusSprite("bg2",{x:0,y:0});
			bg2.view = imgBg;
			add(bg2);
			
			listCitrus.push(bg1);
			listCitrus.push(bg2);
			
			//điều khiển camera để scroll map
			var quadSensor:Quad = new Quad(30,30);
			quadSensor.alpha = 0;
			var quadX:int = 0;
			
			sensor = new CitrusSprite("sensor",{x:0,y:0});
			sensor.group = 40;
			sensor.view = quadSensor;
			add(sensor);
			listCitrus.push(sensor);
			
			var stageRect:Rectangle = new Rectangle(0,0,4000,2000);
			_camera = view.camera.setUp(sensor,stageRect,new Point(0.5 , 0.5),new Point(0.5,0.5));
			_camera.reset();
			_camera.parallaxMode = ACitrusCamera.PARALLAX_MODE_TOPLEFT;
			_camera.allowZoom = true;
			_camera.allowRotation = false;
			//			view.camera.zoom(0.1);
			//			TweenMax.to(sensor,10,{y:2000,ease:Circ.easeOut});
			
			var json:Object = JSON.parse(LevelGame.getJsonStory1());
			var arr:Array = json.layers;
			for (var i:int = 0; i < arr.length; i++) 
			{
				
				var arrObjs:Array = arr[i].objects;
				for (var j:int = 0; j < arrObjs.length; j++) 
				{
					var obje:Object = arrObjs[j];
					obje.group = arr[i].name;
					objItems.push(obje);
				}
				
			}
			
			var parallaxX:Number = 0;
			for (var k:int = 0; k < objItems.length; k++) 
			{
				var object:Object = objItems[k];
				
				if (object.group == "group4") 
				{
					parallaxX = 0.1;
				}
				
				if (object.group == "group3") 
				{
					parallaxX = 0.5;
				}
				
				if (object.group == "group2") 
				{
					parallaxX = 1;
				}
				
				if (object.group == "group1") 
				{
					parallaxX = 1.5;
				}
				switch(object.name)
				{
					case "decor":
					{
						if(int(object.properties.style)!=13){
							var decor:MyDecorStory1 = new MyDecorStory1("sItem"+object.properties.style,{x:object.x,y:object.y,parallaxX:parallaxX},0);
							add(decor);
							if (object.group == "group3" ) 
							{
								decor.view.alpha = 0.5;
							}
							
							if (object.group == "group4" ) 
							{
								decor.view.alpha = 0.3;
							}
							
							if (object.group == "group1") 
							{
								decor.group = 10;
							}
							listCitrus.push(decor);
						}else{
							var title:MyTitleGame = new MyTitleGame("title",{x:object.x,y:object.y});
							add(title);
						}
						
						break;
					}
						
					case "line":{
						var imgLine:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("sItem_Line"));
						imgLine.width = 4000;
						imgLine.smoothing = TextureSmoothing.NONE;
						imgLine.scaleY = 2;
						imgLine.x = 0;
						imgLine.y = object.y;
						imgLine.alpha = 0.4;
						var lineCitrus:CitrusSprite = new CitrusSprite("line",{x:object.x,y:object.y});
						lineCitrus.view = imgLine;
						add(lineCitrus);
						listCitrus.push(lineCitrus);
						break;
					}
						
					case "smalltent":
					{
						var myTent:MyTent = new MyTent("smalltent",{x:object.x,y:object.y,parallaxX:parallaxX});
						add(myTent);
						if (object.group == "group3") 
						{
							myTent.view.alpha = 0.4;
						}
						listCitrus.push(myTent);
						break;
					}
						
					case "bigtent":
					{
						var mybigTent:MyTent = new MyTent("bigtent",{x:object.x,y:object.y,parallaxX:parallaxX});
						add(mybigTent);
						listCitrus.push(mybigTent);
						break;
					}
						
					case "hero":
					{
						myBoy = new MyBoy("hero",{x:object.x,y:object.y},false);
						myBoy.theBoy.startBoy();
						myBoy.theBoy.status ="";
						add(myBoy);
						listCitrus.push(myBoy);
						
						balloon = new MyBalloon("balloon",{x:myBoy.x,y:myBoy.y-myBoy.view.height/2});
						add(balloon);
						listCitrus.push(balloon);
						
						break;
					}
						
					case "gate":{
						var quadGate:Quad = new Quad(100,400);
						quadGate.alpha = 0;
						citrusGate = new CitrusSprite("gate",{x:object.x,y:object.y,width:object.width,height:object.height});
						citrusGate.view = quadGate;
						add(citrusGate);
						listCitrus.push(citrusGate);
						break;
					}
						
					case "star":{
						var starImage:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("sItem_star"));
						starImage.smoothing = TextureSmoothing.NONE;
						starImage.scaleX = starImage.scaleY = 2;
						citrusStarImage = new CitrusSprite("star",{x:object.x,y:object.y,width:object.width,height:object.height});
						citrusStarImage.view = starImage;
						add(citrusStarImage);
						listCitrus.push(citrusStarImage);
						break;
					}
						
					case "detect":{
						var quadDetect:Quad = new Quad(object.width,object.height,0xff);
						quadDetect.alpha = 0;
						citrusQuadDetect = new CitrusSprite("citrusQuadDetect",{x:object.x,y:object.y,width:object.width,height:object.height});
						citrusQuadDetect.view = quadDetect;
						add(citrusQuadDetect);
						listCitrus.push(citrusQuadDetect);
						break;
					}
						
					case "pAnswer":{
						var pAnswerImage:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("storyAnswer2P"));
						pAnswerImage.smoothing = TextureSmoothing.NONE;
						pAnswerImage.scaleX = pAnswerImage.scaleY = 2;
						pAnswerImage.alignPivot();
						citruspAnswerImage = new CitrusSprite("citruspAnswerImage",{x:object.x,y:object.y,width:object.width,height:object.height});
						citruspAnswerImage.view = pAnswerImage;
						add(citruspAnswerImage);
						listCitrus.push(citruspAnswerImage);
						break;
					}
				}
			}
			
			quadMove = new Quad(stage.stageWidth, stage.stageHeight,0x000000);
			quadMove.alpha = 0;
			addChild(quadMove);

			TweenMax.to(sensor,5,{y:myBoy.y + 30,ease:Linear.easeNone,onComplete:function():void{
				sensor.x = myBoy.x;
				sensor.y = myBoy.y;
				var imagePopup:Image = new Image(UtilsAndConfig.defaultAssets.getTexture("story1P"));
				imagePopup.alignPivot("left","bottom");
				imagePopup.scaleX = imagePopup.scaleY = 2;
				imagePopup.smoothing = TextureSmoothing.NONE;
				var citrusImagePopup:CitrusSprite = new CitrusSprite("imagePopup",{x:myBoy.x - myBoy.width/2,y:myBoy.y - myBoy.height});
				citrusImagePopup.view = imagePopup;
				add(citrusImagePopup);
				
				TweenMax.delayedCall(1.5,function():void{
					imagePopup.texture = UtilsAndConfig.defaultAssets.getTexture("story2P");
					imagePopup.readjustSize();
					citrusImagePopup.y -= 20;
					TweenMax.delayedCall(2,function():void{
						imagePopup.texture = UtilsAndConfig.defaultAssets.getTexture("story3P");
						imagePopup.readjustSize();
						TweenMax.delayedCall(1.5,function():void{
							remove(citrusImagePopup);
							imagePopupAnswer = new Image(UtilsAndConfig.defaultAssets.getTexture("storyAnswer1P"));
							imagePopupAnswer.scaleX = imagePopupAnswer.scaleY = 2;
							imagePopupAnswer.smoothing = TextureSmoothing.NONE;
							imagePopupAnswer.x = stage.stageWidth - imagePopupAnswer.width;
							imagePopupAnswer.y = stage.stageHeight - imagePopupAnswer.height - 180;
							addChild(imagePopupAnswer);
							TweenMax.to(imagePopupAnswer,0.5,{alpha:0,yoyo:true,repeat:-1});
							//sau khi sau cau tra loi dau tien
							//							TweenMax.delayedCall(1,showInstruction);
							
							//2 giay sau moi co action cho user hanh dong
							TweenMax.delayedCall(2,function():void{
								if(!flyBalloon){
									TweenMax.to(balloon,17,{y:0,onComplete:function():void{
										remove(balloon);
									}});
								}
								flyBalloon = true;
								//								quadMove.addEventListener(TouchEvent.TOUCH,moveHero);
								(myBoy as MyBoy).theBoy.move();
								TweenMax.staggerTo([sensor,myBoy],10,{x:citrusGate.x,ease:Linear.easeNone,onComplete:function():void{
									(myBoy as MyBoy).theBoy.stopBoy();
									TweenMax.staggerTo([myBoy,sensor],2,{delay:1,x:3840+myBoy.view.width,y:1900+myBoy.view.height/2,onComplete:function():void{
										myBoy.view.alpha = 0;
										TweenMax.delayedCall(0.25,function():void{
											_ce.state = new MirrorRoom();
										});
									},onUpdate:function():void{
										if ((myBoy as MyBoy).theBoy.status != StatusAnimation.MOVE) 
										{
											(myBoy as MyBoy).theBoy.move();
										}
									}});
								}});
								addEventListener(Event.ENTER_FRAME,initEnterFrame);
								
								
							});
							
							
						});
					});
				});
				
			}});
			
		}
		
		private var heroBound:Rectangle;
		private var citrusGateBound:Rectangle;
		
		private var quadMove:Quad;
		private var citrusStarImage:CitrusSprite;
		private function initEnterFrame(e:Event):void
		{
			
			heroBound = (myBoy.view as DisplayObject).getBounds(Starling.current.stage,heroBound);
			citrusGateBound = (citrusGate.view as DisplayObject).getBounds(Starling.current.stage,citrusGateBound);
			
			if (heroBound.intersects(citrusGateBound)) 
			{
//				TweenMax.killTweensOf(myBoy);
				remove(citruspAnswerImage);
//				quadMove.removeEventListener(TouchEvent.TOUCH,moveHero);
				removeEventListener(Event.ENTER_FRAME,initEnterFrame);
				
			}
			
			citrusQuadDetectBound = (citrusQuadDetect.view as DisplayObject).getBounds(Starling.current.stage,citrusQuadDetectBound);
			if (heroBound.intersects(citrusQuadDetectBound)) 
			{
				imagePopupAnswer.removeFromParent(true);
			}
			
		}
		
		private function showInstruction():void{
			if(tween){
				tween.kill();
			}
			
			ins = new InstructionStory1();
			ins.alignPivot();
			ins.x = stage.stageWidth/2;
			ins.y = stage.stageHeight/2;
			addChild(ins);
			
			//4 giay sau man hinh instruction bien mat
			TweenMax.delayedCall(4,function():void{
				ins.hideView();
			});
		}
		
		private var flyBalloon:Boolean = false;
		
		private var balloon:MyBalloon;
		
		private var ins:InstructionStory1;
		private var citrusQuadDetect:CitrusSprite;
		private var citrusQuadDetectBound:Rectangle;
		private var citruspAnswerImage:CitrusSprite;
		
		private function moveHero(e:TouchEvent):void
		{
			// TODO Auto Generated method stub
			if(stage==null) return;
			var touch:Touch = e.getTouch(this);
			if(touch==null) return;
			
			var position:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.ENDED){
				if (!ins) 
				{
					tween = TweenMax.delayedCall(3,showInstruction);
				}
				
				if ((myBoy as MyBoy).theBoy.status != StatusAnimation.IDLE) 
				{
					(myBoy as MyBoy).theBoy.stopBoy();
				}
			}else if(touch.phase == TouchPhase.BEGAN){
				if(!flyBalloon){
					TweenMax.to(balloon,17,{y:0,onComplete:function():void{
						remove(balloon);
					}});
				}
				flyBalloon = true;
				listPositions = [];
				localPos = touch.getLocation(this);
				listPositions.push(localPos.x);
				mouseX = localPos.x;
				mouseY = localPos.y;
				heroX = sensor.x;
				heroY = sensor.y;
				
			}else if(touch.phase == TouchPhase.MOVED){
				localPos2 = touch.getLocation(this);
				if (localPos == null) 
				{
					return;
				}
				listPositions.push(localPos2.x);
				if ((myBoy as MyBoy).theBoy.status != StatusAnimation.MOVE) 
				{
					(myBoy as MyBoy).theBoy.move();
				}
				if (listPositions[listPositions.length - 1] >= listPositions[listPositions.length - 2] && listPositions.length >= 2) 
				{
					(myBoy as MyBoy).lookRight();
					
					TweenMax.staggerTo([myBoy,sensor],0.1,{x:"+20",ease:Linear.easeNone, onUpdate:function():void{
						myBoy.x = Math.floor(myBoy.x);
						myBoy.y = Math.floor(myBoy.y);
						sensor.x = myBoy.x;
						sensor.y = myBoy.y;
					}});
				}
				else{
					(myBoy as MyBoy).lookLeft();
					
					TweenMax.staggerTo([myBoy,sensor],0.1,{x:"-20",ease:Linear.easeNone, onUpdate:function():void{
						myBoy.x = Math.floor(myBoy.x);
						myBoy.y = Math.floor(myBoy.y);
						sensor.x = myBoy.x;
						sensor.y = myBoy.y;
						if(myBoy.x <= 0){
							myBoy.x = 0;
							sensor.x = 0;
						}
					}});
				}
			}
			else if(touch.phase == TouchPhase.STATIONARY){
				//				trace("???");
			}
		}
	}
}
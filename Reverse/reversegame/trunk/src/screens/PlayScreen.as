package screens
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import citrus.core.starling.StarlingState;
	import citrus.math.MathVector;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.simple.DynamicObject;
	import citrus.objects.platformer.simple.Sensor;
	import citrus.physics.simple.SimpleCitrusSolver;
	import citrus.view.ACitrusCamera;
	
	import components.LevelGame;
	
	import elements.SpritePause;
	import elements.loading.LoadingTagged;
	
	import obj.MyBoom;
	import obj.MyButton;
	import obj.MyGateFake;
	import obj.MyGrave;
	import obj.MyGun;
	import obj.MyHand;
	import obj.MyHero;
	import obj.MyLineSaw;
	import obj.MyMap;
	import obj.MyPolygon;
	import obj.MyRadiusSpiderRun;
	import obj.MyRangeBoomSensor;
	import obj.MySaw;
	import obj.MySawStart;
	import obj.MySensor;
	import obj.MySkull;
	import obj.MySpider;
	import obj.MySpiderFollow;
	import obj.MySpiderRun;
	import obj.MyStar;
	import obj.MyStartSpiderRun;
	import obj.MyTeleport;
	import obj.MyThorn;
	import obj.MyThornStart;
	import obj.MyTree;
	import obj.MyWallBlock;
	import obj.MyWallMove;
	import obj.MyWallMoveDetect;
	import obj.MyWheel;
	
	import objpooling.ObjPooling;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	
	
	
	public class PlayScreen extends StarlingState
	{
		
		private var listPostionWhenMoveds:Array = new Array();
		
		private var quadHero:Quad;
		
		private var quadHero2:Quad;
		
		private var sHero:Sensor;
		
		
		
		private var myMap:MyMap;
		
		private var pause:SpritePause;
		
		private var mapWidth:Number;
		
		private var mapHeight:int;
		
		private var hero:MyHero;
		
		private var mouseX:Number;
		
		private var mouseY:Number;
		
		private var heroX:Number;
		
		private var heroY:Number;
		
		private var quad:Quad;
		
		public static var gameOver:Boolean = true;
		
		private var isImmortal:Boolean = false;
		
		public var lvlEnded:Signal;
		public var lvlRestart:Signal;
		
		public var max:int = 4999;
		public var min:int = 353;
		
		//mang chua cac doi tuong can reset sau khi herodie
		private var listObjectResets:Array = [];
		//mang chua cac doi tuong va cham voi hero
		private var listSensors:Array = [];
		private var zoom:Number = 0;
		//mang chua cac doi tuong xoay tron
		private var listWheels:Array = [];
		private var colorMap:int = 0;
		//mang chua cac doi tuong chua sprite de flatten
		private var listObjectCitrus:Array = [];
		//mang chua danh sach cac lanh tho cua con nhen di theo hero
		private var listRadiusSpiders:Array = [];
		
		private var stageRect:Rectangle = new Rectangle(0,0,Starling.current.stage.stageWidth,Starling.current.stage.stageHeight);
		private var time:Number = 0;
		private var die:int = 0;
		private var isTouch:Boolean = true;
		
		private var listPooling:Array = [];
		
		private var localPos:Point;
		
		private var listGun:Array;
		
		private var flagButton:Boolean = false;
		
		private var listButton:Array;
		private var levelNext:String = "";
		
		private var listDetectWallMove:Array;
		
		private var listWallMove:Array;
		
		private var listSawStart:Array;
		
		private var listSaw:Array;
		
		private var listThornStart:Array;
		
		private var listThorn:Array;
		
		private var nameLevel:String;
		
		public function PlayScreen(levelParam:Object = null)
		{
			super();
		}
		
		override public function destroy():void
		{
			
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
			UtilsAndConfig.stopSound("level");
			//			if (sHero) 
			//			{
			//				sHero.onCollide.remove(_collisionStart);
			//			}
			//			for(var i:int=numChildren-1;i>=0;i--){
			//				trace("getChildAt(i)",UtilsAndConfig.getClass(getChildAt(i)));
			//				ObjPooling.release(getChildAt(i));
			//				getChildAt(i).removeFromParent(); //remove tất cả thành phần trong screen
			//			}
			//			
			//			for(var i1:int=listObjectResets.length-1;i1>=0;i1--){
			//				remove(listObjectResets[i1]);
			//			}
			//			
			//			removeEventListeners();
			//			for(var j:int=listSensors.length-1;j>=0;j--){
			//				remove(listSensors[j]); 
			//			}
			
			for(var j:int=listPooling.length-1;j>=0;j--){
				if(!ObjPooling.release(listPooling[j])){
					remove(listPooling[j]);	
				}
				else{
					
				}
			}
			
			System.pauseForGCIfCollectionImminent(0);
			System.gc(); //dọn rác
			
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			
			if (stage.stageHeight >= 640 || stage.stageWidth >= 1156) 
			{
				zoom = 1;
			}else{
				zoom = 1.4;
			}
			
			init();
			time = new Date().getTime();
			
			var loading:LoadingTagged = new LoadingTagged();
			loading.open();
			addChild(loading);
			loading.addEventListener("FINISH",function():void{
				loading.removeMe();
				
			});
			
		}
		
		private function init():void
		{
			if (!Reverse.instance.sound.getSound("level").isPlaying) 
			{
				UtilsAndConfig.playSound("level");
			}
			
			var simpleCitrusSolver:SimpleCitrusSolver = new SimpleCitrusSolver("citrus solver");
			add(simpleCitrusSolver);
			
			simpleCitrusSolver.overlap(DynamicObject, Sensor);
			
			var level:Object = LevelGame.getLevel();
			var mapObj:Object = level;
			trace(mapObj.name);
			nameLevel = mapObj.name;
			//			trace("mapObj.name ",mapObj.name);
			//			trace("mapObj.name ",mapObj.name);
			
			//save lai gia tri cua level hien tai
			if (LevelGame.getCurrentLevel() > int(UtilsAndConfig.getCurrentLevel())) 
			{
				UtilsAndConfig.setLevelCurrent(LevelGame.getCurrentLevel());
			}
			listGun = [];
			listButton=[];
			//			mapWidth = mapObj.width;
			//			mapHeight = mapObj.height;
			listDetectWallMove = [];
			listWallMove = [];
			listSawStart = [];
			listSaw = [];
			listThornStart = [];
			listThorn = [];
			var objs:Object = mapObj.objects;
			var count:int = 0;
			levelNext = mapObj.properties.ln;
			for each (var i:Object in objs) 
			{
				var name:String = i.name;
				var width:int = i.width;
				var height:int = i.height;
				var x:int = i.x ;
				var y:int = i.y;
				var startX:int = x;
				var startY:int = y;
				var stopX:int = int(i.properties.stopX);
				var stopY:int = int(i.properties.stopY);
				var speed:int = int(i.properties.speed);
				var delay:int = int(i.properties.delay);
				var range:uint = uint(i.properties.range);
				var time:uint = uint(i.properties.time);
				var start:Boolean = Boolean(i.properties.start);
				var length:uint = uint(i.properties.length);
				if (count == objs.length-1) 
				{
					gameOver = false;
				}
				count++;
				switch(name.toLowerCase()){
					case "hero":{
						//						hero = new MyHero("hero", {x:x, y:y, width:30, height:30});
						hero = ObjPooling.summon(MyHero,"hero",{x:x, y:y, width:30, height:30}) as MyHero;
						hero.setAtrr(x,y,30,30);
						hero.resetHero();
						hero.changeStatusHeroStart();
						add(hero);
						
						listObjectResets.push(hero);
						
						quadHero2 = new Quad(30,30,0xff0000);
						quadHero2.alignPivot();
						quadHero2.alpha = 0;
						sHero = new Sensor("herotmp",{x:x, y:y, width:30, height:30});
						sHero.view = quadHero2;
						
						add(sHero);
						var stageRect:Rectangle = new Rectangle(0,0,mapWidth,mapHeight);
						view.camera.setUp(hero,stageRect,new Point(0.5 , 0.5),new Point(0.15,0.15));
						view.camera.reset();
						view.camera.parallaxMode = ACitrusCamera.BOUNDS_MODE_OFFSET;
						view.camera.allowZoom = true;
						view.camera.allowRotation = false;
						view.camera.zoom(zoom);
						
					} break;
					
					case "block":{
						var sensor:MySensor = new MySensor((new Date()).getTime()+"Sensor",{x:x,y:y,width:width,height:height},stopX,stopY,speed,delay,i.properties.style); 
						add(sensor);
						listSensors.push(sensor);
					} break;
					
					case "wall_block":{
						var wall_block:MyWallBlock = new MyWallBlock((new Date()).getTime()+"MyWallBlock"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},stopX,stopY,speed,delay,i.properties.style); 
						add(wall_block);
						listSensors.push(wall_block);
					} break;
					
					case "bg":{
						//						myMap = new MyMap("bg",{width:width,height:height,x:x,y:y});
						myMap = ObjPooling.summon(MyMap,"bg",{width:width,height:height,x:x,y:y}) as MyMap;
						myMap.setAttr({width:width,height:height,x:x,y:y});
						add(myMap);
						listPooling.push(myMap);
						colorMap = myMap.color_;
						listObjectResets.push(myMap);
						mapWidth = width;
						mapHeight = height;
						break;
					}
						
						//					case "starcover":{
						//						var starcover:MyGate = new MyGate("starcover",{x:x,y:y,width:width,height:height});
						//						add(starcover);
						//					}break;
						
					case "star":{
						var star:MyStar = ObjPooling.summon(MyStar,"star",{x:x,y:y,width:5,height:5}) as MyStar;
						star.setAttr({x:x,y:y,width:5,height:5});
						listPooling.push(star);
						add(star);
						break;
					}
						
					case "grave":{
						var grave:MyGrave = ObjPooling.summon(MyGrave,"grave",{x:x,y:y,width:width,height:height},i.properties.style) as MyGrave;
						grave.setAttr(i.properties.style);
						listPooling.push(grave);
						add(grave);
						break;
					}
						
					case "tree":{
						var tree:MyTree = ObjPooling.summon(MyTree,"tree",{x:x,y:y,width:width,height:height}) as MyTree;
						tree.setAttr();
						listPooling.push(tree);
						add(tree);
						break;
					}
						
					case "saw":{
						var saw:MySaw = new MySaw((new Date()).getTime()+"saw",{x:x,y:y,width:width,height:height},i.properties.finish, i.properties.listpMove,speed,i.properties.start,i.properties.id);
						add(saw);
						listObjectResets.push(saw);
						listPooling.push(saw);
						listSensors.push(saw);
						listSaw.push(saw);
						break;
					}
						
					case "line":{
						var line :MyLineSaw = new MyLineSaw((new Date()).getTime()+"linesaw",{x:x,y:y,width:width,height:height},i.properties.listp);
						add(line);
						break;
					}
						
					case "boom_radius":{
						var nameBoom:String = (new Date()).getTime()+"boom_range"+UtilsAndConfig.randRange(0,(new Date()).getTime());
						var boom_range: MyRangeBoomSensor = new MyRangeBoomSensor(nameBoom,{x:x,y:y,width:i.properties.range*2,height:i.properties.range*2});
						add(boom_range);
						var boom_radius :MyBoom = new MyBoom(nameBoom+"boom_radius",{x:x,y:y,width:64,height:64},i.properties.range);
						listObjectResets.push(boom_radius);
						add(boom_radius);
						listObjectCitrus.push(boom_radius);
						break;
					}
						
					case "skull":{
						var skull:MySkull = ObjPooling.summon(MySkull,(new Date()).getTime()+"skull"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},range,speed,time) as MySkull;
//						var skull:MySkull = new MySkull((new Date()).getTime()+"skull"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},range,speed,time);
						skull.setAttr({x:x,y:y,width:width,height:height},range,speed,time);
						add(skull);
						listPooling.push(skull);
						skull.hero = hero;
						skull.onCollide.add(_collisionSkullStart);
						
						listObjectResets.push(skull);
						listObjectCitrus.push(skull);
						break;
					}
						
					case "gun":{
						var gun:MyGun = new MyGun((new Date()).getTime()+"gun"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.style,i.properties.time,i.properties.length,i.properties.id,i.properties.status);
						add(gun);
						gun.hero = hero;
						listObjectCitrus.push(gun);
						listObjectResets.push(gun);
						listGun.push(gun);
						gun.onCollide.add(_collisionGunStart);
						break;
					}   
						
					case "hand":{
						//						var hand:MyHand = new MyHand((new Date()).getTime()+"hand"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.act);
						var hand:MyHand = ObjPooling.summon(MyHand,(new Date()).getTime()+"hand"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.act) as MyHand;
						hand.setAttr({x:x,y:y,width:width,height:height});
						add(hand);
						listPooling.push(hand);
						//						listObjectCitrus.push(hand);
						break;
					}
						
					case "thorn":{
						var thorn:MyThorn = new MyThorn((new Date()).getTime()+"thorn"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},uint(i.properties.start),i.properties.time,i.properties.id,i.properties.act);
						add(thorn);
						thorn.hero = hero;
						listObjectCitrus.push(thorn);
						listThorn.push(thorn);
						listObjectResets.push(thorn);
						break;
					}
						
					case "spider":{
						var spider:MySpider = new MySpider((new Date()).getTime()+"spider"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},range,speed) as MySpider;
						add(spider);
						
						spider.hero = hero;
						listPooling.push(spider);
						spider.onCollide.add(_collisionSpiderStart);
						listObjectResets.push(spider);
						//						listObjectCitrus.push(spider);
						break;
					}
						
					case "wheel":{
						var wheel:MyWheel = ObjPooling.summon(MyWheel,(new Date()).getTime()+"wheel"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},speed) as MyWheel;
						//						var wheel:MyWheel = new MyWheel((new Date()).getTime()+"wheel"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},speed);
						wheel.setAttr({x:x,y:y,width:width,height:height},speed);
						add(wheel);
						wheel.tweenMe();
						listWheels = listWheels.concat(wheel.listWheels);
						listObjectCitrus.push(wheel);
						break;
					}
						
					case "fake_g":{
						var fakeGate:MyGateFake = ObjPooling.summon(MyGateFake,(new Date()).getTime()+"fakeGate"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height}) as MyGateFake;
						fakeGate.setAttr({x:x,y:y,width:width,height:height});
						listPooling.push(fakeGate);
						add(fakeGate);
						listObjectResets.push(fakeGate);
						break;
					}
						
					case "teleport":{
						//						var teleport:MyTeleport = ObjPooling.summon(MyTeleport,(new Date()).getTime()+"teleport"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:10,height:10},i.properties.pos) as MyTeleport;
						var teleport:MyTeleport = new MyTeleport((new Date()).getTime()+"teleport"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:10,height:10},i.properties.pos) as MyTeleport;
						//						teleport.setAttr({x:x,y:y,width:10,height:10},i.properties.pos);
						listPooling.push(teleport);
						add(teleport);
						break;
					}
						
					case "wall_m":{
						var wallMove:MyWallMove = new MyWallMove((new Date()).getTime()+"wall_m"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listp,i.properties.speed,i.properties.delay,i.properties.moveStyle,i.properties.start,i.properties.id,i.properties.style);
						add(wallMove);
						wallMove.onCollide.add(_collisionWallMoveStart);
						listObjectResets.push(wallMove);
						listWallMove.push(wallMove);
						break;
					}
						
					case "button":{
						var button:MyButton = new MyButton((new Date()).getTime()+"button"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.list_id);
						add(button);
						listButton.push(button);
						break;
					}
						
					case "fill":{
						var polygon:MyPolygon = new MyPolygon((new Date()).getTime()+"polygon"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:10,height:10},i.polygon);
						add(polygon);
					}break;
					
					case "wall_m_start":{
						var wallMoveStart:MyWallMoveDetect = new MyWallMoveDetect((new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID);
//							ObjPooling.summon(MyWallMoveDetect,(new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID) as MyWallMoveDetect;
//						wallMoveStart.setAttr({x:x,y:y,width:width,height:height},i.properties.listID);
						add(wallMoveStart);
						listPooling.push(wallMoveStart);
						listDetectWallMove.push(wallMoveStart);
						listObjectResets.push(wallMoveStart);
						break;
					}
						
					case "saw_start":{
						var sawStart:MySawStart = new MySawStart((new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID);
//							ObjPooling.summon(MySawStart,(new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID) as MySawStart;
//						sawStart.setAttr({x:x,y:y,width:width,height:height},i.properties.listID);
						add(sawStart);
						listPooling.push(sawStart);
						listSawStart.push(sawStart);
						listObjectResets.push(sawStart);
						break;
					}
						
					case "spiders":{
						var spiderRun:MySpiderRun = new MySpiderRun(nameLevel+"spiderS"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.listpMove,i.properties.speed,i.properties.start);
						//							ObjPooling.summon(MySpiderRun,nameLevel+"spiderS"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.listpMove,i.properties.speed,i.properties.start) as MySpiderRun; 
						//						spiderRun.setAttr("spiderS"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.listpMove,i.properties.speed,i.properties.start);
						listPooling.push(spiderRun);
						add(spiderRun);
						listObjectResets.push(spiderRun);
						listSensors.push(spiderRun);
						break;
					}
						
					case "spiders_start":{
						var spiderStart:MyStartSpiderRun = new MyStartSpiderRun(nameLevel+"spiderStart"+i.properties.listID,{x:x,y:y,width:width,height:height},i.properties.listID);
						//							ObjPooling.summon(MyStartSpiderRun,nameLevel+"spiderStart"+i.properties.listID,{x:x,y:y,width:width,height:height},i.properties.listID) as MyStartSpiderRun; 
						//						spiderStart.setAttr({x:x,y:y,width:width,height:height},i.properties.listID);
						add(spiderStart);
						listPooling.push(spiderStart);
						spiderStart.onCollide.add(detectCollideHeroWithSpiderStart);
						break;
					}
						
					case "spiderr":{
						var spiderFollow:MySpiderFollow = new MySpiderFollow(nameLevel+"spiderR"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.speed);
						//							ObjPooling.summon(MySpiderFollow,nameLevel+"spiderR"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.speed) as MySpiderFollow; 
						//						spiderFollow.setAttr("spiderR"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.speed);
						//						listPooling.push(spiderFollow);
						add(spiderFollow);
						listObjectResets.push(spiderFollow);
						listSensors.push(spiderFollow);
					}break;
					
					case "raspiderr":{
						var radiusSpiderFollow:MyRadiusSpiderRun = new MyRadiusSpiderRun(nameLevel+"radiusSpiderFollow"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.id);
						//							ObjPooling.summon(MyRadiusSpiderRun,nameLevel+"radiusSpiderFollow"+i.properties.id,{x:x,y:y,width:width,height:height},i.properties.id) as MyRadiusSpiderRun;
						//						radiusSpiderFollow.setAttr({x:x,y:y,width:width,height:height},i.properties.id);
						listPooling.push(radiusSpiderFollow);
						add(radiusSpiderFollow);
						listObjectResets.push(radiusSpiderFollow);
						listRadiusSpiders.push(radiusSpiderFollow);
					}break;
					
					case "wall_t_start":{
						var thornStart:MyThornStart = new MyThornStart((new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID);
//							ObjPooling.summon(MyThornStart,(new Date()).getTime()+"detectMove"+UtilsAndConfig.randRange(0,(new Date()).getTime()),{x:x,y:y,width:width,height:height},i.properties.listID) as MyThornStart;
//						thornStart.setAttr({x:x,y:y,width:width,height:height},i.properties.listID);
						add(thornStart);
						listPooling.push(thornStart);
						listThornStart.push(thornStart);
						listObjectResets.push(thornStart);
						break;
					}
						
				}
			}
			quad = new Quad(stage.stageWidth,stage.stageHeight);
			quad.alpha = 0;
			this.addChild(quad);
			
			
			var btnPause:Button = new Button(UtilsAndConfig.defaultAssets.getTexture("btPauseActive"),"",UtilsAndConfig.defaultAssets.getTexture("btPause"));
			Image(Sprite(btnPause.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnPause.scaleX = btnPause.scaleY = 2;
			btnPause.addEventListener(Event.TRIGGERED, btnPauseClickHandle);
			btnPause.x = stage.stageWidth - btnPause.width - 20;
			btnPause.y = 20;
			addChild(btnPause);
			if (quad) 
			{
				quad.addEventListener(TouchEvent.TOUCH, touchHandler);
			}
			
			//cac su kien duoc thuc hien sau khi hero dc load xong
			hero.completeLoader.add(function():void{
				quad.touchable = true;
				isTouch = true;
				if (sHero) 
				{
					sHero.onCollide.add(_collisionStart);
					//					sHero.onPersist.add(_collisionPersist);
					//					sHero.onSeparate.add(_collisionEnd);
				}
				
				addEventListener(Event.ENTER_FRAME, enterFrame);
			});
			
		}	
		
		//doi mau background
		private function changeColorBackground():void{
			TweenMax.delayedCall(10,function():void{
				myMap.reset();
				colorMap = myMap.color_;
				if(pause!==null){
					pause.color = colorMap;
				}
				changeColorBackground();
			});
		}
		
		private function btnPauseClickHandle(e:Event):void
		{
			pause = new SpritePause(colorMap);
			addChild(pause);
			pause.tweenMe();
		}		
		
		
		/**
		 * NHUNG HANH DONG CUA CAC DOI TUONG VAT CAN HERO
		 */
		
		private function detectCollideHeroWithSpiderStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void
		{
			if(!gameOver){
				if (other is MyHero) 
				{
					var spiderStart:MyStartSpiderRun = (self as MyStartSpiderRun);
					for (var i:int = 0; i < spiderStart.lists.length; i++) 
					{
						var spiderRun:MySpiderRun = getObjectByName(nameLevel+"spiderS"+spiderStart.lists[i]) as MySpiderRun;
						if (spiderRun.isActive == MySpiderRun.NOT_RUN) 
						{
							spiderRun.run();
						}
					}
					
				}
			}
			
		}
		
		private function _collisionWallMoveStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			
			if( other is MyGun){
				var gun:MyGun = (other as MyGun);
			}
		}
		
		private function _collisionSkullStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			
			if( ((other is MyBoom) && (other as MyBoom).width != 0)||(other is MyGun)){
				var skull:MySkull = (getObjectByName(self.name) as MySkull);
				skull.die();
			}
		}
		
		private function _collisionSpiderStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			
			if( ((other is MyBoom && !isImmortal) && (other as MyBoom).width != 0)
				||
				(other is MyGun && !isImmortal)
			){
				var spider:MySpider = (getObjectByName(self.name) as MySpider);
				if(spider.isStart){
					spider.die();
				}
			}
		}
		
		private function _collisionGunStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			if( other is MyWallMove && !isImmortal)
			{
				var block:MyWallMove = (other as MyWallMove);
				var gun:MyGun = (self as MyGun);
				if(block.style == MyWallMove.HORIZONTAL){
					
					if(gun.style == "left" || gun.style == "right"){
						switch(gun.style)
						{
							case "left":
							{
								if(block.x - (gun.params.x+gun._lengthRange + gun.widthGun)<=-16){
									gun.tmpLess();
								}
								if(block.x - (gun.params.x+gun._lengthRange + gun.widthGun) > 0){
									gun.reSizeColliteGreat(32);
								}
								break;
							}
								
							case "right":
							{
								if((gun.params.x - gun._lengthRange - gun.widthGun) -(block.x - block.view.width/2)<=-16){
									gun.tmpLess();
								}
								if((gun.params.x - gun._lengthRange - gun.widthGun) -(block.x - block.view.width/2) > -16){
									gun.reSizeColliteGreat(32);
								}
								break;
							}
								
							default:
							{
								break;
							}
						}
					}
					else{
						var tmpX:Number = block.x + block.view.width/2;
						if((block.x - block.view.width/2 < ( gun.x )) &&
							(tmpX>( gun.x)))
						{
							switch(gun.style)
							{
								case "top":
								{
									if(block.y - (gun.params.y+gun._lengthRange + gun.widthGun)<=-32){
										gun.lessVertical(block.y);
									}
									break;
								}
									
								case "bottom":
								{
									if((gun.params.y - gun._lengthRange - gun.widthGun) -(block.y - block.view.height/2)<=-16){
										gun.lessVertical(block.y);
									}
									break;
								}
									
								default:
								{
									break;
								}
							}
						}
						else{
							gun.resetSize();
						}
					}
					
				}
				else{
					var tmpY:Number = block.y + block.view.height/2;
					if(gun.style == "left" || gun.style == "right"){
						if((block.y - block.view.height/2 < ( gun.y )) &&
							(tmpY>( gun.y)))
						{
							switch(gun.style)
							{
								case "left":
								{
									if(block.x - (gun.params.x+gun._lengthRange + gun.widthGun)<=-32){
										gun.lessVertical(block.x);
									}
									break;
								}
									
								case "right":
								{
									if((gun.params.x - gun._lengthRange - gun.widthGun) -(block.x - block.view.width/2)<=-16){
										gun.lessVertical(block.x);
									}
									break;
								}
									
								default:
								{
									break;
								}
							}
						}
						else{
							gun.resetSize();
						}
					}
					else{
						switch(gun.style)
						{
							
							case "top":
							{
								if(block.y - (gun.params.y+gun._lengthRange + gun.widthGun)<=-16){
									gun.tmpLess();
								}
								if(block.y - (gun.params.y+gun._lengthRange + gun.widthGun) > 0){
									gun.reSizeColliteGreat(32);
								}
								break;
							}
								
							case "bottom":
							{
								if((gun.params.y - gun._lengthRange - gun.widthGun) -(block.y - block.view.width/2)<=16){
									gun.tmpLess();
								}
								if((gun.params.y - gun._lengthRange - gun.widthGun) -(block.y - block.view.width/2) > 32){
									gun.reSizeColliteGreat(32);
								}
								break;
							}
								
							default:
							{
								break;
							}
						}
					}
				}
			}
			
		}
		
		/**
		 * NHUNG HANH DONG ACTION LEN VIEW, VI DU NHU RUNG MAN HINH, DI CHUYEN HERO....
		 */
		
		//		public function shakeCam(objectToShake:Object,duration:Number = 1, intensity:Number = 2):void
		//		{
		//			var cam:StarlingCamera = view.camera as StarlingCamera;
		//			//get a temp reference to the current target
		//			var t:Object = objectToShake;
		//			//get camera easing
		//			var teasingX:Number =  cam.easing.x;
		//			var teasingY:Number =  cam.easing.y;
		//			var shakeTarget:* = objectToShake;
		//			//start shaking
		//			cam.easing.x = 1;
		//			cam.easing.y = 1;
		//			cam.target = shakeTarget;
		//			
		//			TweenLite.to(shakeTarget, duration , {onUpdate:
		//				function():void
		//				{
		//					shakeTarget.x = t.x + (Math.random()*2-1)*intensity;
		//					shakeTarget.y = t.y + (Math.random()*2-1)*intensity;
		//				},
		//				onComplete:
		//				function():void{
		//					//return to old target.
		//					cam.easing.x = teasingX;
		//					cam.easing.y = teasingY;
		//					cam.target = t;
		//				}});
		//			
		//		}
		
		private function touchHandler(e:TouchEvent):void
		{
			if (!isTouch) 
			{
				return;
			}
			if (gameOver) 
			{
				return;
			}
			
			if(stage==null) return;
			var touch:Touch = e.getTouch(e.currentTarget as Quad);
			
			if(touch==null) return;
			
			var position:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.ENDED){
			}else if(touch.phase == TouchPhase.BEGAN){
				localPos = touch.getLocation(e.currentTarget as Quad);
				listPostionWhenMoveds = [];
				mouseX = localPos.x;
				mouseY = localPos.y;
				
				heroX = hero.x;
				heroY = hero.y;
			}else if(touch.phase == TouchPhase.MOVED){
				if (localPos == null) 
				{
					return;
				}
				var localPos2:Point = touch.getLocation(e.currentTarget as Quad);
				TweenMax.staggerTo([hero,sHero],1,{x:heroX + mouseX - localPos2.x, y: heroY + mouseY - localPos2.y,ease:Back.easeOut, onUpdate:function():void{
					hero.x = sHero.x = Math.floor(hero.x);
					hero.y = sHero.y = Math.floor(hero.y);
					//					trace(hero.x,"hero.x");
					colisionButton();
					colisionWallDetect();
					colisionSawDetect()
					colisionThornDetect();
					
				}});
				listPostionWhenMoveds.push(localPos2);
			}
		}
		
		/**
		 * NHUNG HANH DONG LIEN QUAN DEN HERO
		 */
		private function enterFrame(e:Event):void
		{
			//kiem tra co dung voi vong xoay
			var heroBounds:Rectangle;
			heroBounds = (sHero.view as DisplayObject).getBounds(Starling.current.stage,heroBounds);
			for (var i:int = 0; i < listWheels.length; i++) 
			{
				var wheelBounds:Rectangle;
				wheelBounds = (listWheels[i] as DisplayObject).getBounds(Starling.current.stage,wheelBounds);
				if (wheelBounds.intersects(heroBounds)) 
				{
					this.removeEventListeners(Event.ENTER_FRAME);
					heroDie();
				}
			}
			
			//kiem tra co dung cac sensor lam cho hero chet
			for (var i1:int = 0; i1 < listSensors.length; i1++) 
			{
				var sensorBounds:Rectangle;
				sensorBounds = (listSensors[i1].view as DisplayObject).getBounds(Starling.current.stage,sensorBounds);
				if (heroBounds.intersects(sensorBounds)) 
				{
					this.removeEventListeners(Event.ENTER_FRAME);
					heroDie();
				}
			}
			
			//kiem tra nhung doi tuong ben trong va ngoai camera
			for (var j:int = 0; j < listObjectCitrus.length; j++) 
			{
				var globalBounds:Rectangle;
				//get the bounds of the view, relative to the stage.
				globalBounds = (listObjectCitrus[j].view as DisplayObject).getBounds(Starling.current.stage,globalBounds);
				if(stageRect.intersects(globalBounds))
				{
					listObjectCitrus[j].view.unflatten();
				}else{
					listObjectCitrus[j].view.flatten();
				}
			}
			
			//kiem tra hero co roi vao lanh tho cua con nhen nao ko
			for (var k:int = 0; k < listRadiusSpiders.length; k++) 
			{
				var sensorSpider:Rectangle;
				sensorSpider = (listRadiusSpiders[k].view as DisplayObject).getBounds(Starling.current.stage,sensorSpider);
				var radiusSpider:MyRadiusSpiderRun = listRadiusSpiders[k];
				var spiderFollow:MySpiderFollow = getObjectByName(nameLevel+"spiderR"+radiusSpider.id) as MySpiderFollow;
				
				if(heroBounds.intersects(sensorSpider))
				{
					if (spiderFollow.isActive == MySpiderFollow.NOT_RUN) 
					{
						spiderFollow.run();
						return;
					}
					if (spiderFollow.isRun) 
					{
						TweenMax.staggerTo([radiusSpider,spiderFollow],spiderFollow.timeRun(new Point(sHero.x,sHero.y)),{x:sHero.x,y:sHero.y,ease:Linear.easeNone});
					}
					
				}else{
					if (spiderFollow.isActive == MySpiderFollow.RUN) 
					{
						TweenMax.killTweensOf(radiusSpider);
						TweenMax.killTweensOf(spiderFollow);
						if (spiderFollow) 
						{
							spiderFollow.sleep();
						}
					}
					
					
				}
			}
			
		}	
		
		private function colisionThornDetect():void{
			if(listThornStart.length > 0){
				var heroBounds:Rectangle;
				heroBounds = (sHero.view as DisplayObject).getBounds(Starling.current.stage,heroBounds);
				for (var i:int = 0; i < listThornStart.length; i++) 
				{
					var sensorBounds:Rectangle;
					sensorBounds = (listThornStart[i].view as DisplayObject).getBounds(Starling.current.stage,sensorBounds);
					if (heroBounds.intersects(sensorBounds)) 
					{
						if((listThornStart[i] as MyThornStart).isActive == false){
							(listThornStart[i] as MyThornStart).isActive = true;
							var tmpListThorn:Array = [];
							for (var i1:int = 0; i1 < listThorn.length; i1++) 
							{
								for (var j:int = 0; j < (listThornStart[i] as MyThornStart).listID.length; j++) 
								{
									trace((listThorn[i1] as MyThorn).id ,(listThornStart[i] as MyThornStart).listID[j]);
									trace((listThorn[i1] as MyThorn).id == (listThornStart[i] as MyThornStart).listID[j]);
									if((listThorn[i1] as MyThorn).id == (listThornStart[i] as MyThornStart).listID[j]){
										tmpListThorn.push(listThorn[i1]);
									}
								}
							}
							
							for (var k:int = 0; k < tmpListThorn.length; k++) 
							{
								(tmpListThorn[k] as MyThorn).action = (tmpListThorn[k] as MyThorn).action*(-1);
							}
						}
					}
					else{
						//						(listDetectWallMove[i] as MyWallMoveDetect).isActive = false;
					}
				}
			}
		}
		
		private function colisionSawDetect():void{
			if(listDetectWallMove.length > 0){
				var heroBounds:Rectangle;
				heroBounds = (sHero.view as DisplayObject).getBounds(Starling.current.stage,heroBounds);
				for (var i:int = 0; i < listDetectWallMove.length; i++) 
				{
					var sensorBounds:Rectangle;
					sensorBounds = (listDetectWallMove[i].view as DisplayObject).getBounds(Starling.current.stage,sensorBounds);
					if (heroBounds.intersects(sensorBounds)) 
					{
						if((listDetectWallMove[i] as MyWallMoveDetect).isActive == false){
							(listDetectWallMove[i] as MyWallMoveDetect).isActive = true;
							var tmpListWallMove:Array = [];
							for (var i1:int = 0; i1 < listWallMove.length; i1++) 
							{
								for (var j:int = 0; j < (listDetectWallMove[i] as MyWallMoveDetect).listID.length; j++) 
								{
									//									trace(int((listWallMove[i1] as MyWallMove).id),int((listDetectWallMove[i] as MyWallMoveDetect).listID[j]),"int((listDetectWallMove[i] as MyWallMoveDetect).listID[j])");
									if((listWallMove[i1] as MyWallMove).id == (listDetectWallMove[i] as MyWallMoveDetect).listID[j]){
										tmpListWallMove.push(listWallMove[i1]);
									}
								}
							}
							
							for (var k:int = 0; k < tmpListWallMove.length; k++) 
							{
								(tmpListWallMove[k] as MyWallMove).start = (tmpListWallMove[k] as MyWallMove).start*(-1);
							}
						}
					}
					else{
						//						(listDetectWallMove[i] as MyWallMoveDetect).isActive = false;
					}
				}
			}
		}
		
		private function colisionWallDetect():void{
			if(listSawStart.length > 0){
				var heroBounds:Rectangle;
				heroBounds = (sHero.view as DisplayObject).getBounds(Starling.current.stage,heroBounds);
				for (var i:int = 0; i < listSawStart.length; i++) 
				{
					var sensorBounds:Rectangle;
					sensorBounds = (listSawStart[i].view as DisplayObject).getBounds(Starling.current.stage,sensorBounds);
					if (heroBounds.intersects(sensorBounds)) 
					{
						if((listSawStart[i] as MySawStart).isActive == false){
							(listSawStart[i] as MySawStart).isActive = true;
							var tmpListSawMove:Array = [];
							for (var i1:int = 0; i1 < listSaw.length; i1++) 
							{
								for (var j:int = 0; j < (listSawStart[i] as MySawStart).listID.length; j++) 
								{
									//									trace(int((listWallMove[i1] as MyWallMove).id),int((listDetectWallMove[i] as MyWallMoveDetect).listID[j]),"int((listDetectWallMove[i] as MyWallMoveDetect).listID[j])");
									if((listSaw[i1] as MySaw).id == (listSawStart[i] as MySawStart).listID[j]){
										tmpListSawMove.push(listSaw[i1]);
									}
								}
							}
							
							for (var k:int = 0; k < tmpListSawMove.length; k++) 
							{
								(tmpListSawMove[k] as MySaw).start = (tmpListSawMove[k] as MySaw).start*(-1);
							}
						}
					}
					else{
						//						(listDetectWallMove[i] as MyWallMoveDetect).isActive = false;
					}
				}
			}
		}
		
		
		private function colisionButton():void{
			if(listButton.length > 0){
				var heroBounds:Rectangle;
				heroBounds = (sHero.view as DisplayObject).getBounds(Starling.current.stage,heroBounds);
				for (var i:int = 0; i < listButton.length; i++) 
				{
					var sensorBounds:Rectangle;
					sensorBounds = (listButton[i].view as DisplayObject).getBounds(Starling.current.stage,sensorBounds);
					if (heroBounds.intersects(sensorBounds)) 
					{
						// nếu trạng thái chưa từng va chạm thì cho đi vào :D
						if((listButton[i] as MyButton).flagButton == false){
							// set lại trạng thái là đang va chạm
							(listButton[i] as MyButton).flagButton = true;
							(listButton[i] as MyButton).status = (listButton[i] as MyButton).status*(-1);
							var tmpListGun:Array = [];
							// tạo ra danh dách tạm các khẫu súng để tìm ra khẩu súng nào mà mình đang quản lý
							for (var i1:int = 0; i1 < listGun.length; i1++) 
							{
								for (var j:int = 0; j < (listButton[i] as MyButton).listID.length; j++) 
								{
									// nếu khẩu súng có id nằm trong list id mà button quản lý thì cho nó vào danh sách
									if((listGun[i1] as MyGun).id == (listButton[i] as MyButton).listID[j]){
										tmpListGun.push(listGun[i1]);
									}
								}
							}
							
							// sau khi đã có được danh sách các khẩu súng quản lý thì bắt đầu thay đổi trạng thài của từng khầu súng
							for (var k:int = 0; k < tmpListGun.length; k++) 
							{
								// thay đổi trạng thái từng khầu súng -- lý do tại sao lại * (-1) -- BÍ MẬT :P
								(tmpListGun[k] as MyGun).status = (tmpListGun[k] as MyGun).status*(-1);
							}
						}
					}
					else{
						// nếu không va chạm nữa thì cho trạng thái cờ về false
						(listButton[i] as MyButton).flagButton = false;
					}
				}
			}
		}
		
		
		private function heroDie():void{
			//			time = new Date().getTime();
			die++;
			gameOver = true;
			TweenMax.killTweensOf(hero);
			TweenMax.killTweensOf(sHero);
			sHero.onCollide.remove(_collisionStart);
			removeEventListeners(Event.ENTER_FRAME);
			isTouch = false;
			quad.touchable = false;
			UtilsAndConfig.playSound("hero_hit");
			hero.changeStatusHeroDie();
			TweenMax.delayedCall(1,function():void{
				resetGame();
				colorMap = myMap.color_;
			});
		}
		
		private function resetGame():void{
			this.x = 0;
			this.y = 0;
			hero.reset();
			sHero.x = hero.xHero;
			sHero.y = hero.yHero;
			if(listObjectResets.length>0){
				for each (var i:Object in listObjectResets) 
				{
					i.reset();
				}
			}
			//			gameOver = false;
			this.addEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		
		private function nextLevel():void{
			hero.changeStatusHeroFinish();
			
			//tinh toan score
			time = new Date().getTime() - time;
			var score:int = max - ((time/1000)*3 + die*5);
			if (score <= min) 
			{
				score = min;
			}
			UtilsAndConfig.score += score;
			
			hero.finish.addOnce(function():void{
				if (levelNext.length == 0) 
				{
					//Ban dau kiem tra level hien tai co bang voi level moi nhat cua game
					if (UtilsAndConfig.getCurrentLevel() == LevelGame.getCurrentLevel())
					{
						//Kiem tra level next co lon hon tong so level hien co
						if (UtilsAndConfig.getCurrentLevel()+1 <= LevelGame.getSumLevel()) 
						{
							UtilsAndConfig.setLevelNext(UtilsAndConfig.getCurrentLevel()+1);
						}
					}
					for(var i:int=listObjectResets.length-1;i>=0;i--){
						if(!ObjPooling.release(listObjectResets[i])){
							//Neu không release đc thì remove nó ra
							remove(listObjectResets[i]);
						}
					}
					removeEventListeners();
					for(var j:int=listSensors.length-1;j>=0;j--){
						//					ObjPooling.release(listSensors[j]);
						remove(listSensors[j]);
					}
					for (var k:int = 0; k < listObjectCitrus.length; k++) 
					{
						if(!ObjPooling.release(listObjectCitrus[k])){
							remove(listObjectCitrus[k]);
						}
					}
					
					addChild(new CompleteGame(colorMap,LevelGame.getCurrentLevel(),time,die));
				}else{
					LevelGame.level(levelNext);
				}
				
			});
			
		}
		
		private function _collisionStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			if(!gameOver){
				if(		
					((other is MyBoom && !isImmortal) && (other as MyBoom).width != 0) || (other is MySkull && !isImmortal && (other as MySkull).isStart == true)
					||  (other is MyHand && !isImmortal && (other as MyHand).action == true)
					||  (other is MyThorn && !isImmortal && (other as MyThorn).isUp != 0
						|| (other is MyGun && !isImmortal)
						|| (other is MyWallMove && !isImmortal)
						|| (other is MySpider && !isImmortal && (other as MySpider).isStart == true 
							&& ((other as MySpider).currentFrame == 0 || (other as MySpider).currentFrame ==1))
					)){
					heroDie();
				}else if(other is MyStar){
					TweenMax.killTweensOf(hero);
					TweenMax.killTweensOf(sHero);
					gameOver = true;
					nextLevel();
					
				}else if(other is MyRangeBoomSensor){
					var b:MyBoom = (getObjectByName(other.name+"boom_radius") as MyBoom);
					b.boom();
				}
					
				else if(other is MySkull){
					var skullNotRun:MySkull = (getObjectByName(other.name) as MySkull);
					skullNotRun.isStart = true;
				}
					
				else if(other is MyHand){
					var handNotAction:MyHand = (getObjectByName(other.name) as MyHand);
					handNotAction.action = true;
				}
					
				else if(other is MySpider){
					var spiderNotAction:MySpider = (getObjectByName(other.name) as MySpider);
					if(spiderNotAction.isStart == false){
						spiderNotAction.isStart = true;
					}
				}
					
				else if(other is MyGateFake){
					var gateFake:MyGateFake = (other as MyGateFake);
					gateFake.hide();
				}
					
				else if(other is MyTeleport){
					UtilsAndConfig.playSound("teleport");
					var teleport:MyTeleport = (other as MyTeleport);
					TweenMax.killTweensOf(hero);
					TweenMax.killTweensOf(sHero);
					sHero.onCollide.remove(_collisionStart);
					removeEventListeners(Event.ENTER_FRAME);
					isTouch = false;
					var flagDp:Boolean = false;
					hero.changeStatusHeroFinish();
					hero.completeAnimationHide.add(
						function hideAndMoveHero():void{
							if(flagDp){
								return;
							}
							flagDp = true
							TweenMax.staggerTo([hero,sHero],1,{x:int(teleport.arr[0]),y:int(teleport.arr[1]),
								ease:Linear.easeNone,
								onComplete:function():void{
									hero.changeStatusHeroStart();
								}
							})
						}
					);
				}
				else if(other is MyButton){
					//					var button:MyButton = (other as MyButton);
					//					if(button.flagButton){
					//						return;
					//					}
					//					button.flagButton = true;
					//					TweenMax.delayedCall(1,function():void{
					//						resetFlagButton(button);
					//					});
					//					
					//					var tmpListGun:Array = [];
					//					for (var i:int = 0; i < listGun.length; i++) 
					//					{
					//						for (var j:int = 0; j < button.listID.length; j++) 
					//						{
					//							if(int((listGun[i] as MyGun).id) == int(button.listID[j])){
					//								tmpListGun.push(listGun[i]);
					//							}
					//						}
					//					}
					//					
					//					for (var k:int = 0; k < tmpListGun.length; k++) 
					//					{
					//						(tmpListGun[k] as MyGun).status = (tmpListGun[k] as MyGun).status*(-1);
					//					}
					
				}
			}
		}
		private function resetFlagButton(button:MyButton):void{
			button.flagButton = false;
		}
	}
}

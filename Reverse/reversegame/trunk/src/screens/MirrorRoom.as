package screens
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import citrus.core.CitrusObject;
	import citrus.core.starling.StarlingState;
	import citrus.math.MathVector;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.simple.DynamicObject;
	import citrus.physics.simple.SimpleCitrusSolver;
	import citrus.view.ACitrusCamera;
	
	import components.LevelGame;
	
	import elements.mirrorRoom.BackgroundRoom;
	import elements.mirrorRoom.Door;
	import elements.mirrorRoom.InstructionMorrorRoom;
	import elements.mirrorRoom.Mirror;
	import elements.mirrorRoom.MyBoy;
	import elements.mirrorRoom.StatusAnimation;
	import elements.mirrorRoom.TheLight;
	
	import obj.MyPopup;
	import obj.MyPopupHandsome;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	import views.AnimationHero;
	
	public class MirrorRoom extends StarlingState
	{

		private var theBoy:MyBoy;

		private var mouseX:Number;

		private var mouseY:Number;

		private var heroX:Number;

		private var heroY:Number;

		public static var boy:Object;
		public static var settingMirror:Mirror;
		public function MirrorRoom()
		{
		}
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
			TweenMax.killAll(false);
			for (var i:int = 0; i < objCitrus.length; i++) 
			{
				remove(objCitrus[i]);
			}
			for(var i1:int=numChildren-1;i1>=0;i1--){
				getChildAt(i1).removeFromParent(); //remove tất cả thành phần trong screen
			}
			
			removeEventListeners();
			UtilsAndConfig.stopSound("story_song_mixdown");
		}
		
		override public function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			
			TweenMax.delayedCall(10,function():void{
				UtilsAndConfig.playSound("ghita");
				
				TweenMax.delayedCall(5,function():void{
					UtilsAndConfig.playSound("windchime");
				});
			});
			background = new BackgroundRoom("background",{x:0,y:0},5);
			add(background);
			var objItems:Array = [];
			objCitrus = [];
			listXMirror = [];
			listMirror = [];
			
			var simpleCitrusSolver:SimpleCitrusSolver = new SimpleCitrusSolver("citrus solver");
			add(simpleCitrusSolver);
			
			simpleCitrusSolver.overlap(DynamicObject, CitrusSprite);
			
			var json:Object = JSON.parse(LevelGame.getJsonStory2());
			var arr:Array = json.layers;
			for (var i:int = 0; i < arr.length; i++) 
			{
				var arrObjs:Array = arr[i].objects;
				for (var j:int = 0; j < arrObjs.length; j++) 
				{
					var obje:Object = arrObjs[j];
					obje.group = arr[i].name;
					objItems.push(obje);
				}
				
			}
			var m:int = 0;
			for (var k:int = 0; k < objItems.length; k++) 
			{
				var object:Object = objItems[k];
				switch(object.name)
				{
					case "lamp":
					{
						var lamp:TheLight = new TheLight("thelight",{x:object.x,y:object.y,group:object.group});
						add(lamp);
						objCitrus.push(lamp);
						break;
					}
						
					case "mirror":
					{
						var mirror:Mirror = new Mirror("mirror",{x:object.x,y:object.y,group:object.group},object.properties.style,object.properties.id);
						add(mirror);
						listXMirror.push(mirror.x);
						objCitrus.push(mirror);
						listMirror.push(mirror);
//						if(object.properties.style != 2){
							
//							m+=1;
//							var collision:CollisionBreakMirror = new CollisionBreakMirror("collision"+m,{x:mirror.x+mirror.view.width/2 - 100,y:mirror.y+mirror.view.height/2});
//							add(collision);
//						}
//						else{
						if(object.properties.id == 0){
							popup = new MyPopup("popup",{x:mirror.x,y:mirror.y,group:object.group});
							popup.group = 50;
							add(popup);
							
							popupHandsome = new MyPopupHandsome("popup",{x:mirror.x,y:mirror.y,group:object.group});
							popupHandsome.group = 50;
							add(popupHandsome);
						}
						break;
					}
						
					case "boy":
					{
						boy = new MyBoy("boy",{x:object.x,y:object.y,group:object.group});
						add(boy as CitrusObject);
						boy.onCollide.add(_collisionStart);
						(boy as MyBoy).theBoy.stopBoy();
						objCitrus.push(boy);
						var stageRect:Rectangle = new Rectangle(0,0,background.view.width,background.view.height);
						view.camera.setUp(boy,stageRect,new Point(0.5 , 0.5),new Point(0.15,0.15));
						view.camera.reset();
						view.camera.parallaxMode = ACitrusCamera.BOUNDS_MODE_OFFSET;
						view.camera.allowZoom = true;
						view.camera.allowRotation = false;
						view.camera.zoom(1);
						break;
					}
						
					case "gate":
					{
						gate = new Door("boy",{x:object.x,y:object.y,group:object.group,width:object.width,height:object.height});
						add(gate);
						objCitrus.push(gate);
						break;
					}
				}
			}
			
			quad = new Quad(stage.stageWidth,stage.stageHeight);
			quad.alpha = 0;
			addChild(quad);
			
			quad.addEventListener(TouchEvent.TOUCH,moveHero);
			moveTheBoy();
		}
		
		private function moveTheBoy():void{
			if(count==1){
				TweenMax.to(popupHandsome.imgPopup,1,{alpha:0,onComplete:function():void{
					remove(popupHandsome);
				}});
			}
			var xMirror:int;
			if(count<listXMirror.length){
				(boy as MyBoy).lookRight();
				(boy as MyBoy).theBoy.move();
				tweenHero(listXMirror[count] - 80,lookMirror);
			}
		}
		
		private function lookMirror():void{
			if(count==0 || count ==3){
				(boy as MyBoy).theBoy.lookMirror();
			}
			else{
				(boy as MyBoy).theBoy.stopBoy();
			}
			if(count==0){
				TweenMax.to(popupHandsome.imgPopup,1,{delay:2.7,alpha:1});
			}
//			else{
//				(boy as MyBoy).theBoy.lookMirror();
//			}
			
			var time:int = 0;
			if(count == 0){
				time = 7;
			}
			count++;
			TweenMax.delayedCall(time,moveTheBoy);
		}
		
		private function resetPositionElements():void{
			var tmp:Number = 0;
			boy.x = listMirror[0].x;
			for (var i:int = 0; i < listXMirror.length/2; i++) 
			{
				tmp = listMirror[i].x;
				listMirror[i].x = listMirror[listXMirror.length-1 - i].x;
				listMirror[listXMirror.length - 1 - i].x = tmp;
			}
			gate.x = listMirror[0].x + 570;
			gate.scale();
		}
		
		private function tweenHero(toX:int,complete:Function = null):void{
			var time:Number = 3;
			if(count ==0){
				time =1.5;
			}
			TweenMax.to(boy,time,{x:toX,ease:Linear.easeNone,onComplete:complete});
		}
		
		private var isMove:Boolean = true;
		private var isCollision:Boolean = false;
		private var flag:Boolean = false;
		private var tmpMirror:Mirror; 

		private var quad:Quad;
		
		/**
		 *	Động đất 
		 * 
		 */		
		private function earthquake():void{
//			UtilsAndConfig.playSound("broken_glass");
			TweenMax.to(this,5,{onUpdate:function():void{
				UtilsAndConfig.playSound("earth_quake");
				x = UtilsAndConfig.randRangeNumber(-3,3);
				y = UtilsAndConfig.randRangeNumber(-3,3);
			},onComplete:function():void{
				x = 0;
				y = 0;
				isMove = true;
				instrustion = new InstructionMorrorRoom();
				addChild(instrustion);
			}});
		}
		
		private var numLinghting:int = 0;

		private var background:BackgroundRoom;
		private var listPositions:Array;
		private var localPos:Point;
		private var localPos2:Point;

		private var popup:MyPopup;

		private var key:Boolean = true;

		private var objCitrus:Array;

		private var listXMirror:Array;

		private var count:int = 0;
		private var timeLight:Number;

		private var listMirror:Array;

		private var gate:Door;

		private var popupHandsome:MyPopupHandsome;

		private var instrustion:InstructionMorrorRoom;

		private var comeHere:Image;
		/**
		 *	Làm sấm sét 
		 * 
		 */		
		private function lighting():void{
			timeLight = UtilsAndConfig.randRangeNumber(0.0001,0.5);
			numLinghting ++;
			if(numLinghting>15){
				return;
			}
//			trace(UtilsAndConfig.randRangeNumber(0.0001,0.002),"UtilsAndConfig.randRangeNumber(0.0001,0.002)");
			TweenMax.to(quad,timeLight,{alpha:1,onComplete:function():void{
				TweenMax.to(quad,0.001,{alpha:0,onComplete:lighting});
			}});
		}
		
		private function _collisionGoGame(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			if(other is Door){
				removeChild(comeHere);
				TweenMax.to(quad,2,{alpha:1});
				TweenMax.to(boy.view,1.5,{alpha:0});
				TweenMax.to(boy,2,{x:"+200",onComplete:moveToMap});
			}
		}
		
		private function moveToMap():void{
			TweenMax.killAll();
			_ce.state = new LevelScreen();
		}
		
		private function _collisionStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			if(!isMove)
				return;
			
			if( other is Mirror)
			{
				isCollision = true;
				flag = true;
				tmpMirror = (other as Mirror);
				if(tmpMirror.style == 2){
					boy.onCollide.remove(_collisionStart);
					isMove = false;
					TweenMax.to(boy,1,{x:"+100",ease:Linear.easeNone,onComplete:function():void{
						boy.theBoy.theBoy.stop();
						boy.theBoy.lookMirror();
						
						TweenMax.delayedCall(1,function():void{
							
							TweenMax.to(tmpMirror.hero,0.5,{alpha:0.6});
							
							TweenMax.delayedCall(2,function():void{
								
								TweenMax.to(tmpMirror.hero,0.5,{alpha:1});
								
								TweenMax.delayedCall(1.5,function():void{
									earthquake();
									
									lighting();
									
									var tmpQuad:Quad = new Quad(stage.stageWidth,stage.stageHeight,0xffffff);
									tmpQuad.alpha = 0;
									addChild(tmpQuad);
									TweenMax.to(tmpQuad,2,{alpha:1,onComplete:function():void{
										var xBoy:Number = boy.x;
										var yBoy:Number = boy.y;
										remove(boy as CitrusObject);
										key = false;
										tmpMirror.removeHero();
										var _view:AnimationHero = new AnimationHero();
										boy = new DynamicObject("hero",{x:xBoy,y:yBoy - 70,width:32,height:32});
										boy.group = 100;
										boy.view = _view;
										add(boy as CitrusObject);
										
										TweenMax.to(popup.imgPopup,2,{alpha:1,onComplete:function():void{
											boy.onCollide.add(_collisionGoGame);
										}});
										
										var stageRect:Rectangle = new Rectangle(0,0,background.view.width,background.view.height);
										view.camera.setUp(boy,stageRect,new Point(0.5 , 0.5),new Point(0.15,0.15));
										view.camera.reset();
										view.camera.parallaxMode = ACitrusCamera.BOUNDS_MODE_OFFSET;
										view.camera.allowZoom = true;
										view.camera.allowRotation = false;
										view.camera.zoom(1);
										UtilsAndConfig.playSound("broken_glass");
										for (var i:int = 0; i < listMirror.length; i++) 
										{
											
											if((listMirror[i] as Mirror).style!=2){
												(listMirror[i] as Mirror).breakMirror();
												(listMirror[i] as Mirror).removeElements();
											}
										}
										resetPositionElements();
										
										TweenMax.to(tmpQuad,2,{alpha:0,onComplete:function():void{
											removeChild(tmpQuad);
										}});
									}});
								});
							});
						});
					}});
				}
				else{
					tmpMirror.tweenShadow(boy.theBoy.status);
				}
			}
			else if( other is MyBoy)
			{
				
			}
		}
		private function breakMirror():void{
			UtilsAndConfig.playSound("broken_glass");
		}
		
		private var addComeHere:Boolean = false
		private function moveHero(e:TouchEvent):void
		{
			if(!isMove || (boy is MyBoy)){
				return;
			}
			
			if(stage==null) return;
			var touch:Touch = e.getTouch(this);
			if(touch==null) return;
			
			var position:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.ENDED){
				if(boy is MyBoy){
					if ((boy as MyBoy).theBoy.status != StatusAnimation.IDLE) 
					{
						(boy as MyBoy).theBoy.stopBoy();
					}
				}
			}else if(touch.phase == TouchPhase.BEGAN){
				if(instrustion!=null){
					removeChild(instrustion);
					if(addComeHere == false){
						addComeHere=true;
						comeHere = new Image(UtilsAndConfig.defaultAssets.getTexture("popupComeHere"));
						comeHere.smoothing = TextureSmoothing.NONE;
						comeHere.scaleX = comeHere.scaleY = 2;
						comeHere.x = stage.stageWidth - comeHere.width;
						comeHere.y = stage.stageHeight/2 + comeHere.height*2;
						addChild(comeHere);
						TweenMax.from(comeHere,1,{alpha:0,onComplete:function():void{
							TweenMax.to(comeHere,0.5,{alpha:0,yoyo:true,repeat:-1});
						}});
					}
					
				}
				listPositions = [];
				localPos = touch.getLocation(this);
				listPositions.push(localPos.x);
				mouseX = localPos.x;
				mouseY = localPos.y;
				heroX = boy.x;
				heroY = boy.y;
				if(!key){
					TweenMax.to(popup.imgPopup,2,{alpha:0,onComplete:function():void{
						remove(popup);
					}});
				}
				
			}else if(touch.phase == TouchPhase.MOVED){
				if (localPos == null) 
				{
					return;
				}
				localPos2 = touch.getLocation(this);
				if(boy is MyBoy){
					listPositions.push(localPos2.x);
					if ((boy as MyBoy).theBoy.status != StatusAnimation.MOVE) 
					{
						(boy as MyBoy).theBoy.move();
					}
					if (listPositions[listPositions.length - 1] > listPositions[listPositions.length - 2] && listPositions.length >= 2) 
					{
						(boy as MyBoy).lookRight();
						
						TweenMax.to([boy],0.1,{x:"+20",ease:Linear.easeNone, onUpdate:function():void{
							boy.x = Math.floor(boy.x);
							boy.y = Math.floor(boy.y);
						}});
					}
					else{
						(boy as MyBoy).lookLeft();
						
						TweenMax.to([boy],0.1,{x:"-20",ease:Linear.easeNone, onUpdate:function():void{
							boy.x = Math.floor(boy.x);
							boy.y = Math.floor(boy.y);
							if(boy.x <= 0){
								boy.x = 0;
							}
						}});
					}
				}
				else{
					TweenMax.to([boy],1,{x:heroX + mouseX - localPos2.x, y: heroY + mouseY - localPos2.y,ease:Back.easeOut, onUpdate:function():void{
						boy.x = Math.floor(boy.x);
						boy.y = Math.floor(boy.y);
						if(boy.y > stage.stageHeight){
							boy.y = stage.stageHeight;
						}
						if(boy.y <=0){
							boy.y = 0;
						}
						if(boy.x<0){
							boy.x = 0;
						}
					}});
				}
			}
		}
	}
}
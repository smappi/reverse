package screens
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import citrus.core.starling.StarlingState;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.simple.Sensor;
	
	import components.LevelGame;
	
	import elements.ChatGhost;
	import elements.ChooseLevel;
	import elements.LevelButton;
	import elements.PopupSetting;
	import elements.loading.LoadingTagged;
	
	import feathers.controls.ScrollContainer;
	
	import obj.MyBat;
	import obj.MyHero;
	import obj.MyItemLevelBg;
	import obj.MyMapGhost;
	import obj.MyMapLine;
	import obj.MySpiderMap;
	
	import objpooling.ObjPooling;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureSmoothing;
	
	import utils.UtilsAndConfig;
	
	
	public class LevelScreen extends StarlingState
	{
		private var container:ScrollContainer = new ScrollContainer();
		private var isScroll:Boolean = false;
		
		private var btnBack:Button;
		private var lsMaps:Array = [];
		
		private var sensor:Sensor;
		private var mouseX:Number;
		private var mouseY:Number;
		private var heroX:Number;
		private var heroY:Number;
		
		private var localPos:Point;
		
		private var localPos2:Point;
		private var widthMap:int = 1252*2;
		private var heightMap:int = 640;
		
		private var levelBtn:LevelButton;
		
		private var hero:MyHero;
		
		private var objectLevel:Object;
		
		private var ls:Array;
		
		private var listP:Array;
		
		private var line:MyMapLine;
		
		private var bg:Quad;
		
		private var imgBgShadown:Image;
		
		//danh sach chua cac doi tuong object
		private var listObjects:Array = [];
		
		private var heightMiss:Number = 0;
		
		private var isTouch:Boolean = true;
		
		private var level_:int = 0;
		
		public function LevelScreen()
		{
			super();
		}
		
		
		override public function destroy():void
		{
			// TODO Auto Generated method stub
			super.destroy();
			
			for(var i:int=numChildren-1;i>=0;i--){
				getChildAt(i).removeFromParent(true); //remove tất cả thành phần trong screen
			}
			
			removeEventListeners();
			for(var j:int=listObjects.length-1;j>=0;j--){
				if(!ObjPooling.release(listObjects[j])){
					remove(listObjects[j]);	
				}
			}
			
			UtilsAndConfig.stopSound("reverse_mapoverview");
			System.pauseForGCIfCollectionImminent(0);
			System.gc(); //dọn rác
		}
		
		override public function initialize():void
		{
			super.initialize();
			//			UtilsAndConfig.setLevelCurrent(10);
			//			UtilsAndConfig.setLevelNext(11);
			bg = new Quad(widthMap,heightMap,0x7D7F79);
			if (heightMap > stage.stageHeight)
			{
				bg.height = heightMap;
			}else{
				bg.height = stage.stageHeight;
				heightMiss = stage.stageHeight - heightMap;
			}
			
			var bgCitrus:CitrusSprite = new CitrusSprite("bg",{x:0,y:0});
			bgCitrus.view = bg;
			add(bgCitrus);
			bgCitrus.touchable = true;
			listObjects.push(bgCitrus);
			level_ = int(UtilsAndConfig.getLevelNext());
			//add cac doi tuong tinh len man hinh
			for (var i:int = 0; i < LevelGame.getDecors().length; i++) 
			{
				var objDecor:Object = LevelGame.getDecors()[i];
				//				var myDecor:MyItemLevelBg = new MyItemLevelBg("item"+objDecor.properties.style,{x:objDecor.x, y:objDecor.y+heightMiss/2});
				var myDecor:MyItemLevelBg = ObjPooling.summon(MyItemLevelBg,"item"+objDecor.properties.style,{x:objDecor.x, y:objDecor.y+heightMiss/2}) as MyItemLevelBg;
				myDecor.setAttr("item"+objDecor.properties.style,{x:objDecor.x, y:objDecor.y+heightMiss/2});
				add(myDecor);
				listObjects.push(myDecor);
			}
			
			//load level
			objectLevel = LevelGame.getlevelMaps()[0];
			//draw line
			line = new MyMapLine("line",{x:0,y:heightMiss/2},objectLevel.properties.number,heightMiss/2);
			line.addLine(int(UtilsAndConfig.getCurrentLevel()));
			add(line);
			
			//add cac nut select level len man hinh
			listP = String(objectLevel.properties.number).split(";");
			ls = [];
			for (var k:int = 0; k < listP.length; k++) 
			{
				var sP:String = listP[k];
				var p:Array = sP.split(",");
				var btLevel:LevelButton = new LevelButton("btLevel"+k,{
					x:Number(p[0]), 
					y:Number(p[1])+heightMiss/2},k+1);
				add(btLevel);
				ls.push(btLevel);
				btLevel.touchable = true;
				listObjects.push(btLevel);
				btLevel.onSelected.add(function(level:int,x:Number,y:Number):void{
					if (level != level_) 
					{
						level_ = level;
						TweenMax.to(hero,1,{x:x - 15,y:y - 60});
					}else{
						var loading:LoadingTagged = new LoadingTagged();
						loading.close();
						addChild(loading);
						loading.addEventListener("FINISH",function():void{
							loading.removeMe();
							LevelGame.level(String(level));
						});
					}
				});
				btLevel.view.addEventListener(TouchEvent.TOUCH, gotoPlayScreen);
			}
			
			//add cac doi tuong chuyen dong len map
			for (var j:int = 0; j < LevelGame.getChars().length; j++) 
			{
				var objChar:Object = LevelGame.getChars()[j];
				switch(objChar.name)
				{
					case "ghost":
					{
						var ghost:MyMapGhost = new MyMapGhost(objChar.name,null,String(objChar.properties.move),heightMiss/2);
						add(ghost);
						listObjects.push(ghost);
						break;
					}
						
					case "spider":
					{
						var spider:MySpiderMap = new MySpiderMap("spider"+Math.random()*100*Math.random(),{x:objChar.x,y:objChar.y+heightMiss/2},int(objChar.properties.style));
						add(spider);
						listObjects.push(spider);
						break;
					}
						
					case "bat":
					{
						var bat:MyBat = new MyBat("bat"+Math.random(),{x:objChar.x,y:objChar.y+heightMiss/2});
						add(bat);
						listObjects.push(bat);
						break;
					}
						
					default:
					{
						break;
					}
				}
			}
			
			//loading ban dau
			var loading:LoadingTagged = new LoadingTagged();
			loading.open();
			addChild(loading);
			loading.addEventListener("FINISH",function():void{
				loading.removeMe();
				addAnimationOnView();
				if (UtilsAndConfig.getFirst() == 0) 
				{
					Reverse.instance.sound.getSound("reverse_mapoverview").volume = 0;
					var chatGhost:ChatGhost = new ChatGhost();
					chatGhost.addEventListener("COMPLETE",function():void{
						Reverse.instance.sound.getSound("reverse_mapoverview").volume = 1;
					});
					addChild(chatGhost);
				}
				
			});
			
		}
		
		private var isTouched:Boolean = false;
		private var button:Sprite;
		private var btLevelGame:LevelButton;
		private function gotoPlayScreen(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(e.target as DisplayObject);
			button = e.currentTarget as Sprite;
			btLevelGame = getObjectByName("btLevel"+button.name) as LevelButton;
			if(touch==null) return;
			
			if(touch.phase == TouchPhase.ENDED){
				if (isTouched) 
				{
					if (btLevelGame.level_ <= int(UtilsAndConfig.getCurrentLevel())) 
					{
						//						if (!isTouch) 
						//						{
						//							return;
						//						}
						TweenMax.killTweensOf(hero);
						btLevelGame.onSelected.dispatch(btLevelGame.level_,btLevelGame.x,btLevelGame.y);
						isTouch = false;
					}
				}
				
			}else if(touch.phase == TouchPhase.BEGAN){
				isTouched = true;
				localPos = touch.getLocation(this);
				
				mouseX = localPos.x;
				mouseY = localPos.y;
				if (sensor.x < stage.stageWidth/2) 
				{
					sensor.x = stage.stageWidth/2;
				}else if(sensor.x >= widthMap - 150){
					sensor.x = widthMap - 350;
				}
				heroX = sensor.x;
				heroY = sensor.y;
			}else if(touch.phase == TouchPhase.MOVED){
				isTouched = false;
				localPos2 = touch.getLocation(this);
				
				TweenMax.to(sensor,0.1,{x:heroX + mouseX - localPos2.x,ease:Linear.easeNone,onUpdate:function():void{
					if (sensor.x > widthMap - 350) 
					{
						return;
					}
					sensor.x = Math.floor(sensor.x);
				}});
			}
		}
		
		//cac animation duoc the hien len man hinh
		private function addAnimationOnView():void{
			UtilsAndConfig.playSound("reverse_mapoverview");
			if (int(UtilsAndConfig.getCurrentLevel()) < int(UtilsAndConfig.getLevelNext()) && int(UtilsAndConfig.getCurrentLevel()) > 0) 
			{
				hero = new MyHero("hero",
					{x:ls[int(UtilsAndConfig.getLevelNext()) -2].x - 15,
						y:ls[int(UtilsAndConfig.getLevelNext()) -2].y - 60,
						width:20, height:20});
				
			}else{
				hero = new MyHero("hero",
					{x:ls[int(UtilsAndConfig.getLevelNext()) -1].x - 15,
						y:ls[int(UtilsAndConfig.getLevelNext()) -1].y - 60,
						width:20, height:20});
			}
			add(hero);
			
			//Hanh dong show level moi va detect camera
			levelBtn = ls[int(UtilsAndConfig.getLevelNext()) -1];
			TweenMax.delayedCall(1,function():void{
				levelBtn.changeButton();
				levelBtn.onComplete.add(function():void{
					if (int(UtilsAndConfig.getCurrentLevel()) < int(UtilsAndConfig.getLevelNext()) && int(UtilsAndConfig.getCurrentLevel()) >= 0) 
					{
						if (int(UtilsAndConfig.getCurrentLevel()) > 0) 
						{
							line.tweenLine([levelBtn.x,levelBtn.y]);
							TweenMax.to(hero,1,{x:levelBtn.x - 15,y:levelBtn.y - 60,delay:0.5});
						}
						
						UtilsAndConfig.setLevelCurrent(int(UtilsAndConfig.getLevelNext()));
					}
					
				});
			});
			
			if (UtilsAndConfig.getCurrentLevel() < UtilsAndConfig.getLevelNext()) 
			{
				var chooseLevel:ChooseLevel = new ChooseLevel("choose");
				add(chooseLevel);
				chooseLevel.x = levelBtn.x;
				chooseLevel.y = levelBtn.y;
				chooseLevel.view.touchable = false;
				chooseLevel.tweenMe();				
			}
			
			//điều khiển camera để scroll map
			var quadSensor:Quad = new Quad(100,100);
			quadSensor.alpha = 0;
			var quadX:int = 0;
			if (levelBtn.x < stage.stageWidth/2) 
			{
				quadX = stage.stageWidth/2;
			}else{
				quadX = levelBtn.x;
			}
			sensor = new Sensor("sensor",{x:stage.stageWidth/2,y:0});
			sensor.view = quadSensor;
			add(sensor);
			
			var stageRect:Rectangle = new Rectangle(0,0,widthMap,stage.stageHeight);
			view.camera.setUp(sensor,stageRect,new Point(0.5 , 0.5),new Point(0.5,0.5));
			view.camera.reset();
			view.camera.allowZoom = true;
			view.camera.allowRotation = false;
			
			TweenMax.to(sensor,1,{x:quadX,onComplete:function():void{
				bg.addEventListener(TouchEvent.TOUCH, touchHandler);
			}});
			
			
			var btnSetting:Button = new Button(UtilsAndConfig.defaultAssets.getTexture("btnSettingLevel"));
			btnSetting.scaleX = btnSetting.scaleY = 2;
			Image(Sprite(btnSetting.getChildAt(0)).getChildAt(0)).smoothing = TextureSmoothing.NONE;
			btnSetting.x = stage.stageWidth - btnSetting.width - 30;
			btnSetting.y = 30;
			addChild(btnSetting);
			btnSetting.addEventListener(Event.TRIGGERED, function settingHandler():void{
				addChild(new PopupSetting(0x7c9bd4));
			});
		}
		
		//scroll map
		private function touchHandler(e:TouchEvent):void
		{
			
			if(stage==null) return;
			var touch:Touch = e.getTouch(e.currentTarget as Quad);
			if(touch==null) return;
			
			var position:Point = touch.getLocation(stage);
			if(touch.phase == TouchPhase.ENDED){
			}else if(touch.phase == TouchPhase.BEGAN){
				localPos = touch.getLocation(this);
				
				mouseX = localPos.x;
				mouseY = localPos.y;
				if (sensor.x < stage.stageWidth/2) 
				{
					sensor.x = stage.stageWidth/2;
				}else if(sensor.x >= widthMap - 150){
					sensor.x = widthMap - 350;
				}
				heroX = sensor.x;
				heroY = sensor.y;
				
			}else if(touch.phase == TouchPhase.MOVED){
				localPos2 = touch.getLocation(this);
				
				TweenMax.to(sensor,0.1,{x:heroX + mouseX - localPos2.x,ease:Linear.easeNone,onUpdate:function():void{
					if (sensor.x > widthMap - 350) 
					{
						return;
					}
					sensor.x = Math.floor(sensor.x);
				}});
			}
		}
	}
}
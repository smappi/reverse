package screens
{
	
	import citrus.core.starling.StarlingState;
	
	import components.LevelGame;
	import components.app42.GetSetScore;
	
	import starling.display.Quad;
	import starling.display.Shape;
	import starling.display.Sprite;
	
	public class GameOverScreen extends StarlingState
	{
		private var ls:Array = [
			{
				"x":0,
				"y":0
			}, 
			{
				"x":1024,
				"y":0
			}, 
			{
				"x":1024,
				"y":256
			}, 
			{
				"x":960,
				"y":256
			}, 
			{
				"x":960,
				"y":32
			}, 
			{
				"x":32,
				"y":32
			}, 
			{
				"x":32,
				"y":288
			}, 
			{
				"x":768,
				"y":288
			}, 
			{
				"x":768,
				"y":352
			}, 
			{
				"x":32,
				"y":352
			}, 
			{
				"x":32,
				"y":608
			}, 
			{
				"x":960,
				"y":608
			}, 
			{
				"x":960,
				"y":288
			}, 
			{
				"x":1024,
				"y":288
			}, 
			{
				"x":1024,
				"y":672
			}, 
			{
				"x":0,
				"y":672
			}, 
			{
				"x":0,
				"y":0
			}];
		public function GameOverScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			
			var insertScoreOnCloud:GetSetScore = new GetSetScore();
			insertScoreOnCloud.insertScoreOnCloud("",1909);
////			LevelGame.level("2");
//			init();
//			var shape:Shape = new Shape();
//			shape.graphics.lineStyle(2, 0x000000, 0.3);
//			shape.graphics.beginFill(0x141414,1);
//			for (var i:int = 1; i < ls.length; i++) 
//			{
//				var obj:Object = ls[i];
//				shape.graphics.lineTo(Number(obj.x),Number(obj.y));
//			}
//			
//			shape.graphics.endFill();
//			var sprite:Sprite = new Sprite();
//			sprite.addChild(shape);
//			addChild(sprite);
//			
//			sprite.x = 0;
//			sprite.y = 0;
//			
//			addChild(new CompleteGame(0x7d7f79,1,4000));
		}
		
		private function init():void
		{
			var quad:Quad = new Quad(stage.stageWidth,stage.stageHeight,0xffffff);
			this.addChild(quad);
			//			
			//			Reverse.instance.chartboost.showRewardedVideo("show quang cao");
			//			Reverse.instance.chartboost.addDelegateEvent(ChartboostEvent.DID_COMPLETE_REWARDED_VIDEO, function (location:String, reward:int):void {
			//				trace( "Chartboost: on Interstitial clicked: " + location );
			//			});
			
			//			addChild(new CompleteGame(0xcd8b39));
		}		
		
		private function handler():void
		{
			// TODO Auto Generated method stub
			_ce.state = new PlayScreen();
		}
		
	}
}



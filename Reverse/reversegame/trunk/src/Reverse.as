package
{
	import com.chartboost.plugin.air.Chartboost;
	import com.milkmangames.nativeextensions.GoViral;
	import com.shephertz.app42.paas.sdk.as3.App42API;
	import com.shephertz.app42.paas.sdk.as3.App42Log;
	import com.shephertz.app42.paas.sdk.as3.game.GameService;
	import com.shephertz.app42.paas.sdk.as3.game.ScoreBoardService;
	import com.shephertz.app42.paas.sdk.as3.social.SocialService;
	import com.shephertz.app42.paas.sdk.as3.storage.StorageService;
	
	import flash.display.Bitmap;
	import flash.display.StageOrientation;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import citrus.core.starling.StarlingCitrusEngine;
	import citrus.core.starling.ViewportMode;
	import citrus.sounds.CitrusSoundGroup;
	
	import components.LevelGame;
	import components.ReverseGameData;
	
	import screens.FirstStoryGameScreen;
	import screens.LevelScreen;
	import screens.YouWinScreen;
	
	import starling.display.Image;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import starling.utils.formatString;
	
	import utils.UtilsAndConfig;
	
	[SWF(frameRate="60",backgroundColor="0x141414")]
	public class Reverse extends StarlingCitrusEngine
	{
		public var assets:AssetManager;
		
		// Startup image
		[Embed(source="../startup/splashscreen.jpg")]
		//		[Embed(source="../startup/splashscreen_portrail.jpg")]
		private static var Background:Class;
		private var background:Bitmap = new Background();
		private var imgbg:Image;
		private var progressBar:AppProgressBar;
		
		public static var instance:Reverse;
		
		public var socialService:SocialService;
		public var storageService:StorageService;
		public var scoreBoardService:ScoreBoardService;
		
		//use this variable if you want to find real scaleFactor to full fill device screen
		public static var realScaleFactor:Number;
		public var chartboost:Chartboost;
		
		
		[Embed(source="assets/fonts/Minecrafter_3.ttf",  mimeType = "application/x-font", embedAsCFF="false", fontFamily="OUTLI")]
		protected static const OUTLI:Class;
		
		[Embed(source = "assets/fonts/alpharuler_bitmap-export.png")]
		public static const PixelmixTexture:Class;
		
		[Embed(source="assets/fonts/alpharuler_bitmap-export.xml", mimeType="application/octet-stream")]
		public static const PixelmixXML:Class;
		
		[Embed(source="assets/fonts/mini_pixel-7.ttf",  mimeType = "application/x-font", embedAsCFF="false", fontFamily="mini_pixel-7")]
		protected static const MiniPixel:Class;

		public var gameService:GameService;
		
		public function Reverse()
		{
			instance = this;
			_baseWidth = 1156;
			_baseHeight = 640;
			//			_baseWidth = 768;
			//			_baseHeight = 1024;
			_viewportMode = ViewportMode.FULLSCREEN;
			gameData = new ReverseGameData();
		}
		
		override protected function handleAddedToStage(e:Event):void
		{
			super.handleAddedToStage(e);
			
			//calculate scale viewport
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, stage.stageWidth, stage.stageHeight), 
				new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight), 
				ScaleMode.SHOW_ALL);
			
			var backgroundScale:Number = 0; 
			if(isOrientationLandscape()==true){
				backgroundScale = stage.fullScreenWidth/background.width;
			}else{
				//tính ra xem đang scale toàn bộ content ở tỉ lệ nào?
				backgroundScale = stage.fullScreenHeight/background.height;
			}
			//scale hình ra cho nó full width
			background.scaleX  = background.scaleY = backgroundScale;
			
			if(isOrientationLandscape()==true){
				//set y để center theo chiều cao của đt
				background.y = - ((background.height - stage.fullScreenHeight)/2);
			}else{
				//set x để center theo chiều doc của đt
				background.x = - ((background.width - stage.fullScreenWidth)/2);
			}
			background.smoothing = true;
			addChild(background);
			
			sound.addSound("themesong", {sound:"assets/sounds/themesound.mp3",loops:-1,group:CitrusSoundGroup.BGM,volume:0.8});
			sound.addSound("button", {sound:"assets/sounds/button.mp3",group:CitrusSoundGroup.SFX,volume:1});
//			sound.addSound("explosion", {sound:"assets/sounds/explosion.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("level", {sound:"assets/sounds/reverse_soundtrack.mp3",loops:-1,group:CitrusSoundGroup.BGM,volume:1});
			sound.addSound("level1", {sound:"assets/sounds/reverse_soundtrack_1.mp3",loops:-1,group:CitrusSoundGroup.BGM,volume:1});
			sound.addSound("hand", {sound:"assets/sounds/monster_2_hand_2.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("thorn", {sound:"assets/sounds/trap_thorn.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("readyBoom", {sound:"assets/sounds/trap_bomb_countdown.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("Boom", {sound:"assets/sounds/trap_bomb_explosion.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("cancel", {sound:"assets/sounds/btn_cancel.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("ok", {sound:"assets/sounds/btn_ok.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("bat_wing_flap", {sound:"assets/sounds/bat_wing_flap.mp3",loops:-1,group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("story_song_mixdown", {sound:"assets/sounds/story_song_mixdown.mp3",loops:-1,group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("teleport", {sound:"assets/sounds/teleport.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("monster_3_skull_chatter", {sound:"assets/sounds/monster_3_skull_chatter.mp3",loops:-1,group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("map", {sound:"assets/sounds/bg_crow.mp3",loops:-1,group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("open", {sound:"assets/sounds/slam_screen_open.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("close", {sound:"assets/sounds/slam_screen_close.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("popup", {sound:"assets/sounds/metal_chain_short.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("ghita", {sound:"assets/sounds/haunting_string.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("hero_hit", {sound:"assets/sounds/explosion.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("windchime", {sound:"assets/sounds/windchime.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("monster_1_spider", {sound:"assets/sounds/monster_1_spider.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("trap_laser", {sound:"assets/sounds/trap_laser.mp3",loops:-1,group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("trap_wheel", {sound:"assets/sounds/trap_wheel.mp3",group:CitrusSoundGroup.SFX,volume:0.8});
			sound.addSound("stage_complete", {sound:"assets/sounds/stage_complete.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("earth_quake", {sound:"assets/sounds/earth_quake.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("broken_glass", {sound:"assets/sounds/broken_glass.mp3",group:CitrusSoundGroup.SFX,volume:5});
			sound.addSound("reverse_mapoverview", {sound:"assets/sounds/reverse_mapoverview.mp3",loops:-1,group:CitrusSoundGroup.BGM,volume:1});
			sound.addSound("laugh", {sound:"assets/sounds/laugh.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("talk", {sound:"assets/sounds/talk.mp3",group:CitrusSoundGroup.SFX,volume:1});
			
			setUpStarling(true);
		}
		
		override public function handleStarlingReady():void
		{
			super.handleStarlingReady();
			
			GoViral.create();
			if(GoViral.goViral.isFacebookSupported()){
				GoViral.goViral.initFacebook("436043106564391","");
			}else{
				trace("FacebookSupported k support");
			}
			
			if(Chartboost.isPluginSupported()){
				chartboost = Chartboost.getInstance();
				chartboost.init("5508eb9bc909a64704c03083", "5418dc83a20923dd5bdccb5e23adde648359592b");
			}
			//			sound.playSound("themesong");
			
			App42API.initialize("b36a0b57eb839fe14281eb240e77d906bf9251ee85ecf600693be501c26c9870","1b9147b9e36125e91951a741ebc520ac6467ee9ccc609e1b7153a05168e5b192");
			App42Log.setDebug(true);
			gameService = App42API.buildGameService();   
			
			storageService = App42API.buildStorageService();
			scoreBoardService = App42API.buildScoreBoardService();
			socialService = App42API.buildSocialService();
			
			removeChild(background);
			
			
			_setupFonts();
			var bgTexture:Texture = Texture.fromEmbeddedAsset(
				Background, false, false, scaleFactor);
			imgbg = new Image(bgTexture);
			imgbg.alpha = 0;
			
//			if(isOrientationLandscape()==true){
//				if(stage.stageWidth != 1024){
//					//tính toán khoảng cách còn dư trên màn hình đt
//					var whitespaceWidth:Number = (stage.fullScreenWidth - (imgbg.width * starling.contentScaleFactor))/starling.contentScaleFactor;
//					//tính chiều rộng thật sự cần kéo ra để full hết đt
//					var realWidth:Number = whitespaceWidth + imgbg.width;
//					//tính ra tỉ lệ scale để apply cho height
//					realScaleFactor = realWidth/imgbg.width;
//					//phải lưu heiht cũ lại để mình còn trù cho y sau đó, cho nó center
//					var oldHeight:Number = imgbg.height;
//					imgbg.scaleX = imgbg.scaleY = realScaleFactor;
//					imgbg.y = -((imgbg.height) - oldHeight)/2;
//				}
//			}else{
//				//tương tự cách tính trên nếu đt đang ở chế độ portrail
//				if(stage.stageHeight != 1024){
//					var whitespaceHeight:Number = (stage.fullScreenHeight - (imgbg.height * starling.contentScaleFactor))/starling.contentScaleFactor;
//					var realHeight:Number = whitespaceHeight + imgbg.height;
//					realScaleFactor = realHeight/imgbg.height;
//					var oldWidth:Number = imgbg.width;
//					imgbg.scaleX = imgbg.scaleY = realScaleFactor;
//					imgbg.x = -((imgbg.width) - oldWidth)/2;
//				}
//			}
			
			this.starling.stage.addChild(imgbg);
			
			//show progress bar to load assets
			progressBar = new AppProgressBar(383, 40);
			if(isOrientationLandscape()==true){
				progressBar.x = (imgbg.width  - progressBar.width)  / 2 ;
			}else{
				progressBar.x = (768  - progressBar.width)  / 2 ;
			}
			
			progressBar.y = stage.stageHeight * 0.91/starling.contentScaleFactor;
			this.starling.stage.addChild(progressBar);
			
			
			//setup and load assets
			var appDir:File = File.applicationDirectory;
			
			assets = new AssetManager(scaleFactor);
			assets.enqueue( appDir.resolvePath(formatString("assets/textures/1x")));
			//			var levelGame:LevelGame = new LevelGame();
			//			levelGame.readJsonOnline("http://118.69.70.144:8080/downloads/smappi/reverse/levels.json?i="+Math.random()+99999);
			LevelGame.saveLevel();
			LevelGame.saveMapLevel();
			UtilsAndConfig.defaultAssets = assets;
			
			assets.loadQueue(function(ratio:Number):void
			{
				progressBar.ratio = ratio;
				
				//assets fully loaded
				if (ratio == 1){ 
//					if (UtilsAndConfig.getFirst() == 0) 
//					{
//						state = new FirstStoryGameScreen();		
//					}else{
//						state = new LevelScreen();		
//					}
					state = new YouWinScreen();
					background = null;
					imgbg.removeFromParent(true);
					progressBar.removeFromParent(true);
					
					System.pauseForGCIfCollectionImminent(0);
					System.gc();
					
				}
			});
		}
		
		private function _setupFonts():void{
			TextField.registerBitmapFont(new BitmapFont(Texture.fromBitmap(new PixelmixTexture()), XML(new PixelmixXML())),"AlphaRuler");
		}
		
		private function isOrientationLandscape():Boolean {
			if (stage.orientation == StageOrientation.DEFAULT || stage.orientation == StageOrientation.UPSIDE_DOWN) return false;
			return true;
		}
	}
}
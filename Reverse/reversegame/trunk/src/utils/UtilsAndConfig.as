package utils
{
	import flash.events.StatusEvent;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.system.Capabilities;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import air.net.URLMonitor;
	
	import citrus.sounds.CitrusSoundGroup;
	
	import components.ReverseGameData;
	
	import org.osflash.signals.Signal;
	
	import starling.utils.AssetManager;

	public class UtilsAndConfig
	{
		public static var defaultAssets:AssetManager;
		public static var updateDelayMiliseconds:Number = 250;
		public static const CONNECTION:String = "CONNECTION";
		public static var score:int = 0;
		//these use for register pushnotification
		public static const APP_ID:String = "babylearnshape";
		public static const GOOGLE_PROJECT_ID:String = "hale-yew-454";
		
		public static const BASE_URL:String = "http://buzzkid.vn/services";		
		
		public static function getClass(obj:Object):Class {
		    return Class(getDefinitionByName(getQualifiedClassName(obj)));
		}
		
		/**
		 * detect current running device is iOS (true) or android (false) 
		 * @return 
		 * 
		 */
		public static function is_IOS():Boolean
		{
			if(Capabilities.version.toLowerCase().indexOf("ios") > -1)
			{
				return true;
			}
			else if(Capabilities.version.toLowerCase().indexOf("and") > -1)
			{
				return false;
			}
			
			return true;
		}
		
		public static function checkConnection(functionEvent:Function):void{
			var signal:Signal = new Signal();
			var monitor:URLMonitor = new URLMonitor(new URLRequest("https://www.google.com/"));
			monitor.addEventListener(StatusEvent.STATUS, checkHTTP);
			monitor.start();
			function checkHTTP(e:StatusEvent):void {
				if (monitor.available) {
					signal.dispatch(1);
				} else {
					signal.dispatch(0);
				}
			}
			signal.add(functionEvent);
		}
		
		/**
		 * this function will return random value between min and max parametor 
		 * @param minNum
		 * @param maxNum
		 * @return 
		 * 
		 */		
		public static function randRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		public static function randRangeNumber(minNum:Number, maxNum:Number):Number 
		{
			return ((Math.random() * (maxNum - minNum)) + minNum);
		}
		
		/**
		 * play sound
		 */
		public static function playSound(nameSound:String):void{
			if((Reverse.instance.sound.getSound(nameSound).group.groupID == CitrusSoundGroup.BGM && (Reverse.instance.gameData as ReverseGameData).playSound == 1)
				||
				(Reverse.instance.sound.getSound(nameSound).group.groupID == CitrusSoundGroup.SFX && (Reverse.instance.gameData as ReverseGameData).playFX == 1)
			){
				Reverse.instance.sound.playSound(nameSound);
			}
		}
		
		public static function stopSound(nameSound:String):void{
			Reverse.instance.sound.stopSound(nameSound);
		}
		
		public static function stopSoundFX():void{
			Reverse.instance.sound.getGroup(CitrusSoundGroup.SFX).stopAllSounds();
		}
		
//		public static function playSoundFX():void{
//			Reverse.instance.sound.getGroup(CitrusSoundGroup.SFX).
//		}
		
		public static function stopSoundBG():void{
			Reverse.instance.sound.getGroup(CitrusSoundGroup.BGM).volume = 0;
		}
		
		public static function playSoundBG():void{
			Reverse.instance.sound.getGroup(CitrusSoundGroup.BGM).volume = 1;
		}
		
		public static function tweenSound(nameSound:String,volume:Number,time:Number):void{
			Reverse.instance.sound.tweenVolume(nameSound,volume,time);
		}
		
		public static function objectLength(myObject:Object):int {
			var cnt:int=0;
			
			for (var s:Object in myObject) {
				cnt++;
			}
			
			return cnt;
		}
		
		/**
		 * save data in game
		 */
		public static function getData(key:String):Object{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			return shareData.data[key];
		}
		
		public static function setData(key:String,value:Object):void{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			shareData.data[key] = value;
			shareData.flush();
		}
		
		/**
		 * first play game 
		 */
		public static function getFirst():Object{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			if (shareData.data["First"] == undefined) 
			{
				return 0;
			}else{
				return shareData.data["First"];
			}
		}
		
		public static function setFirst(value:int):void{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			shareData.data["First"] = value;
		}
		
		/**
		 * Level current 
		 */
		public static function getCurrentLevel():Object{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			if (shareData.data["LevelCurrent"] == undefined) 
			{
				return 0;
			}else{
				return shareData.data["LevelCurrent"];
			}
		}
		
		public static function setLevelCurrent(value:int):void{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			shareData.data["LevelCurrent"] = value;
		}
		
		/**
		 * Level next 
		 */
		public static function getLevelNext():Object{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			if (shareData.data["LevelNext"] == undefined) 
			{
				return 1;
			}else{
				return shareData.data["LevelNext"];
			}
		}
		
		public static function setLevelNext(value:int):void{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			shareData.data["LevelNext"] = value;
		}
		
		/**
		 * get list score 
		 */
		public static function getLevelScore():Array{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			if (shareData.data["LevelScore"] == undefined) 
			{
				return [];
			}else{
				return shareData.data["LevelScore"];
			}
		}
		
		public static function setLevelScore(value:Array):void{
			var shareData:SharedObject;
			shareData = SharedObject.getLocal("dataReverse");
			shareData.data["LevelScore"] = value;
		}
		
		public static function findInArray(str:String):int {
			var arr:Array = getLevelScore();
			for(var i:int = 0; i < arr.length; i++){
				if(arr[i].name == str){
					return i;
					break;
				}
			}
			return -1; //If not found
		}
	}
}
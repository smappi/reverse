package 
{
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class AppProgressBar extends Sprite
	{
		
		[Embed(source="../startup/progressbarBg.png")] 
		private static var Progressbar:Class;		
		
		[Embed(source="../startup/progressbarLeft.png")] 
		private static var progressbarLeft:Class;	
		
		[Embed(source="../startup/progressbarCenter.png")] 
		private static var progressbarCenter:Class;	
		
		[Embed(source="../startup/progressbarRight.png")] 
		private static var progressbarRight:Class;			
		
		[Embed(source="../startup/progressbarStar.png")] 
		private static var progressbarStar:Class;			
		
		private var mLeft:Image;
		private var mCenter:Image;
		private var mRight:Image;
		private var mBackground:Image;
		private var mStar:Image;
		
		private var value:int;
		private var progressbarWidth:int;
		
		public function AppProgressBar(width:int, height:int)
		{
			progressbarWidth = width;
			init(width, height);
		}
		
		private function init(width:int, height:int):void
		{
			var scale:Number = Starling.contentScaleFactor;
			var padding:Number = 0;
			var cornerRadius:Number = padding * scale * 2;
			
			mBackground = Image.fromBitmap(new Progressbar());
			
			addChild(mBackground);
			
			// create progress bar quad
			mLeft = Image.fromBitmap(new progressbarLeft());	
			mLeft.x = 1.5;
			mLeft.y = 2;
			mLeft.visible = false;
			addChild(mLeft);            
			
			mCenter = Image.fromBitmap(new progressbarCenter());	
			mCenter.x = mLeft.x + mLeft.width - 1;
			mCenter.y = 2;	
			mCenter.width = 1;
			mCenter.visible = false;
			addChild(mCenter);   
			
			mRight = Image.fromBitmap(new progressbarRight());		
			mRight.x = width - mRight.width - 3;
			mRight.y = 2;
			mRight.visible = false;
			addChild(mRight);   	
			
			mStar = Image.fromBitmap(new progressbarStar());	
			mStar.alignPivot();
			mStar.x = 20;
			mStar.y = 20;
			addChild(mStar);   				
		}
		
		public function set ratio(value:Number):void 
		{ 
			value = Math.max(0.0, Math.min(1.0, value)); 
			mLeft.visible = true;
			mCenter.visible = true;
			if (value > 0.1 && value <= 0.15){
				
				mStar.x = progressbarWidth * value - 20;
			}
			else if (value > 0.15 && value <= 0.91){
				mCenter.width = progressbarWidth * value;
				mStar.x = progressbarWidth * value + 5;
			}
			else if (value > 0.91){
				mCenter.width = progressbarWidth * value - 20;
				mRight.visible = true;
				mStar.x = progressbarWidth * value;
			}
		}
	}
}
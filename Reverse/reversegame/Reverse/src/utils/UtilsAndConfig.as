package utils
{
	import flash.system.Capabilities;
	
	import starling.utils.AssetManager;

	public class UtilsAndConfig
	{
		public static var defaultAssets:AssetManager;
		public static var updateDelayMiliseconds:Number = 250;
		
		//these use for register pushnotification
		public static const APP_ID:String = "babylearnshape";
		public static const GOOGLE_PROJECT_ID:String = "hale-yew-454";
		
		public static const BASE_URL:String = "http://buzzkid.vn/services";		
		
		/**
		 * detect current running device is iOS (true) or android (false) 
		 * @return 
		 * 
		 */
		public static function is_IOS():Boolean
		{
			if(Capabilities.version.toLowerCase().indexOf("ios") > -1)
			{
				return true;
			}
			else if(Capabilities.version.toLowerCase().indexOf("and") > -1)
			{
				return false;
			}
			
			return true;
		}
		
		/**
		 * this function will return random value between min and max parametor 
		 * @param minNum
		 * @param maxNum
		 * @return 
		 * 
		 */		
		public static function randRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		public static function objectLength(myObject:Object):int {
			var cnt:int=0;
			
			for (var s:Object in myObject) {
//				trace("objectLength "+cnt);
				cnt++;
			}
			
			return cnt;
		}
	}
}
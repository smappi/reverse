package screens
{
	import citrus.core.starling.StarlingState;
	
	import feathers.controls.Button;
	
	import starling.events.Event;
	
	public class YouWinScreen extends StarlingState
	{

		public function YouWinScreen()
		{
			super();
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			init();
		}
		
		private function init():void
		{
			var btn:Button = new Button();
			btn.label = "YOU WIN!!! PLAY AGAIN";
			btn.addEventListener(Event.TRIGGERED,handler);
			btn.x = stage.stageWidth/2;
			btn.y = stage.stageHeight/2;
			addChild(btn);			
		}		
		
		private function handler():void
		{
			// TODO Auto Generated method stub
			_ce.state = new PlayScreen();
		}
		
	}
}



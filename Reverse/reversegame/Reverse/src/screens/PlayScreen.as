package screens
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.core.starling.StarlingState;
	import citrus.math.MathVector;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.simple.DynamicObject;
	import citrus.objects.platformer.simple.Sensor;
	import citrus.physics.simple.SimpleCitrusSolver;
	import citrus.view.starlingview.StarlingArt;
	import citrus.view.starlingview.StarlingCamera;
	
	import cubeplay.levelmanager.LevelManager;
	import cubeplay.levelmanager.LevelsScreen;
	
	import feathers.controls.Button;
	
	import obj.MyHero;
	import obj.MyMap;
	import obj.MySensor;
	import obj.MyStar;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.particles.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.UtilsAndConfig;
	
	public class PlayScreen extends StarlingState
	{
		
		private var logobt:Button;
		
		private var hero:MyHero;
		
		private var mouseX:Number;
		
		private var mouseY:Number;
		
		private var heroX:Number;
		
		private var heroY:Number;
		
		private var quad:Quad;
		
		private var useParticle:Boolean = true;
		
		public static var particleCoffee:CitrusSprite;
		
		public static var particleHero:CitrusSprite;
		
		public static var level:Object;
		
		private var gameOver:Boolean = false;
		
		private var isImmortal:Boolean = false;
		
		private var btn2:Button;
		
		public var lvlEnded:Signal;
		public var lvlRestart:Signal;
		public var nameLevel:String;
		
		public function PlayScreen(levelParam:Object = null)
		{
			super();
			if(levelParam != null){
				level = levelParam;
				trace("level name = "+level.name);
				nameLevel = (level.name as String).toUpperCase();
			}
			
			lvlEnded = new Signal();
			lvlRestart = new Signal();
			
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
			
			
		}
		
		override public function initialize():void
		{
			// this function will be call when state ready
			super.initialize();
			if(level == null){
				LevelManager.getInstance().showMe();
				return;
			}else{
				init();
			}
		}
		
		private function init():void
		{
			
			
			var simpleCitrusSolver:SimpleCitrusSolver = new SimpleCitrusSolver("citrus solver");
			add(simpleCitrusSolver);
			
			//			simpleCitrusSolver.collide(DynamicObject, StaticObject);
			simpleCitrusSolver.overlap(DynamicObject, Sensor);
			//			simpleCitrusSolver.
			
			var mapObj:Object = level;
			
			
			var mapWidth:int = mapObj.mapWidth;
			var mapHeight:int = mapObj.mapHeight;
			var myMap:MyMap = new MyMap("",{width:mapWidth,height:mapHeight});
			add(myMap);
			
			if(useParticle){
				//				particleCoffee = new CitrusSprite("particleCoffee", {view:new PDParticleSystem(XML(new ParticleAssets.ParticleCoffeeXML()), Texture.fromBitmap(new ParticleAssets.ParticleTexture()))});
				//				add(particleCoffee);
				//				
				particleHero = new CitrusSprite("particleHero", {view:new PDParticleSystem(XML(new ParticleAssets.ParticleHeroXML()), Texture.fromBitmap(new ParticleAssets.ParticleHeroTexture()))});
				add(particleHero);
				
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).start();
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).start();
				
			}
			
			var objs:Object = mapObj.objects;
			//			trace("level = "+objs);
			for each (var i:Object in objs) 
			{
				var name:String = i.name;
				var width:int = i.width;
				var height:int = i.height;
				var x:int = i.x;
				var y:int = i.y;
				var startX:int = x;
				var startY:int = y;
				var stopX:int = int(i.properties.stopX);
				var stopY:int = int(i.properties.stopY);
				var speed:int = int(i.properties.speed);
				var delay:int = int(i.properties.delay);
				//				trace(i.name + "  " +stopX);
				
				switch(name.toLowerCase()){
					case "hero":{
						hero = new MyHero("hero", {x:x, y:y, width:width, height:height}, null);
						add(hero);
					} break;
					
					case "sensor":{
						var sensor:MySensor = new MySensor(UtilsAndConfig.randRange(1,10000)+"Sensor",{x:x,y:y,width:width,height:height},stopX,stopY,speed,delay); 
						add(sensor);
					} break;
					
					case "star":{
						//						trace("width = "+width);
						var star:MyStar = new MyStar("Star",{x:x,y:y,width:width,height:height},stopX,stopY,speed); 
						add(star);
					} break;					
				}
			}
			
			
			
			hero.onCollide.add(_collisionStart);
			hero.onPersist.add(_collisionPersist);
			hero.onSeparate.add(_collisionEnd);
			
			var quad:Quad = new Quad(stage.stageWidth,stage.stageHeight);
			quad.alpha = 0;
			this.addChild(quad);
			quad.addEventListener(TouchEvent.TOUCH, touchHandler);
			
			var btn:Button = new Button();
			btn.label = "LEVELS";
			btn.addEventListener(Event.TRIGGERED,levelHandler);
			btn.x = stage.stageWidth - 125;
			addChild(btn);	
			
			btn2 = new Button();
			btn2.label = "IMMORTAL";
			btn2.addEventListener(Event.TRIGGERED,immortalHandler);
			btn2.x = stage.stageWidth - 125;
			btn2.y = 80;
			
			addChild(btn2);	
			
			var text:TextField = new TextField(160,80,nameLevel,"Verdana",20,0xFFFFFF);
			text.x = stage.stageWidth - 160;
			text.y = 160;
			addChild(text);	
			
		}		
		
		private function immortalHandler():void
		{
			if(!isImmortal){
				isImmortal = true;
				btn2.label = "NORMAL";
			}else{
				isImmortal = false;
				btn2.label = "IMMORTAL";
			}
		}
		
		private function levelHandler():void
		{
			_ce.state = new LevelsScreen(LevelManager.getInstance());
		}		
		
		private function _collisionStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			
			trace('collision', self, other);
			if(!gameOver){
				if(other is MySensor && !isImmortal){
					gameOver = true;
					trace("Game Over");
					Reverse.instance.sound.playSound("explosion");
					hero.hurt_Shake();
					shakeCam(this);
					TweenMax.delayedCall(0.5,function():void{
						TweenMax.killTweensOf(hero);
						TweenMax.killTweensOf(this);
						lvlRestart.dispatch();
						//						_ce.state = new GameOverScreen();
					});
					
				}else if(other is MyStar){
					trace("You win");
					lvlEnded.dispatch();
				}
			}
		}
		
		private function _collisionPersist(self:CitrusSprite, other:CitrusSprite, normal:MathVector):void {
			
			trace('persit', self, other);
		}
		
		private function _collisionEnd(self:CitrusSprite, other:CitrusSprite):void {
			
			trace('end', self, other);
		}
		
		public function shakeCam(objectToShake:Object,duration:Number = 1, intensity:Number = 2):void
		{
			var cam:StarlingCamera = view.camera as StarlingCamera;
			//get a temp reference to the current target
			var t:Object = objectToShake;
			//get camera easing
			var teasingX:Number =  cam.easing.x;
			var teasingY:Number =  cam.easing.y;
			var shakeTarget:* = objectToShake;
			//start shaking
			cam.easing.x = 1;
			cam.easing.y = 1;
			cam.target = shakeTarget;
			
			TweenLite.to(shakeTarget, duration , {onUpdate:
				function():void
				{
					shakeTarget.x = t.x + (Math.random()*2-1)*intensity;
					shakeTarget.y = t.y + (Math.random()*2-1)*intensity;
				},
				onComplete:
				function():void{
					//return to old target.
					cam.easing.x = teasingX;
					cam.easing.y = teasingY;
					cam.target = t;
				}});
			
			
		}
		
		private function touchHandler(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				var localPos:Point = touch.getLocation(this);
				mouseX = localPos.x;
				mouseY = localPos.y;
				
				heroX = hero.x;
				heroY = hero.y;
			}
			
			var touch2:Touch = event.getTouch(this, TouchPhase.MOVED);
			if (touch2)
			{
				var localPos2:Point = touch2.getLocation(this);
				hero.x = heroX + mouseX - localPos2.x;
				hero.y = heroY + mouseY - localPos2.y;
			}
		}
		
		override public function update(timeDelta:Number):void
		{
			// TODO Auto Generated method stub
			super.update(timeDelta);
			if(useParticle && hero != null){
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).emitterX = hero.x;
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).emitterY = hero.y;
				
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).emitterX = hero.x;
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).emitterY = hero.y;
			}
			//					
		}
		
		
		
		
	}
}




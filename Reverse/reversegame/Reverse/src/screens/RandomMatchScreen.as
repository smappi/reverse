package screens
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import citrus.math.MathVector;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.simple.Sensor;
	import citrus.physics.simple.SimpleCitrusSolver;
	import citrus.view.starlingview.StarlingArt;
	import citrus.view.starlingview.StarlingCamera;
	
	import cubeplay.basicscreens.CPRandomMatchScreen;
	import cubeplay.levelmanager.LevelManager;
	import cubeplay.levelmanager.LevelsScreen;
	import cubeplay.smappiOnlineGame.entities.UserDataObject;
	import cubeplay.smappiOnlineGame.players.Player;
	
	import feathers.controls.Button;
	
	import obj.MyHero;
	import obj.MyMap;
	import obj.MySensor;
	import obj.MyStar;
	import obj.RemoteHero;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.particles.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import utils.UtilsAndConfig;
	
	public class RandomMatchScreen extends CPRandomMatchScreen
	{
		
		private var logobt:Button;
		
		private var hero:MyHero;
		
		private var mouseX:Number;
		
		private var mouseY:Number;
		
		private var heroX:Number;
		
		private var heroY:Number;
		
		public var heroFirstX:Number;
		
		public var heroFirstY:Number;		
		
		public var heroWidth:Number;
		
		public var heroHeight:Number;	
		
		private var quad:Quad;
		
		private var useParticle:Boolean = true;
		
		public static var particleCoffee:CitrusSprite;
		
		public static var particleHero:CitrusSprite;
		
		public static var level:Object;
		
		private var gameOver:Boolean = false;
		
		private var isImmortal:Boolean = false;
		
		private var btn2:Button;
		
		public var lvlEnded:Signal;
		public var lvlRestart:Signal;
		public var nameLevel:String;
		
		private var quad2:Quad;
		
		private var _miliseconds:Number = 0;
		private var _isPlaying:Boolean = false;
		private var _remotePlayer:Array = new Array();
		
		public function RandomMatchScreen()
		{
			super();
			
			lvlEnded = new Signal();
			lvlRestart = new Signal();
			
		}
		
		override public function destroy():void
		{
			// this function will be call when state is off, clean up your resource at here
			super.destroy();
			
			
		}
		
		
		override public function initSprite(data:Object = null):void
		{
			if(data != null && data.level != null){
				level = LevelManager.getInstance().getLevelByIndex(data.level);
			}
			if(level == null){
				level = LevelManager.getInstance().getCurrentLevel();
			}
			
			var simpleCitrusSolver:SimpleCitrusSolver = new SimpleCitrusSolver("citrus solver");
			add(simpleCitrusSolver);
			
			//			simpleCitrusSolver.collide(DynamicObject, StaticObject);
			simpleCitrusSolver.overlap(MyHero, Sensor);
			//			simpleCitrusSolver.
			
			var mapObj:Object = level;
			
			
			var mapWidth:int = mapObj.mapWidth;
			var mapHeight:int = mapObj.mapHeight;
			var myMap:MyMap = new MyMap("",{width:mapWidth,height:mapHeight});
			add(myMap);
			
			if(useParticle){
				//				particleCoffee = new CitrusSprite("particleCoffee", {view:new PDParticleSystem(XML(new ParticleAssets.ParticleCoffeeXML()), Texture.fromBitmap(new ParticleAssets.ParticleTexture()))});
				//				add(particleCoffee);
				//				
				particleHero = new CitrusSprite("particleHero", {view:new PDParticleSystem(XML(new ParticleAssets.ParticleHeroXML()), Texture.fromBitmap(new ParticleAssets.ParticleHeroTexture()))});
				add(particleHero);
				
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).start();
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).start();
				
			}
			
			var objs:Object = mapObj.objects;
			//			trace("level = "+objs);
			for each (var i:Object in objs) 
			{
				var name:String = i.name;
				var width:int = i.width;
				var height:int = i.height;
				var x:int = i.x;
				var y:int = i.y;
				var startX:int = x;
				var startY:int = y;
				var stopX:int = int(i.properties.stopX);
				var stopY:int = int(i.properties.stopY);
				var speed:int = int(i.properties.speed);
				var delay:int = int(i.properties.delay);
				//				trace(i.name + "  " +stopX);
				
				switch(name.toLowerCase()){
					case "hero":{
						heroFirstX = x + width/2;
						heroFirstY = y + height/2;
						heroWidth = width;
						heroHeight = height;
						hero = new MyHero("hero", {x:x, y:y, width:width, height:height}, null);
						//						add(hero);
					} break;
					
					case "sensor":{
						var sensor:MySensor = new MySensor(UtilsAndConfig.randRange(1,10000)+"Sensor",{x:x,y:y,width:width,height:height},stopX,stopY,speed,delay); 
						add(sensor);
					} break;
					
					case "star":{
						//						trace("width = "+width);
						var star:MyStar = new MyStar("Star",{x:x,y:y,width:width,height:height},stopX,stopY,speed); 
						add(star);
					} break;					
				}
			}
			
			
			
			hero.onCollide.add(_collisionStart);
			hero.onPersist.add(_collisionPersist);
			hero.onSeparate.add(_collisionEnd);
			
			quad2 = new Quad(stage.stageWidth,stage.stageHeight);
			quad2.alpha = 0;
			this.addChild(quad2);
			
			
			var btn:Button = new Button();
			btn.label = "LEVELS";
			btn.addEventListener(Event.TRIGGERED,levelHandler);
			btn.x = stage.stageWidth - 125;
			addChild(btn);	
			
			var button:Button = new Button();
			button.label = "Leave";
			button.x = stage.stageWidth - 50;
			button.addEventListener(Event.TRIGGERED,buttonHandler);
			addChild( button );		
			
			btn2 = new Button();
			btn2.label = "IMMORTAL";
			btn2.addEventListener(Event.TRIGGERED,immortalHandler);
			btn2.x = stage.stageWidth - 125;
			btn2.y = 80;
			
			addChild(btn2);	
			
			var text:TextField = new TextField(160,80,nameLevel,"Verdana",20,0xFFFFFF);
			text.x = stage.stageWidth - 160;
			text.y = 160;
			addChild(text);	
			
		}		
		
		override public function joinedRoomSuccessfullyHandler(e:Event, roomIdParam:String):void
		{
			roomId = roomIdParam;
			waitingRoomPanel.addPlayer(userData);
			
//						this.startRoom();
		}
		
		override public function startGameHandler(e:Event,data:Object):void
		{
			if(!_isPlaying){
				_isPlaying = true;
				// TODO Auto Generated method stub
				waitingRoomPanel.visible = false;
				initSprite(data);
				
				for (var i:int = 0; i < _remotePlayer.length; i++) 
				{
					
					(_remotePlayer[i] as RemoteHero).x = heroFirstX;
					(_remotePlayer[i] as RemoteHero).y = heroFirstY;
					(_remotePlayer[i] as RemoteHero).setView({width:heroWidth,height:heroHeight});
					trace("addRemotePlayer 1");
					add(_remotePlayer[i]);
				}

				localPlayer = gameController.addPlayer(hero);
				quad2.addEventListener(TouchEvent.TOUCH, touchHandler);
			}
		}
		
		override public function touchHandler(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.BEGAN);
			if (touch)
			{
				
				var localPos:Point = touch.getLocation(this);
				mouseX = localPos.x;
				mouseY = localPos.y;
				
				heroX = hero.x;
				heroY = hero.y;
			}
			
			var touch2:Touch = event.getTouch(this, TouchPhase.MOVED);
			if (touch2)
			{
				var localPos2:Point = touch2.getLocation(this);
				var x:Number = heroX + mouseX - localPos2.x;
				var y:Number = heroY + mouseY - localPos2.y;
				sendRealtimeUpdate(x,y);
				
			}
			
			var touch3:Touch = event.getTouch(this, TouchPhase.ENDED);
			if (touch3)
			{
				var localPos3:Point = touch3.getLocation(this);
				var x1:Number = heroX + mouseX - localPos3.x;
				var y1:Number = heroY + mouseY - localPos3.y;
				sendRealtimeUpdate(x1,y1,false,true);
				
			}
			
		}
		
		override public function reconnectedToServerHandler():void
		{
			trace("reconnectedToServerHandler");
			connectedToServerHandler();
		}
		
		override public function connectedToServerHandler():void
		{
			trace("connectedToServerHandler");
			userData = new UserDataObject();	
			userData.id = gameController.me.id;
			userData.fullname = gameController.me.fullname;
			userData.url = gameController.me.url;
			userData.x = heroFirstX;
			userData.y = heroFirstY;
			userData.width = heroWidth;
			userData.height= heroHeight;
			var msg:String = JSON.stringify(userData);					 
			trace(msg);
			gameController.setUserDataObject(gameController.me.id,msg);
			gameController.getRoomsInRange();
		}	
		
		override public function startRoom():void{
			if(gameController.isHost) {
				gameController.startRoom({level:LevelManager.getInstance().getRandomLevelIndex()});
			}
		}
		
		override public function addRemotePlayer(player:Player):void
		{
			
			_remotePlayer.push(player);
			if(_isPlaying){
				trace("addRemotePlayer 2");
				player.x = heroFirstX;
				player.y = heroFirstY;
				player.setView({width:heroWidth,height:heroHeight});
				add(player);
			}
		}
		
		
		
		private function sendRealtimeUpdate(xParam:Number,yParam:Number,resetPos:Boolean = false,forceUpdate:Boolean = false):void{
			if(!resetPos) localPlayer.moveTo(new flash.geom.Point(xParam,yParam));
			else {
				localPlayer.x = xParam;
				localPlayer.y = yParam;
			}
			
			var currentMiliseconds:Number = (new Date()).getTime();
			if(forceUpdate || resetPos || currentMiliseconds - _miliseconds >= UtilsAndConfig.updateDelayMiliseconds){
				_miliseconds = currentMiliseconds;
				var userData:UserDataObject = new UserDataObject();	
				userData.id = gameController.me.id;
				userData.x = xParam;
				userData.y = yParam;
				userData.score = localPlayer.score;
				userData.fullname = gameController.me.fullname;
				
				var msg:String = JSON.stringify(userData);					
				trace(msg);
				gameController.sendMessage(msg);
				gameController.setUserDataObject(localPlayer.id,msg);	
			}
		}
		
		private function immortalHandler():void
		{
			if(!isImmortal){
				isImmortal = true;
				btn2.label = "NORMAL";
			}else{
				isImmortal = false;
				btn2.label = "IMMORTAL";
			}
		}
		
		private function levelHandler():void
		{
			_ce.state = new LevelsScreen(LevelManager.getInstance());
		}		
		
		private function _collisionStart(self:CitrusSprite, other:CitrusSprite, normal:MathVector, impact:Number):void {
			if(!gameOver){
				if(other is MySensor && !isImmortal){
					hero.onCollide.remove(_collisionStart);
					gameOver = true;
					quad2.removeEventListener(TouchEvent.TOUCH, touchHandler);
					Reverse.instance.sound.playSound("explosion");
					hero.hurt_Shake();
					shakeCam(this);
					TweenMax.delayedCall(1,function():void{
						TweenMax.killTweensOf(hero);
						TweenMax.killTweensOf(this);
						//						lvlRestart.dispatch();
						quad2.addEventListener(TouchEvent.TOUCH, touchHandler);
						gameOver = false;
						sendRealtimeUpdate(heroFirstX,heroFirstY,true);
						//						gameOver = false;
						hero.onCollide.add(_collisionStart);
						//						_ce.state = new GameOverScreen();
					});
					
				}else if(other is MyStar){
					trace("You win");
					gameController.stopRoom();
					lvlEnded.dispatch();
				}
			}
		}
		
		private function _collisionPersist(self:CitrusSprite, other:CitrusSprite, normal:MathVector):void {
			
			trace('persit', self, other);
		}
		
		private function _collisionEnd(self:CitrusSprite, other:CitrusSprite):void {
			
			trace('end', self, other);
		}
		
		public function shakeCam(objectToShake:Object,duration:Number = 1, intensity:Number = 2):void
		{
			var cam:StarlingCamera = view.camera as StarlingCamera;
			//get a temp reference to the current target
			var t:Object = objectToShake;
			//get camera easing
			var teasingX:Number =  cam.easing.x;
			var teasingY:Number =  cam.easing.y;
			var shakeTarget:* = objectToShake;
			//start shaking
			cam.easing.x = 1;
			cam.easing.y = 1;
			cam.target = shakeTarget;
			
			TweenLite.to(shakeTarget, duration , {onUpdate:
				function():void
				{
					shakeTarget.x = t.x + (Math.random()*2-1)*intensity;
					shakeTarget.y = t.y + (Math.random()*2-1)*intensity;
				},
				onComplete:
				function():void{
					//return to old target.
					cam.easing.x = teasingX;
					cam.easing.y = teasingY;
					cam.target = t;
				}});
			
			
		}
		
		
		override public function update(timeDelta:Number):void
		{
			// TODO Auto Generated method stub
			super.update(timeDelta);
			if(useParticle && hero != null){
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).emitterX = hero.x;
				//				((view.getArt(particleCoffee) as StarlingArt).content as PDParticleSystem).emitterY = hero.y;
				
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).emitterX = hero.x;
				((view.getArt(particleHero) as StarlingArt).content as PDParticleSystem).emitterY = hero.y;
			}
			//					
		}
		
		
		
		
	}
}




package obj
{
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class HeroView extends Sprite
	{
		
		private var _score:int;
		public var sensorDebug:Quad;
		private var txtField:TextField;
		public function HeroView(w:int,h:int,isRemotePlayer:Boolean = false)
		{
			super();
			score = 0;
			var quad:Quad;
			if(isRemotePlayer){
				txtField = new TextField(100,100,score+"","Verdana",50,0xc0392b);
				quad = new Quad(w,h,0xc0392b);
			}else{
				txtField = new TextField(100,100,score+"","Verdana",50,0x2ecc71);
				quad = new Quad(w,h,0x2ecc71);
			}
			addChild(quad);
			quad.visible = false;
			sensorDebug = new Quad(50,50,0xFFFFFF);
			addChild(sensorDebug);
			//			addChild(txtField);
		}
		
		public function get score():int
		{
			return _score;
		}
		
		public function set score(value:int):void
		{
			_score = value;
			if(txtField != null) txtField.text = score+"";
		}
		
		public function increaseScore():void{
			score++;
		}
		
		public function decreaseScore():void{
			score++;
		}		
	}
}
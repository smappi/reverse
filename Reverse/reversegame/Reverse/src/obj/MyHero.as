package obj
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Quart;
	import com.greensock.easing.RoughEase;
	
	import cubeplay.smappiOnlineGame.players.Player;
	
	public class MyHero extends Player
	{
		public function MyHero(name:String, params:Object,customParam:Object)
		{
			super(name, params, customParam);
			view = new HeroView(params.width,params.height,false);
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			(view as HeroView).sensorDebug.x = offsetX;
			(view as HeroView).sensorDebug.y = offsetY;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			trace("MyHero "+x+ " "+y + " " + offsetX + " " +offsetY);
			group = 5;
		}
		
		public function hurt_Shake():void {
			TweenLite.to(this, 1, {y:"10", x:"10", ease:RoughEase.create(1, 25, false,
				Quart.easeInOut, "out", false), onComplete:endShake});
			
		}
		public function endShake():void{
			TweenLite.to(this, 0, {y:0, x:0});
		}
		
		
	}
}
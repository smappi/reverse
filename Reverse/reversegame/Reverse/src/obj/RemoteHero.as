package obj
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Quart;
	import com.greensock.easing.RoughEase;
	
	import flash.geom.Point;
	
	import cubeplay.smappiOnlineGame.players.Player;
	import cubeplay.utils.GameFactory;
	
	import utils.UtilsAndConfig;
	
	public class RemoteHero extends Player
	{
		public function RemoteHero(name:String, params:Object,customParam:Object)
		{
			super(name, params, customParam);
			view = new HeroView(customParam.width,customParam.height,true);
			offsetX = -customParam.width/2;
			offsetY = -customParam.height/2;
			x = customParam.x + Math.abs(offsetX);
			y = customParam.y + Math.abs(offsetY);
			trace("RemoteHero "+x+ " "+y + " " + offsetX + " " +offsetY);
			group = 4;
		}
		
		public function hurt_Shake():void {
			
			TweenLite.to(this, 1, {y:"10", x:"10", ease:RoughEase.create(1, 25, false,
				Quart.easeInOut, "out", false), onComplete:endShake});
			
		}
		public function endShake():void{
			TweenLite.to(this, 0, {y:0, x:0});
		}
		
		override public function setView(params:Object=null):void
		{
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			view = new GameFactory.PLAYVIEW_CLASS(params.width,params.height,true);
		}
		
		
		
		override public function moveTo(point:flash.geom.Point):void
		{
			trace("moveTo "+point);
			TweenMax.to(this,UtilsAndConfig.updateDelayMiliseconds/1000,{x:point.x,y:point.y});
			
		}
		
		
	}
}
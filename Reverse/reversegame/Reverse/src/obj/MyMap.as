package obj
{
	import citrus.objects.CitrusSprite;
	
	import starling.display.Quad;
	
	public class MyMap extends CitrusSprite
	{
		public function MyMap(name:String, params:Object=null)
		{
			super(name, params);
			view = new Quad(params.width,params.height,0x000000);
		}
	}
}
package obj
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import citrus.objects.platformer.simple.Sensor;
	
	import starling.display.Quad;
	
	public class MySensor extends Sensor
	{
		public var w:int;
		public var h:int;
		public var startX:int;
		public var startY:int; 
		public var stopX:int;
		public var stopY:int;
		public var speed:int;
		public function MySensor(name:String = "", params:Object=null,stopXP:int = 0,stopYP:int = 0,speedP:int = 0,delay:int = 0)
		{
			super(name, params);
			view = new Quad(params.width,params.height,0xffffff);
			
			offsetX = -params.width/2;
			offsetY = -params.height/2;
			x = params.x + Math.abs(offsetX);
			y = params.y + Math.abs(offsetY);
			startX = x;
			startY = y;
			if(stopXP && stopYP && speedP){
				stopX = stopXP + Math.abs(offsetX);
				stopY = stopYP + Math.abs(offsetY);
				speed = speedP;
				
				var dX:int = Math.abs(stopX - startX);
				var dY:int = Math.abs(stopY - startY);
				var s:Number = Math.sqrt(dX*dX + dY*dY);
				TweenMax.to(this,s/speed,{x:stopX,y:stopY,repeatDelay:delay,repeat:-1,yoyo:true,ease:Linear.easeNone});
				
			}
		}
	}
}
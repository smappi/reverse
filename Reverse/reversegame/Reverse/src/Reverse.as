package
{
	import flash.display.Bitmap;
	import flash.display.StageOrientation;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import citrus.core.starling.StarlingCitrusEngine;
	import citrus.core.starling.ViewportMode;
	import citrus.sounds.CitrusSoundGroup;
	
	import cubeplay.basicscreens.CPHomeScreen;
	import cubeplay.levelmanager.LevelManager;
	import cubeplay.smappiOnlineGame.controllers.SmappiController;
	import cubeplay.utils.GameFactory;
	
	import obj.HeroView;
	import obj.MyHero;
	import obj.RemoteHero;
	
	import screens.FriendsMatchScreen;
	import screens.PlayScreen;
	import screens.RandomMatchScreen;
	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import starling.utils.formatString;
	
	import utils.UtilsAndConfig;
	
	[SWF(frameRate="60")]
	public class Reverse extends StarlingCitrusEngine
	{
		public var assets:AssetManager;
		
		// Startup image
		[Embed(source="../startup/splashscreen.jpg")]
		//		[Embed(source="../startup/splashscreen_portrail.jpg")]
		private static var Background:Class;
		private var background:Bitmap = new Background();
		private var imgbg:Image;
		private var progressBar:AppProgressBar;
		
		
		public static var instance:Reverse;
		
		//use this variable if you want to find real scaleFactor to full fill device screen
		public static var realScaleFactor:Number;
		
		private var levelManager2:LevelManager;
		
		public function Reverse()
		{
			instance = this;
			_baseWidth = 1024;
			_baseHeight = 768;
			//			_baseWidth = 768;
			//			_baseHeight = 1024;
			_viewportMode = ViewportMode.FULLSCREEN;
		}
		
		override protected function handleAddedToStage(e:Event):void
		{
			super.handleAddedToStage(e);
			
			//calculate scale viewport
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, stage.stageWidth, stage.stageHeight), 
				new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight), 
				ScaleMode.SHOW_ALL);
			
			var backgroundScale:Number = 0; 
			if(isOrientationLandscape()==true){
				backgroundScale = stage.fullScreenWidth/background.width;
			}else{
				//tính ra xem đang scale toàn bộ content ở tỉ lệ nào?
				backgroundScale = stage.fullScreenHeight/background.height;
			}
			//scale hình ra cho nó full width
			background.scaleX  = background.scaleY = backgroundScale;
			trace(isOrientationLandscape());
			
			if(isOrientationLandscape()==true){
				//set y để center theo chiều cao của đt
				background.y = - ((background.height - stage.fullScreenHeight)/2);
			}else{
				//set x để center theo chiều doc của đt
				background.x = - ((background.width - stage.fullScreenWidth)/2);
			}
			background.smoothing = true;
			addChild(background);
			
			sound.addSound("themesong", {sound:"assets/sounds/themesound.mp3",loops:-1,group:CitrusSoundGroup.BGM,volume:0.8});
			sound.addSound("button", {sound:"assets/sounds/button.mp3",group:CitrusSoundGroup.SFX,volume:1});
			sound.addSound("explosion", {sound:"assets/sounds/explosion.mp3",group:CitrusSoundGroup.SFX,volume:1});
			
			
			
			//setting up starling
			setUpStarling(true);
		}
		
		override public function handleStarlingReady():void
		{
			super.handleStarlingReady();
			
			
			//			sound.playSound("themesong");
			
			removeChild(background);
			
			var bgTexture:Texture = Texture.fromEmbeddedAsset(
				Background, false, false, scaleFactor);
			imgbg = new Image(bgTexture);
			
			if(isOrientationLandscape()==true){
				if(stage.stageWidth != 1024){
					//tính toán khoảng cách còn dư trên màn hình đt
					var whitespaceWidth:Number = (stage.fullScreenWidth - (imgbg.width * starling.contentScaleFactor))/starling.contentScaleFactor;
					//tính chiều rộng thật sự cần kéo ra để full hết đt
					var realWidth:Number = whitespaceWidth + imgbg.width;
					//tính ra tỉ lệ scale để apply cho height
					realScaleFactor = realWidth/imgbg.width;
					//phải lưu heiht cũ lại để mình còn trù cho y sau đó, cho nó center
					var oldHeight:Number = imgbg.height;
					imgbg.scaleX = imgbg.scaleY = realScaleFactor;
					imgbg.y = -((imgbg.height) - oldHeight)/2;
				}
			}else{
				//tương tự cách tính trên nếu đt đang ở chế độ portrail
				if(stage.stageHeight != 1024){
					var whitespaceHeight:Number = (stage.fullScreenHeight - (imgbg.height * starling.contentScaleFactor))/starling.contentScaleFactor;
					var realHeight:Number = whitespaceHeight + imgbg.height;
					realScaleFactor = realHeight/imgbg.height;
					var oldWidth:Number = imgbg.width;
					imgbg.scaleX = imgbg.scaleY = realScaleFactor;
					imgbg.x = -((imgbg.width) - oldWidth)/2;
				}
			}
			
			
			this.starling.stage.addChild(imgbg);
			
			//show progress bar to load assets
			progressBar = new AppProgressBar(383, 40);
			if(isOrientationLandscape()==true){
				progressBar.x = (imgbg.width  - progressBar.width)  / 2 ;
			}else{
				progressBar.x = (768  - progressBar.width)  / 2 ;
			}
			
			progressBar.y = stage.stageHeight * 0.91/starling.contentScaleFactor;
			this.starling.stage.addChild(progressBar);
			
			
			
			//setup and load assets
			var appDir:File = File.applicationDirectory;
			
			//			uncomment these line if you want to copy resources from app to doc folder
			//			var docDir:File = File.documentsDirectory;
			//			var levelfiles:File = new File(docDir.resolvePath("levels").nativePath);
			//			if(levelfiles.exists==false){
			//				var levelafiles:File = new File(appDir.resolvePath("assets/levels").nativePath);
			//				levelafiles.copyTo(levelfiles);
			//				levelfiles.preventBackup = true;
			//			}
			
			assets = new AssetManager(scaleFactor);
			assets.enqueue( appDir.resolvePath(formatString("assets/textures/1x")));
			
			UtilsAndConfig.defaultAssets = assets;
			
			assets.loadQueue(function(ratio:Number):void
			{
				progressBar.ratio = ratio;
				
				//assets fully loaded
				if (ratio == 1){ 
					SmappiController.initialize();
//					SmappiController.logout();
					GameFactory.PRACTICE_SCREEN_CLASS = PlayScreen;
					GameFactory.RANDOM_SCREEN_CLASS = RandomMatchScreen;
					GameFactory.FRIENDS_SCREEN_CLASS = FriendsMatchScreen;
					
					GameFactory.LOCAL_PLAYER_CLASS = MyHero; 
					GameFactory.REMOTE_PLAYER_CLASS = RemoteHero;
					GameFactory.PLAYVIEW_CLASS = HeroView;
					
					levelManager2 = new LevelManager(PlayScreen,CPHomeScreen); 
					background = null;
					imgbg.removeFromParent(true);
					progressBar.removeFromParent(true);
					
					System.pauseForGCIfCollectionImminent(0);
					System.gc();
					
				}
			});
			
			
		}

		
		private function isOrientationLandscape():Boolean {
			if (stage.orientation == StageOrientation.DEFAULT || stage.orientation == StageOrientation.UPSIDE_DOWN) return false;
			return true;
		}
	}
}